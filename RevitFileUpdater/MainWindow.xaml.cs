﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using DJ;
using DJ.Targets;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using NLog;
using NLog.Config;

namespace RevitFileUpdaterUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly MainWindowViewModel _viewModel;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel(DialogCoordinator.Instance);
            _viewModel = DataContext as MainWindowViewModel;
        }

        private void LaunchRepo(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo {FileName = "https://gitlab.com/kraftwerk15/revitfileupdater", UseShellExecute = true});
            //_ = System.Diagnostics.Process.Start("https://gitlab.com/kraftwerk15/revitfileupdater");
            Logger.Log(LogLevel.Info, "Repo URL Launched");
        }

        private void LaunchHelp(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo
                {FileName = "https://github.com/MahApps/MahApps.Metro.IconPacks", UseShellExecute = true});
            //_ = System.Diagnostics.Process.Start("https://github.com/MahApps/MahApps.Metro.IconPacks");
            Logger.Log(LogLevel.Info, "Help URL Launched");
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel != null && _viewModel.RevitAddinsInstalled == false)
            {
                //disabled while testing
                //_viewModel.RevitInstallation(this);
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }

            if (_viewModel.CanStart) return;
            e.Cancel = true;

            // We have to delay the execution through BeginInvoke to prevent potential re-entrancy
            Dispatcher.BeginInvoke(new Action(async () => await ConfirmShutdown()));
        }

        private async Task ConfirmShutdown()
        {
            MetroDialogSettings mySettings = new()
            {
                AffirmativeButtonText = "Quit",
                NegativeButtonText = "Cancel",
                AnimateShow = true,
                AnimateHide = false
            };

            MessageDialogResult result = await this.ShowMessageAsync("Quit application while Process is Running?", 
                "Are you sure you want to quit application? " +
                "If you Quit, it will still take a few minutes for the process to wind down. No new files will be processed.",
                MessageDialogStyle.AffirmativeAndNegative, mySettings);

            if (result == MessageDialogResult.Affirmative)
            {
                _viewModel.CancelRequested = true;
            }
        }
    }
}
