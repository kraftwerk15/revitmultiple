﻿using System.Windows;
using System.Windows.Controls;

namespace RevitFileUpdaterUI.Views
{
    /// <summary>
    /// Interaction logic for SetDatabaseCredentials.xaml
    /// </summary>
    public partial class SetDatabaseCredentials : UserControl
    {
        public SetDatabaseCredentials()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (DataContext != null)
            { ((dynamic)DataContext).Password = ((PasswordBox)sender).SecurePassword; }
        }
    }
}
