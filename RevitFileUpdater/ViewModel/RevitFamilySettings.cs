﻿namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel
    {
        private bool _deleteExisting;
        public bool DeleteExisting { get => _deleteExisting;
            set { _deleteExisting = value; OnPropertyChanged(); }
        }

        private bool _deleteBackups;
        public bool DeleteBackups { get => _deleteBackups;
            set { _deleteBackups = value; OnPropertyChanged(); }
        }
        
        private bool _renameBackups;
        public bool RenameRevitBackupFiles { get => _renameBackups;
            set { _renameBackups = value; OnPropertyChanged(); }
        }

        private bool _ignoreBackups;
        public bool IgnoreRevitBackupFiles { get => _ignoreBackups;
            set { _ignoreBackups = value; OnPropertyChanged(); }
        }
    }
}