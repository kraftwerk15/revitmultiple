﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json.Linq;
using RevitFileCore;
using RevitFileUpdaterCore;
using RevitFileUpdaterUI.Views;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel
    {
        #region Commands

        protected async void Start(object sender)
        {
            Logger.Info("Starting the Start Command");
            bool validationPassed = Is2019Available && Is2019Selected || Is2020Available && Is2020Selected ||
                                    Is2021Available && Is2021Selected ||
                                    Is2022Available && Is2022Selected;

            if (!IsFromAvailable && string.IsNullOrEmpty(UpgradePath))
            {
                await _dialogCoordinator.ShowMessageAsync(this, "Process Cancelled!",
                    "An issue was encountered with the Year Version and the File Upgrade Path.");
                Logger.Warn("Start Command Cancelled because Upgrade Path or IsFromAvailable fields were empty.");
                return;
            }
            //Validation in this method does not work if you user selected a list of files.
            //The application should bypass this 
            if (!validationPassed)
            {
                await _dialogCoordinator.ShowMessageAsync(this, "Process Cancelled!",
                    "At least one of the year versions of Revit must be specified.");
                Logger.Warn("Start Command Cancelled because User did not select an upgrade year.");
                return;
            }

            ValidationText = string.Empty;

            CanStart = false;
            CanCancel = true;
            Is2018Available = false;
            Is2019Available = false;
            Is2020Available = false;
            Is2021Available = false;
            Is2022Available = false;
            IsFromAvailable = false;
            Guid instance = Guid.NewGuid();
            Logger.Info("New GUID Instance created : {0}", instance);
            WindowTitle = string.Concat(_windowTitle, " - Session Number: ", instance);
            
            //What happens when the Settings File was not saved? I should pick that up and save the file here?
            
            string settingsFilePath = RevitFileUpdaterUiSettings.GetDefaultSettingsFilePath();
            _settings.SaveToFile(settingsFilePath);
            // Testing whether this should be a specified Log Folder from the user using the "Settings" object.
            string logFolderPath = string.IsNullOrEmpty(LogFolder) ? string.Empty : _settings.LogFolder.GetValue();
            RevitFileUpdaterProcess = BatchRvt.StartBatchRvt(settingsFilePath, logFolderPath, instance.ToString());
            
            
            
            
            
            //if (UpgradePath == string.Empty)
            //{
            //    new RevitFileController.UpdateFamilyController().UpdateFamiliesInLibrary(string.Empty, instance);
            //}
            //else if (Path.HasExtension(UpgradePath))
            //{
            //    new RevitFileController.UpdateFamilyController().UpdateFamily(settings, UpgradePath, instance);
            //}
            //else
            //{
            //    new RevitFileController.UpdateFamilyController().UpdateFamiliesInFolder(UpgradePath, instance);
            //}
        }

        protected async void Cancel(object sender)
        {
            
        }

        private async void SelectFile(object sender)
        {
            string location = UI_IO_Options.BrowseForFile("Select Revit File...", ".rfa, .rft, .rvt, .rte",
                "Revit Family (*.rfa)|*.rfa|Revit Family Template (*.rft)|.rft|Revit Project (*.rvt)|.rvt|Revit Project Template (*.rte)|.rte",
                false);
            if (location == string.Empty) return;
            SearchEnabled = false;
            UpgradePath = location;
            ProcessingMultipleOption = false;
            IsFromAvailable = true;
            Is2018Available = true;
            Is2019Available = true;
            Is2020Available = true;
            Is2021Available = true;
            Is2022Available = true;
        }

        /// <summary>
        /// Prompts the User to select multiple Revit files to process.
        /// </summary>
        /// <param name="sender">null</param>
        protected virtual async void SelectFiles(object sender)
        {
            string[] locations = UI_IO_Options.BrowseForFiles("Select Revit File...", ".rfa, .rft, .rvt, .rte",
                "Revit Family (*.rfa)|*.rfa|Revit Family Template (*.rft)|.rft|Revit Project (*.rvt)|.rvt|Revit Project Template (*.rte)|.rte",
                false);

            if (locations is null)
            {
                UpgradePath = string.Empty;
                return;
            }

            List<DirectoryInfo> dis = locations.Select(location => new DirectoryInfo(location)).ToList();

            List<string> result = (from m in dis select m.Parent.FullName).Distinct().ToList();
            UpgradePath = result.Count > 1 ? "Multiple Folder Locations Selected" : result[0];
            SearchEnabled = false;
            IsFromAvailable = false;
            ProcessingMultipleOption = true;
            //Get Year from File Path

            List<string> yearVersions = new();
            foreach (string location in locations)
            {
                yearVersions.Add(Regex.Match(location));
            }
            
            if (yearVersions.Exists(string.IsNullOrEmpty))
            {
                MetroDialogSettings metroDialogSettings = new()
                {
                    AffirmativeButtonText = "Yes",
                    NegativeButtonText = "No, Live Dangerously"
                };
                //do something because the year could not be found.
                MessageDialogResult dialogResult = await _dialogCoordinator.ShowMessageAsync(this, "Fetch Revit File Information?",
                    "The Year Versions of the Revit Files could not be parsed automatically. " +
                    "The process runs more efficiently if the Revit Version is know before running. " +
                    "Would you like to extract Revit File Info now?", MessageDialogStyle.AffirmativeAndNegative, metroDialogSettings);
                if (dialogResult == MessageDialogResult.Affirmative)
                {
                    //TODO this will do nothing for now.
                }
            }
            Is2018Available = true;
            Is2019Available = true;
            Is2020Available = true;
            Is2021Available = true;
            Is2022Available = true;
        }
        
        /// <summary>
        /// Prompts the User to Upgrade their defined Library Location
        /// </summary>
        /// <param name="sender"></param>
        private void StartLibrary(object sender)
        {
            
            if (string.IsNullOrEmpty(LibraryLocation))
            {
                _dialogCoordinator.ShowModalMessageExternal(this, "Library Location is not Set",
                    "The Base Folder for the Library Location is not set and must be set before continuing.");
                SetLibraryLocation(sender);
            }

            if (string.IsNullOrEmpty(LibraryLocation))
            {
                return;
            }
            UpgradePath = LibraryLocation;
            IsFromAvailable = true;
            SearchEnabled = false;
            ProcessingMultipleOption = true;
            
            Is2018Available = true;
            Is2019Available = true;
            Is2020Available = true;
            Is2021Available = true;
            Is2022Available = true;
        }

        /// <summary>
        /// Prompts the user for a Folder Location to Revit Files.
        /// </summary>
        /// <param name="sender">null</param>
        protected virtual void SelectFolder(object sender)
        {
            string searchLocation = FolderList.SearchLocation();
            if(string.IsNullOrWhiteSpace(searchLocation)) return;
            UpgradePath = searchLocation;
            SearchEnabled = true;
            ProcessingMultipleOption = true;
            _dialogCoordinator.ShowModalMessageExternal(this, "Options",
                "Set your options and hit the search buttons to fetch the files in this folder before moving forward.");
            
            IsFromAvailable = true;
            Is2018Available = true;
            Is2019Available = true;
            Is2020Available = true;
            Is2021Available = true;
            Is2022Available = true;
        }
        
        /// <summary>
        /// After the User selects a Folder Location, searches through the folder for Revit files based upon options.
        /// </summary>
        /// <param name="sender">null</param>
        private async void StartFileSearch(object sender)
        {
            if (!ProjectAndFamilySelected && !ProjectSelected && !FamilySelected && !FamilyTemplates)
            {
                await _dialogCoordinator.ShowMessageAsync(this, "Error",
                    "At least one of the file types (Project, Family, Templates, etc.) on the previous panel must be specified.");
                return;
            }

            if (string.IsNullOrEmpty(UpgradePath))
            {
                await _dialogCoordinator.ShowMessageAsync(this, "Process Cancelled!",
                    "The folder path must be specified.");
                return;
            }

            if (ProjectAndFamilySelected)
                SelectedRevitFileType = RevitFileScanning.RevitFileType.ProjectAndFamily;
            else if (ProjectSelected)
                SelectedRevitFileType = RevitFileScanning.RevitFileType.Project;
            else if (FamilySelected)
                SelectedRevitFileType = RevitFileScanning.RevitFileType.Family;
            else if (FamilyTemplates)
                SelectedRevitFileType = RevitFileScanning.RevitFileType.FamilyTemplates;

            if (SelectedRevitFileType == RevitFileScanning.RevitFileType.FamilyTemplates)
            {
                _ = await _dialogCoordinator.ShowMessageAsync(this,
                    "Processing Family Templates",
                    "When processing family templates, the process requires creating the existing Family Template as a new Revit Family" +
                    " and then converting that new family back to a Revit Family Template. The Revit Families will then be moved to an" +
                    " Originals folder adjacent to the templates.");
            }

            bool completedSuccessfully = await FolderList.ShowProcessingDialog(_dialogCoordinator, this);

            if (!completedSuccessfully || FilePaths.Count < 1)
            {
                await _dialogCoordinator.ShowMessageAsync(this, "Process Cancelled!", "The process did not finish!");
            }
            else
            {
                MetroDialogSettings mySettings = new()
                {
                    AffirmativeButtonText = "Save",
                    MaximumBodyHeight = 100
                };

                MessageDialogResult result = await _dialogCoordinator.ShowMessageAsync(this, "Save Result to Text File",
                    "We have found a number of files that need to be saved to a Text File to process. Please save them now.",
                    MessageDialogStyle.Affirmative, mySettings);

                if (result == MessageDialogResult.Affirmative)
                {
                    string resultExecute = new FolderList().Execute(UpgradePath, FilePaths);
                    if (resultExecute == string.Empty)
                    {
                        _ = await _dialogCoordinator.ShowMessageAsync(this,
                            "User Cancelled the Save Operation!",
                            "Process Escaped and nothing was saved.");
                    }
                    else
                    {
                        UpgradePath = resultExecute;

                        _ = await _dialogCoordinator.ShowMessageAsync(this,
                            "Process Completed!",
                            "Completed the process. The Upgrade Path on the Main Screen was modified to include the Text File you just saved.");
                    }
                }
            }

            Is2018Available = true;
            Is2019Available = true;
            Is2020Available = true;
            Is2021Available = true;
            Is2022Available = true;
            IsFolderFlyoutOpen = false;
        }
        
        /// <summary>
        /// Prompts the User to run updates from Families saved from a SQL Database.
        /// </summary>
        /// <param name="sender"></param>
        public async void SelectDatabase(object sender)
        {
            if (Password is null || UserName is null || ConnectionString is null)
            {
                MetroDialogSettings settings = new()
                {
                    AffirmativeButtonText = "Yes",
                    NegativeButtonText = "Cancel"
                };
                MessageDialogResult result = await _dialogCoordinator.ShowMessageAsync(this, "Database Credentials are Invalid", 
                    "The Database Credentials are invalid. Would you like to enter those now?",
                    MessageDialogStyle.AffirmativeAndNegative, settings);
                if (result == MessageDialogResult.Negative)
                {
                    return;
                }
                //TODO test this
                SetDatabaseCredentials(sender);
            }
            UpgradePath = ConnectionString;
            SearchEnabled = false;
            ProcessingMultipleOption = true;
            IsFromAvailable = true;
            Is2018Available = true;
            Is2019Available = true;
            Is2020Available = true;
            Is2021Available = true;
            Is2022Available = true;
        }
        /// <summary>
        /// Displays a Modal Dialog for SQL Database Credentials.
        /// </summary>
        /// <param name="sender"></param>
        private async void SetDatabaseCredentials(object sender)
        {
            CustomDialog customDialog = new()
            {
                Title = "Set Database Credentials"
            };

            SetDatabaseCredentialsViewModel dataContext = new(instance =>
            {
                _dialogCoordinator.HideMetroDialogAsync(this, customDialog);
            });
            customDialog.Content = new SetDatabaseCredentials() { DataContext = dataContext };
            await _dialogCoordinator.ShowMetroDialogAsync(this, customDialog);
            await customDialog.WaitUntilUnloadedAsync();
            if (dataContext.ConnectionString is null || dataContext.UserName is null || dataContext.Password.Length < 1)
            {
                return;
            }

            if (dataContext.ConnectionString.Equals(string.Empty)) return;
            if (dataContext.UserName.Equals(string.Empty)) return;
            if (dataContext.Password.Length < 1) return;

            ConnectionString = dataContext.ConnectionString;
            UpgradePath = dataContext.ConnectionString;
            UserName = dataContext.UserName;
            Password = dataContext.Password;
        }

        
        /// <summary>
        /// Clears all settings from the UI.
        /// </summary>
        /// <param name="sender"></param>
        private void ClearSettings(object sender)
        {
            Logger.Info("Clearing Settings.");
            Is2019Selected = false;
            Is2018Selected = false;
            Is2020Selected = false;
            Is2021Selected = false;
            Is2022Selected = false;
            CanStart = true;
            CanCancel = false;
            FilePath = string.Empty;
            ProjectSelected = false;
            ProjectAndFamilySelected = false;
            FamilySelected = false;
            FamilyTemplates = false;
            IncludeSubfolders = false;
            ExpandNetworkPaths = false;
            ExtractRevitVersionInfo = false;
            IgnoreRevitBackupFiles = false;
            FilePaths = new List<string[]>();
            StartDateTime = new DateTime();
            CompletedDateTime = new DateTime();
            ElapsedStopwatch = new Stopwatch();
            AverageTime = TimeSpan.Zero;
            LongTime = TimeSpan.Zero;
            CurrentProgress = 0;
            FamiliesCompleted = 0;
            TotalFamilies = 0;
            FamiliesRemaining = 0;
            StartTime = string.Empty;
            CompletedTime = string.Empty;
            ElapsedTime = string.Empty;
            Element = string.Empty;
            Status = string.Empty;
            CompletionLabel = string.Empty;
            FolderLevel = 0;
            UpgradePath = string.Empty;
            DeleteBackups = false;
            DeleteExisting = false;
            ValidationText = string.Empty;
            Logger.Info("Settings Cleared Successfully.");
        }

        /// <summary>
        /// Loads Settings from a Folder Location.
        /// </summary>
        /// <param name="sender"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private async void LoadSettings(object sender)
        {
            Logger.Info("Starting LoadSettings Command.");
            string filePath = UI_IO_Options.BrowseForFile(
                "Import RevitMultiple Settings file",
                RevitFileUpdaterUiSettings.SettingsFileExtension,
                RevitFileUpdaterUiSettings.SettingsFileFilter,
                true
            );

            if (string.IsNullOrEmpty(filePath))
            {
                _ = await _dialogCoordinator.ShowMessageAsync(this, "User Cancelled",
                    "The User Cancelled the process. Returning to the application.");
                Logger.Info("User Cancelled LoadSettings Command.");
                return;
            }

            RevitFileUpdaterUiSettings newRevitFileUpdaterUiSettings = new();

            bool isLoaded = newRevitFileUpdaterUiSettings.LoadFromFile(filePath);

            if (!isLoaded)
            {
                Logger.Info("Load Settings Failed.");
                ValidationText = "Settings Did Not Load Successfully";
                _ = await _dialogCoordinator.ShowMessageAsync(this, "Settings Did Not Load Successfully",
                    "Settings file did not correctly.");
                return;
            }
            
            _settings = newRevitFileUpdaterUiSettings;
            Logger.Info("Load Settings Completed.");
            ValidationText = "Settings Loaded Successfully";

            // Revit File List settings
            UpgradePath = _settings.RevitFileListFilePath.GetValue();
            
            
            // Central File Processing settings
            BatchRvt.CentralFileOpenOption centralFileOpenOption = _settings.CentralFileOpenOption.GetValue();
            switch (centralFileOpenOption)
            {
                case BatchRvt.CentralFileOpenOption.NotDefined:
                    CreateNewLocal = false;
                    DetachFromCentral = false;
                    DeleteLocalAfter = false;
                    DiscardWorksets = false;
                    break;
                case BatchRvt.CentralFileOpenOption.CreateNewLocal:
                    CreateNewLocal = true;
                    DetachFromCentral = false;
                    break;
                case BatchRvt.CentralFileOpenOption.Detach:
                    CreateNewLocal = false;
                    DetachFromCentral = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            if(CreateNewLocal)
                DeleteLocalAfter = _settings.DeleteLocalAfter.GetValue();
            if(DetachFromCentral)
                DiscardWorksets = _settings.DiscardWorksetsOnDetach.GetValue();

            BatchRvt.WorksetConfigurationOption worksetConfigurationOption =
                _settings.WorksetConfigurationOption.GetValue();

            switch (worksetConfigurationOption)
            {
                case BatchRvt.WorksetConfigurationOption.NotDefined:
                    CloseAllWorksets = false;
                    OpenAllWorksets = false;
                    OpenLastViewed = false;
                    break;
                case BatchRvt.WorksetConfigurationOption.CloseAllWorksets:
                    CloseAllWorksets = true;
                    OpenAllWorksets = false;
                    OpenLastViewed = false;
                    break;
                case BatchRvt.WorksetConfigurationOption.OpenAllWorksets:
                    CloseAllWorksets = false;
                    OpenAllWorksets = true;
                    OpenLastViewed = false;
                    break;
                case BatchRvt.WorksetConfigurationOption.OpenLastViewed:
                    CloseAllWorksets = false;
                    OpenAllWorksets = false;
                    OpenLastViewed = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            BatchRvt.AccServer accServer = _settings.ACCServer.GetValue();
            switch (accServer)
            {
                case BatchRvt.AccServer.NotDefined:
                    BIM360US = false;
                    BIM360EU = false;
                    break;
                case BatchRvt.AccServer.Us:
                    BIM360US = true;
                    BIM360EU = false;
                    break;
                case BatchRvt.AccServer.Eu:
                    BIM360US = false;
                    BIM360EU = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            DeleteExisting = _settings.DeleteExistingFamilies.GetValue();
            DeleteBackups = _settings.DeleteBackups.GetValue();
            RenameRevitBackupFiles = _settings.RenameBackups.GetValue();
            IgnoreRevitBackupFiles = _settings.IgnoreBackups.GetValue();
            // General Task Script settings
            
            
            
            // Revit Processing settings
            BatchRvt.RevitProcessingOption option9 = _settings.RevitProcessingOption.GetValue();

            

            

            

            // Single Revit Task Processing settings
            _settings.SingleRevitTaskRevitVersion.GetValue();

            // Batch Revit File Processing settings
            BatchRvt.RevitFileProcessingOption fileProcessingOption = _settings.RevitFileProcessingOption.GetValue();
            switch (fileProcessingOption)
            {
                case BatchRvt.RevitFileProcessingOption.NotDefined:
                    UseRevitVersion = false;
                    UseSpecificRevitVersion = false;
                    UseMinimumVersion = false;
                    break;
                case BatchRvt.RevitFileProcessingOption.UseFileRevitVersionIfAvailable:
                    UseRevitVersion = true;
                    UseSpecificRevitVersion = false;
                    break;
                case BatchRvt.RevitFileProcessingOption.UseSpecificRevitVersion:
                    UseRevitVersion = false;
                    UseSpecificRevitVersion = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            UseMinimumVersion = _settings.IfNotAvailableUseMinimumAvailableRevitVersion.GetValue();
            
            
            
            
            
            _settings.BatchRevitTaskRevitVersion.GetValue();
            
            AuditOnOpening = _settings.AuditOnOpening.GetValue();
            
            //Scripting User Control
            TaskScriptMainFileName = _settings.TaskScriptMainFilePath.GetValue();
            TaskScriptSingleFileName = _settings.TaskScriptSingleFilePath.GetValue();
            TaskScriptPostFileName = _settings.TaskScriptPreFilePath.GetValue();
            TaskScriptPreFileName = _settings.TaskScriptPostFilePath.GetValue();
            
            
            //Revit Multiple User Control
            BatchRvt.RevitSessionOption revitSessionOption = _settings.RevitSessionOption.GetValue();
            switch (revitSessionOption)
            {
                case BatchRvt.RevitSessionOption.NotDefined:
                    RevitSeparateSession = false;
                    RevitSameSession = false;
                    break;
                case BatchRvt.RevitSessionOption.UseSeparateSessionPerFile:
                    RevitSeparateSession = true;
                    RevitSameSession = false;
                    break;
                case BatchRvt.RevitSessionOption.UseSameSessionForFilesOfSameVersion:
                    RevitSeparateSession = false;
                    RevitSameSession = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            OpenInUi = _settings.OpenInUI.GetValue();
            WindowAlwaysOnTop = _settings.WindowOnTop.GetValue();
            ProcessingTime = _settings.ProcessingTimeOutInMinutes.GetValue();
            ProcessingTimeEnabled = ProcessingTime > 0;

            DataExportEnabled = _settings.EnableDataExport.GetValue();
            DataExportPath = _settings.DataExportFolderPath.GetValue();
        }
        /// <summary>
        /// Saves UI Settings to a File Location.
        /// </summary>
        /// <param name="sender"></param>
        private async void SaveSettings(object sender)
        {
            Logger.Info("Starting SaveSettings Command.");
            
            Action<string> action = Action;

            string filePath = UI_IO_Options.BrowseForSave("Export RevitFileUpdater Settings file", action,RevitFileUpdaterUiSettings.SettingsFileExtension, 
                RevitFileUpdaterUiSettings.SettingsFileFilter, RevitFileUpdaterUiSettings.GetDefaultSettingsFilePath(), "MyRevitMultipleGUISettings");
            

            if (string.IsNullOrEmpty(filePath))
            {
                _ = await _dialogCoordinator.ShowMessageAsync(this, "User Cancelled",
                    "The User Cancelled the process. Returning to the application.");
                Logger.Info("User Cancelled SaveSettings Command.");
                return;
            }
        }

        private async void Action(string filePath)
        {
            // ReSharper disable once IdentifierTypo
            JObject jobject = new();

            try
            {
                _settings.Store(jobject);
                string settingsText = JsonSerialization.SerializeToJson(jobject, true);
                FileInfo fileInfo = new(filePath);
                fileInfo.Directory?.Create();
                await File.WriteAllTextAsync(fileInfo.FullName, settingsText);
                Logger.Info("Save Settings Completed Successfully.");
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }


        private async void VerifyExcelInstallation(object sender)
        {
            if (ExcelUtil.IsExcelInstalled())
            {
                _ = await _dialogCoordinator.ShowMessageAsync(this, "Successfully Found Excel!",
                    "No need to worry further. Excel was found!");
            }
            else
            {
                _ = await _dialogCoordinator.ShowMessageAsync(this, "Excel Was Not Found!",
                    "We could not find an Excel Installation.");
            }
        }

        public async void SpecifyLogFile(object sender)
        {
            string logFolder = string.IsNullOrEmpty(this._settings.LogFolder.GetValue()) ? 
                "Log Folder is not yet set. The application will automatically store them." : this._settings.LogFolder.GetValue();
            MetroDialogSettings metroDialogSettings = new()
            {
                AffirmativeButtonText = "Change Folder Location?",
                NegativeButtonText = "Cancel"
            };
            MessageDialogResult result = await _dialogCoordinator.ShowMessageAsync(this, "Specified Log Folder",
                "Location : " + logFolder,
                MessageDialogStyle.AffirmativeAndNegative, metroDialogSettings);
            if (result != MessageDialogResult.Affirmative) return;
            string logFolderNew = UI_IO_Options.BrowseForFolder();
            if (string.IsNullOrEmpty(logFolderNew)) return;
            MetroDialogSettings settings1 = new()
            {
                AffirmativeButtonText = "Confirm Log Folder",
                NegativeButtonText = "Cancel"
            };
            MessageDialogResult result1 = await _dialogCoordinator.ShowMessageAsync(this, "Specified Log Folder",
                "Location : " + logFolderNew,
                MessageDialogStyle.AffirmativeAndNegative, settings1);

            if (result1 != MessageDialogResult.Affirmative) return;
            this._settings.LogFolder.SetValue(logFolderNew);
            LogFolder = logFolderNew;
        }
        /// <summary>
        /// Verifies Revit Applications are installed on the machine.
        /// </summary>
        /// <param name="sender"></param>
        public async void VerifyRevitInstallation(object sender)
        {
            IEnumerable<int> revitVersions = RevitVersion.GetInstalledRevitVersions();
            IEnumerable<int> enumerable = revitVersions.ToList();
            if (enumerable.Any())
            {
                string message = string.Join(Environment.NewLine,
                    enumerable.Select(k => string.Concat("Autodesk Revit ", k)).ToList());
                    
                MetroDialogSettings metroDialogSettings = new()
                {
                    AffirmativeButtonText = "Close"
                };
                _ = await _dialogCoordinator.ShowMessageAsync(this, "Revit Installations Detected : ",
                    message, MessageDialogStyle.Affirmative, metroDialogSettings);
            }
            else
            {
                MetroDialogSettings settings = new()
                {
                    AffirmativeButtonText = "Close"
                };
                const string message = "No Revit Installations Detected on this machine. At least one valid Revit instance is required.";
                _ = await _dialogCoordinator.ShowMessageAsync(this, "Revit Installations Were Not Detected",
                    message, MessageDialogStyle.Affirmative, settings);
                return;
            }
            VerifyRevitAddinInstallation();
        }
        /// <summary>
        /// Verifies the RevitMultiple Addin is installed on the machine.
        /// </summary>
        public async void VerifyRevitAddinInstallation()
        {
            IEnumerable<int> addinsVersions = RevitVersion.GetInstalledRevitAddinVersions();
            IEnumerable<int> enumerable = addinsVersions as int[] ?? addinsVersions.ToArray();
            if (enumerable.Any())
            {
                List<string> lis = enumerable.Select(k => string.Concat("RevitMultiple ", k)).ToList();
                string message = string.Join(Environment.NewLine, lis);
                MetroDialogSettings metroDialogSettings = new()
                {
                    AffirmativeButtonText = "Close"
                };
                _ = await _dialogCoordinator.ShowMessageAsync(this, "RevitMultiple Addins Detected : ",
                    message, MessageDialogStyle.Affirmative, metroDialogSettings);
            }
            else
            {
                RevitInstallation(this);
            }
        }
        /// <summary>
        /// Controls movement through the UI.
        /// </summary>
        /// <param name="sender"></param>
        public void TabIncrement(object sender)
        {
            if(sender is not null)
            {
                switch (sender.ToString())
                {
                    // ReSharper disable once PossibleNullReferenceException
                    case "Project":
                        IsFamily = false;
                        IsProject = true;
                        break;
                    case "Family":
                        IsFamily = true;
                        IsProject = false;
                        break;
                }
            }
            SelectedTab++;

        }
        /// <summary>
        /// Controls Movement through the UI.
        /// </summary>
        /// <param name="sender"></param>
        public void TabDecrement(object sender)
        {
            if (SelectedTab > 0)
                SelectedTab--;
        }
        

        public async void DataExportBrowse(object sender)
        {
            string filePath = UI_IO_Options.BrowseForFile(
                "Data Export File Path",
                RevitFileUpdaterUiSettings.SettingsFileExtension,
                RevitFileUpdaterUiSettings.SettingsFileFilter,
                true
            );

            if (!string.IsNullOrEmpty(filePath)) return;
            _ = await _dialogCoordinator.ShowMessageAsync(this, "User Cancelled",
                "The User Cancelled the process. Returning to the application.");
            DataExportEnabled = false;
            Logger.Info("User Cancelled Data Export Command.");
        }
        /// <summary>
        /// Prompts the User to Install the RevitMultiple Addins if not installed.
        /// </summary>
        /// <param name="sender"></param>
        public async void RevitInstallation(object sender)
        {
            MetroDialogSettings settings = new()
            {
                AffirmativeButtonText = "Install Now",
                NegativeButtonText = "Do Not Install and Close"
            };
            MessageDialogResult result = await _dialogCoordinator.ShowMessageAsync(this, "Error: Could not detect addins installed!",
                "Could not detect the RevitMultiple addin for any version of Revit installed on this machine. " +
                "You must install at least one version of the addin for at least one version of Revit. " + Environment.NewLine + 
                "If you do not install, you will not be able to use the application and this application will close.",
                MessageDialogStyle.AffirmativeAndNegative, settings);

            if (result == MessageDialogResult.Affirmative)
            {
                //install the .addin files
                //TODO write code to install addins here
                //set flag to true
                RevitAddinsInstalled = true;
            }
            else
            {
                CloseApplication(null);
            }
        }
        
        /// <summary>
        /// Displays the Applications License
        /// </summary>
        /// <param name="sender"></param>

        public async void License(object sender)
        {
            MetroDialogSettings settings = new()
            {
                AffirmativeButtonText = "Close"
            };
            await _dialogCoordinator.ShowMessageAsync(this, "Revit Multiple (c) 2021 Micah Gray",
                "This application allows you to set an action upon Revit, Dynamo, or Python (future) to complete that action upon multiple families or projects. " + Environment.NewLine + 
                "This program is free software: you can redistribute it and/or modify " + Environment.NewLine + 
                "it under the terms of the GNU General Public License as published by" + Environment.NewLine + 
                "the Free Software Foundation, either version 3 of the License, or"+ Environment.NewLine + 
                "any later version." + Environment.NewLine + Environment.NewLine + 
                "This program is distributed in the hope that it will be useful," + Environment.NewLine + 
                "but WITHOUT ANY WARRANTY; without even the implied warranty of" + Environment.NewLine + 
                "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" + Environment.NewLine + 
                "GNU General Public License for more details." + Environment.NewLine + Environment.NewLine +
                "You should have received a copy of the GNU General Public License"+ Environment.NewLine + 
                "along with this program.  If not, see <https://www.gnu.org/licenses/>.",
                MessageDialogStyle.AffirmativeAndNegative, settings);
            //should this application also house the license?
        }
        /// <summary>
        /// Closes this Application.
        /// </summary>
        /// <param name="sender"></param>
        public static void CloseApplication(object sender)
        {
            System.Windows.Application.Current.Shutdown();
        }
        #endregion
    }
}