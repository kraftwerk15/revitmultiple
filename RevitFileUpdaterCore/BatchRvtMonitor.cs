﻿#region License
//
// Revit Batch Processor
//
// Copyright (c) 2020  Dan Rumery, BVN
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion

#region using
using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Autodesk.Revit.UI.Events;
using NLog.Fluent;
using RevitFileDatabase;
using RevitFileCore;
#endregion


namespace RevitFileUpdaterCore
{
    public class BatchRevitMonitor
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();

        private static bool HasSupportedRevitFilePath(RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo) {
            var fullFilePath = supportedRevitFileInfo.RevitFileInfo.GetFullPath();
            return true;
        }

        private static bool HasSupportedRevitVersion(RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo) {
            return RevitVersion.GetInstalledRevitVersions().Contains(supportedRevitFileInfo.RevitVersionNumber);
        }

        private static bool HasSupportedRevitVersions(int revitVersion)
        {
            return RevitVersion.GetInstalledRevitVersionsAboveVersion(revitVersion).Any();
        }

        private static object GetRevitFileSize(RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo) {
            return supportedRevitFileInfo.RevitFileInfo.GetFileSize();
        }

        private static bool HasAllowedRevitVersion(BatchRevitSettingsConfiguration batchRvtConfig, RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo) {
            bool hasAllowedRevitVersion = false;
            if (batchRvtConfig.RevitFileProcessingOption == BatchRvt.RevitFileProcessingOption.UseSpecificRevitVersion) {
                int revitVersion = supportedRevitFileInfo.RevitVersionNumber;
                if (revitVersion > -1 || revitVersion <= batchRvtConfig.BatchRevitTaskRevitVersion) {
                    hasAllowedRevitVersion = true;
                }
            } else if (batchRvtConfig.RequestLibrary)
            {
                return HasSupportedRevitVersions(supportedRevitFileInfo.RevitVersionNumber);
            }
            else if (HasSupportedRevitVersion(supportedRevitFileInfo)) {
                hasAllowedRevitVersion = true;
            } else if (batchRvtConfig.IfNotAvailableUseMinimumAvailableRevitVersion) {
                hasAllowedRevitVersion = true;
            }
            return hasAllowedRevitVersion;
        }
        
        public static bool RevitFileExists(RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo) {
            return supportedRevitFileInfo.RevitFileInfo.Exists();
        }
        
        public static List<RevitFileList.SupportedRevitFileInfo> GetSupportedRevitFiles(BatchRevitSettingsConfiguration batchRvtConfig) {
            List<RevitFileList.SupportedRevitFileInfo> finalSupportedList = new();
            List<RevitFileList.RevitFilePathData> revitFileListData = new();
            if (batchRvtConfig.RequestDatabase)
            {
                List<Family> families = new SqlRevit().Get();
                foreach (Family fam in families)
                {
                    batchRvtConfig.RevitFileLists.Add(fam.FamilyPath);
                }
            }

            if (batchRvtConfig.RequestLibrary)
            {
                List<string> families = RevitFileScanning.FindRevitFiles(batchRvtConfig.LibraryPath,
                    SearchOption.AllDirectories,
                    RevitFileScanning.RevitFileType.Family, true).ToList();
                
                batchRvtConfig.RevitFileLists.AddRange(families);
                
                Logger.Info("Files added to BatchRevitSettingsConfiguration.RevitFileLists");
            }
            revitFileListData = batchRvtConfig.ReadRevitFileListData();
            if (revitFileListData != null) {
                List<RevitFileList.SupportedRevitFileInfo> supportedRevitFileList = new();
                Logger.Info("Checking {0} files to see if they are supported files.", revitFileListData.Count);
                foreach (RevitFileList.RevitFilePathData revitFileData in revitFileListData)
                {
                    supportedRevitFileList.Add(new RevitFileList.SupportedRevitFileInfo(revitFileData));
                }
                List<RevitFileList.SupportedRevitFileInfo> nonExistentRevitFileList = new();
                foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList)
                {
                    //If the file is not a Cloud model and it is not found at the File Path
                    if (!supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid && !RevitFileExists(supportedRevitFileInfo))
                    {
                        nonExistentRevitFileList.Add(supportedRevitFileInfo);
                    }
                }

                Logger.Info("Out of {0} files requested to be processed, {1} could not be found.",
                    revitFileListData.Count, nonExistentRevitFileList.Count);
                //Old Code
                //var nonExistentRevitFileList = (from supportedRevitFileInfo in supportedRevitFileList
                //    where !supportedRevitFileInfo.IsCloudModel() && !RevitFileExists(supportedRevitFileInfo)
                //    select supportedRevitFileInfo).ToList();
                List<RevitFileList.SupportedRevitFileInfo> unsupportedRevitFileList = new ();
                foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList)
                {
                    if (!HasAllowedRevitVersion(batchRvtConfig, supportedRevitFileInfo))
                    {
                        unsupportedRevitFileList.Add(supportedRevitFileInfo);
                    }
                }

                Logger.Info("Out of {0} files, {1} files contain an unsupported Revit Version by the process", revitFileListData.Count,
                    unsupportedRevitFileList.Count);
                //Old Code
                //var unsupportedRevitFileList = (from supportedRevitFileInfo in supportedRevitFileList
                //    where !HasAllowedRevitVersion(batchRvtConfig, supportedRevitFileInfo)
                //    select supportedRevitFileInfo).ToList();
                List<RevitFileList.SupportedRevitFileInfo> unsupportedRevitFilePathRevitFileList = new();
                foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList)
                {
                    if (!supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid && !HasSupportedRevitFilePath(supportedRevitFileInfo))
                    {
                        unsupportedRevitFilePathRevitFileList.Add(supportedRevitFileInfo);
                    }
                }
                Logger.Info("Out of {0} files, {1} files contain an unsupported File Path by the process", revitFileListData.Count,
                    unsupportedRevitFilePathRevitFileList.Count);
                //Old Code
                //var unsupportedRevitFilePathRevitFileList = (from supportedRevitFileInfo in supportedRevitFileList
                //    where !supportedRevitFileInfo.IsCloudModel() && !HasSupportedRevitFilePath(supportedRevitFileInfo)
                //    select supportedRevitFileInfo).ToList();
                List<RevitFileList.SupportedRevitFileInfo> temp = new ();
                foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList)
                {
                    if (supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid ||
                        RevitFileExists(supportedRevitFileInfo))
                    {
                        temp.Add(supportedRevitFileInfo); 
                    }
                }

                supportedRevitFileList = temp;
                //Old Code
                //supportedRevitFileList = (from supportedRevitFileInfo in supportedRevitFileList
                //    where supportedRevitFileInfo.IsCloudModel() || RevitFileExists(supportedRevitFileInfo)
                //    select supportedRevitFileInfo).ToList();
                
                foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList)
                {
                    if ((supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid ||
                         RevitFileExists(supportedRevitFileInfo))
                        && HasAllowedRevitVersion(batchRvtConfig, supportedRevitFileInfo) &&
                        (supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid
                         || HasSupportedRevitFilePath(supportedRevitFileInfo)))
                    {
                        finalSupportedList.Add(supportedRevitFileInfo);
                    }
                }

                if (batchRvtConfig.RequestLibrary)
                {
                    IEnumerable<int> versions = RevitVersion.GetInstalledRevitVersionsAboveVersion(
                        batchRvtConfig.BatchRevitTaskRevitVersion);
                    List<RevitFileList.SupportedRevitFileInfo> name = new();
                    foreach (RevitFileList.SupportedRevitFileInfo k in finalSupportedList)
                    {
                        foreach (int version in versions)
                        {
                            if (k.RevitVersionNumber < version)
                            {
                                RevitFileList.SupportedRevitFileInfo r = k.Clone();
                                r.RevitVersionNumber = version;
                                name.Add(r);
                            }
                        }
                    }
                    //clear the original list
                    finalSupportedList.Clear();
                    //set to the new list
                    finalSupportedList = name;
                }
                
                
                
                
                // ReSharper disable once CA1806
                // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                finalSupportedList.OrderBy(supportedRevitFileInfo => !supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid ? 
                    GetRevitFileSize(supportedRevitFileInfo) : 0);
                //Old Code
                //supportedRevitFileList = (from supportedRevitFileInfo in supportedRevitFileList
                //    where (
                //    select supportedRevitFileInfo).ToList()
                //    .OrderBy(supportedRevitFileInfo => !supportedRevitFileInfo.IsCloudModel() ? GetRevitFileSize(supportedRevitFileInfo) : System.Int64(0)).ToList();
                int nonExistentCount = nonExistentRevitFileList.Count;
                int unsupportedCount = unsupportedRevitFileList.Count;
                int unsupportedRevitFilePathCount = unsupportedRevitFilePathRevitFileList.Count;
                if (nonExistentCount > 0) {
                    Logger.Warn("WARNING: From the list provided," + nonExistentCount +" Revit Files do not exist. Names are shown below :");
                    foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in nonExistentRevitFileList) {
                        BatchRevitMonitorUtil.ShowSupportedRevitFileInfo(supportedRevitFileInfo);
                    }
                }
                if (unsupportedCount > 0) {
                    Logger.Warn("WARNING: The following Revit Files are of an unsupported version (" + unsupportedCount.ToString() + "):");
                    foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in unsupportedRevitFileList) {
                        BatchRevitMonitorUtil.ShowSupportedRevitFileInfo(supportedRevitFileInfo);
                    }
                }
                if (unsupportedRevitFilePathCount > 0) {
                    Logger.Warn("WARNING: The following Revit Files have an unsupported file path (" + unsupportedRevitFilePathCount.ToString() + "):");
                    foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in unsupportedRevitFilePathRevitFileList) {
                        BatchRevitMonitorUtil.ShowSupportedRevitFileInfo(supportedRevitFileInfo);
                    }
                }
            }
            return finalSupportedList;
        }

        private static ScriptUtil InitializeScriptUtil(BatchRevitSettingsConfiguration batchRvtConfig)
        {
            return new(batchRvtConfig);
        }

        private static bool ExecutePreProcessingScript(BatchRevitSettingsConfiguration batchRvtConfig) {
            var aborted = false;
            try {
                ScriptUtil scriptUtil = InitializeScriptUtil(batchRvtConfig);
                Logger.Info("Pre-processing script operation started.");
                ScriptUtil.ExecuteScript(batchRvtConfig.PreProcessingScriptFilePath);
                Logger.Info("Pre-processing script operation completed.");
            } catch (Exception e) {
                Logger.Error("ERROR: An error occurred while executing the pre-processing script! Operation aborted.");
                ExceptionUtil.LogOutputErrorDetails(e);
                aborted = true;
            }
            return aborted;
        }

        private static bool ExecutePostProcessingScript(BatchRevitSettingsConfiguration batchRvtConfig) {
            bool aborted = false;
            try {
                InitializeScriptUtil(batchRvtConfig);
                Logger.Info("Post-processing script operation started.");
                ScriptUtil.ExecuteScript(batchRvtConfig.PostProcessingScriptFilePath);
                Logger.Info("Post-processing script operation completed.");
            } catch (Exception e) {
                Logger.Error("ERROR: An error occurred while executing the post-processing script! Operation aborted.");
                ExceptionUtil.LogOutputErrorDetails(e);
                aborted = true;
            }
            return aborted;
        }

        private static bool RunSingleRevitTask(BatchRevitSettingsConfiguration batchRvtConfig) {
            bool aborted = false;
            int revitVersion = batchRvtConfig.SingleRevitTaskRevitVersion;
            Logger.Info("Revit Version:" + RevitVersion.GetRevitVersionText(revitVersion));
            if (!RevitVersion.GetInstalledRevitVersions().Contains(revitVersion)) 
            {
                Logger.Error("ERROR: The specified Revit version is not installed or the addin is not installed for it.");
                return true;
            }
            if (batchRvtConfig.ExecutePreProcessingScript) {
                aborted = ExecutePreProcessingScript(batchRvtConfig);
            }
            if (!aborted) {
                Logger.Info("Starting single task operation...");
                Logger.Info("Starting Revit " + RevitVersion.GetRevitVersionText(revitVersion) + " session...");
                ScriptDataUtil.ScriptData scriptData = new ();
                scriptData.SessionId.SetValue(batchRvtConfig.SessionId.ToString());
                scriptData.TaskScriptFilePath.SetValue(batchRvtConfig.ScriptFilePath);
                scriptData.TaskData.SetValue(batchRvtConfig.TaskData);
                scriptData.EnableDataExport.SetValue(batchRvtConfig.EnableDataExport);
                scriptData.SessionDataFolderPath.SetValue(batchRvtConfig.SessionDataFolderPath);
                scriptData.ShowMessageBoxOnTaskScriptError.SetValue(batchRvtConfig.ShowMessageBoxOnTaskError);
                scriptData.RevitProcessingOption.SetValue(batchRvtConfig.RevitProcessingOption);
                scriptData.ProgressNumber.SetValue(1);
                scriptData.ProgressMax.SetValue(1);
                string batchRvtScriptsFolderPath = BatchRvt.GetBatchRvtScriptsFolderPath();
                BatchRevitMonitorUtil.RunScriptedRevitSession(revitVersion, batchRvtScriptsFolderPath, batchRvtConfig.ScriptFilePath, new List<ScriptDataUtil.ScriptData> {
                    scriptData
                }, 1, batchRvtConfig.ProcessingTimeOutInMinutes, batchRvtConfig.ShowRevitProcessErrorMessages, batchRvtConfig.TestModeFolderPath);
            }
            if (!aborted) {
                if (batchRvtConfig.ExecutePostProcessingScript) {
                    aborted = ExecutePostProcessingScript(batchRvtConfig);
                }
            }
            return aborted;
        }

        private static int GetRevitVersionForRevitFileSession(BatchRevitSettingsConfiguration batchRvtConfig, RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo) {
            int revitVersion = RevitVersion.GetMinimumInstalledRevitVersion();
            if (batchRvtConfig.RevitFileProcessingOption == BatchRvt.RevitFileProcessingOption.UseSpecificRevitVersion) {
                revitVersion = batchRvtConfig.BatchRevitTaskRevitVersion;
            } 
            else if (HasSupportedRevitVersion(supportedRevitFileInfo))
            {
                revitVersion = supportedRevitFileInfo.RevitVersionNumber;
            }
            return revitVersion;
        }

        // ReSharper disable once ReturnTypeCanBeEnumerable.Local
        private static List<(int, List<RevitFileList.SupportedRevitFileInfo>)> GroupByRevitVersion(
            BatchRevitSettingsConfiguration batchRvtConfig,
            // ReSharper disable once ParameterTypeCanBeEnumerable.Local
            List<RevitFileList.SupportedRevitFileInfo> supportedRevitFileList)
        {
            return supportedRevitFileList.GroupBy(supportedRevitFileInfo => 
                GetRevitVersionForRevitFileSession(batchRvtConfig, supportedRevitFileInfo)).OrderBy(g => g.Key)
                .Select(g => (g.Key, g.ToList())).ToList();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchRvtConfig">A valid BatchRevitSettingsConfiguration.</param>
        /// <param name="supportedRevitFileList"></param>
        /// <returns>A if the process was aborted.</returns>
        private static bool ProcessRevitFiles(BatchRevitSettingsConfiguration batchRvtConfig, 
            List<RevitFileList.SupportedRevitFileInfo> supportedRevitFileList) 
        {
            const bool aborted = false;
            int totalFilesCount = supportedRevitFileList.Count;
            int progressNumber = 1;
            foreach ((int revitVersion, List<RevitFileList.SupportedRevitFileInfo> supportedRevitFiles) in GroupByRevitVersion(batchRvtConfig, supportedRevitFileList)) 
            {
                List<RevitFileList.SupportedRevitFileInfo> sessionRevitFiles = new();
                List<RevitFileList.SupportedRevitFileInfo> queuedRevitFiles = supportedRevitFiles.ToList();
                while (queuedRevitFiles.Any()) {
                    List<ScriptDataUtil.ScriptData> scriptDatas = new ();
                    List<string> snapshotDataExportFolderPaths = new ();
                    //TODO need to figure out what this is trying to do
                    if (batchRvtConfig.RevitSessionOption == BatchRvt.RevitSessionOption.UseSameSessionForFilesOfSameVersion) {
                        sessionRevitFiles = queuedRevitFiles;
                        queuedRevitFiles.Clear();
                    } 
                    else {
                        sessionRevitFiles.Add(queuedRevitFiles[0]);
                        queuedRevitFiles.Insert(0, queuedRevitFiles[1]);
                    }
                    int sessionFilesCount = sessionRevitFiles.Count;
                    if (sessionRevitFiles.Count == 1) {
                        Logger.Info("Processing Revit file ( {0} of {1} ) in Revit {2} session.",
                            progressNumber, totalFilesCount, RevitVersion.GetRevitVersionText(revitVersion));
                    } else {
                        Logger.Info("Processing Revit files ( {0} to {1} of {2}) in Revit {3} session.",
                            progressNumber, progressNumber + sessionFilesCount - 1, totalFilesCount, RevitVersion.GetRevitVersionText(revitVersion));
                    }
                    foreach (var supportedRevitFileInfo in sessionRevitFiles) {
                        BatchRevitMonitorUtil.ShowSupportedRevitFileInfo(supportedRevitFileInfo);
                    }
                    
                    Logger.Info("Starting Revit " + RevitVersion.GetRevitVersionText(revitVersion) + " session...");
                    foreach (var _tup_2 in sessionRevitFiles.Select((_p_1,_p_2) => Tuple.Create(_p_2, _p_1))) {
                        int index = _tup_2.Item1;
                        RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo = _tup_2.Item2;
                        bool isCloudModel = supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid;
                        string cloudModelId;
                        string cloudProjectId;
                        string revitFilePath;
                        if (isCloudModel) {
                            revitFilePath = string.Empty;
                            RevitFileList.RevitCloudModelInfo revitCloudModelInfo = supportedRevitFileInfo.RevitFileInfo.CloudModelInfo;
                            cloudProjectId = revitCloudModelInfo.ProjectGuid.ToString();
                            cloudModelId = revitCloudModelInfo.ModelGuid.ToString();
                        } else {
                            revitFilePath = supportedRevitFileInfo.RevitFileInfo.GetFullPath();
                            cloudProjectId = string.Empty;
                            cloudModelId = string.Empty;
                        }
                        string snapshotDataExportFolderPath = string.Empty;
                        if (batchRvtConfig.EnableDataExport) {
                            snapshotDataExportFolderPath = SnapshotDataUtil.GetSnapshotFolderPath(batchRvtConfig.DataExportFolderPath, revitFilePath, 
                                isCloudModel, cloudProjectId, cloudModelId, batchRvtConfig.SessionStartTime);
                            PathUtil.CreateDirectory(snapshotDataExportFolderPath);
                            snapshotDataExportFolderPaths.Add(snapshotDataExportFolderPath);
                        }
                        ScriptDataUtil.ScriptData scriptData = new ();
                        scriptData.SessionId.SetValue(batchRvtConfig.SessionId.ToString());
                        scriptData.TaskScriptFilePath.SetValue(batchRvtConfig.ScriptFilePath);
                        scriptData.RevitFilePath.SetValue(revitFilePath);
                        scriptData.IsCloudModel.SetValue(isCloudModel);
                        scriptData.CloudProjectId.SetValue(cloudProjectId);
                        scriptData.CloudModelId.SetValue(cloudModelId);
                        scriptData.TaskData.SetValue(batchRvtConfig.TaskData);
                        scriptData.OpenInUI.SetValue(batchRvtConfig.OpenInUi);
                        scriptData.EnableDataExport.SetValue(batchRvtConfig.EnableDataExport);
                        scriptData.SessionDataFolderPath.SetValue(batchRvtConfig.SessionDataFolderPath);
                        scriptData.DataExportFolderPath.SetValue(snapshotDataExportFolderPath);
                        scriptData.ShowMessageBoxOnTaskScriptError.SetValue(batchRvtConfig.ShowMessageBoxOnTaskError);
                        scriptData.RevitProcessingOption.SetValue(batchRvtConfig.RevitProcessingOption);
                        scriptData.CentralFileOpenOption.SetValue(batchRvtConfig.CentralFileOpenOption);
                        scriptData.DeleteLocalAfter.SetValue(batchRvtConfig.DeleteLocalAfter);
                        scriptData.DiscardWorksetsOnDetach.SetValue(batchRvtConfig.DiscardWorksetsOnDetach);
                        scriptData.WorksetConfigurationOption.SetValue(batchRvtConfig.WorksetConfigurationOption);
                        scriptData.AuditOnOpening.SetValue(batchRvtConfig.AuditOnOpening);
                        scriptData.ProgressNumber.SetValue(progressNumber + index);
                        scriptData.ProgressMax.SetValue(totalFilesCount);
                        scriptData.AssociatedData.SetValue(supportedRevitFileInfo.RevitFilePathData.AssociatedData);
                        scriptDatas.Add(scriptData);
                    }
                    string batchRvtScriptsFolderPath = BatchRvt.GetBatchRvtScriptsFolderPath();
                    while (scriptDatas.Any()) {
                        int? nextProgressNumber = BatchRevitMonitorUtil.RunScriptedRevitSession(revitVersion, batchRvtScriptsFolderPath, 
                            batchRvtConfig.ScriptFilePath, scriptDatas, progressNumber, batchRvtConfig.ProcessingTimeOutInMinutes, 
                            batchRvtConfig.ShowRevitProcessErrorMessages, batchRvtConfig.TestModeFolderPath);
                        if (!nextProgressNumber.HasValue) {
                            Logger.Warn("WARNING: The Revit session failed to initialize properly! No Revit files were processed in this session!");
                            // Leave progress number as-is (i.e. do not skip any files if the Revit session terminated before processing any files.)
                        } 
                        else {
                            progressNumber = nextProgressNumber.Value;
                        }
                        scriptDatas = scriptDatas.Where(scriptData => scriptData.ProgressNumber.GetValue() >= progressNumber).ToList();
                        if (!batchRvtConfig.EnableDataExport) continue;
                        Logger.Info("Consolidating snapshots data.");
                        foreach (string snapshotDataExportFolderPath in snapshotDataExportFolderPaths) {
                            SnapshotDataUtil.ConsolidateSnapshotData(snapshotDataExportFolderPath);
                            // NOTE: Have disabled copying of journal files for now because if many files were processed
                            //       in the same Revit session, too many copies of a potentially large journal file
                            //       will be made. Consider modifying the logic so that the journal file is copied only
                            //       once per Revit session. Perhaps copy it to the BatchRvt session folder.
                            if (false) {
                                try {
                                    SnapshotDataUtil.CopySnapshotRevitJournalFile(snapshotDataExportFolderPath);
                                } catch (Exception e) {
                                        
                                    Logger.Warn("WARNING: failed to copy the Revit session's journal file to snapshot data folder:");
                                        
                                    Logger.Warn("\t" + snapshotDataExportFolderPath);
                                    ExceptionUtil.LogOutputErrorDetails(e);
                                }
                            }
                        }
                    }
                }
            }
            return aborted;
        }

        private static bool RunBatchRevitTasks(BatchRevitSettingsConfiguration batchRvtConfig) {
            List<RevitFileList.SupportedRevitFileInfo> supportedRevitFileList = GetSupportedRevitFiles(batchRvtConfig);
            bool aborted = false;
            if (batchRvtConfig.ExecutePreProcessingScript) {
                aborted = ExecutePreProcessingScript(batchRvtConfig);
            }
            if (supportedRevitFileList == null) {
                return true;
            }
            int supportedCount = supportedRevitFileList.Count;
            if (supportedCount == 0) {
                Logger.Error("ERROR: All specified Revit Files are of an unsupported version or have an unsupported file path.");
                return true;
            }
            Logger.Info("Revit Files for processing (" + supportedCount + "):");
            foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList) {
                BatchRevitMonitorUtil.ShowSupportedRevitFileInfo(supportedRevitFileInfo);
            }
            if (batchRvtConfig.EnableDataExport)
            {
                IEnumerable<string> sessionFiles = new List<string>();
                foreach (RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo in supportedRevitFileList)
                {
                    sessionFiles.Append(supportedRevitFileInfo.RevitFileInfo.GetFullPath());
                }
                SessionDataExporter.ExportSessionFilesData(batchRvtConfig.SessionDataFolderPath, batchRvtConfig.SessionId.ToString(), 
                    sessionFiles);
            }
            Logger.Info("Starting batch operation...");
            aborted = ProcessRevitFiles(batchRvtConfig, supportedRevitFileList);

            if (aborted) return true;
            if (batchRvtConfig.ExecutePostProcessingScript) {
                aborted = ExecutePostProcessingScript(batchRvtConfig);
            }
            return aborted;
        }

        private static CommandSettings.Data TryGetCommandSettingsData() {
            CommandSettings.Data commandSettingsData = null;
            try {
                //TODO this should be set already and then called? this below is not correct. leftover from Python
                commandSettingsData = new CommandSettings.Data();
            } 
            catch (Exception ex){
                Logger.Error(ex);
            }
            return commandSettingsData;
        }

        //currently called from the command line only.....
        public static bool Main(Guid instance) {
            bool aborted = false;
            Logger.Info("Started Main Method");
            CommandSettings.Data commandSettingsData = TryGetCommandSettingsData();
            commandSettingsData.SessionId = instance.ToString();
            Logger.Info("SessionID is set to CommandSettings.Data Successfully");
            BatchRevitSettingsConfiguration batchRvtConfig = BatchRevitSettingsConfiguration.ConfigureBatchRvt(commandSettingsData);
            if (batchRvtConfig == null)
            {
                return false;
            }
            
            if (batchRvtConfig.EnableDataExport)
            {
                Logger.Info("Enable Data Export is {0}", batchRvtConfig.EnableDataExport);
                PathUtil.CreateDirectory(batchRvtConfig.SessionDataFolderPath);
                SessionDataExporter.ExportSessionData(batchRvtConfig.SessionId.ToString(), batchRvtConfig.SessionStartTime, 
                    DateTime.MinValue, batchRvtConfig.SessionDataFolderPath, null);
            }
            object sessionError = null;
            try 
            {
                // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
                if (batchRvtConfig.RevitProcessingOption == BatchRvt.RevitProcessingOption.BatchRevitFileProcessing)
                {
                    Logger.Debug("Batch Revit File Processing is Enabled.");
                    aborted = RunBatchRevitTasks(batchRvtConfig);
                } else
                {
                    Logger.Debug("Single Revit File Processing is Enabled.");
                    aborted = RunSingleRevitTask(batchRvtConfig);
                }
            } 
            catch (Exception e)
            {
                ExceptionUtil.LogOutputErrorDetails(e);
                Logger.Error(e);
                throw;
            } 
            finally 
            {
                if (batchRvtConfig.EnableDataExport) {
                    DateTime sessionEndTime = TimeUtil.GetDateTimeNow();
                    SessionDataExporter.ExportSessionData(batchRvtConfig.SessionId.ToString(), 
                        batchRvtConfig.SessionStartTime, sessionEndTime, batchRvtConfig.SessionDataFolderPath, sessionError);
                }
            }
            
            if (aborted) {
                Logger.Warn("Operation aborted.");
            } 
            else {
                Logger.Info("Operation completed.");
                string plainTextLogFilePath = LogFile.LOG_FILE.FirstOrDefault().DumpPlainTextLogFile();
                if (!string.IsNullOrWhiteSpace(plainTextLogFilePath)) 
                {
                    Logger.Info("A plain-text copy of the Log File has been saved to:" + plainTextLogFilePath);
                }
            }
            return true;
        }
    }
}