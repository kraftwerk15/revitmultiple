﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using RevitFileCore;
using RevitFileUpdaterCore;

namespace RevitFileUpdaterCore
{
    internal class BatchRevitMonitorUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        
        private const int SecondsPerMinute = 60;

        private const int RevitProcessExitTimeoutInSeconds = 10 * SecondsPerMinute;

        private const int RevitProgressCheckIntervalInSeconds = 5;

        private const int RevitProcessBeginProcessingTimeoutInSeconds = 5 * SecondsPerMinute;

        private record MonitorRecord
        {
            internal DateTime ProgressRecordCheckTimeUtc      { get; set; }
            internal DateTime ProgressRecordChangedTimeUtc    { get; set; }
            internal int      CurrentProgressRecordNumber     { get; set; }
            internal DateTime SnapshotDataFilesExistTimestamp { get; set; }
        }
        
        //Needs to be a list so it can be captured by reference in closures.
        private static Task<string> _pendingReadLineTask;
        //As Above
        private static Task<string> _pendingProcessOutputReadLineTask;
        //As Above
        private static Task<string> _pendingProcessErrorReadLineTask;
        private static DateTime _snapshotDataFilesExistTimestamp  = DateTime.Today;
        private static int _currentProgressRecordNumber = -1;
        private static DateTime _progressRecordCheckTimeUtc = TimeUtil.GetDateTimeUtcNow();
        private static DateTime _progressRecordChangedTimeUtc = TimeUtil.GetDateTimeUtcNow();

        //private int RevitProcessId;

        public static void ShowSupportedRevitFileInfo(RevitFileList.SupportedRevitFileInfo supportedRevitFileInfo)
        {
            if (supportedRevitFileInfo.RevitFileInfo.CloudModelInfo.IsValid)
            {
                RevitFileList.RevitCloudModelInfo revitCloudModelInfo = supportedRevitFileInfo.RevitFileInfo.CloudModelInfo;
                string projectGuidText = revitCloudModelInfo.ProjectGuid.ToString();
                string modelGuidText = revitCloudModelInfo.ModelGuid.ToString();
                Logger.Info("CLOUD MODEL");
                Logger.Info("Project ID : ", projectGuidText);
                Logger.Info("Model ID: ", modelGuidText);
                string revitVersionText = supportedRevitFileInfo.RevitVersionText;
                revitVersionText = !string.IsNullOrWhiteSpace(revitVersionText) ? revitVersionText : "NOT SPECIFIED!";
                Logger.Info("Revit version: ", revitVersionText);
            }
            else
            {
                RevitFileList.RevitFileInfo    revitFileInfo = supportedRevitFileInfo.RevitFileInfo;
                string revitFilePath = revitFileInfo.RevitFilePath;
                bool   fileExists    = revitFileInfo.Exists();
                long    fileSize      = revitFileInfo.RevitFileSize;
                string fileSizeText  = fileSize != -1 ? $"{fileSize / (1024.0 * 1024.0):0.00}MB" : "<UNKNOWN>";
                Logger.Info(revitFilePath);
                Logger.Info("File exists: ", fileExists ? "YES" : "NO");
                Logger.Info("File size: ", fileSizeText);
                if (!fileExists) return;
                string revitVersionText = supportedRevitFileInfo.RevitVersionText;
                revitVersionText = !string.IsNullOrWhiteSpace(revitVersionText) ? revitVersionText : "NOT DETECTED!";
                Logger.Info("Revit version: " + revitVersionText);
            }
        }

        private static Process UsingClientHandle(IDisposable serverStream, Func<Process> action)
        {
            Process result = null;
            try
            {
                result = action();
            }
            finally
            {
                //Original
                //serverStream.DisposeLocalCopyOfClientHandle();
                serverStream.Dispose();
            }
            return result;
        }

        private static Task<string> ShowRevitScript(StreamReader scriptOutputStream, Task<string> pendingReadLineTask = null)
        {
            (List<string> outputLines, Task<string> item2) = StreamIOUtil.ReadAvailableLines(scriptOutputStream, pendingReadLineTask);
            pendingReadLineTask                            = item2;
            if (!outputLines.Any()) return pendingReadLineTask;
            foreach (string line in outputLines)
            {
                Logger.Info("- " + line);
            }
            return pendingReadLineTask;
        }

        private static Task<string> ShowRevitProcess(StreamReader processOutputStream, Task<string> pendingReadLineTask = null)
        {
            (List<string> outputLines, Task<string> item2) = StreamIOUtil.ReadAvailableLines(processOutputStream, pendingReadLineTask);
            pendingReadLineTask                            = item2;
            if (!outputLines.Any()) return pendingReadLineTask;
            foreach (string line in outputLines)
            {
                // Change to True to see Revit standard output (non-script output)
                Logger.Info("- [ REVIT MESSAGE ] : " + line);
                    
            }
            return pendingReadLineTask;
        }

        private static Task<string> ShowRevitProcessError(StreamReader processErrorStream, bool showRevitProcessErrorMessages, 
            Task<string> pendingReadLineTask = null)
        {
            (List<string> outputLines, Task<string> item2) = StreamIOUtil.ReadAvailableLines(processErrorStream, pendingReadLineTask);
            pendingReadLineTask                            = item2;
            if (!outputLines.Any()) return pendingReadLineTask;
            foreach (string line in outputLines)
            {
                if (line.StartsWith("log4cplus:"))
                {
                    // ignore pesky log4cplus messages (an Autodesk thing?)
                }
                else if (showRevitProcessErrorMessages)
                {
                    Logger.Info("- [ REVIT ERROR MESSAGE ] : " + line);
                }
            }
            return pendingReadLineTask;
        }

        /// <summary>
        /// Terminates the Revit Process
        /// </summary>
        /// <param name="hostRevitProcess">The Revit Process</param>
        private static void TerminateHostRevitProcess(Process hostRevitProcess)
        {
            try
            {
                hostRevitProcess.Kill();
            }
            catch (Exception)
            {
                Logger.Error("ERROR: an error occurred while attempting to kill the Revit process!");
            }
        }

        public static int? RunScriptedRevitSession(int revitVersion, string batchRvtScriptsFolderPath, string scriptFilePath,
            IEnumerable<ScriptDataUtil.ScriptData> scriptDatas, int progressNumber, int processingTimeOutInMinutes, 
            bool showRevitProcessErrorMessages, string testModeFolderPath)
        {
            Logger.Info("Start Running Scripted Revit Session");
            string scriptDataFilePath = ScriptDataUtil.GetUniqueScriptDataFilePath();
            ScriptDataUtil.SaveManyToFile(scriptDataFilePath, scriptDatas);
            string progressRecordFilePath = ScriptDataUtil.GetProgressRecordFilePath(scriptDataFilePath);
            Logger.Info("Starting AnonymousPipeServer");
            AnonymousPipeServerStream serverStream = ServerUtil.CreateAnonymousPipeServer(ServerUtil.IN, HandleInheritability.Inheritable);

            object ServerStreamAction()
            {
                Stream scriptOutputStreamReader = StreamIOUtil.GetStreamReader(serverStream).BaseStream;

                object StreamReaderAction()
                {
                    string scriptOutputPipeHandleString = serverStream.GetClientHandleAsString();
                    Func<Process> clientHandleAction = ClientHandleAction(revitVersion, batchRvtScriptsFolderPath, scriptFilePath, progressNumber, 
                        testModeFolderPath, scriptDataFilePath, scriptOutputPipeHandleString);
                    Process hostRevitProcess = UsingClientHandle(serverStream, clientHandleAction);
                    int hostRevitProcessId = hostRevitProcess.Id;
                    Logger.Info("Revit Process ID : {0}", hostRevitProcessId);
                    GlobalTestMode.ExportRevitProcessId(hostRevitProcessId);
                    List<string> snapshotDataFilePaths = (from scriptData in scriptDatas select SnapshotDataUtil.GetSnapshotDataFilePath(scriptData.DataExportFolderPath.GetValue())).ToList();
                    
                    MonitorRecord monitorRecord = new() {ProgressRecordCheckTimeUtc = DateTime.Now};
                    //TODO Monitor Record; check if this works.
                    MonitorProcess.MonitorHostRevitProcess(hostRevitProcess, 
                       () => MonitoringAction(hostRevitProcess, processingTimeOutInMinutes, showRevitProcessErrorMessages, scriptOutputStreamReader, 
                            progressRecordFilePath, snapshotDataFilePaths, monitorRecord));
                    return null;
                }

                StreamIOUtil.UsingStream(scriptOutputStreamReader, StreamReaderAction);
                return null;
            }

            StreamIOUtil.UsingStream(serverStream, ServerStreamAction);
            int? lastProgressNumber = ScriptDataUtil.GetProgressNumber(progressRecordFilePath);
            int? nextProgressNumber = lastProgressNumber + 1;
            return nextProgressNumber;
        }

        private static Func<Process> ClientHandleAction(int revitVersion, string batchRvtScriptsFolderPath, string scriptFilePath,
            int progressNumber, string testModeFolderPath, string scriptDataFilePath, string scriptOutputPipeHandleString)
        {
            Process HandleAction()
            {
                Process hostRevitProcess = RevitProcessHost.StartRevitProcess(revitVersion, batchRvtScriptsFolderPath, scriptFilePath, 
                    scriptDataFilePath, progressNumber, scriptOutputPipeHandleString, testModeFolderPath);
                return hostRevitProcess;
            }

            return HandleAction;
        }

        private static void MonitoringAction(Process hostRevitProcess, int processingTimeOutInMinutes, 
            bool showRevitProcessErrorMessages, Stream scriptOutputStreamReader, string progressRecordFilePath, 
            IEnumerable<string> snapshotDataFilePaths, MonitorRecord monitorRecord)
        {
            
            _pendingProcessOutputReadLineTask = ShowRevitProcess(hostRevitProcess.StandardOutput, _pendingProcessOutputReadLineTask);
            _pendingProcessErrorReadLineTask = ShowRevitProcessError(hostRevitProcess.StandardError, 
                showRevitProcessErrorMessages, _pendingProcessErrorReadLineTask);
            _pendingReadLineTask = ShowRevitScript(new StreamReader(scriptOutputStreamReader), _pendingReadLineTask);
            
            if (TimeUtil.GetSecondsElapsedSinceUtc(monitorRecord.ProgressRecordCheckTimeUtc) > RevitProgressCheckIntervalInSeconds)
            {
                monitorRecord.ProgressRecordCheckTimeUtc = TimeUtil.GetDateTimeUtcNow();
                int? progressRecordNumber = ScriptDataUtil.GetProgressNumber(progressRecordFilePath);
                if (progressRecordNumber is not null)
                {
                    if (monitorRecord.CurrentProgressRecordNumber != progressRecordNumber)
                    {
                        // Progress update detected.
                        _currentProgressRecordNumber = progressRecordNumber.GetValueOrDefault();
                        _progressRecordChangedTimeUtc = TimeUtil.GetDateTimeUtcNow();
                    }
                }
            }
        
            if (processingTimeOutInMinutes > 0)
            {
                if (_currentProgressRecordNumber != 0)
                {
                    if (TimeUtil.GetSecondsElapsedSinceUtc(_progressRecordChangedTimeUtc) > processingTimeOutInMinutes * SecondsPerMinute)
                    {
                        Logger.Warn("WARNING: Timed-out waiting for Revit task / file to be processed. Forcibly terminating the Revit process...");
                        TerminateHostRevitProcess(hostRevitProcess);
                    }
                }
            }
        
            if (_currentProgressRecordNumber == 0)
            {
                if (TimeUtil.GetSecondsElapsedSinceUtc(_progressRecordChangedTimeUtc) > RevitProcessBeginProcessingTimeoutInSeconds)
                {
                    Logger.Warn("WARNING: Timed-out waiting for Revit script host to begin task / file processing. Forcibly terminating the Revit process...");
                    TerminateHostRevitProcess(hostRevitProcess);
                }
            }
            //TODO fix this
            if (monitorRecord.SnapshotDataFilesExistTimestamp != DateTime.MinValue)
            {
                if (TimeUtil.GetSecondsElapsedSinceUtc(_snapshotDataFilesExistTimestamp) > RevitProcessExitTimeoutInSeconds)
                {
                    Logger.Warn("WARNING: Timed-out waiting for the Revit process to exit. Forcibly terminating the Revit process...");
                    TerminateHostRevitProcess(hostRevitProcess);
                }
            }
            if (snapshotDataFilePaths.All(File.Exists))
            {
                Logger.Info("Detected snapshot data files. Waiting for Revit process to exit...");
                _snapshotDataFilesExistTimestamp = TimeUtil.GetDateTimeUtcNow();
            }
        
            try
            {
                RevitDialogDetection.DismissCheekyRevitDialogBoxes(hostRevitProcess.Id);
            }
            catch (Exception e)
            {
                Logger.Error("WARNING: an error occurred in the cheeky Revit dialog box dismisser!");
                Logger.Error(e);
            }
        }
    }
}