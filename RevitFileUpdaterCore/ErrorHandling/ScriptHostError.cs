﻿


#region Using
using System;
using System.Windows.Forms;
using AppDomain = System.AppDomain;
using StringBuilder = System.Text.StringBuilder;
using MessageBox = System.Windows.Forms.MessageBox;
using RevitFileCore;
using RevitFileCore.WinForms;
#endregion

namespace RevitFileUpdaterCore.ErrorHandling
{
    public class ScriptHostError
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        
        private const string BatchRvtErrorWindowTitle = "Revit Multiple Script Error";

        private const string ScriptHostErrorDataVariable = "revit_script_host";

        private static void SetDataInCurrentDomain(string name, object data) {
            AppDomain.CurrentDomain.SetData(name, data);
        }

        internal static void ShowScriptErrorMessageBox(string errorMessage) {
            IWin32Window mainWindowHandle = WinFormUtil.WindowHandleWrapper.GetMainWindowHandle();
            MessageBox.Show(mainWindowHandle, errorMessage, BatchRvtErrorWindowTitle);
        }
        
        public static object WithErrorHandling(Func<object> action, string errorMessage, bool output = false, bool showErrorMessageBox = false) {
            object result = null;
            try {
                result = action();
            } catch (Exception e) {
                if (output) {
                    Logger.Error(errorMessage);
                    ExceptionUtil.LogOutputErrorDetails(e);
                }
                if (showErrorMessageBox) {
                    StringBuilder fullErrorMessage = new();
                    fullErrorMessage.AppendLine(errorMessage);
                    fullErrorMessage.AppendLine();
                    fullErrorMessage.AppendLine(ExceptionUtil.GetExceptionDetails(e));
                    ShowScriptErrorMessageBox(fullErrorMessage.ToString());
                }
                SetDataInCurrentDomain(ScriptHostErrorDataVariable, e);
            }
            return result;
        }
    }
}