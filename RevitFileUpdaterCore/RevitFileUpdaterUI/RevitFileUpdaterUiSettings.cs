﻿#region license
//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using RevitFileUpdaterCore;

namespace RevitFileCore
{
    /// <summary>
    /// Establishes a JSON of settings from the UI. The user can save and load these settings from their machine.
    /// </summary>
    public class RevitFileUpdaterUiSettings : IPersistent
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        public const string SettingsFileExtension = ".json";
        public const string SettingsFileFilter = "RevitMultiple Settings files (*.json)|*.json";
        // ReSharper disable once IdentifierTypo
        // ReSharper disable once InconsistentNaming
        public const string BatchRVTGUISettingsFilename = "RevitMultipleGui.Settings" + SettingsFileExtension;
        // ReSharper disable once InconsistentNaming
        public const string BatchRVTSettingsFilename = "RevitMultiple.Settings" + SettingsFileExtension;

        private PersistentSettings _persistentSettings;

        // General Task Script settings
        public StringSetting TaskScriptMainFilePath = new("taskScriptFilePath");
        public StringSetting TaskScriptSingleFilePath = new("taskScriptFilePath");
        public StringSetting TaskScriptPreFilePath = new("taskScriptFilePath");
        public StringSetting TaskScriptPostFilePath = new("taskScriptFilePath");
        public BooleanSetting ShowMessageBoxOnTaskScriptError = new("showMessageBoxOnTaskScriptError");

        public IntegerSetting ProcessingTimeOutInMinutes = new("processingTimeOutInMinutes");
        public BooleanSetting ShowRevitProcessErrorMessages = new("showRevitProcessErrorMessages");

        // Revit File List settings
        public StringSetting RevitFileListFilePath = new("revitFileListFilePath");

        // Data Export settings
        public BooleanSetting EnableDataExport = new("enableDataExport");
        public StringSetting DataExportFolderPath = new("dataExportFolderPath");

        // Pre-processing Script settings
        public BooleanSetting ExecutePreProcessingScript = new("executePreProcessingScript");
        public StringSetting PreProcessingScriptFilePath = new("preProcessingScriptFilePath");

        // Post-processing Script settings
        public BooleanSetting ExecutePostProcessingScript = new("executePostProcessingScript");
        public StringSetting PostProcessingScriptFilePath = new("PostProcessingScriptFilePath");

        // Central File Processing settings
        public EnumSetting<BatchRvt.CentralFileOpenOption> CentralFileOpenOption = new("centralFileOpenOption");
        public BooleanSetting DeleteLocalAfter = new("deleteLocalAfter");
        public BooleanSetting DiscardWorksetsOnDetach = new("discardWorksetsOnDetach");
        public EnumSetting<BatchRvt.WorksetConfigurationOption> WorksetConfigurationOption = new("worksetConfigurationOption");

        // Revit Session settings
        public EnumSetting<BatchRvt.RevitSessionOption> RevitSessionOption = new("revitSessionOption");

        // Revit Processing settings
        public EnumSetting<BatchRvt.RevitProcessingOption> RevitProcessingOption = new("revitProcessingOption");

        // Single Revit Task Processing settings
        public IntegerSetting SingleRevitTaskRevitVersion = new("singleRevitTaskRevitVersion");

        // Batch Revit File Processing settings
        public EnumSetting<BatchRvt.RevitFileProcessingOption> RevitFileProcessingOption = new("revitFileProcessingOption");
        public BooleanSetting IfNotAvailableUseMinimumAvailableRevitVersion = new("ifNotAvailableUseMinimumAvailableRevitVersion");
        public IntegerSetting BatchRevitTaskRevitVersion = new("batchRevitTaskRevitVersion");
        // ReSharper disable once InconsistentNaming
        public BooleanSetting OpenInUI = new("openInUi");
        public BooleanSetting AuditOnOpening = new("auditOnOpening");

        public StringSetting SessionDataFolderPath = new("sessionDataFolderPath");

        public StringSetting LogFolder = new("logFolder");
        // UI settings
        public BooleanSetting WindowOnTop = new("windowOnTop");

        public BooleanSetting DeleteExistingFamilies = new("deleteExistingFamilies");
        public BooleanSetting DeleteBackups = new("deleteBackups");
        public BooleanSetting RenameBackups = new("renameBackups");
        public BooleanSetting IgnoreBackups = new("ignoreBackups");

        public EnumSetting<BatchRvt.AccServer> ACCServer = new("accServer");

        /// <summary>
        /// Provides the option for the User to store settings between sessions of the UI.
        /// </summary>
        public RevitFileUpdaterUiSettings()
        {
            _persistentSettings = new PersistentSettings(
                new IPersistent[] {
                    TaskScriptMainFilePath,
                    TaskScriptSingleFilePath,
                    TaskScriptPreFilePath,
                    TaskScriptPostFilePath,
                    ShowMessageBoxOnTaskScriptError,
                    ProcessingTimeOutInMinutes,
                    ShowRevitProcessErrorMessages,
                    RevitFileListFilePath,
                    EnableDataExport,
                    DataExportFolderPath,
                    ExecutePreProcessingScript,
                    PreProcessingScriptFilePath,
                    ExecutePostProcessingScript,
                    PostProcessingScriptFilePath,
                    CentralFileOpenOption,
                    DeleteLocalAfter,
                    DiscardWorksetsOnDetach,
                    WorksetConfigurationOption,
                    RevitSessionOption,
                    RevitProcessingOption,
                    RevitFileProcessingOption,
                    IfNotAvailableUseMinimumAvailableRevitVersion,
                    OpenInUI,
                    AuditOnOpening,
                    DeleteExistingFamilies,
                    DeleteBackups,
                    RenameBackups,
                    IgnoreBackups,
                    ACCServer,
                    WindowOnTop
                }
            );
        }

        public void Load(JObject jObject)
        {
            _persistentSettings.Load(jObject);
        }

        public void Store(JObject jObject)
        {
            _persistentSettings.Store(jObject);
        }

        public bool TryLoad(JObject jObject)
        {
            bool success;

            try
            {
                Load(jObject);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool TryStore(JObject jObject)
        {
            bool success;

            try
            {
                Store(jObject);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public bool LoadFromFile(string filePath = null)
        {
            bool success;

            filePath = string.IsNullOrWhiteSpace(filePath) ? GetDefaultSettingsFilePath() : filePath;

            if (!File.Exists(filePath)) return false;
            try
            {
                string text = File.ReadAllText(filePath);
                JObject jObject = JsonSerialization.DeserializeFromJson(text);
                Load(jObject);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            return success;
        }

        public bool SaveToFile(string filePath = null)
        {
            bool success;
            Logger.Info("Save GUI settings to JSON file.");
            filePath = string.IsNullOrWhiteSpace(filePath) ? GetDefaultSettingsFilePath() : filePath;
            Logger.Info("GUI file path : " + filePath);
            JObject jObject = new();

            try
            {
                Store(jObject);
                string settingsText = JsonSerialization.SerializeToJson(jObject, true);
                Logger.Info("Creating File Info from : " + filePath);
                FileInfo fileInfo = new(filePath);
                fileInfo.Directory?.Create();
                File.WriteAllText(fileInfo.FullName, settingsText);

                success = true;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                success = false;
            }

            return success;
        }

        public string ToJsonString()
        {
            JObject jObject = new();
            Store(jObject);
            return jObject.ToString();
        }

        public static RevitFileUpdaterUiSettings FromJsonString(string batchRvtSettingsJson)
        {
            RevitFileUpdaterUiSettings revitFileUpdaterUiSettings;

            try
            {
                JObject jObject = JsonSerialization.DeserializeFromJson(batchRvtSettingsJson);
                revitFileUpdaterUiSettings = new RevitFileUpdaterUiSettings();
                revitFileUpdaterUiSettings.Load(jObject);
            }
            catch (Exception)
            {
                revitFileUpdaterUiSettings = null;
            }

            return revitFileUpdaterUiSettings;
        }

        public static RevitFileUpdaterUiSettings Create(
                string taskScriptFilePath,
                string revitFileListFilePath,
                BatchRvt.RevitProcessingOption revitProcessingOption,
                BatchRvt.CentralFileOpenOption centralFileOpenOption,
                bool deleteLocalAfter,
                bool discardWorksetsOnDetach,
                BatchRvt.RevitSessionOption revitSessionOption,
                BatchRvt.RevitFileProcessingOption revitFileVersionOption,
                int taskRevitVersion,
                int fileProcessingTimeOutInMinutes,
                bool fallbackToMinimumAvailableRevitVersion
            )
        {
            RevitFileUpdaterUiSettings revitFileUpdaterUiSettings = new();

            // General Task Script settings
            revitFileUpdaterUiSettings.TaskScriptMainFilePath.SetValue(taskScriptFilePath);
            revitFileUpdaterUiSettings.ProcessingTimeOutInMinutes.SetValue(fileProcessingTimeOutInMinutes);

            // Revit Processing settings
            revitFileUpdaterUiSettings.RevitProcessingOption.SetValue(revitProcessingOption);

            // Revit File List settings
            revitFileUpdaterUiSettings.RevitFileListFilePath.SetValue(revitFileListFilePath);

            // Central File Processing settings
            revitFileUpdaterUiSettings.CentralFileOpenOption.SetValue(centralFileOpenOption);
            revitFileUpdaterUiSettings.DeleteLocalAfter.SetValue(deleteLocalAfter);
            revitFileUpdaterUiSettings.DiscardWorksetsOnDetach.SetValue(discardWorksetsOnDetach);

            // Revit Session settings
            revitFileUpdaterUiSettings.RevitSessionOption.SetValue(revitSessionOption);

            // Single Revit Task Processing settings
            revitFileUpdaterUiSettings.SingleRevitTaskRevitVersion.SetValue(taskRevitVersion);

            // Batch Revit File Processing settings
            revitFileUpdaterUiSettings.RevitFileProcessingOption.SetValue(revitFileVersionOption);
            revitFileUpdaterUiSettings.IfNotAvailableUseMinimumAvailableRevitVersion.SetValue(fallbackToMinimumAvailableRevitVersion);
            revitFileUpdaterUiSettings.BatchRevitTaskRevitVersion.SetValue(taskRevitVersion);
            revitFileUpdaterUiSettings.AuditOnOpening.SetValue(false); // TODO: implement this option for this function?

            return revitFileUpdaterUiSettings;
        }

        public static string GetDefaultSettingsFilePath()
        {
            return Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "RevitMultiple",
                    BatchRVTGUISettingsFilename
                );
        }
    }
}
