﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RevitFileCore;

namespace RevitFileUpdaterCore
{
    /// <summary>
    /// This class is reserved for Dynamo nodes?
    /// </summary>
    public static class BatchRevitTasks
    {
        /// <summary>
        /// Run Task given a Task Script File Path
        /// </summary>
        /// <param name="taskScriptFilePath">List of Task Script files.</param>
        /// <param name="revitFileList">List of Revit Files.</param>
        /// <param name="useRevitVersion">Enumeration for Revit version.</param>
        /// <param name="centralFileOpenOption">The option for opening the central model.</param>
        /// <param name="discardWorksetsOnDetach">Are Worksets Discard when detaching the model.</param>
        /// <param name="deleteLocalAfter">Should the Revit Local File be deleted after completion.</param>
        /// <param name="openLogFileWhenDone">Should the Log File display when completed.</param>
        /// <param name="taskData"></param>
        /// <param name="testModeFolderPath"></param>
        /// <returns></returns>
        public static string RunTask(string taskScriptFilePath, IEnumerable<string> revitFileList, UseRevitVersion useRevitVersion,
                BatchRvt.CentralFileOpenOption centralFileOpenOption, bool discardWorksetsOnDetach, bool deleteLocalAfter,
                bool openLogFileWhenDone, string taskData, string testModeFolderPath
            )
        {
            return RunTask(
                    taskScriptFilePath,
                    revitFileList, // Input is a list of Revit file paths.
                    BatchRvt.RevitProcessingOption.BatchRevitFileProcessing,
                    useRevitVersion,
                    centralFileOpenOption,
                    discardWorksetsOnDetach,
                    deleteLocalAfter,
                    openLogFileWhenDone,
                    logFolderPath: null,
                    fileProcessingTimeOutInMinutes: 0,
                    fallbackToMinimumAvailableRevitVersion: false,
                    taskData: taskData,
                    testModeFolderPath: testModeFolderPath
                );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskScriptFilePath"></param>
        /// <param name="useRevitVersion"></param>
        /// <param name="openLogFileWhenDone"></param>
        /// <param name="taskData"></param>
        /// <param name="testModeFolderPath"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static string RunSingleTask(string taskScriptFilePath, UseRevitVersion useRevitVersion, bool openLogFileWhenDone, 
            string taskData, string testModeFolderPath)
        {
            if (useRevitVersion == UseRevitVersion.RevitFileVersion)
            {
                throw new ArgumentException("useRevitVersion argument must specify a specific Revit version!");
            }

            return RunTask(
                    taskScriptFilePath,
                    null, // Revit File list is N/A for single task processing
                    BatchRvt.RevitProcessingOption.SingleRevitTaskProcessing,
                    useRevitVersion,
                    BatchRvt.CentralFileOpenOption.Detach,  // N/A for single task processing
                    discardWorksetsOnDetach: true, // N/A for single task processing
                    deleteLocalAfter: true, // N/A for single task processing
                    openLogFileWhenDone: openLogFileWhenDone,
                    logFolderPath: null,
                    fileProcessingTimeOutInMinutes: 0, // N/A for single task processing
                    fallbackToMinimumAvailableRevitVersion: false,
                    taskData: taskData,
                    testModeFolderPath: testModeFolderPath
                );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskScriptFilePath"></param>
        /// <param name="revitFileList"></param>
        /// <param name="useRevitVersion"></param>
        /// <param name="centralFileOpenOption"></param>
        /// <param name="discardWorksetsOnDetach"></param>
        /// <param name="deleteLocalAfter"></param>
        /// <param name="openLogFileWhenDone"></param>
        /// <param name="logFolderPath"></param>
        /// <param name="fileProcessingTimeOutInMinutes"></param>
        /// <param name="fallbackToMinimumAvailableRevitVersion"></param>
        /// <param name="taskData"></param>
        /// <param name="testModeFolderPath"></param>
        /// <returns></returns>
        public static string RunTaskAdvanced(string taskScriptFilePath, IEnumerable<string> revitFileList, UseRevitVersion useRevitVersion, 
            BatchRvt.CentralFileOpenOption centralFileOpenOption, bool discardWorksetsOnDetach, bool deleteLocalAfter, bool openLogFileWhenDone,
             string logFolderPath, int fileProcessingTimeOutInMinutes, bool fallbackToMinimumAvailableRevitVersion, string taskData, string testModeFolderPath)
        {
            return RunTask(
                    taskScriptFilePath,
                    revitFileList, // Input is a list of Revit file paths.
                    BatchRvt.RevitProcessingOption.BatchRevitFileProcessing,
                    useRevitVersion,
                    centralFileOpenOption,
                    discardWorksetsOnDetach,
                    deleteLocalAfter,
                    openLogFileWhenDone,
                    logFolderPath,
                    fileProcessingTimeOutInMinutes,
                    fallbackToMinimumAvailableRevitVersion,
                    taskData: taskData,
                    testModeFolderPath: testModeFolderPath
                );
        }
        
        /// <summary>
        /// Run Task from a Settings File, if provided.
        /// </summary>
        /// <param name="settingsFilePath"></param>
        /// <param name="logFolderPath"></param>
        /// <param name="openLogFileWhenDone"></param>
        /// <param name="taskData"></param>
        /// <param name="testModeFolderPath"></param>
        /// <returns></returns>
        public static string RunTaskFromSettingsFile(string settingsFilePath, string logFolderPath, bool openLogFileWhenDone, string taskData = null,
                string testModeFolderPath = null)
        {
            CommandSettings.Data commandSettingsData = new()
            {
                SettingsFilePath = settingsFilePath,
                LogFolderPath = logFolderPath,
                TaskData = taskData,
                TestModeFolderPath = testModeFolderPath
            };
            
            return RunTask(commandSettingsData, openLogFileWhenDone);
        }
        /// <summary>
        /// Dynamo Node
        /// </summary>
        /// <param name="taskScriptFilePath"></param>
        /// <param name="revitFileListInput"></param>
        /// <param name="revitProcessingOption"></param>
        /// <param name="useRevitVersion"></param>
        /// <param name="centralFileOpenOption"></param>
        /// <param name="discardWorksetsOnDetach"></param>
        /// <param name="deleteLocalAfter"></param>
        /// <param name="openLogFileWhenDone"></param>
        /// <param name="logFolderPath"></param>
        /// <param name="fileProcessingTimeOutInMinutes"></param>
        /// <param name="fallbackToMinimumAvailableRevitVersion"></param>
        /// <param name="taskData"></param>
        /// <param name="testModeFolderPath"></param>
        /// <returns></returns>
        private static string RunTask(string taskScriptFilePath, object revitFileListInput, BatchRvt.RevitProcessingOption revitProcessingOption, 
            UseRevitVersion useRevitVersion, BatchRvt.CentralFileOpenOption centralFileOpenOption, bool discardWorksetsOnDetach, bool deleteLocalAfter,
            bool openLogFileWhenDone, string logFolderPath, int fileProcessingTimeOutInMinutes, bool fallbackToMinimumAvailableRevitVersion,
            string taskData, string testModeFolderPath)
        {
            BatchRvt.RevitFileProcessingOption batchRvtRevitFileProcessingOption = (
                    useRevitVersion == UseRevitVersion.RevitFileVersion ?
                    BatchRvt.RevitFileProcessingOption.UseFileRevitVersionIfAvailable :
                    BatchRvt.RevitFileProcessingOption.UseSpecificRevitVersion
                );

            // NOTE: can be any version if useRevitVersion is set to RevitFileVersion.
            int taskRevitVersion = (
                    useRevitVersion switch
                    {
                        UseRevitVersion.Revit2018 => 2018,
                        UseRevitVersion.Revit2019 => 2019,
                        UseRevitVersion.Revit2020 => 2020,
                        UseRevitVersion.Revit2021 => 2021,
                        _ => 2022
                    }
                );

            RevitFileUpdaterUiSettings revitFileUpdaterUiSettings = RevitFileUpdaterUiSettings.Create(
                    taskScriptFilePath,
                    (revitFileListInput as string) ?? string.Empty,
                    revitProcessingOption,
                    centralFileOpenOption,
                    deleteLocalAfter,
                    discardWorksetsOnDetach,
                    BatchRvt.RevitSessionOption.UseSeparateSessionPerFile,
                    batchRvtRevitFileProcessingOption,
                    taskRevitVersion,
                    fileProcessingTimeOutInMinutes,
                    fallbackToMinimumAvailableRevitVersion
                );

            CommandSettings.Data commandSettingsData = new()
            {
                RevitFileList = revitFileListInput as IEnumerable<string>,
                Settings = revitFileUpdaterUiSettings,
                LogFolderPath = logFolderPath,
                TaskData = taskData,
                TestModeFolderPath = testModeFolderPath
            };

            return RunTask(commandSettingsData, openLogFileWhenDone);
        }
        
        /// <summary>
        /// Run Task when CommandSettings.Data and a bool are provided.
        /// </summary>
        /// <param name="commandSettingsData">A valid CommandSettings.Data</param>
        /// <param name="openLogFileWhenDone">User Decides if the Log File should display when completed.</param>
        /// <returns>The Generated Log File Path</returns>
        private static string RunTask(
                CommandSettings.Data commandSettingsData, bool openLogFileWhenDone)
        {
            commandSettingsData = ValidateCommandSettingsData(commandSettingsData);

            string batchRvtFolderPath = BatchRvt.GetBatchRvtFolderPath();
            
            //This is pointing below to the Python instance.    
            //BatchRvt.ExecuteMonitorScript(batchRvtFolderPath, commandSettingsData);
            //TODO should this point to main with a valid commandSettingsData now? That what this execute monitor script was doing
            string logFilePath = commandSettingsData.GeneratedLogFilePath;

            logFilePath = PostProcessLogFile(logFilePath);
            //TODO this can't be right
            if (!openLogFileWhenDone)
            {
                return logFilePath;
            }

            if (!string.IsNullOrWhiteSpace(logFilePath))
            {
                _ = Process.Start(logFilePath);
            }

            return logFilePath;
        }

        private static string PostProcessLogFile(string logFilePath)
        {
            if (string.IsNullOrWhiteSpace(logFilePath)) return logFilePath;
            string plainTextLogFilePath = Path.Combine(
                Path.GetDirectoryName(logFilePath) ?? throw new InvalidOperationException(),
                Path.GetFileNameWithoutExtension(logFilePath) + ".txt"
            );

            File.WriteAllLines(
                plainTextLogFilePath,
                LogFile.ReadLinesAsPlainText(logFilePath)
            );

            logFilePath = plainTextLogFilePath;

            return logFilePath;
        }

        private static CommandSettings.Data ValidateCommandSettingsData(CommandSettings.Data commandSettingsData)
        {
            if (string.IsNullOrWhiteSpace(commandSettingsData.SettingsFilePath))
            {
                commandSettingsData.SettingsFilePath = null;
            }

            if (string.IsNullOrWhiteSpace(commandSettingsData.LogFolderPath))
            {
                commandSettingsData.LogFolderPath = null;
            }

            if (string.IsNullOrWhiteSpace(commandSettingsData.SessionId))
            {
                commandSettingsData.SessionId = null;
            }

            if (string.IsNullOrWhiteSpace(commandSettingsData.TaskData))
            {
                commandSettingsData.TaskData = null;
            }

            if (string.IsNullOrWhiteSpace(commandSettingsData.TestModeFolderPath))
            {
                commandSettingsData.TestModeFolderPath = null;
            }

            RevitFileUpdaterUiSettings batchRvtSettings = commandSettingsData.Settings;

            if (batchRvtSettings != null)
            {
                if (batchRvtSettings.RevitProcessingOption.GetValue() == BatchRvt.RevitProcessingOption.BatchRevitFileProcessing)
                {
                    if (
                            string.IsNullOrWhiteSpace(batchRvtSettings.RevitFileListFilePath.GetValue())
                            &&
                            commandSettingsData.RevitFileList == null
                        )
                    {
                        throw new ArgumentNullException("No Revit file list was specified for Batch processing mode.");
                    }
                }
            }

            commandSettingsData.GeneratedLogFilePath = null;

            return commandSettingsData;
        }

        // NOTE: Dynamo scripts are not supported in Revit versions earlier than 2016.
        public enum UseRevitVersion
        {
            RevitFileVersion = 0,
            Revit2018 = 4,
            Revit2019 = 5,
            Revit2020 = 6,
            Revit2021 = 7,
            Revit2022 = 8
        }
    }
}