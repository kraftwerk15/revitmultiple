﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using RevitFileCore;

namespace RevitFileUpdaterCore
{
    public static class BatchRvt
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public const string ScriptsFolderName = "Scripts";

        public enum CentralFileOpenOption { NotDefined = -1, Detach = 0, CreateNewLocal = 1 }
        public enum RevitSessionOption { NotDefined = -1, UseSeparateSessionPerFile = 0, UseSameSessionForFilesOfSameVersion = 1 }
        public enum RevitProcessingOption { NotDefined = -1, BatchRevitFileProcessing = 0, SingleRevitTaskProcessing = 1 }
        public enum RevitFileProcessingOption { NotDefined = -1, UseFileRevitVersionIfAvailable = 0, UseSpecificRevitVersion = 1 }
        public enum WorksetConfigurationOption { NotDefined = -1, CloseAllWorksets = 0, OpenAllWorksets = 1, OpenLastViewed = 2 }
        
        public enum AccServer { NotDefined =-1, Us = 0, Eu = 1}

        private const string ScriptDataFolderName = "RevitMultiple";
        //Not Used because of NLOG
        //private const string MONITOR_SCRIPT_FILE_NAME = "batch_rvt.py";

        

        private static string ConstructCommandLineArguments(IEnumerable<KeyValuePair<string, string>> arguments)
        {
            return string.Join(" ", arguments.Select(arg => "--" + arg.Key + " " + arg.Value));
        }

        public static bool IsBatchRvtLine(string line)
        {
            string[] parts = line.Split();

            TimeSpan timeSpan = default;

            bool success = TimeSpan.TryParseExact(parts.First(), @"hh\:mm\:ss", CultureInfo.InvariantCulture, out timeSpan);

            return success;
        }
        //called from the UI
        /// <summary>
        /// Starts the RevitFileUpdater.exe Process along with Arguments
        /// </summary>
        /// <param name="settingsFilePath">CommandSettings.SettingsFilePathOption</param>
        /// <param name="logFolderPath">Allows the user to specify a different Folder Path for Log files. Optional.</param>
        /// <param name="sessionId"></param>
        /// <param name="taskData"></param>
        /// <param name="testModeFolderPath"></param>
        /// <returns>The RevitBatchFile Process</returns>
        public static Process StartBatchRvt(string settingsFilePath, string logFolderPath = null, string sessionId = null, 
            string taskData = null, string testModeFolderPath = null)
        {
            string baseDirectory = GetBatchRvtFolderPath();
            Logger.Info("Creating RevitMultiple Settings");
            Dictionary<string, string> batchRvtOptions =
                new()
                {
                    { CommandSettings.SettingsFilePathOption, settingsFilePath },
                };

            if (!string.IsNullOrWhiteSpace(logFolderPath))
            {
                batchRvtOptions[CommandSettings.LogFolderPathOption] = logFolderPath;
            }

            if (!string.IsNullOrWhiteSpace(sessionId))
            {
                batchRvtOptions[CommandSettings.SessionIdOption] = sessionId;
            }

            if (!string.IsNullOrWhiteSpace(taskData))
            {
                batchRvtOptions[CommandSettings.TaskDataOption] = taskData;
            }

            if (!string.IsNullOrWhiteSpace(testModeFolderPath))
            {
                batchRvtOptions[CommandSettings.TestModeFolderPathOption] = testModeFolderPath;
            }
            Logger.Info("RevitMultiple Settings Created");
            Logger.Info("Preparing the Command Line to run.");
            string pathToCommandLineTemp = Path.GetFullPath(Path.Combine(baseDirectory, @"..\..\..\..\"));
            string pathToCommandLine =
                Path.GetFullPath(Path.Combine(pathToCommandLineTemp, @"RevitEntry\bin\Debug\net5.0-windows\RevitFileUpdater.exe"));
            string fileName = pathToCommandLine;
            if (File.Exists(pathToCommandLine))
            {
                ProcessStartInfo psi = new()
                {
                    UseShellExecute = false,
                    WorkingDirectory = Path.GetFullPath(Path.Combine(baseDirectory, @"..\")),
                    Arguments = ConstructCommandLineArguments(batchRvtOptions),
                    RedirectStandardInput = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    FileName = pathToCommandLine
                };

                Logger.Info("Starting the Process");
                Process batchRvtProcess = Process.Start(psi);
                Logger.Info("Process Started");
                return batchRvtProcess;
            }
            Logger.Fatal("Process did not start correctly.");
            //alert user the Process does not exists
            return null;
        }
        
        //public static void ExecuteMonitorScript( string batchRvtFolderPath, Guid instance, CommandSettings.Data commandSettingsData = null)
        //{
        //    //var engine = ScriptUtil.CreatePythonEngine();
        //
        //    //var mainModuleScope = ScriptUtil.CreateMainModule(engine);
        //
        //    var scriptsFolderPath = Path.Combine(batchRvtFolderPath, SCRIPTS_FOLDER_NAME);
        //
        //    var monitorScriptFilePath = Path.Combine( scriptsFolderPath, MONITOR_SCRIPT_FILE_NAME );
        //
        //    //ScriptUtil.AddSearchPaths(engine,
        //    //        new[] {
        //    //            scriptsFolderPath,
        //    //            batchRvtFolderPath
        //    //        }
        //    //    );
        //    //
        //    //ScriptUtil.AddBuiltinVariables(
        //    //        engine,
        //    //        new Dictionary<string, object> {
        //    //            { "__scope__", mainModuleScope },
        //    //            { "__command_settings_data__", commandSettingsData }
        //    //        }
        //    //    );
        //    //
        //    //ScriptUtil.AddPythonStandardLibrary(mainModuleScope);
        //    //
        //    //var scriptSource = ScriptUtil.CreateScriptSourceFromFile(engine, monitorScriptFilePath);
        //    //
        //    //scriptSource.Execute(mainModuleScope);
        //
        //    return;
        //}

        public static string GetDataFolderPath()
        {
            return Path.Combine(PathUtil.GetLocalAppDataFolderPath(), ScriptDataFolderName);
        }

        public static string GetBatchRvtFolderPath()
        {
            return Path.GetDirectoryName(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().Location).LocalPath);
        }

        public static string GetBatchRvtScriptsFolderPath()
        {
            return Path.Combine(GetBatchRvtFolderPath(), ScriptsFolderName);
        }

        
    }
}