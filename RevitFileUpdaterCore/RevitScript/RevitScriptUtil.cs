﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using NLog;
using RevitFileCore;
using RevitFileUpdaterCore;

namespace RevitFileUpdaterCore.RevitScript
{
    public class RevitScriptUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static string ScriptDataFilePath { get; set; }
        public static Document ScriptDocument { get; set; }
        
        public static List<object> OUTPUT_FUNCTION_CONTAINER = new() { null };

        public static List<object> SCRIPT_DATA_FILE_PATH_CONTAINER = new() { null };

        private static readonly List<ScriptDataUtil.ScriptData> ScriptDataContainer = new() {null};

        public static object SCRIPT_DOCUMENT_CONTAINER = new List<object>
        {
            null
        };

        public static List<object> SCRIPT_UIAPPLICATION_CONTAINER = new List<object>
        {
            null
        };

        public static void SetOutputFunction(object output)
        {
            OUTPUT_FUNCTION_CONTAINER.Add(output);
        }

        public static void SetScriptDataFilePath(string scriptDataFilePath)
        {
            SCRIPT_DATA_FILE_PATH_CONTAINER.Add(scriptDataFilePath);
        }

        //public static void SetScriptDocument(Document doc)
        //{
        //    SCRIPT_DOCUMENT_CONTAINER[0] = doc;
        //}

        public static void SetUIApplication(UIApplication uiapp)
        {
            SCRIPT_UIAPPLICATION_CONTAINER.Add(uiapp);
        }

        //public static void Output(string m = "", string msgId = "")
        //{
        //    var message = msgId != "" ? "[" + msgId.ToString() + "]" + " " : "" + m;
        //    OUTPUT_FUNCTION_CONTAINER[0](message);
        //}

        //private static string GetScriptDataFilePath()
        //{
        //    var scriptDataFilePath = SCRIPT_DATA_FILE_PATH_CONTAINER[0];
        //    return scriptDataFilePath;
        //}

        public static IEnumerable<ScriptDataUtil.ScriptData> LoadScriptDatas()
        {
            //string scriptDataFilePath = GetScriptDataFilePath();
            if (ScriptDataFilePath == null)
            {
                throw new Exception("ERROR: could not retrieve script data file path from host.");
            }

            IEnumerable<ScriptDataUtil.ScriptData> scriptDatas = ScriptDataUtil.LoadManyFromFile(ScriptDataFilePath);
            if (scriptDatas == null)
            {
                throw new Exception("ERROR: could not load script data file.");
            }

            return scriptDatas;
        }

        public static ScriptDataUtil.ScriptData GetCurrentScriptData()
        {
            return ScriptDataContainer[0];
        }

        public static void SetCurrentScriptData(ScriptDataUtil.ScriptData scriptData)
        {
            ScriptDataContainer[0] = scriptData;
        }

        public static bool IsCloudModel()
        {
            return ScriptDataContainer[0].IsCloudModel.GetValue();
        }

        public static string GetCloudProjectId()
        {
            return ScriptDataContainer[0].CloudProjectId.GetValue();
        }

        public static string GetCloudModelId()
        {
            return ScriptDataContainer[0].CloudModelId.GetValue();
        }
        //TODO this is probably not working correctly
        public static string GetCloudRegion()
        {
            return ScriptDataContainer[0].CloudRegion.GetValue();
        }

        public static string GetRevitFilePath()
        {
            return ScriptDataContainer[0].RevitFilePath.GetValue();
        }

        public static bool GetOpenInUi()
        {
            return ScriptDataContainer[0].OpenInUI.GetValue();
        }

        public static string GetProjectFolderName()
        {
            string projectFolderName = null;
            string revitFilePath = GetRevitFilePath();
            if (!string.IsNullOrWhiteSpace(revitFilePath))
            {
                projectFolderName = PathUtil.GetProjectFolderNameFromRevitProjectFilePath(revitFilePath);
            }

            return projectFolderName;
        }

        public static string GetDataExportFolderPath()
        {
            return ScriptDataContainer[0].DataExportFolderPath.GetValue();
        }

        public static string GetSessionId()
        {
            return ScriptDataContainer[0].SessionId.GetValue();
        }

        public static string GetTaskScriptFilePath()
        {
            return ScriptDataContainer[0].TaskScriptFilePath.GetValue();
        }

        public static string GetTaskData()
        {
            return ScriptDataContainer[0].TaskData.GetValue();
        }

        public static string GetSessionDataFolderPath()
        {
            return ScriptDataContainer[0].SessionDataFolderPath.GetValue();
        }

        public static bool GetShowMessageBoxOnTaskError()
        {
            return ScriptDataContainer[0].ShowMessageBoxOnTaskScriptError.GetValue();
        }

        public static bool GetEnableDataExport()
        {
            return ScriptDataContainer[0].EnableDataExport.GetValue();
        }

        public static BatchRvt.RevitProcessingOption GetRevitProcessingOption()
        {
            return ScriptDataContainer[0].RevitProcessingOption.GetValue();
        }

        public static BatchRvt.CentralFileOpenOption GetCentralFileOpenOption()
        {
            return ScriptDataContainer[0].CentralFileOpenOption.GetValue();
        }

        public static bool GetDeleteLocalAfter()
        {
            return ScriptDataContainer[0].DeleteLocalAfter.GetValue();
        }

        public static bool GetDiscardWorksetsOnDetach()
        {
            return ScriptDataContainer[0].DiscardWorksetsOnDetach.GetValue();
        }

        public static BatchRvt.WorksetConfigurationOption GetWorksetConfigurationOption()
        {
            return ScriptDataContainer[0].WorksetConfigurationOption.GetValue();
        }

        public static bool GetAuditOnOpening()
        {
            return ScriptDataContainer[0].AuditOnOpening.GetValue();
        }

        public static int GetProgressNumber()
        {
            return ScriptDataContainer[0].ProgressNumber.GetValue();
        }

        public static int GetProgressMax()
        {
            return ScriptDataContainer[0].ProgressMax.GetValue();
        }

        public static object GetAssociatedData()
        {
            return ScriptDataContainer[0].AssociatedData.GetValue();
        }

        //public static object GetScriptDocument()
        //{
        //    var doc = SCRIPT_DOCUMENT_CONTAINER[0];
        //    return doc;
        //}

        //public static UIApplication GetUIApplication()
        //{
        //    var uiapp = SCRIPT_UIAPPLICATION_CONTAINER[0];
        //    return uiapp;
        //}

        private static object WithExceptionLogging(Func<object> action)
        {
            object result = null;
            try
            {
                result = action();
            }
            catch (Exception e)
            {
                ExceptionUtil.LogOutputErrorDetails(e);
                throw;
            }

            return result;
        }

        private static WorksetConfiguration CreateWorksetConfiguration(
            BatchRvt.WorksetConfigurationOption batchRvtWorksetConfigurationOption)
        {
            WorksetConfigurationOption worksetConfigurationOption = batchRvtWorksetConfigurationOption switch
            {
                BatchRvt.WorksetConfigurationOption.CloseAllWorksets => WorksetConfigurationOption.CloseAllWorksets,
                BatchRvt.WorksetConfigurationOption.OpenAllWorksets => WorksetConfigurationOption.OpenAllWorksets,
                _ => WorksetConfigurationOption.OpenLastViewed
            };
            return new WorksetConfiguration(worksetConfigurationOption);
        }

        // ReSharper disable once IdentifierTypo
        private static ModelPath GetWorksharingCentralModelPath(Document doc)
        {
            ModelPath centralModelPath;
            try
            {
                centralModelPath = doc.GetWorksharingCentralModelPath();
            }
            catch (InvalidOperationException)
            {
                centralModelPath = null;
            }

            return centralModelPath;
        }

        public static string GetCentralModelFilePath(Document doc)
        {
            ModelPath modelPath = GetWorksharingCentralModelPath(doc);
            string filePath = modelPath != null
                ? ModelPathUtils.ConvertModelPathToUserVisiblePath(modelPath)
                : string.Empty;
            return filePath;
        }

        public static Document GetActiveDocument(UIApplication uiApp)
        {
            UIDocument uiDoc = uiApp.ActiveUIDocument;
            Document doc = uiDoc?.Document;
            return doc;
        }

        private static void SafeCloseWithoutSave(Document doc, bool isOpenedInUi, string closedMessage)
        {
            Application app = doc.Application;
            try
            {
                if (!isOpenedInUi)
                {
                    RevitFileUtil.CloseWithoutSave(doc);
                    Logger.Info(closedMessage);
                }
            }
            catch (InvalidOperationException e)
            {
                Logger.Error("WARNING: Could not close the document!");
                Logger.Error(e.Message);
            }
            catch (Exception ex)
            {
                Logger.Error("WARNING: Could not close the document!");
                ExceptionUtil.LogOutputErrorDetails(ex, false);
            }

            app.PurgeReleasedAPIObjects();
        }

        public static object WithOpenedDetachedDocument(UIApplication uiApp, bool openInUi, string centralFilePath,
            bool discardWorksets, WorksetConfiguration worksetConfig, bool audit, Func<Document, object> documentAction)
        {
            Application app = uiApp.Application;
            object result;
            Logger.Info("Opening detached instance of central file: " + centralFilePath);
            bool closeAllWorksets = worksetConfig == null;
            Document doc;
            if (openInUi)
            {
                UIDocument uiDoc = discardWorksets
                    ? RevitFileUtil.OpenAndActivateDetachAndDiscardWorksets(uiApp, centralFilePath, audit)
                    : RevitFileUtil.OpenAndActivateDetachAndPreserveWorksets(uiApp, centralFilePath, closeAllWorksets,
                        worksetConfig, audit);
                doc = uiDoc.Document;
            }
            else if (discardWorksets)
            {
                doc = RevitFileUtil.OpenDetachAndDiscardWorksets(app, centralFilePath, audit);
            }
            else
            {
                doc = RevitFileUtil.OpenDetachAndPreserveWorksets(app, centralFilePath, closeAllWorksets, worksetConfig,
                    audit);
            }

            try
            {
                result = documentAction(doc);
            }
            finally
            {
                SafeCloseWithoutSave(doc, openInUi, "Closed detached instance of central file: " + centralFilePath);
            }

            return result;
        }

        public static object WithOpenedNewLocalDocument(UIApplication uiApp, bool openInUi, string centralFilePath, string localFilePath,
            WorksetConfiguration worksetConfig, bool audit, Func<Document, object> documentAction)
        {
            object result = null;
            try
            {
                Application app = uiApp.Application;

                Logger.Info("Opening local instance of central file: " + centralFilePath);
                Logger.Info("New local file: " + localFilePath);
                bool closeAllWorksets = worksetConfig == null;
                Document doc;
                if (openInUi)
                {
                    UIDocument uiDoc = RevitFileUtil.OpenAndActivateNewLocal(uiApp, centralFilePath, localFilePath,
                        closeAllWorksets, worksetConfig, audit);
                    doc = uiDoc.Document;
                }
                else
                {
                    doc = RevitFileUtil.OpenNewLocal(app, centralFilePath, localFilePath, closeAllWorksets,
                        worksetConfig, audit);
                }

                try
                {
                    result = documentAction(doc);
                }
                finally
                {
                    SafeCloseWithoutSave(doc, openInUi, "Closed local file: " + localFilePath);
                }
            }
            catch (ArgumentException e)
            {
                if (e.Message == "The model is a local file.\r\nParameter name: sourcePath")
                {
                    Logger.Error("ERROR: The model is a local file. Cannot create another local file from it!");
                }
                else
                {
                    throw;
                }
            }

            return result;
        }

        public static object WithOpenedCloudDocument(UIApplication uiApp, bool openInUi, string region,
            string cloudProjectId, string cloudModelId,
            WorksetConfiguration worksetConfig, bool audit, Func<Document,object> documentAction)
        {
            Document doc;
            Application app = uiApp.Application;
            object result = null;
            Logger.Info("Opening cloud model.");
            bool closeAllWorksets = worksetConfig == null;
            if (openInUi)
            {
                UIDocument uiDoc = RevitFileUtil.OpenAndActivateCloudDocument(uiApp, region, cloudProjectId,
                    cloudModelId, closeAllWorksets, worksetConfig, audit);
                doc = uiDoc.Document;
            }
            else
            {
                doc = RevitFileUtil.OpenCloudDocument(app, region, cloudProjectId, cloudModelId, closeAllWorksets,
                    worksetConfig, audit);
            }

            try
            {
                string cloudModelPathText = ModelPathUtils.ConvertModelPathToUserVisiblePath(doc.GetCloudModelPath());
                Logger.Info("Cloud model path is: " + cloudModelPathText);
                result = documentAction(doc);
            }
            finally
            {
                SafeCloseWithoutSave(doc, openInUi, "Closed cloud model.");
            }

            return result;
        }

        public static object WithOpenedDocument(UIApplication uiApp, bool openInUi, string revitFilePath,
            bool audit, Func<Document,object> documentAction)
        {
            Document doc;
            Application app = uiApp.Application;
            object result = null;
            Logger.Info("Opening file: " + revitFilePath);
            // TODO: decide if worksets should be closed or open (currently the script closes them)
            if (openInUi)
            {
                UIDocument uiDoc = RevitFileUtil.OpenAndActivateDocumentFile(uiApp, revitFilePath, audit);
                doc = uiDoc.Document;
            }
            else
            {
                doc = RevitFileUtil.OpenDocumentFile(app, revitFilePath, audit);
            }

            try
            {
                result = documentAction(doc);
            }
            finally
            {
                SafeCloseWithoutSave(doc, openInUi, "Closed file: " + revitFilePath);
            }

            return result;
        }

        public static object RunDetachedDocumentAction(UIApplication uiApp, bool openInUi, string centralFilePath,
            bool discardWorksets,
            BatchRvt.WorksetConfigurationOption batchRvtWorksetConfigurationOption, bool auditOnOpening,
            Func<Document, object> documentAction)
        {
            object RevitAction()
            {
                object localResult = WithOpenedDetachedDocument(uiApp, openInUi, centralFilePath, discardWorksets, 
                    CreateWorksetConfiguration(batchRvtWorksetConfigurationOption), auditOnOpening, documentAction);
                return localResult;
            }

            object result = WithErrorReportingAndHandling(uiApp, RevitAction);
            return result;
        }

        public static object RunNewLocalDocumentAction(UIApplication uiApp, bool openInUi, string centralFilePath, string localFilePath,
            BatchRvt.WorksetConfigurationOption batchRvtWorksetConfigurationOption, bool auditOnOpening, Func<Document, object> documentAction)
        {
            object RevitAction()
            {
                object localResult = WithOpenedNewLocalDocument(uiApp, openInUi, centralFilePath, localFilePath, 
                    CreateWorksetConfiguration(batchRvtWorksetConfigurationOption), auditOnOpening, documentAction);
                return localResult;
            }

            var result = WithErrorReportingAndHandling(uiApp, RevitAction);
            return result;
        }

        public static object RunCloudDocumentAction(UIApplication uiApp, bool openInUi, string region,
            string cloudProjectId, string cloudModelId,
            BatchRvt.WorksetConfigurationOption batchRvtWorksetConfigurationOption, bool auditOnOpening, Func<Document,object> documentAction)
        {
            object RevitAction()
            {
                object localResult = WithOpenedCloudDocument(uiApp, openInUi, region, cloudProjectId, cloudModelId, CreateWorksetConfiguration(batchRvtWorksetConfigurationOption), auditOnOpening, documentAction);
                return localResult;
            }

            var result = WithErrorReportingAndHandling(uiApp, RevitAction);
            return result;
        }

        public static object RunDocumentAction(UIApplication uiApp, bool openInUi, string revitFilePath,
            bool auditOnOpening, Func<Document, object> documentAction)
        {
            object RevitAction()
            {
                var result = WithOpenedDocument(uiApp, openInUi, revitFilePath, auditOnOpening, documentAction);
                return result;
            }

            var result = WithErrorReportingAndHandling(uiApp, RevitAction);
            return result;
        }

        public static object WithErrorReportingAndHandling(UIApplication uiApp, Func<object> revitAction)
        {
            object Action() => WithDocumentOpeningErrorReporting(revitAction);
            object result = WithAutomatedErrorHandling(uiApp, Action);
            return result;
        }

        public static object WithDocumentOpeningErrorReporting(Func<object> documentOpeningAction)
        {
            object result = null;
            try
            {
                result = documentOpeningAction();
            }
            catch (OperationCanceledException e)
            {
                Logger.Error("ERROR: The operation was canceled: " + e.Message);
                throw;
            }
            catch (Autodesk.Revit.Exceptions.CorruptModelException ex)
            {
                Logger.Error("ERROR: Model is corrupt: " + ex.Message);
                throw;
            }

            return result;
        }

        public static object WithAutomatedErrorHandling(UIApplication uiApp, Func<object> revitAction)
        {
            object Func()
            {
                object Action()
                {
                    object result = RevitDialogUtil.WithDialogBoxShowingHandler(uiApp, revitAction);
                    return result;
                }

                object result = RevitFileCore.RevitFailure.RevitFailureHandling.WithFailuresProcessingHandler(uiApp.Application, Action);
                return result;
            }

            object result = WithExceptionLogging(Func);
            return result;
        }
    }
}