﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using RevitFileCore;

namespace RevitFileUpdaterCore
{
    /// <summary>
    /// Settings passed over to the CommandLine to Run
    /// </summary>
    public static class CommandSettings
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public const string SettingsFilePathOption = "settings_file";
        public const string LogFolderPathOption = "log_folder";
        public const string SessionIdOption = "session_id";
        public const string TaskDataOption = "task_data";
        public const string TestModeFolderPathOption = "test_mode_folder_path";
        public const string RevitFileListOption = "file_list";
        public const string RevitVersionOption = "revit_version";
        public const string TaskScriptFilePathOption = "task_script";
        public const string DetachOption = "detach";
        public const string CreateNewLocalOption = "create_new_local";
        public const string WorksetsOption = "worksets";
        public const string CloseAllWorksetsOptionValue = "close_all";
        public const string OpenAllWorksetsOptionValue = "open_all";
        public const string OpenLastViewedWorksetsOptionValue = "last_viewed";
        public const string AuditOnOpeningOption = "audit";
        public const string PerFileProcessingTimeoutOption = "per_file_timeout";
        public const string DatabaseOption = "database";
        public const string LibraryOption = "library";
        public const string LicenseOption = "license";
        public const string HelpOption = "help";
        public static readonly string[] AllValidOptions = {
                LibraryOption,    
                SettingsFilePathOption,
                LogFolderPathOption,
                SessionIdOption,
                TaskDataOption,
                TestModeFolderPathOption,
                RevitFileListOption,
                RevitVersionOption,
                TaskScriptFilePathOption,
                DetachOption,
                CreateNewLocalOption,
                WorksetsOption,
                AuditOnOpeningOption,
                PerFileProcessingTimeoutOption,
                DatabaseOption,
                LicenseOption,
                HelpOption
            };

        private static readonly Dictionary<string, Func<string, object>> OptionParsers =
            new()
            {
                { LibraryOption, ParseTextOptionValue},
                { SettingsFilePathOption, ParseExistingFilePathOptionValue },
                { LogFolderPathOption, ParseExistingFolderPathOptionValue },
                { SessionIdOption, ParseTextOptionValue },
                { TaskDataOption, ParseTextOptionValue },
                { TestModeFolderPathOption, ParseTextOptionValue },
                { RevitVersionOption, optionValue => ParseRevitVersionOptionValue(optionValue) },
                { RevitFileListOption, ParseExistingFilePathOptionValue },
                { TaskScriptFilePathOption, ParseExistingFilePathOptionValue },
                { DetachOption, null },
                { CreateNewLocalOption, null },
                { WorksetsOption, optionValue => ParseWorksetsOptionValue(optionValue) },
                { AuditOnOpeningOption, null },
                { PerFileProcessingTimeoutOption, optionValue => ParsePositiveIntegerOptionValue(optionValue) },
                { DatabaseOption, optionValue => ParseBooleanOptionValue(optionValue)},
                { LicenseOption, optionValue => ParseBooleanOptionValue(optionValue)},
                { HelpOption, optionValue => ParseBooleanOptionValue(optionValue) }
            };

        public static string ParseTextOptionValue(string textOptionValue)
        {
            string parsedValue = null;

            if (!string.IsNullOrWhiteSpace(textOptionValue))
            {
                parsedValue = textOptionValue.Trim();
            }

            return parsedValue;
        }

        public static int? ParsePositiveIntegerOptionValue(string integerOptionValue)
        {
            bool parsed = int.TryParse(integerOptionValue, out int parsedValue);

            return (parsed && parsedValue > 0) ? parsedValue : null;
        }

        public static string ParseExistingFilePathOptionValue(string filePathOptionValue)
        {
            string parsedValue = null;

            if (string.IsNullOrWhiteSpace(filePathOptionValue))
            {
                return null;
            }

            string fullFilePath = PathUtil.GetFullPath(filePathOptionValue);

            if (PathUtil.FileExists(fullFilePath))
            {
                parsedValue = fullFilePath;
            }

            return parsedValue;
        }

        public static string ParseExistingFolderPathOptionValue(string folderPathOptionValue)
        {
            string parsedValue = null;

            if (string.IsNullOrWhiteSpace(folderPathOptionValue))
                return null;

            string fullFolderPath = PathUtil.GetFullPath(folderPathOptionValue);

            if (PathUtil.DirectoryExists(fullFolderPath)) parsedValue = fullFolderPath;
            return parsedValue;
        }

        public static bool? ParseBooleanOptionValue(string booleanOptionValue)
        {
            bool? parsedValue = null;

            if (string.IsNullOrWhiteSpace(booleanOptionValue))
            {
                return null;
            }

            if (new[] { "TRUE", "YES" }.Any(s => string.Equals(s, booleanOptionValue, StringComparison.OrdinalIgnoreCase)))
            {
                parsedValue = true;
            }
            else if (new[] { "FALSE", "NO" }.Any(s => string.Equals(s, booleanOptionValue, StringComparison.OrdinalIgnoreCase)))
            {
                parsedValue = false;
            }

            return parsedValue;
        }

        public static int? ParseRevitVersionOptionValue(string revitVersionOptionValue)
        {
            int? revitVersion = null;

            if (string.IsNullOrWhiteSpace(revitVersionOptionValue))
            {
                return null;
            }

            if (RevitVersion.IsSupportedRevitVersionNumber(revitVersionOptionValue))
            {
                revitVersion = RevitVersion.GetSupportedRevitVersion(revitVersionOptionValue);
            }

            return revitVersion;
        }

        public static BatchRvt.WorksetConfigurationOption? ParseWorksetsOptionValue(string worksetsOptionValue)
        {
            BatchRvt.WorksetConfigurationOption? worksetsOption = null;

            string parsedTextOptionValue = ParseTextOptionValue(worksetsOptionValue);

            worksetsOption = parsedTextOptionValue switch
            {
                OpenAllWorksetsOptionValue => BatchRvt.WorksetConfigurationOption.OpenAllWorksets,
                CloseAllWorksetsOptionValue => BatchRvt.WorksetConfigurationOption.CloseAllWorksets,
                OpenLastViewedWorksetsOptionValue => BatchRvt.WorksetConfigurationOption.OpenLastViewed,
                _ => worksetsOption
            };

            return worksetsOption;
        }

        public static Dictionary<string, object> GetCommandLineOptions()
        {
            Logger.Debug("Starting Getting Command Line Options.");
            return OptionParsers
                .Select(
                    kv => new { Option = kv.Key, Parser = kv.Value }
                ).ToDictionary(
                    optionAndValue => optionAndValue.Option,
                    optionAndValue => (
                        (optionAndValue.Parser != null) ?
                        optionAndValue.Parser(CommandLineUtil.GetCommandLineOption(optionAndValue.Option, expectOptionValue: true)) :
                        CommandLineUtil.HasCommandLineOption(optionAndValue.Option, expectOptionValue: false)
                    )
                );
        }

        public static IEnumerable<string> GetInvalidOptions()
        {
            return CommandLineUtil.GetAllCommandLineOptionSwitches()
                .Where(optionSwitch => !AllValidOptions.Contains(optionSwitch))
                .ToList();
        }
        /// <summary>
        /// Stores the functional items necessary to complete a task. More Documentation necessary.
        /// </summary>
        public class Data
        {
            // Command line options
            public string   SettingsFilePath   { get; set; }
            public string   LogFolderPath      { get; set; }
            public string   LogFilePath        { get; set; }
            public string   SessionId          { get; set; }
            public DateTime SessionStartTime   { get; set; }
            public string   TaskData           { get; set; }
            public string   TestModeFolderPath { get; set; }
            
            public bool   Database { get; set; }

            // Programmatic options
            public RevitFileUpdaterUiSettings Settings { get; set; }
            public IEnumerable<string> RevitFileList { get; set; }

            // BatchRvt itself will set this to the path of the generated log file.
            public string GeneratedLogFilePath { get; set; }
            
            public Data()
            {
                SettingsFilePath = null;
                LogFolderPath = null;
                SessionId = null;
                TaskData = null;
                TestModeFolderPath = null;
                Settings = null;
                RevitFileList = null;
                GeneratedLogFilePath = null;
            }
        }
    }
}
