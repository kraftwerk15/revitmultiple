﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NLog;
using RevitFileUpdaterCore;
using File = System.IO.File;

namespace RevitFileCore
{
    public class BatchRevitSettingsConfiguration
    {

        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        public static  object   LOG_NAME = "RevitMultiple";
        // BatchRvt settings
        private string SettingsFilePath;
        private string LogFolderPath;
        private string LogFilePath;
        public Guid SessionId;
        public DateTime SessionStartTime;
        internal string TaskData;
        internal string TestModeFolderPath;
        internal string SessionDataFolderPath;
        internal bool ShowRevitProcessErrorMessages;

        internal bool ShowMessageBoxOnTaskError;
        
        
        // Revit File List settings
        private string RevitFileListFilePath;
        public List<string> RevitFileLists = new();
        private List<RevitFileList.RevitFilePathData> RevitFileListData;
        
        //Project Options
        // Central File Processing settings
        internal BatchRvt.CentralFileOpenOption CentralFileOpenOption = BatchRvt.CentralFileOpenOption.NotDefined;
        internal bool DeleteLocalAfter = false;
        internal bool DiscardWorksetsOnDetach = false;

        internal BatchRvt.WorksetConfigurationOption WorksetConfigurationOption = BatchRvt.WorksetConfigurationOption.NotDefined;
        internal BatchRvt.AccServer AccServer = BatchRvt.AccServer.NotDefined;
        
        //Family Options
        internal bool DeleteExisting = false;
        internal bool DeleteBackups = false;
        internal bool RenameRevitBackupFiles = false;
        internal bool IgnoreRevitBackupFiles = false;
        
        internal bool AuditOnOpening = false;
        
        // Single Revit Task Processing settings
        internal int SingleRevitTaskRevitVersion = -1;
        // Batch Revit File Processing settings
        internal BatchRvt.RevitFileProcessingOption RevitFileProcessingOption = BatchRvt.RevitFileProcessingOption.NotDefined;
        internal bool IfNotAvailableUseMinimumAvailableRevitVersion = false;
        
        // Revit Processing settings
        internal BatchRvt.RevitProcessingOption RevitProcessingOption = BatchRvt.RevitProcessingOption.NotDefined;

        internal int BatchRevitTaskRevitVersion = -1;
        
        internal bool OpenInUi = false;
        internal bool RequestDatabase;
        internal bool RequestLibrary;
        internal string LibraryPath;
        
        //Scripting
        // General Task Script settings
        internal string ScriptFilePath;
        // Pre-processing Script settings
        internal bool ExecutePreProcessingScript = false;
        internal string PreProcessingScriptFilePath;
        // Post-processing Script settings
        internal bool ExecutePostProcessingScript = false;
        internal string PostProcessingScriptFilePath;
        
        //RevitMultiple Settings
        internal int ProcessingTimeOutInMinutes;
        // Revit Session settings
        internal BatchRvt.RevitSessionOption RevitSessionOption = BatchRvt.RevitSessionOption.NotDefined;
        // Data Export settings
        internal bool EnableDataExport = false;
        internal string DataExportFolderPath;
        

         public List<RevitFileList.RevitFilePathData> ReadRevitFileListData()
         {
             List<RevitFileList.RevitFilePathData> revitFileListData = null;
             if (RevitProcessingOption != BatchRvt.RevitProcessingOption.BatchRevitFileProcessing)
             {
                 Logger.Warn("Revit Batch Processing was not enabled, but Read Revit File List was called.");
                 return null;
             }
                 
             if (RevitFileLists != null)
             {
                 Logger.Info("Reading Revit File list from object input.");
                 //BUG this cannot be correct
                 List<string> revitFilePath = RevitFileList.FromLines(RevitFileLists).Select(k => k.RevitFilePath).ToList();
                 RevitFileListData = RevitFileList.FromLines(RevitFileLists);
                 revitFileListData = RevitFileListData;
             }
             else
             {
                 Logger.Info("Reading Revit File list : {0}", !string.IsNullOrWhiteSpace(RevitFileListFilePath) 
                     ? RevitFileListFilePath : "Not specified!");
         
                 if (!File.Exists(RevitFileListFilePath))
                 {
                     Logger.Info("ERROR: No Revit file list specified or file not found.");
                 }
                 else if (ExcelUtil.HasExcelExtension(RevitFileListFilePath) && !ExcelUtil.IsExcelInstalled())
                 {
                     Logger.Info("ERROR: Could not read from the Excel Revit File list. An Excel installation was not detected!");
                 }
                 else
                 {
                     revitFileListData = RevitFileList.FromFile(RevitFileListFilePath);
                     List<string> revitFileList = revitFileListData != null ? (from revitFilePathData in revitFileListData
                         select revitFilePathData.RevitFilePath).ToList() : null;
                     RevitFileLists = revitFileList;
                     RevitFileListData = revitFileListData;
                 }
             }
             if (revitFileListData == null)
             {
                 Logger.Error("ERROR: Could not read the Revit File list.");
             }
             else if (revitFileListData.Count == 0)
             {
                 Logger.Error("ERROR: Revit File list is empty.");
                 revitFileListData = null;
             }
             return revitFileListData;
         }
            

        public static Tuple<string, DateTime> ParseSessionIdAndStartTime(string sessionId)
        {
            DateTime sessionStartTime;
            // NOTE: If the session ID looks too much like a serialized json date/time then the deserializer will
            //       deserialize it into a date/time object, which is undesirable in this case.
            //       Hence the addition of the surrounding angle brackets to thwart this behaviour.
            bool hasSessionId = sessionId != null && (sessionId.StartsWith("<") && sessionId.EndsWith(">"));
            if (hasSessionId)
            {
                sessionStartTime = TimeUtil.GetDateTimeUtcFromISO8601FormattedDate(sessionId.Substring(1, sessionId.Length-1));
            }
            else
            {
                sessionStartTime = TimeUtil.GetDateTimeUtcNow();
                sessionId = "<" + TimeUtil.GetISO8601FormattedUtcDate(sessionStartTime) + ">";
            }
            return Tuple.Create(sessionId, sessionStartTime);
        }

        private static string InitializeLogging(string logFolderPath, DateTime sessionStartTime)
        {
            Logger.Debug("Initializing Log Path");
            if (string.IsNullOrWhiteSpace(logFolderPath))
            {
                string location = BatchRvt.GetDataFolderPath();
                Logger.Debug("User did not provide Log Folder Location. Default location is used. {0}", location);
                logFolderPath = location;
            }
            else
            {
                Logger.Debug("User requested Log Folder Location at {0}", logFolderPath);
            }
            PathUtil.CreateDirectory(logFolderPath);
            string logName = LOG_NAME + "_" + SnapshotDataUtil.GetSnapshotFolderName(sessionStartTime.ToLocalTime());
            Logger.Debug("Log Name for this Session : {0}", logName);
            LogFile lg = LogFile.InitializeLogging(logName, logFolderPath);
            string logFilePath = lg.GetLogFilePath();
            Logger.Debug("Full Log Path : {0}", logFilePath);
            return logFilePath;
        }

        private static bool ConfigureBatchRvtSettings(BatchRevitSettingsConfiguration commandData, RevitFileUpdaterUiSettings revitFileUpdaterUiSettings)
        {
            bool             aborted        = false;
            BatchRevitSettingsConfiguration batchRvtConfig = commandData;
            // General Task Script settings
            string mainTaskScript = revitFileUpdaterUiSettings.TaskScriptMainFilePath.GetValue();
            string singleTaskScript = revitFileUpdaterUiSettings.TaskScriptSingleFilePath.GetValue();
            if (!mainTaskScript.Equals(string.Empty) && !singleTaskScript.Equals(string.Empty))
            {
                Logger.Error("Both Main Task Script and Single Task Script were provided values. Implementing the Main Task Script only.");
            }
            if (!mainTaskScript.Equals(string.Empty))
            {
                batchRvtConfig.ScriptFilePath = mainTaskScript;
            }
            else if (!singleTaskScript.Equals(string.Empty))
            {
                batchRvtConfig.ScriptFilePath = singleTaskScript;
            }
            else
            {
                batchRvtConfig.ScriptFilePath = string.Empty;
            }
            
            batchRvtConfig.ShowMessageBoxOnTaskError = revitFileUpdaterUiSettings.ShowMessageBoxOnTaskScriptError.GetValue();
            batchRvtConfig.ProcessingTimeOutInMinutes = revitFileUpdaterUiSettings.ProcessingTimeOutInMinutes.GetValue();
            batchRvtConfig.ShowRevitProcessErrorMessages = revitFileUpdaterUiSettings.ShowRevitProcessErrorMessages.GetValue();
            // Revit File List settings
            batchRvtConfig.RevitFileListFilePath = revitFileUpdaterUiSettings.RevitFileListFilePath.GetValue();
            // Data Export settings
            batchRvtConfig.EnableDataExport = revitFileUpdaterUiSettings.EnableDataExport.GetValue();
            batchRvtConfig.DataExportFolderPath = revitFileUpdaterUiSettings.DataExportFolderPath.GetValue();
            // Pre-processing Script settings
            batchRvtConfig.ExecutePreProcessingScript = revitFileUpdaterUiSettings.ExecutePreProcessingScript.GetValue();
            batchRvtConfig.PreProcessingScriptFilePath = revitFileUpdaterUiSettings.PreProcessingScriptFilePath.GetValue();
            // Post-processing Script settings
            batchRvtConfig.ExecutePostProcessingScript = revitFileUpdaterUiSettings.ExecutePostProcessingScript.GetValue();
            batchRvtConfig.PostProcessingScriptFilePath = revitFileUpdaterUiSettings.PostProcessingScriptFilePath.GetValue();
            // Central File Processing settings
            batchRvtConfig.CentralFileOpenOption = revitFileUpdaterUiSettings.CentralFileOpenOption.GetValue();
            batchRvtConfig.DeleteLocalAfter = revitFileUpdaterUiSettings.DeleteLocalAfter.GetValue();
            batchRvtConfig.DiscardWorksetsOnDetach = revitFileUpdaterUiSettings.DiscardWorksetsOnDetach.GetValue();
            batchRvtConfig.WorksetConfigurationOption = revitFileUpdaterUiSettings.WorksetConfigurationOption.GetValue();
            // Revit Session settings
            batchRvtConfig.RevitSessionOption = revitFileUpdaterUiSettings.RevitSessionOption.GetValue();
            // Revit Processing settings
            batchRvtConfig.RevitProcessingOption = revitFileUpdaterUiSettings.RevitProcessingOption.GetValue();
            // Single Revit Task Processing settings
            batchRvtConfig.SingleRevitTaskRevitVersion = revitFileUpdaterUiSettings.SingleRevitTaskRevitVersion.GetValue();
            // Batch Revit File Processing settings
            batchRvtConfig.RevitFileProcessingOption = revitFileUpdaterUiSettings.RevitFileProcessingOption.GetValue();
            //batchRvtConfig.IfNotAvailableUseMinimumAvailableRevitVersion = revitFileUpdaterUiSettings.IfNotAvailableUseMinimumAvailableRevitVersion.GetValue();
            batchRvtConfig.BatchRevitTaskRevitVersion = revitFileUpdaterUiSettings.BatchRevitTaskRevitVersion.GetValue();
            batchRvtConfig.AuditOnOpening = revitFileUpdaterUiSettings.AuditOnOpening.GetValue();
            batchRvtConfig.OpenInUi = revitFileUpdaterUiSettings.OpenInUI.GetValue();
            //TODO no script or dynamo path loaded such as --library or --database
            if (!File.Exists(batchRvtConfig.ScriptFilePath))
            {
                Logger.Error("ERROR: No script file specified or script file not found.");
                aborted = true;
            }
            else
            {
                Logger.Info("Task Script:" + batchRvtConfig.ScriptFilePath);
                bool isDynamoTaskScript = PathUtil.HasFileExtension(batchRvtConfig.ScriptFilePath, @".dyn");
                if (isDynamoTaskScript)
                {
                    batchRvtConfig.OpenInUi = true;
                }
                if (isDynamoTaskScript)
                {
                    // Always use a separate Revit session for each Revit file when executing a Dynamo task script.
                    // This restriction is due a limitation on closing documents that are active in the UI (which Dynamo requires).
                    batchRvtConfig.RevitSessionOption = BatchRvt.RevitSessionOption.UseSeparateSessionPerFile;
                }
                if (batchRvtConfig.ShowMessageBoxOnTaskError)
                {
                    Logger.Info("Show Message Box on Task Script Error is enabled.");
                }
                if (batchRvtConfig.ProcessingTimeOutInMinutes > 0)
                {
                    string pass = batchRvtConfig.ProcessingTimeOutInMinutes == 1 ? "minute" : "minutes";
                    Logger.Info("Revit Task / File processing time-out setting: {0}", 
                        batchRvtConfig.ProcessingTimeOutInMinutes + " " + pass);
                }
                string revitProcessingModeDescription = batchRvtConfig.RevitProcessingOption == 
                                                        BatchRvt.RevitProcessingOption.BatchRevitFileProcessing ? 
                    "Batch Revit File processing" : "Single Revit Task processing";
                
                Logger.Info("Revit Processing mode : {0}", revitProcessingModeDescription);
                if (batchRvtConfig.EnableDataExport)
                {
                    if (string.IsNullOrWhiteSpace(batchRvtConfig.DataExportFolderPath))
                    {
                        Logger.Info("ERROR: No data export folder specified.");
                        aborted = true;
                    }
                    else
                    {
                        Logger.Info("Data Export is enabled.");
                        Logger.Info("Data Export Folder : {0}", batchRvtConfig.DataExportFolderPath);
                        batchRvtConfig.SessionDataFolderPath = Path.Combine(batchRvtConfig.DataExportFolderPath,
                            SnapshotDataUtil.GetSnapshotFolderName(commandData.SessionStartTime.ToLocalTime())); 

                        
                        Logger.Info("Session Folder : {0}", batchRvtConfig.SessionDataFolderPath);
                    }
                }
                if (batchRvtConfig.ExecutePreProcessingScript)
                {
                    if (!File.Exists(batchRvtConfig.PreProcessingScriptFilePath))
                    {
                        Logger.Info("ERROR: Pre-processing script file does not exist.");
                        aborted = true;
                    }
                    else
                    {
                        Logger.Info("Pre-Processing Script : {0}", batchRvtConfig.PreProcessingScriptFilePath);
                    }
                }
                if (batchRvtConfig.ExecutePostProcessingScript)
                {
                    if (!File.Exists(batchRvtConfig.PostProcessingScriptFilePath))
                    {
                        Logger.Info("ERROR: Post-processing script file does not exist.");
                        aborted = true;
                    }
                    else
                    {
                        Logger.Info("Post-Processing Script : {0}", batchRvtConfig.PostProcessingScriptFilePath);
                    }
                }
                if (batchRvtConfig.RevitProcessingOption == BatchRvt.RevitProcessingOption.BatchRevitFileProcessing)
                {
                    string centralFileProcessingDescription = batchRvtConfig.CentralFileOpenOption 
                                                              == BatchRvt.CentralFileOpenOption.CreateNewLocal ? "Create New Local" : "Detach from Central";
                    
                    Logger.Info("Central File Processing mode : {0}", centralFileProcessingDescription);
                    // ReSharper disable once IdentifierTypo
                    bool usingWorksetConfigurationOption = false;
                    if (batchRvtConfig.CentralFileOpenOption == BatchRvt.CentralFileOpenOption.CreateNewLocal)
                    {
                        usingWorksetConfigurationOption = true;
                        if (batchRvtConfig.DeleteLocalAfter)
                        {
                            Logger.Info("Local File will be deleted after processing.");
                        }
                    }
                    else if (batchRvtConfig.CentralFileOpenOption == BatchRvt.CentralFileOpenOption.Detach)
                    {
                        if (batchRvtConfig.DiscardWorksetsOnDetach)
                        {
                            Logger.Info("Worksets will be discarded upon detach.");
                        }
                        else
                        {
                            usingWorksetConfigurationOption = true;
                        }
                    }
                    if (usingWorksetConfigurationOption)
                    {
                        // ReSharper disable once IdentifierTypo
                        string worksetConfigurationOptionDescription = 
                            batchRvtConfig.WorksetConfigurationOption == BatchRvt.WorksetConfigurationOption.CloseAllWorksets ?
                                "Close All" : batchRvtConfig.WorksetConfigurationOption == BatchRvt.WorksetConfigurationOption.OpenAllWorksets ?
                                    "Open All" : "Open Last Viewed";
                        
                        Logger.Info("Worksets Configuration : " + worksetConfigurationOptionDescription);
                    }
                    if (batchRvtConfig.AuditOnOpening)
                    {
                        Logger.Info("Revit files will be audited on opening.");
                    }
                }
            }
            return aborted;
        }

        private static RevitFileUpdaterUiSettings GetBatchRvtSettings(string settingsFilePath)
        {
            if (!File.Exists(settingsFilePath))
            {
                Logger.Error("ERROR: No settings file specified or settings file not found.");
            }
            else
            {
                RevitFileUpdaterUiSettings revitFileUpdaterUiSettings = new();
                bool isSettingsLoaded = revitFileUpdaterUiSettings.LoadFromFile(settingsFilePath);
                if (isSettingsLoaded) return revitFileUpdaterUiSettings;
                Logger.Error("ERROR: Could not load settings from the settings file!");
            }
            return null;
        }

        private static object GetCommandSettingsOption(
            IReadOnlyDictionary<string, object> commandLineOptions,
            string        commandSettingsOption,
            string        invalidValueErrorMessage = null,
            string        missingValueErrorMessage = null)
        {
            // NOTE: option==None doesn't necessarily mean aborted==True. Specifically, if an optional option is not specified, option==None but aborted==False.
            object option = null;
            if (CommandLineUtil.HasCommandLineOption(commandSettingsOption))
            {
                object optionValue = commandLineOptions[commandSettingsOption];
                if (optionValue == null)
                {
                    if (invalidValueErrorMessage != null)
                    {
                        Logger.Warn(invalidValueErrorMessage);
                    }
                    else
                    {
                        Logger.Warn("Warning: Invalid " + CommandLineUtil.OptionSwitchPrefix + commandSettingsOption + " option value!");
                    }
                }
                else
                {
                    option = optionValue;
                }
            }
            else if (CommandLineUtil.HasCommandLineOption(commandSettingsOption, false))
            {
                if (missingValueErrorMessage != null)
                {
                    Logger.Warn(missingValueErrorMessage);
                }
                else
                {
                    Logger.Warn("Warning: Missing " + CommandLineUtil.OptionSwitchPrefix + commandSettingsOption + " option value!");
                    
                }
            }
            return option;
        }
        
        

        private static RevitFileUpdaterUiSettings InitializeBatchRevitUiSettings(CommandSettings.Data commandSettingsData, 
            BatchRevitSettingsConfiguration batchRvtConfig, bool databaseOption, 
            object revitFileListOption = null, object taskScriptFilePathOption = null)
        {
            RevitFileUpdaterUiSettings revitFileUpdaterUiSettings = null;
            if (commandSettingsData is {Settings: { }})
            {
                // Initialize from in-memory object.
                revitFileUpdaterUiSettings = commandSettingsData.Settings;
            }
            else if (batchRvtConfig.SettingsFilePath != string.Empty)
            {
                // Initialize from settings file specified at the command-line.
                revitFileUpdaterUiSettings = GetBatchRvtSettings(batchRvtConfig.SettingsFilePath);
            }
            else if (revitFileListOption != null && taskScriptFilePathOption != null)
            {
                // Initialize settings for non-settings-file mode.
                revitFileUpdaterUiSettings = new RevitFileUpdaterUiSettings();
                revitFileUpdaterUiSettings.RevitProcessingOption.SetValue(BatchRvt.RevitProcessingOption.BatchRevitFileProcessing);
                revitFileUpdaterUiSettings.RevitSessionOption.SetValue(BatchRvt.RevitSessionOption.UseSameSessionForFilesOfSameVersion);
                revitFileUpdaterUiSettings.RevitFileProcessingOption.SetValue(BatchRvt.RevitFileProcessingOption.UseFileRevitVersionIfAvailable);
                revitFileUpdaterUiSettings.IfNotAvailableUseMinimumAvailableRevitVersion.SetValue(false);
                revitFileUpdaterUiSettings.AuditOnOpening.SetValue(false);
                revitFileUpdaterUiSettings.ProcessingTimeOutInMinutes.SetValue(0);
            }
            else if (databaseOption)
            {
                
            }
            else
            {
                Logger.Error("ERROR: No settings file specified or settings file not found.");
            }
            return revitFileUpdaterUiSettings;
        }

        /// <summary>
        /// Attempts to get a value from CommandLineOptions from the Program.
        /// </summary>
        /// <param name="commandLineOptions">Dictionary of the CommandLineOptions</param>
        /// <param name="key">The Key the method should attempt to find.</param>
        /// <returns>The object if found. Null if not.</returns>
        private static object GetValueFromDictionary(IReadOnlyDictionary<string, object> commandLineOptions, string key)
        {
            object placeholder = null;
            Logger.Debug("Getting Value From CommandLineOptions : {0}", key);
            if (!commandLineOptions.TryGetValue(key, out object vacant))
            {
                Logger.Debug("CommandLineOption was not found. Returning null.");
                return null;
            }
            if(vacant is not null)
            {
                Logger.Debug("CommandLineOption was found : {0}", vacant);
                placeholder = commandLineOptions[key].ToString();
            }
            Logger.Debug("Returning value : {0}", placeholder);
            return placeholder;
        }
        
        /// <summary>
        /// Extracts Data provided from CommandSettings, if CommandSettings is provided by the user.
        /// </summary>
        /// <param name="commandLineOptions"></param>
        /// <param name="commandSettingsData"></param>
        /// <returns>Unknown at this point</returns>
        private static BatchRevitSettingsConfiguration InitializeBatchRvtConfig(Dictionary<string, object> commandLineOptions, CommandSettings.Data commandSettingsData = null)
        {
            BatchRevitSettingsConfiguration batchRvtConfig = new();
            Logger.Debug("CommandSettingsData is null : {0}", commandSettingsData is null);
            //CommandSettings.Data batchRvtConfig = new();
            //Check from Command Settings Data
            if (commandSettingsData is not null)
            {
                if (commandSettingsData.SettingsFilePath != null)
                {
                    Logger.Debug("CommandSettingsData.SettingsFilePath Found : {0}", commandSettingsData.SettingsFilePath);
                    batchRvtConfig.SettingsFilePath = commandSettingsData.SettingsFilePath;
                    Logger.Debug("SettingsFilePath Set to Batch Revit Config Settings Path. Settings Path is now : {0}", batchRvtConfig.SettingsFilePath);
                }
                if (commandSettingsData.LogFolderPath != null)
                {
                    Logger.Debug("CommandSettingsData.LogFolderPath Found : {0}", commandSettingsData.LogFolderPath);
                    batchRvtConfig.LogFolderPath = commandSettingsData.LogFolderPath;
                    Logger.Debug("LogFolderPath Set to Batch Revit Config LogFolderPath. LogFolderPath is now : {0}", batchRvtConfig.LogFolderPath);
                }
                if (commandSettingsData.SessionId != null)
                {
                    Logger.Debug("CommandSettingsData.SessionId Found : {0}", commandSettingsData.SessionId);
                    batchRvtConfig.SessionId = new Guid(commandSettingsData.SessionId);
                    Logger.Debug("SessionId Set to Batch Revit Config SessionId. SessionId is now : {0}", batchRvtConfig.SessionId);
                }
                if (commandSettingsData.TaskData != null)
                {
                    Logger.Debug("CommandSettingsData.TaskData Found : {0}", commandSettingsData.TaskData);
                    batchRvtConfig.TaskData = commandSettingsData.TaskData;
                    Logger.Debug("TaskData Set to Batch Revit Config TaskData. TaskData is now : {0}", batchRvtConfig.TaskData);
                }
                if (commandSettingsData.TestModeFolderPath != null)
                {
                    Logger.Debug("CommandSettingsData.TestModeFolderPath Found : {0}", commandSettingsData.TestModeFolderPath);
                    batchRvtConfig.TestModeFolderPath = commandSettingsData.TestModeFolderPath;
                    Logger.Debug("TestModeFolderPath Set to Batch Revit Config TestModeFolderPath. TestModeFolderPath is now : {0}", batchRvtConfig.TestModeFolderPath);
                }
                
                if (commandSettingsData.RevitFileList != null)
                {
                    // NOTE: list is constructed here because although the source object is an IEnumerable<string> it may not be a list.
                    batchRvtConfig.RevitFileLists = (from revitFilePath in commandSettingsData.RevitFileList
                        select revitFilePath).ToList();
                }
            }
            
            //Check from Command Line Overrides
            object settingsFilePathFromDictionary = GetValueFromDictionary(commandLineOptions, CommandSettings.SettingsFilePathOption);
            if (settingsFilePathFromDictionary is not null)
            {
                batchRvtConfig.SettingsFilePath = settingsFilePathFromDictionary.ToString();
                Logger.Debug("SettingsFilePath Set to Batch Revit Config SettingsFilePath. SettingsFilePath is now : {0}", batchRvtConfig.SettingsFilePath);
            }
            
            object logFolderPathFromDictionary = GetValueFromDictionary(commandLineOptions, CommandSettings.LogFolderPathOption);
            if (logFolderPathFromDictionary is not null)
            {
                batchRvtConfig.LogFolderPath = logFolderPathFromDictionary.ToString();
                Logger.Debug("LogFolderPath Set to Batch Revit Config LogFolderPath. LogFolderPath is now : {0}", batchRvtConfig.LogFolderPath);
            }
            
            object sessionIdFromDictionary = GetValueFromDictionary(commandLineOptions, CommandSettings.SessionIdOption);
            if (sessionIdFromDictionary is not null)
            {
                batchRvtConfig.SessionId = new Guid(sessionIdFromDictionary.ToString()!);
                Logger.Debug("SessionId Set to Batch Revit Config SessionId. SessionId is now : {0}", batchRvtConfig.SessionId);
            }
            
            object taskDataFromDictionary = GetValueFromDictionary(commandLineOptions, CommandSettings.TaskDataOption);
            if (taskDataFromDictionary is not null)
            {
                batchRvtConfig.TaskData = taskDataFromDictionary.ToString();
                Logger.Debug("TaskData Set to Batch Revit Config TaskData. TaskData is now : {0}", batchRvtConfig.TaskData);
            }

            object testModeFolderPathFromDictionary =
                GetValueFromDictionary(commandLineOptions, CommandSettings.TestModeFolderPathOption);
            if (testModeFolderPathFromDictionary is not null)
            {
                batchRvtConfig.TestModeFolderPath = testModeFolderPathFromDictionary.ToString();
                Logger.Debug("TestModeFolderPath Set to Batch Revit Config TestModeFolderPath. TestModeFolderPath is now : {0}", batchRvtConfig.TestModeFolderPath);
            }
            
            //Final Check
            if (batchRvtConfig.SettingsFilePath is null)
            {
                Logger.Debug("BatchRevitSettingsConfiguration.SettingsFilePath was Not Found. SettingsFilePath will be skipped.");
                batchRvtConfig.SettingsFilePath ??= string.Empty;
            }

            if (batchRvtConfig.LogFolderPath is null)
            {
                Logger.Debug("BatchRevitSettingsConfiguration.LogFolderPath was Not Found. LogFolderPath will be skipped.");
                batchRvtConfig.LogFolderPath ??= string.Empty;
            }

            if (batchRvtConfig.SessionId == Guid.Empty)
            {
                Logger.Debug("BatchRevitSettingsConfiguration.SessionId was Not Found. SessionId will be set to a new value.");
                batchRvtConfig.SessionId = Guid.NewGuid();
            }
            
            batchRvtConfig.SessionStartTime = DateTime.Now;

            if (batchRvtConfig.TaskData is null)
            {
                Logger.Debug("BatchRevitSettingsConfiguration.TaskData was Not Found. TaskData will be skipped.");
                batchRvtConfig.TaskData ??= string.Empty;
            }
            
            // NOTE: use of output function must occur after the log file initialization
            //TODO user probably does not have access to write data here, check on an AppData Roaming location
            batchRvtConfig.LogFilePath = InitializeLogging(batchRvtConfig.LogFolderPath, batchRvtConfig.SessionStartTime);
            
            string testModeFolderPath = string.Empty;
            batchRvtConfig.TestModeFolderPath = !string.IsNullOrWhiteSpace(testModeFolderPath) ? PathUtil.GetFullPath(testModeFolderPath) : null;
            
            return batchRvtConfig;
        }


        /// <summary>
        /// Handles Command-Line Overrides from the User
        /// </summary>
        /// <param name="commandSettingsData"></param>
        /// <returns></returns>
        public static BatchRevitSettingsConfiguration ConfigureBatchRvt(CommandSettings.Data commandSettingsData = null)
        {
            Logger.Info("Starting Configure Batch Revit.");
            Dictionary<string, object>  commandLineOptions = CommandSettings.GetCommandLineOptions();
            BatchRevitSettingsConfiguration                      batchRvtConfig     = InitializeBatchRvtConfig(commandLineOptions, commandSettingsData);
            GlobalTestMode.InitializeGlobalTestMode(batchRvtConfig.TestModeFolderPath);
            //Test GUID against ToString
            GlobalTestMode.ExportSessionId(batchRvtConfig.SessionId.ToString());
            
            Logger.Info("Session ID : " + batchRvtConfig.SessionId);
            Logger.Info("Log File : " + batchRvtConfig.LogFilePath);

            commandSettingsData ??= new CommandSettings.Data();

            //Check if CommandSettings contains InvalidOptions
            IEnumerable<string> invalidOptions = CommandSettings.GetInvalidOptions();
            IEnumerable<string> enumerable = invalidOptions.ToList();
            if (enumerable.Any())
            {
                RevitFileUpdaterCore.CommandLine.Settings.ShowInvalidOptionsError(enumerable);
                return null;
            }
            object revitFileListOption = GetCommandSettingsOption(commandLineOptions, CommandSettings.RevitFileListOption,
  "ERROR: Revit file list not found.", "ERROR: Missing Revit file list option value!");
            
            object taskScriptFilePathOption = GetCommandSettingsOption(commandLineOptions, CommandSettings.TaskScriptFilePathOption, 
"ERROR: Task script file not found.", "ERROR: Missing Task script file option value!");
            
            BatchRvt.CentralFileOpenOption centralFileOpenOption = BatchRvt.CentralFileOpenOption.NotDefined;
            
            bool haveDetachOption = CommandLineUtil.HasCommandLineOption(CommandSettings.DetachOption, false);
            bool haveCreateNewLocalOption = CommandLineUtil.HasCommandLineOption(CommandSettings.CreateNewLocalOption, false);
            if (haveDetachOption && haveCreateNewLocalOption)
            {
                Logger.Error("ERROR: You cannot specify both " + CommandLineUtil.OptionSwitchPrefix + 
                             CommandSettings.DetachOption + " and " + CommandLineUtil.OptionSwitchPrefix + 
                             CommandSettings.CreateNewLocalOption + " options simultaneously.");
                return null;
            }
            if (haveDetachOption || haveCreateNewLocalOption)
            {
                centralFileOpenOption = haveCreateNewLocalOption ? BatchRvt.CentralFileOpenOption.CreateNewLocal : BatchRvt.CentralFileOpenOption.Detach;
                Logger.Info("CentralFileOption : {0}", centralFileOpenOption);
            }
            object worksetsOptionvar = GetCommandSettingsOption(commandLineOptions, CommandSettings.WorksetsOption);
            
            BatchRvt.WorksetConfigurationOption worksetsOption = BatchRvt.WorksetConfigurationOption.NotDefined;
            
            bool closeAllWorksets = CommandLineUtil.HasCommandLineOption(CommandSettings.CloseAllWorksetsOptionValue, false);
            bool openAllWorksets = CommandLineUtil.HasCommandLineOption(CommandSettings.OpenAllWorksetsOptionValue, false);
            bool openLastViewedOption = CommandLineUtil.HasCommandLineOption(CommandSettings.OpenLastViewedWorksetsOptionValue, false);
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (closeAllWorksets && openAllWorksets && openLastViewedOption)
            {
                Logger.Error("ERROR: You cannot specify "    + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.CloseAllWorksetsOptionValue + " and " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.OpenAllWorksetsOptionValue + " and " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.OpenLastViewedWorksetsOptionValue + " options simultaneously.");
                return null;
            }
            if (closeAllWorksets && openAllWorksets)
            {
                Logger.Error("ERROR: You cannot specify " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.CloseAllWorksetsOptionValue + " and " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.OpenAllWorksetsOptionValue + " options simultaneously.");
                return null;
            }
            if (openAllWorksets && openLastViewedOption)
            {
                Logger.Error("ERROR: You cannot specify " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.OpenLastViewedWorksetsOptionValue + " and " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.OpenAllWorksetsOptionValue + " options simultaneously.");
                return null;
            }
            if (closeAllWorksets && openLastViewedOption)
            {
                Logger.Error("ERROR: You cannot specify both " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.CloseAllWorksetsOptionValue + " and " + CommandLineUtil.OptionSwitchPrefix +
                             CommandSettings.OpenLastViewedWorksetsOptionValue + " options simultaneously.");
                return null;
            }
            if (closeAllWorksets || openAllWorksets || openLastViewedOption)
            {
                if (closeAllWorksets) worksetsOption     = BatchRvt.WorksetConfigurationOption.CloseAllWorksets;
                if (openAllWorksets) worksetsOption      = BatchRvt.WorksetConfigurationOption.OpenAllWorksets;
                if (openLastViewedOption) worksetsOption = BatchRvt.WorksetConfigurationOption.OpenLastViewed;
            }
            if(worksetsOption is not BatchRvt.WorksetConfigurationOption.NotDefined) Logger.Info("Worksets Options : {0}", worksetsOption);

            object auditOnOpeningOption = null;
            
            bool haveAuditOnOpeningOption = CommandLineUtil.HasCommandLineOption(CommandSettings.AuditOnOpeningOption, false);
            if (haveAuditOnOpeningOption)
            {
                auditOnOpeningOption = haveAuditOnOpeningOption;
                Logger.Info("Open with Audit : {0}", auditOnOpeningOption);
            }
            
            object perFileProcessingTimeoutOption = GetCommandSettingsOption(commandLineOptions, CommandSettings.PerFileProcessingTimeoutOption);
            //TODO this needs to be tested with a --database flag
            object databaseOption = GetCommandSettingsOption(commandLineOptions, CommandSettings.DatabaseOption);
            
            object libraryOption = GetCommandSettingsOption(commandLineOptions, CommandSettings.LibraryOption);
            object revitVersionOption = GetCommandSettingsOption(commandLineOptions, CommandSettings.RevitVersionOption);
            int revitVersion = -1;
            if (revitVersionOption != null)
            {
                revitVersion = Convert.ToInt32(revitVersionOption);
                Logger.Info("Using specific Revit version : " + RevitVersion.GetRevitVersionText(Convert.ToInt32(revitVersionOption)));
                if(libraryOption != null) Logger.Info("Library Option set to true. Specific Revit Version will be used as the baseline.");
                if(databaseOption != null) Logger.Info("Database Option set to true. Specific Revit Version will be used as the baseline.");
            }

            bool aborted = false;
            
            if (!RevitVersion.GetInstalledRevitAddinVersions().Any())
            {
                
                Logger.Error("ERROR: Could not detect the Revit File Updater addin for any version of Revit installed on this machine!" +
                             "You must first install the Revit File Updater addin for at least one version of Revit.");
                return null;
            }
            //if (!aborted)
            //{
            //    RevitFileUpdaterUiSettings batchRvtSettings = 
            //        InitializeBatchRevitUiSettings(commandSettingsData, batchRvtConfig, 
            //            revitFileListOption, taskScriptFilePathOption);
            //    if (batchRvtSettings == null)
            //    {
            //        aborted = true;
            //    }
            //}

            batchRvtConfig.RequestDatabase = databaseOption != null;
            batchRvtConfig.LibraryPath = libraryOption + revitVersion.ToString();
            batchRvtConfig.RequestLibrary = libraryOption != null;
            batchRvtConfig.BatchRevitTaskRevitVersion = revitVersion;
            commandSettingsData.Settings ??=
                InitializeBatchRevitUiSettings(commandSettingsData, batchRvtConfig, batchRvtConfig.RequestDatabase);
            // Handles command-line overrides
            // The User has created some command line overrides and also provided a .json of GUI settings.
            // We will set the Command Line Overrides on top of the UI settings
            // And then we will configure our final BatchRevitSettingsConfiguration
            if (commandSettingsData.Settings is null)
            {
                return batchRvtConfig;
            }
            if (revitVersionOption != null)
            {
                commandSettingsData.Settings.RevitFileProcessingOption.SetValue(BatchRvt.RevitFileProcessingOption.UseSpecificRevitVersion);
                commandSettingsData.Settings.BatchRevitTaskRevitVersion.SetValue(revitVersion);
            }
            if (revitFileListOption != null)
            {
                commandSettingsData.Settings.RevitFileListFilePath.SetValue(revitFileListOption.ToString());
            }
            if (taskScriptFilePathOption != null)
            {
                commandSettingsData.Settings.TaskScriptMainFilePath.SetValue(taskScriptFilePathOption.ToString());
            }
            if (centralFileOpenOption != BatchRvt.CentralFileOpenOption.NotDefined)
            {
                commandSettingsData.Settings.CentralFileOpenOption.SetValue(centralFileOpenOption);
            }
            if (worksetsOptionvar != null)
            {
                commandSettingsData.Settings.WorksetConfigurationOption.SetValue(worksetsOption);
            }
            if (auditOnOpeningOption != null)
            {
                if(auditOnOpeningOption.Equals("true"))
                    commandSettingsData.Settings.AuditOnOpening.SetValue(true);
                else if(auditOnOpeningOption.Equals("false"))
                    commandSettingsData.Settings.AuditOnOpening.SetValue(false);
            }
            if (perFileProcessingTimeoutOption != null)
            {
                int option = Convert.ToInt32(perFileProcessingTimeoutOption);
                commandSettingsData.Settings.ProcessingTimeOutInMinutes.SetValue(option);
                Logger.Info("Per File Processing Timeout Option (in minutes) : {0}", option);
            }
            aborted = ConfigureBatchRvtSettings(batchRvtConfig, commandSettingsData.Settings);
            return !aborted ? batchRvtConfig : null;
        }
    }
}