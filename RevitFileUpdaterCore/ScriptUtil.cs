﻿using System.Collections.Generic;
using System.IO;
using RevitFileCore;

namespace RevitFileUpdaterCore
{
    public class ScriptUtil
    {
        public string SessionId { get; set; }
        public string TaskData { get; set; }
        public string ExportFolderPath { get; set; }
        public string SessionDataFolderPath { get; set; }
        public List<RevitFileList.RevitFilePathData> RevitFileListFilePath { get; set; }
        public ScriptUtil(BatchRevitSettingsConfiguration batchRevitSettingsConfiguration)
        {
            SessionId = batchRevitSettingsConfiguration.SessionId.ToString();
            TaskData = batchRevitSettingsConfiguration.TaskData;
            ExportFolderPath = batchRevitSettingsConfiguration.DataExportFolderPath;
            SessionDataFolderPath = batchRevitSettingsConfiguration.SessionDataFolderPath;
            RevitFileListFilePath = batchRevitSettingsConfiguration.ReadRevitFileListData();
        }

        public static void ExecuteScript(string value)
        {
            //PathUtil.AddSearchPath(Path.GetDirectoryName(value));
            //TODO!!!! something here!!!!
        }
    }
}