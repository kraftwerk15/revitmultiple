﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RevitFileCore
{
    public class LogFile
    {
        private static string SessionId { get; set;}

        private const string DateFormat = "dd/MM/yyyy";
        private const string TimeFormat = "HH:mm:ss";

        private readonly string logFolderPath_;
        private readonly string logFileName_;
        private readonly string logFilePath_;
        private readonly string logName_;

        private StreamWriter appendTextStreamWriter_ = null;

        public LogFile(string logName, string logFolderPath, bool includeUsernamePrefix = true)
        {
            logFolderPath_ = logFolderPath;
            logName_ = logName;

            string logFilenamePrefix = includeUsernamePrefix ? Environment.UserName : string.Empty;
            string separator = (logFilenamePrefix != string.Empty) ? "_" : string.Empty;

            logFileName_ = logFilenamePrefix + separator + logName + ".log";

            logFilePath_ = Path.Combine(logFolderPath_, logFileName_);
        }

        public void Open()
        {
            Close();

            try
            {
                appendTextStreamWriter_ = new FileInfo(logFilePath_).AppendText();
            }
            catch (Exception)
            {
                appendTextStreamWriter_ = null;
            }
        }

        public static string GetSerializedLogEntry(DateTime dateTime, string sessionId, object message)
        {
            return SerializeAsJson(
                    GetLogObject(dateTime, sessionId, message)
                );
        }

        public string GetLogFilePath()
        {
            return logFilePath_;
        }

        private static object GetLogObject(DateTime dateTime, string sessionId, object message)
        {
            DateTime utcDateTime = dateTime.ToUniversalTime();

            var logEntry = new
            {
                date = new
                {
                    local = dateTime.ToString(DateFormat),
                    utc = utcDateTime.ToString(DateFormat)
                },
                time = new
                {
                    local = dateTime.ToString(TimeFormat),
                    utc = utcDateTime.ToString(TimeFormat)
                },
                sessionId = sessionId,
                message = message
            };

            return logEntry;
        }

        private static string SerializeAsJson(object logObject)
        {
            return JObject.FromObject(logObject).ToString(Formatting.None);
        }

        public bool WriteMessage(string sessionId, object message)
        {
            bool success = false;

            bool useExistingOpenStream = (appendTextStreamWriter_ != null);

            try
            {
                DateTime dateTimeNow = DateTime.Now;

                string logEntry = null;

                try
                {
                    logEntry = GetSerializedLogEntry(dateTimeNow, sessionId, message);
                }
                catch (Exception e)
                {
                    var errorMessage = new
                    {
                        error = "FAILED TO PARSE LOG MESSAGE OBJECT",
                        exceptionType = e.GetType(),
                        exceptionMessage = e.Message
                    };

                    logEntry = GetSerializedLogEntry(dateTimeNow, sessionId, errorMessage);
                }

                if (!useExistingOpenStream)
                {
                    Open();
                }

                appendTextStreamWriter_.WriteLine(logEntry);
                appendTextStreamWriter_.Flush();

                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            if (!useExistingOpenStream)
            {
                Close();
            }

            return success;
        }

        public bool WriteMessage(object message)
        {
            return WriteMessage(GetSessionId(), message);
        }

        public void Close()
        {
            if (appendTextStreamWriter_ != null)
            {
                try
                {
                    appendTextStreamWriter_.Close();
                }
                catch (Exception)
                {
                }
                appendTextStreamWriter_ = null;
            }

            return;
        }

        private static string GetSessionId()
        {
            return SessionId;
        }

        private static string ReadLineAsPlainText(string logLine, bool useUniversalTime)
        {
            string plainTextLine = logLine;

            JObject jobject = JsonSerialization.DeserializeFromJson(logLine);

            if (jobject == null)
            {
                return plainTextLine;
            }

            JToken? dateString = jobject["date"][useUniversalTime ? "utc" : "local"];
            JToken? timeString = jobject["time"][useUniversalTime ? "utc" : "local"];
            JToken? message = jobject["message"]["message"];

            plainTextLine = dateString + " " + timeString + " : " + message;

            return plainTextLine;
        }

        public static IEnumerable<string> ReadLinesAsPlainText(string logFilePath, bool useUniversalTime = false)
        {
            IEnumerable<string> lines = null;

            try
            {
                lines = File.ReadAllLines(logFilePath).Select(line => ReadLineAsPlainText(line, useUniversalTime)).ToList();
            }
            catch (Exception)
            {
                lines = null;
            }

            return lines;
        }

        public static List<LogFile> LOG_FILE = new();

        public static LogFile InitializeLogging(string logName, string logFolderPath)
        {
            LogFile lg = new(logName, logFolderPath, false);
            LOG_FILE.Add(lg);
            //LOG_FILE[0] = lg;
            return lg;
        }

        //public static object GetLogFilePath()
        //{
        //    LogFile logFile = LOG_FILE[0];
        //    return logFile != null ? logFile.GetLogFilePath() : string.Empty;
        //}

        public string DumpPlainTextLogFile()
        {
            string plainTextLogFilePath = null;
            string logFilePath          = GetLogFilePath();
            if (string.IsNullOrWhiteSpace(logFilePath)) return plainTextLogFilePath;
            plainTextLogFilePath = Path.Combine(Path.GetDirectoryName(logFilePath), Path.GetFileNameWithoutExtension(logFilePath) + ".txt");
            try
            {
                File.WriteAllLines(plainTextLogFilePath, LogFile.ReadLinesAsPlainText(logFilePath));
            }
            catch (Exception)
            {
                plainTextLogFilePath = null;
            }
            return plainTextLogFilePath;
        }
    }
}
