﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using NLog;

namespace RevitFileCore
{
    public class SessionDataExporter
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        
        private const string SessionDataFilename = "session.json";

        private const string SessionFilesDataFilename = "session_files.json";

        private static JObject GetSessionData(
            object sessionId,
            DateTime sessionStartTime,
            DateTime sessionEndTime,
            string sessionDataFolderPath,
            object sessionError) {
            Dictionary<object, object> sessionData = new (){
                {
                    "sessionStartTime",
                    TimeUtil.GetTimestampObject(sessionStartTime)},
                {
                    "sessionEndTime",
                    TimeUtil.GetTimestampObject(sessionEndTime)},
                {
                    "sessionId",
                    sessionId},
                {
                    "sessionFolder",
                    PathUtil.ExpandedFullNetworkPath(sessionDataFolderPath)},
                {
                    "sessionError",
                    sessionError},
                {
                    "username",
                    Environment.UserName},
                {
                    "machineName",
                    Environment.MachineName},
                {
                    "gatewayAddresses",
                    NetworkUtil.GetGatewayAddresses()},
                {
                    "ipAddresses",
                    NetworkUtil.GetIPAddresses()}};
            return new JObject(sessionData);
        }
        
        public static JObject ExportSessionData(
            string sessionId,
            DateTime sessionStartTime,
            DateTime sessionEndTime,
            string sessionDataFolderPath,
            object sessionError)
        {
            Logger.Debug("Exporting Session Data");
            JObject sessionData = GetSessionData(sessionId, sessionStartTime, sessionEndTime, sessionDataFolderPath, sessionError);
            var serializedSessionData = JsonSerialization.SerializeToJson(sessionData);
            Logger.Debug("Writing Session Data to Text File");
            string sessionDataFilePath = Path.Combine(sessionDataFolderPath, SessionDataFilename);
            TextFileUtil.WriteToTextFile(sessionDataFilePath, serializedSessionData);
            return sessionData;
        }

        private static JObject GetSessionFilesData(string sessionId, IEnumerable<string> sessionFiles) {
            Dictionary<string, object> sessionFilesData = new() {
                {
                    "sessionId",
                    sessionId},
                {
                    "sessionFiles",
                    (from filePath in sessionFiles
                        select Path.IsPathRooted(filePath) ? PathUtil.ExpandedFullNetworkPath(filePath) : filePath).ToList()}};
            return new JObject(sessionFilesData);
        }
        
        public static JObject ExportSessionFilesData(string sessionDataFolderPath, string sessionId, IEnumerable<string> sessionFiles) {
            JObject sessionFilesData = GetSessionFilesData(sessionId, sessionFiles);
            string serializedSessionFilesData = JsonSerialization.SerializeToJson(sessionFilesData);
            string sessionFilesDataFilePath = Path.Combine(sessionDataFolderPath, SessionFilesDataFilename);
            TextFileUtil.WriteToTextFile(sessionFilesDataFilePath, serializedSessionFilesData);
            return sessionFilesData;
        }
    }
}