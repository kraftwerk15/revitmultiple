﻿#region License
//
// Revit Batch Processor
//
// Copyright (c) 2020  Dan Rumery, BVN
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
#region Using
using System;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Environment = System.Environment;
using Path = System.IO.Path;
#endregion

namespace RevitFileCore
{
    public class RevitFileUtil
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();

        private sealed class CentralLockedCallback : ICentralLockedCallback 
        {
            private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
            
            private bool shouldWaitForLock { get; set; }
            
            public CentralLockedCallback(bool shouldWaitForLockAvailabilityCallback) {
                shouldWaitForLock = shouldWaitForLockAvailabilityCallback;
                ShouldWaitForLockAvailability();
            }

            public bool ShouldWaitForLockAvailability() {
                return shouldWaitForLock;
            }
        }
        private static TransactWithCentralOptions CreateTransactWithCentralOptions(bool shouldWaitForLockAvailabilityCallback = false) {
            TransactWithCentralOptions transactWithCentralOptions = new();
            if (shouldWaitForLockAvailabilityCallback) return transactWithCentralOptions;
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            CentralLockedCallback centralLockedCallback = new(shouldWaitForLockAvailabilityCallback);
            transactWithCentralOptions.SetLockCallback(centralLockedCallback);
            return transactWithCentralOptions;
        }

        private static SynchronizeWithCentralOptions CreateSynchronizeWithCentralOptions(string comment = "", bool compact = true, bool saveLocalBefore = true,
            bool saveLocalAfter = true, RelinquishOptions relinquishOptions = null) 
        {
            SynchronizeWithCentralOptions syncOptions = new()
            {
                Comment = comment,
                Compact = compact,
                SaveLocalBefore = saveLocalBefore,
                SaveLocalAfter = saveLocalAfter
            };
            relinquishOptions ??= new RelinquishOptions(true);
            syncOptions.SetRelinquishOptions(relinquishOptions);
            return syncOptions;
        }
        
        public static void SynchronizeWithCentral(Document doc, string comment = "") {
            TransactWithCentralOptions transactOptions = CreateTransactWithCentralOptions();
            SynchronizeWithCentralOptions syncOptions = CreateSynchronizeWithCentralOptions(comment);
            doc.SynchronizeWithCentral(transactOptions, syncOptions);
        }
        
        // ReSharper disable once UnusedMember.Global
        // ReSharper disable once IdentifierTypo
        public static void ReloadLastest(Document doc) {
            doc.ReloadLatest(new ReloadLatestOptions());
        }

        private static void CopyModel(Application app, string sourceModelFilePath, string destinationFilePath, bool overwrite = true) {
            ModelPath sourceModelPath = ToModelPath(sourceModelFilePath);
            app.CopyModel(sourceModelPath, destinationFilePath, overwrite);
        }

        private static Document CreateNewProjectFile(Application app, string revitFilePath) {
            Document newDoc = app.NewProjectDocument(app.DefaultProjectTemplate);
            SaveAsOptions saveAsOptions = new() {OverwriteExistingFile = true};
            newDoc.SaveAs(revitFilePath, saveAsOptions);
            return newDoc;
        }
        
        public static object OpenAndActivateBatchRvtTemporaryDocument(UIApplication uiApplication) {
            Application application = uiApplication.Application;
            string batchrvtTemporaryRevitFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "BatchRvt", "TemporaryProject." + application.VersionNumber + ".rvt");
            if (!PathUtil.FileExists(batchrvtTemporaryRevitFilePath)) {
                PathUtil.CreateDirectoryForFilePath(batchrvtTemporaryRevitFilePath);
                Document newDoc = CreateNewProjectFile(application, batchrvtTemporaryRevitFilePath);
                newDoc.Close(false);
            }
            UIDocument uiDoc = uiApplication.OpenAndActivateDocument(batchrvtTemporaryRevitFilePath);
            return uiDoc;
        }

        private static WorksetConfiguration ParseWorksetConfigurationOption(bool closeAllWorksets, WorksetConfiguration worksetConfig = null) {
            worksetConfig = worksetConfig != null ? worksetConfig : closeAllWorksets ? 
                new WorksetConfiguration(WorksetConfigurationOption.CloseAllWorksets) : new WorksetConfiguration();
            return worksetConfig;
        }

        private static ModelPath ToModelPath(string modelPath)
        {
            return modelPath != null ? ModelPathUtils.ConvertUserVisiblePathToModelPath(modelPath) : null;
        }

        private static object ToUserVisiblePath(ModelPath modelPath)
        {
            return modelPath != null ? ModelPathUtils.ConvertModelPathToUserVisiblePath(modelPath) : null;
        }

        private static Guid ToGuid(string guidOrGuidText) {
            return new(guidOrGuidText);
        }
        
        public static ModelPath ToCloudPath(string region, string cloudProjectId, string cloudModelId) {
            Guid cloudProjectGuid = ToGuid(cloudProjectId);
            Guid cloudModelGuid = ToGuid(cloudModelId);
            ModelPath cloudPath = ModelPathUtils.ConvertCloudGUIDsToCloudPath(region, cloudProjectGuid, cloudModelGuid);
            return cloudPath;
        }
        
        public static Document OpenNewLocal(Application application, string modelFilePath, string localModelFilePath,
            bool closeAllWorksets = false, WorksetConfiguration worksetConfig = null, bool audit = false) 
        {
            ModelPath modelPath = ToModelPath(modelFilePath);
            ModelPath localModelPath = ToModelPath(localModelFilePath);
            OpenOptions openOptions = new() {DetachFromCentralOption = DetachFromCentralOption.DoNotDetach};
            worksetConfig = ParseWorksetConfigurationOption(closeAllWorksets, worksetConfig);
            openOptions.SetOpenWorksetsConfiguration(worksetConfig);
            WorksharingUtils.CreateNewLocal(modelPath, localModelPath);
            if (audit) {
                openOptions.Audit = true;
            }
            return application.OpenDocumentFile(localModelPath, openOptions);
        }
        
        public static UIDocument OpenAndActivateNewLocal(UIApplication uiApplication, string modelFilePath, string localModelFilePath,
            bool closeAllWorksets = false, WorksetConfiguration worksetConfig = null, bool audit = false) 
        {
            ModelPath modelPath = ToModelPath(modelFilePath);
            ModelPath localModelPath = ToModelPath(localModelFilePath);
            OpenOptions openOptions = new() {DetachFromCentralOption = DetachFromCentralOption.DoNotDetach};
            worksetConfig = ParseWorksetConfigurationOption(closeAllWorksets, worksetConfig);
            openOptions.SetOpenWorksetsConfiguration(worksetConfig);
            WorksharingUtils.CreateNewLocal(modelPath, localModelPath);
            if (audit) {
                openOptions.Audit = true;
            }
            return uiApplication.OpenAndActivateDocument(localModelPath, openOptions, false);
        }
        
        public static Document OpenDetachAndPreserveWorksets(Application application, string modelFilePath, bool closeAllWorksets = false,
            WorksetConfiguration worksetConfig = null, bool audit = false) {
            ModelPath modelPath = ToModelPath(modelFilePath);
            OpenOptions openOptions = new()
            {
                DetachFromCentralOption = DetachFromCentralOption.DetachAndPreserveWorksets
            };
            worksetConfig = ParseWorksetConfigurationOption(closeAllWorksets, worksetConfig);
            openOptions.SetOpenWorksetsConfiguration(worksetConfig);
            if (audit) {
                openOptions.Audit = true;
            }
            return application.OpenDocumentFile(modelPath, openOptions);
        }
        
        public static UIDocument OpenAndActivateDetachAndPreserveWorksets(UIApplication uiApplication, string modelFilePath,
            bool closeAllWorksets = false, WorksetConfiguration worksetConfig = null, bool audit = false) {
            ModelPath modelPath = ToModelPath(modelFilePath);
            OpenOptions openOptions = new()
            {
                DetachFromCentralOption = DetachFromCentralOption.DetachAndPreserveWorksets
            };
            worksetConfig = ParseWorksetConfigurationOption(closeAllWorksets, worksetConfig);
            openOptions.SetOpenWorksetsConfiguration(worksetConfig);
            if (audit) {
                openOptions.Audit = true;
            }
            return uiApplication.OpenAndActivateDocument(modelPath, openOptions, false);
        }
        
        public static Document OpenCloudDocument(Application application, string region, string cloudProjectId, string cloudModelId,
            bool closeAllWorksets = false, WorksetConfiguration worksetConfig = null, bool audit = false) {
            ModelPath cloudPath = ToCloudPath(region, cloudProjectId, cloudModelId);
            OpenOptions openOptions = new();
            worksetConfig = ParseWorksetConfigurationOption(closeAllWorksets, worksetConfig);
            openOptions.SetOpenWorksetsConfiguration(worksetConfig);
            if (audit) {
                openOptions.Audit = true;
            }
            return application.OpenDocumentFile(cloudPath, openOptions);
        }
        
        public static UIDocument OpenAndActivateCloudDocument(UIApplication uiApplication, string region, string cloudProjectId, string cloudModelId,
            bool closeAllWorksets = false, WorksetConfiguration worksetConfig = null, bool audit = false) {
            ModelPath cloudPath = ToCloudPath(region, cloudProjectId, cloudModelId);
            OpenOptions openOptions = new OpenOptions();
            worksetConfig = ParseWorksetConfigurationOption(closeAllWorksets, worksetConfig);
            openOptions.SetOpenWorksetsConfiguration(worksetConfig);
            if (audit) {
                openOptions.Audit = true;
            }
            return uiApplication.OpenAndActivateDocument(cloudPath, openOptions, false);
        }
        
        public static Document OpenDetachAndDiscardWorksets(Application application, string modelFilePath, bool audit = false) {
            ModelPath modelPath = ToModelPath(modelFilePath);
            OpenOptions openOptions = new()
            {
                DetachFromCentralOption = DetachFromCentralOption.DetachAndDiscardWorksets
            };
            if (audit) {
                openOptions.Audit = true;
            }
            return application.OpenDocumentFile(modelPath, openOptions);
        }
        
        public static UIDocument OpenAndActivateDetachAndDiscardWorksets(UIApplication uiApplication, string modelFilePath, bool audit = false) {
            ModelPath modelPath = ToModelPath(modelFilePath);
            OpenOptions openOptions = new()
            {
                DetachFromCentralOption = DetachFromCentralOption.DetachAndDiscardWorksets
            };
            if (audit) {
                openOptions.Audit = true;
            }
            return uiApplication.OpenAndActivateDocument(modelPath, openOptions, false);
        }
        
        public static Document OpenDocumentFile(Application application, string modelFilePath, bool audit = false) {
            Document doc;
            if (audit) {
                OpenOptions openOptions = new() {Audit = true};
                ModelPath modelPath = ToModelPath(modelFilePath);
                doc = application.OpenDocumentFile(modelPath, openOptions);
            } else {
                //modelPath = ToUserVisiblePath(modelPath);
                doc = application.OpenDocumentFile(modelFilePath);
            }
            return doc;
        }
        
        public static UIDocument OpenAndActivateDocumentFile(UIApplication uiApplication, string modelFilePath, bool audit = false) {
            UIDocument uiDoc;
            if (audit) {
                OpenOptions openOptions = new() {Audit = true};
                ModelPath modelPath = ToModelPath(modelFilePath);
                uiDoc = uiApplication.OpenAndActivateDocument(modelPath, openOptions, false);
            } else {
                //modelPath = ToUserVisiblePath(modelPath);
                uiDoc = uiApplication.OpenAndActivateDocument(modelFilePath);
            }
            return uiDoc;
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private static RelinquishedItems RelinquishAll(Document doc, bool shouldWaitForLockAvailabilityCallback = false) {
            RelinquishOptions relinquishOptions = new(true);
            TransactWithCentralOptions transactWithCentralOptions = CreateTransactWithCentralOptions(shouldWaitForLockAvailabilityCallback);
            return WorksharingUtils.RelinquishOwnership(doc, relinquishOptions, transactWithCentralOptions);
        }

        private static void SaveAsNewCentral(Document doc, ModelPath modelPath, bool overwrite = true, bool clearTransmitted = false) {
            SaveAsOptions saveAsOptions = new()
            {
                Compact = true, OverwriteExistingFile = overwrite, MaximumBackups = 1
            };
            // ReSharper disable once IdentifierTypo
            WorksharingSaveAsOptions worksharingSaveAsOptions = new()
            {
                SaveAsCentral = true, ClearTransmitted = clearTransmitted
            };
            saveAsOptions.SetWorksharingOptions(worksharingSaveAsOptions);
            doc.SaveAs(modelPath, saveAsOptions);
        }
        
        public static bool CloseWithSave(Document doc) {
            return doc.Close(true);
        }
        
        public static bool CloseWithoutSave(Document doc) {
            return doc.Close(false);
        }
        
        public static void Save(Document doc, bool compact = false, ElementId previewViewId = null) {
            SaveOptions saveOptions = new() {Compact = compact};
            if (previewViewId != null) {
                saveOptions.PreviewViewId = previewViewId;
            }
            try
            {
                doc.Save(saveOptions);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }
        
        public static void SaveAs(Document doc, string modelFilePath, bool overwriteExisting = false, bool compact = false, ElementId previewViewId = null,
            WorksharingSaveAsOptions workSharingSaveAsOptions = null, int maximumBackups = 3) 
        {
            ModelPath modelPath = ToModelPath(modelFilePath);
            SaveAsOptions saveAsOptions = new()
            {
                Compact = compact, OverwriteExistingFile = overwriteExisting
            };
            if (previewViewId != null) {
                saveAsOptions.PreviewViewId = previewViewId;
            }
            if (workSharingSaveAsOptions != null) {
                saveAsOptions.SetWorksharingOptions(workSharingSaveAsOptions);
            }
            if (maximumBackups != 3 && maximumBackups > 0) {
                saveAsOptions.MaximumBackups = maximumBackups;
            }
            doc.SaveAs(modelPath, saveAsOptions);
        }
        
        // ReSharper disable once IdentifierTypo
        public static object CreateWorksharingSaveAsOptions(bool saveAsCentral = false, 
            SimpleWorksetConfiguration openWorksetsDefault = SimpleWorksetConfiguration.AskUserToSpecify, bool clearTransmitted = false) {
            // ReSharper disable once IdentifierTypo
            WorksharingSaveAsOptions worksharingSaveAsOptions = new()
            {
                OpenWorksetsDefault = openWorksetsDefault,
                ClearTransmitted = clearTransmitted,
                SaveAsCentral = saveAsCentral
            };
            return worksharingSaveAsOptions;
        }
        
        public static object DetachAndSaveModel(Application app, string centralModelFilePath, string detachedModelFilePath, bool audit = false) {
            ModelPath centralModelPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(centralModelFilePath);
            CopyModel(app, centralModelFilePath, detachedModelFilePath);
            ModelPath detachedModelPath = ModelPathUtils.ConvertUserVisiblePathToModelPath(detachedModelFilePath);
            Document doc = OpenDetachAndPreserveWorksets(app, detachedModelFilePath, audit: audit);
            SaveAsNewCentral(doc, detachedModelPath);
            // Relinquish ownership (Saving the new central file takes ownership of worksets so relinquishing must be done
            // after it, if at all)
            RelinquishAll(doc);
            return doc;
        }

        /// <summary>
        /// Identifies the BasicFileInfo information from the Revit object.
        /// </summary>
        /// <param name="revitFilePath">The File Path to the File.</param>
        /// <returns>A BasicFileInfo object from Revit.</returns>
        private static Autodesk.Revit.DB.BasicFileInfo TryGetBasicFileInfo(string revitFilePath) {
            try {
                return Autodesk.Revit.DB.BasicFileInfo.Extract(revitFilePath);
            } catch (Exception e) {
                Logger.Error("ERROR: Could not extract BasicFileInfo from FilePath.");
                Logger.Error(e);
            }
            return null;
        }

        private static string GetSavedInVersion(Autodesk.Revit.DB.BasicFileInfo basicFileInfo) {
            string savedInVersion = string.Empty;
            try {
                // <= Revit 2019
                if (string.IsNullOrEmpty(basicFileInfo.Format))
                {
                    //savedInVersion = basicFileInfo.SavedInVersion;
                }
            } catch {
                // Revit 2020+
                savedInVersion = basicFileInfo.Format;
            }
            return savedInVersion;
        }
        
        /// <summary>
        /// Identifies the Year Version of Model at the File Path.
        /// </summary>
        /// <param name="revitFilePath">The File Path to the File.</param>
        /// <returns>A string Value of the Year Version.</returns>
        public static string GetRevitFileVersion(string revitFilePath) {
            Autodesk.Revit.DB.BasicFileInfo basicFileInfo = TryGetBasicFileInfo(revitFilePath);
            string savedInVersion = basicFileInfo != null ? GetSavedInVersion(basicFileInfo) : null;
            return savedInVersion;
        }
        
        /// <summary>
        /// Identifies if the Model at the File Path is a valid Revit Central Model.
        /// </summary>
        /// <param name="revitFilePath">The File Path to the File.</param>
        /// <returns>A bool if the Model is a Local Model.</returns>
        public static bool IsLocalModel(string revitFilePath) {
            bool isLocalModel = false;
            Autodesk.Revit.DB.BasicFileInfo basicFileInfo = TryGetBasicFileInfo(revitFilePath);
            if (basicFileInfo == null) return false;
            bool isWorkshared = basicFileInfo.IsWorkshared;
            if (isWorkshared) {
                // NOTE: see: https://forums.autodesk.com/t5/revit-api-forum/basicfileinfo-iscreatedlocal-property-outputting-unexpected/td-p/7111503
                isLocalModel = basicFileInfo.IsCreatedLocal || basicFileInfo.IsLocal;
            }
            return isLocalModel;
        }
        
        /// <summary>
        /// Identifies if the Model at the File Path is a valid Revit Central Model.
        /// </summary>
        /// <param name="revitFilePath">The File Path to the File.</param>
        /// <returns>A bool if the Model is a Central Model.</returns>
        public static bool IsCentralModel(string revitFilePath) {
            bool isCentralModel = false;
            Autodesk.Revit.DB.BasicFileInfo basicFileInfo = TryGetBasicFileInfo(revitFilePath);
            if (basicFileInfo == null) return false;
            bool isWorkshared = basicFileInfo.IsWorkshared;
            if (isWorkshared) {
                isCentralModel = basicFileInfo.IsCentral;
            }
            return isCentralModel;
        }
        
        /// <summary>
        /// Identifies if the Model at the File Path is Workshared.
        /// </summary>
        /// <param name="revitFilePath">The File Path to the File.</param>
        /// <returns>A bool if the Model is Workshared.</returns>
        public static bool IsWorkshared(string revitFilePath) {
            bool isWorkshared = false;
            Autodesk.Revit.DB.BasicFileInfo basicFileInfo = TryGetBasicFileInfo(revitFilePath);
            if (basicFileInfo != null) {
                isWorkshared = basicFileInfo.IsWorkshared;
            }
            return isWorkshared;
        }
    }
}