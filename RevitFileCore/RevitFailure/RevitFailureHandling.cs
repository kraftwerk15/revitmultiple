﻿#region Using
using System;
using System.Collections.Generic;
using EventHandler = System.EventHandler;
using FailuresProcessingEventArgs = Autodesk.Revit.DB.Events.FailuresProcessingEventArgs;
using System.Linq;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
#endregion

namespace RevitFileCore.RevitFailure
{
    public class RevitFailureHandling
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        
        public const string RevitWarningsMessageHandlerPrefix = "[ REVIT WARNINGS HANDLER ]";

        public static object ElementIdsToSemicolonDelimitedText(ICollection<ElementId> elementIds) {
            return string.Join("; ", (from elementId in elementIds
                select elementId.IntegerValue.ToString()).ToList());
        }
        
        public static void ReportFailureWarning(FailureMessageAccessor failure, FailureDefinitionAccessor failureDefinition) {
            FailureSeverity failureSeverity = failure.GetSeverity();
            // TODO: more thorough reporting?
            Logger.Info(failureSeverity + " - " + failure.GetDescriptionText() + " - " + "(" + "GUID: " + failure.GetFailureDefinitionId().Guid + ")");
            if (failureSeverity is FailureSeverity.Error or FailureSeverity.Warning) {
                ICollection<ElementId> failingElementIds = failure.GetFailingElementIds();
                if (failingElementIds.Count > 0) {
                    Logger.Info("Failing element ids: " + ElementIdsToSemicolonDelimitedText(failingElementIds));
                }
                ICollection<ElementId> additionalElementIds = failure.GetAdditionalElementIds();
                if (additionalElementIds.Count > 0) {
                    Logger.Info("Additional element ids: " + ElementIdsToSemicolonDelimitedText(additionalElementIds));
                }
            }

            if (failureSeverity != FailureSeverity.Error) return;
            if (failure.HasResolutions()) {
                Logger.Info("\t" + "Applicable resolution types:");
                FailureResolutionType defaultResolutionType = failureDefinition.GetDefaultResolutionType();
                foreach (FailureResolutionType resolutionType in failureDefinition.GetApplicableResolutionTypes()) {
                    Logger.Info(resolutionType == defaultResolutionType ? " (Default)" : string.Empty + " - " + "'" + failureDefinition.GetResolutionCaption(resolutionType) + "'");
                }
            } else {
                Logger.Info("\t" + "WARNING: no resolutions available");
            }
        }
        
        public static FailureProcessingResult ProcessFailures(FailuresAccessor failuresAccessor, bool rollBackOnWarning = false) {
            FailureProcessingResult result;
            try {
                result = FailureProcessingResult.Continue;
                Document doc = failuresAccessor.GetDocument();
                //Application app = doc.Application;
                FailureDefinitionRegistry failureReg = Application.GetFailureDefinitionRegistry();
                IList<FailureMessageAccessor> failures = failuresAccessor.GetFailureMessages();
                if (failures.Any()) {
                    Logger.Info("Processing Revit document warnings / failures (" + failures.Count.ToString() + "):");
                    foreach (FailureMessageAccessor failure in failures) {
                        FailureDefinitionAccessor failureDefinition = failureReg.FindFailureDefinition(failure.GetFailureDefinitionId());
                        ReportFailureWarning(failure, failureDefinition);
                        FailureSeverity failureSeverity = failure.GetSeverity();
                        switch (failureSeverity)
                        {
                            case FailureSeverity.Warning when !rollBackOnWarning:
                                failuresAccessor.DeleteWarning(failure);
                                break;
                            case FailureSeverity.Error when failure.HasResolutions() && result != FailureProcessingResult.ProceedWithRollBack && !rollBackOnWarning:
                            {
                                // If Unlock Constraints is a valid resolution type for the current failure, use it.
                                if (failure.HasResolutionOfType(FailureResolutionType.UnlockConstraints)) {
                                    failure.SetCurrentResolutionType(FailureResolutionType.UnlockConstraints);
                                } else if (failureDefinition.IsResolutionApplicable(FailureResolutionType.UnlockConstraints)) {
                                    Logger.Info("WARNING: UnlockConstraints is not a valid resolution for this failure despite the definition reporting that it is an applicable resolution!");
                                }
                                Logger.Info("Attempting to resolve error using resolution: " + failure.GetCurrentResolutionType().ToString());
                                failuresAccessor.ResolveFailure(failure);
                                result = FailureProcessingResult.ProceedWithCommit;
                                break;
                            }
                            case FailureSeverity.None:
                                break;
                            case FailureSeverity.DocumentCorruption:
                                break;
                            default:
                                result = FailureProcessingResult.ProceedWithRollBack;
                                break;
                        }
                    }
                } else {
                    result = FailureProcessingResult.Continue;
                }
            } catch (Exception e) {
                Logger.Info("ERROR: the failure handler generated an error!");
                ExceptionUtil.LogOutputErrorDetails(e);
                result = FailureProcessingResult.Continue;
            }
            return result;
        }
        
        public class FailuresPreprocessor : IFailuresPreprocessor {
            
            public FailuresPreprocessor() {
                return;
            }
            
            public virtual FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor) {
                FailureProcessingResult result = ProcessFailures(failuresAccessor);
                return result;
            }
        }
        
        public static void SetTransactionFailureOptions(Transaction transaction) {
            FailureHandlingOptions failureOptions = transaction.GetFailureHandlingOptions();
            failureOptions.SetForcedModalHandling(true);
            failureOptions.SetClearAfterRollback(true);
            failureOptions.SetFailuresPreprocessor(new FailuresPreprocessor());
            transaction.SetFailureHandlingOptions(failureOptions);
        }
        
        public static void SetFailuresAccessorFailureOptions(FailuresAccessor failuresAccessor) {
            FailureHandlingOptions failureOptions = failuresAccessor.GetFailureHandlingOptions();
            failureOptions.SetForcedModalHandling(true);
            failureOptions.SetClearAfterRollback(true);
            failuresAccessor.SetFailureHandlingOptions(failureOptions);
        }
        
        public static void FailuresProcessingEventHandler(object sender, FailuresProcessingEventArgs args) {
            object app = sender;
            var failuresAccessor = args.GetFailuresAccessor();
            SetFailuresAccessorFailureOptions(failuresAccessor);
            FailureProcessingResult result = ProcessFailures(failuresAccessor);
            args.SetProcessingResult(result);
        }
        
        public static object WithFailuresProcessingHandler(Application app, Func<object> action) {
            string output = GlobalTestMode.PrefixedOutputForGlobalTestMode(RevitWarningsMessageHandlerPrefix);
            object result = null;
            EventHandler<FailuresProcessingEventArgs> failuresProcessingEventHandler = FailuresProcessingEventHandler;
            app.FailuresProcessing += failuresProcessingEventHandler;
            try {
                result = action();
            } finally {
                app.FailuresProcessing -= failuresProcessingEventHandler;
            }
            return result;
        }
    }
}