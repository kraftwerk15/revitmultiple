﻿using System;
using NLog;
using System.Text;

namespace RevitFileCore
{
    public class ExceptionUtil
    {
        private const string ExceptionMessageHandlerPrefix = "[ EXCEPTION MESSAGE ]";

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        //public static object GetInterpretedFrameInfo(object clsExceptionData) {
        //    object interpretedFrameInfo = null;
        //    foreach (var kv in clsExceptionData) {
        //        object entryName = null;
        //        try {
        //            entryName = kv.Key.Name;
        //        } catch {
        //        }
        //        if (entryName == "InterpretedFrameInfo") {
        //            interpretedFrameInfo = kv.Value;
        //            break;
        //        }
        //    }
        //    return interpretedFrameInfo;
        //}
        
        //public static object GetClrException(Exception exception) {
        //    return exception is exceptions.Exception ? exception.clsException : exception is System.Exception ? exception : null;
        //}
        
        public static void LogOutputErrorDetails(Exception exception, bool verbose = true) {
            object output = GlobalTestMode.PrefixedOutputForGlobalTestMode(ExceptionMessageHandlerPrefix);
            Logger.Error("Exception: [" + exception.GetType().Name + "] " + exception.Message);
            //try {
            //    //var clsException = GetClrException(exception);
            //    //if (clsException == null) return;
            //    //string clsExceptionType = exception.GetType().Name;
            //    //Logger.Error(".NET exception: [" + clsExceptionType + "] " + clsException.Message.ToString());
            //    //if (!verbose) return;
            //    //var interpretedFrameInfo = GetInterpretedFrameInfo(clsException.Data);
            //    //if (interpretedFrameInfo == null) return;
            //    //Logger.Error("Further exception information:");
            //    //foreach (var i in interpretedFrameInfo) {
            //    //    if (i.ToString() != "CallSite.Target") {
            //    //        Logger.Error("\t" + i.ToString());
            //    //    }
            //    //}
            //} 
            //catch {
            //    Logger.Error("Could not obtain further exception information.");
            //}
        }
        
        public static string GetExceptionDetails(Exception exception) {
            StringBuilder exceptionDetails = new();
            Func<string, object> output = message => {
                exceptionDetails.AppendLine(message);
                return null;
            };
            LogOutputErrorDetails(exception);
            return exceptionDetails.ToString();
        }
    }
}