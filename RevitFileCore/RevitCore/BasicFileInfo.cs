﻿using System;

namespace RevitFileCore
{
    public struct BasicFileInfo
    {
        public Int32 A { get; set; }
        public Int32 B { get; set; }
        public Int16 C { get; set; }
        public string Path1 { get; set; }
        public string Version { get; set; }
        public string Path2 { get; set; }
        public string Data { get; set; }
        public byte[] Unknown { get; set; }
        public string UID { get; set; }
        public string Localization { get; set; }
    }
}