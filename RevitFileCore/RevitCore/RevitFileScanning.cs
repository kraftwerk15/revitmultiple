﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NLog;

namespace RevitFileCore
{
    public static class RevitFileScanning
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        public enum RevitFileType { Project, Family, ProjectAndFamily, FamilyTemplates }

        private const string AllFilesWithAnExtensionPattern = "*.*";

        private const string RevitProjectFileExtension = ".rvt";
        private const string RevitProjectFilePattern = "*" + RevitProjectFileExtension;
        private const string RevitFamilyFileExtension = ".rfa";
        private const string RevitFamilyFilePattern = "*" + RevitFamilyFileExtension;
        private const string RevitFamilyTemplateFileExtension = ".rft";
        private const string RevitFamilyTemplateFilePattern = "*" + RevitFamilyTemplateFileExtension;
        private const string RevitBackupFolderName = "backup";
        private const string RevitBackupFolderName2 = "_backup";


        public static IEnumerable<string> FindRevitFiles(string folderPath, SearchOption searchOption, RevitFileType revitFileType, bool ignoreRevitBackups)
        {
            Logger.Debug("Starting FindRevitFiles");
            string searchFilePattern = AllFilesWithAnExtensionPattern;

            // ReSharper disable once ConvertSwitchStatementToSwitchExpression
            // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
            switch (revitFileType)
            {
                case RevitFileType.Project:
                    searchFilePattern = RevitProjectFilePattern;
                    break;
                case RevitFileType.Family:
                    searchFilePattern = RevitFamilyFilePattern;
                    break;
                case RevitFileType.ProjectAndFamily:
                    searchFilePattern = AllFilesWithAnExtensionPattern;
                    break;
                case RevitFileType.FamilyTemplates:
                    searchFilePattern = RevitFamilyTemplateFilePattern;
                    break;
            }

            Logger.Info("Search Pattern is : {0}", searchFilePattern);
            Logger.Info("Search Folder is : {0}", folderPath);
            IEnumerable<string> foldersToScan;
            if (searchOption == SearchOption.AllDirectories)
            {
                foldersToScan = new[] { folderPath }.Concat(PathUtil.SafeEnumerateFolders(folderPath, "*", SearchOption.AllDirectories));
            }
            else
            {
                foldersToScan = new[] { folderPath };
            }
            
            List<string> revitFilePaths = new();

            foreach (string folderToScan in foldersToScan)
            {
                revitFilePaths.AddRange(
                        PathUtil.SafeEnumerateFiles(folderToScan, searchFilePattern, SearchOption.TopDirectoryOnly)
                        .Where(filePath => HasRevitFileExtension(filePath, ignoreRevitBackups))
                    );
            }
            Logger.Info("Number of Revit Files Found : {0}", revitFilePaths.Count);
            return revitFilePaths;
        }

        public static IEnumerable<string> ExpandNetworkPaths(List<string> revitFilePaths)
        {
            IEnumerable<string> indexedExpandedRevitFilePaths =
                PathUtil.EnumerateExpandedFullNetworkPaths(revitFilePaths);

            return indexedExpandedRevitFilePaths;
        }

        public static IEnumerable<string[]> ExtractVersionInfo(List<string> revitFilePaths)
        {
            var indexedRevitVersionTexts =
                PathUtil.EnumerateRevitVersionTexts(revitFilePaths);

            return indexedRevitVersionTexts;
        }


        public static bool HasRevitProjectFileExtension(string filePath, bool ignoreRevitBackups)
        {
            string extension = Path.GetExtension(filePath).ToLower();

            if (ignoreRevitBackups && IsBackupFile(filePath))
            {
                return false;
            }
            return extension.ToLower() == RevitProjectFileExtension;
        }

        public static bool HasRevitFamilyFileExtension(string filePath, bool ignoreRevitBackups)
        {
            string extension = Path.GetExtension(filePath).ToLower();

            if (ignoreRevitBackups && IsBackupFile(filePath))
            {
                return false;
            }

            return extension.ToLower() == RevitFamilyFileExtension;
        }

        public static bool HasRevitFileExtension(string filePath, bool ignoreRevitBackups)
        {
            string extension = Path.GetExtension(filePath).ToLower();

            if (ignoreRevitBackups && (IsBackupFile(filePath) || InBackupFolder(filePath)))
            {
                return false;
            }

            return new[] {
                RevitProjectFileExtension,
                RevitFamilyFileExtension
            }.Any(revitExtension => extension == revitExtension.ToLower());
        }

        private static bool IsBackupFile(string filePath)
        {
            const string pattern = "\\.\\d\\d\\d\\d\\.(rvt|rfa)$";
            return System.Text.RegularExpressions.Regex.IsMatch(filePath, pattern);
        }

        private static bool InBackupFolder(string filePath)
        {
            return filePath.Contains($@"{RevitBackupFolderName}\") ||
                   filePath.Contains($@"{RevitBackupFolderName2}\");
        }
    }
}
