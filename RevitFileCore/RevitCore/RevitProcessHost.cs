﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;

namespace RevitFileCore
{
    public static class RevitProcessHost
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        
        private const string ProcessUniqueIdDelimiter = "|";

        private static string GetUniqueIdForProcess(Process process)
        {
            return string.Join(ProcessUniqueIdDelimiter, process.Id.ToString(), TimeUtil.GetISO8601FormattedUtcDate(process.StartTime));
        }

        public static bool IsBatchRvtProcessRunning(string batchRvtProcessUniqueId)
        {
            Process[] processes = Process.GetProcesses();
            bool processRunning = processes.Any(variable => IsBatchRevitProcess(variable, batchRvtProcessUniqueId));
            Logger.Info(processRunning ? "Batch Revit Process Running" : "Batch Revit Process Is Not Running.");
            return processRunning;
        }

        private static bool IsBatchRevitProcess(Process process, string batchRvtProcessUniqueId)
        {
            bool isTargetProcess;
            try
            {
                isTargetProcess = GetUniqueIdForProcess(process) == batchRvtProcessUniqueId;
            }
            catch (Exception)
            {
                isTargetProcess = false;
            }

            return isTargetProcess;
        }

        //public static Process StartHostRevitProcess(
        //    string revitVersion,
        //    string batchRvtScriptsFolderPath,
        //    string scriptFilePath,
        //    string scriptDataFilePath,
        //    int progressNumber,
        //    string scriptOutputPipeHandleString,
        //    string testModeFolderPath)
        //{
        //    string batchRvtProcessUniqueId = GetUniqueIdForProcess(Process.GetCurrentProcess());
        //    StringDictionary dict = ScriptEnvironment.GetEnvironmentVariables();
        //    dict = ScriptEnvironment.InitEnvironmentVariables(dict, batchRvtScriptsFolderPath, scriptFilePath, scriptDataFilePath, 
        //        progressNumber, scriptOutputPipeHandleString, batchRvtProcessUniqueId, testModeFolderPath);
        //    
        //    return StartRevitProcess(revitVersion, initEnvironmentVariables);
        //}

        public static Process StartRevitProcess(int revitVersion, string batchRvtScriptsFolderPath, string scriptFilePath, string scriptDataFilePath,
            int progressNumber, string scriptOutputPipeHandleString, string testModeFolderPath)
        {
            Logger.Info("Attempting to Start Revit {0}", revitVersion);
            string revitExecutableFilePath = RevitVersion.GetRevitExecutableFilePath(revitVersion);
            ProcessStartInfo psi = new(revitExecutableFilePath)
            {
                UseShellExecute        = false,
                RedirectStandardError  = true,
                RedirectStandardOutput = true,
                WorkingDirectory       = RevitVersion.GetRevitExecutableFolderPath(revitVersion)
            };
            string batchRvtProcessUniqueId = GetUniqueIdForProcess(Process.GetCurrentProcess());
            ScriptEnvironment.InitEnvironmentVariables(psi.EnvironmentVariables, batchRvtScriptsFolderPath, scriptFilePath, scriptDataFilePath,
                progressNumber, scriptOutputPipeHandleString, batchRvtProcessUniqueId, testModeFolderPath);
            Logger.Info("Attempting to Start Revit {0}", revitVersion);
            Process revitProcess = Process.Start(psi);
            if(revitProcess is not null) Logger.Info("Revit {0} started successfully.", revitVersion);
            return revitProcess;
        }
    }
}
