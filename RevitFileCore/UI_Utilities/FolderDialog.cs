﻿using System.Windows.Forms;

namespace RevitFileCore
{
    public class FolderDialog
    {
        public string Browse(bool IsFolderPicker, string title)
        {
            string _title = title;
            if (IsFolderPicker)
            {
                // Initializes the variables to pass to the MessageBox.Show method.

                string message = "You can select either a file or folder here. The folder will be used for the operation.";
                string caption = "Folder Alert Dialog";
                MessageBoxButtons buttons = MessageBoxButtons.OK;

                // Displays the MessageBox.
                
                MessageBox.Show(message, caption, buttons, MessageBoxIcon.Information);
            }

            // ReSharper disable once UseObjectOrCollectionInitializer
            using OpenFileDialog commonOpenFileDialog = new();
            commonOpenFileDialog.Title = _title;
            if (commonOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (string.IsNullOrWhiteSpace(commonOpenFileDialog.FileName))
                {
                    return commonOpenFileDialog.FileName;
                }

                if (!System.IO.Directory.Exists(commonOpenFileDialog.FileName))
                {
                    return commonOpenFileDialog.FileName;
                }
                return commonOpenFileDialog.FileName;
            }
            return string.Empty;
        }

        public string BrowseLegacy(string title)
        {
            using FolderBrowserDialog folderBrowserDialog = new();
            folderBrowserDialog.Description = title;
            //folderBrowserDialog.SelectedPath = selectedPath;
            if (folderBrowserDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK || string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                return string.Empty;
            else
                return folderBrowserDialog.SelectedPath;
        }
    }
}
