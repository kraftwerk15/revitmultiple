﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitFileCore
{
    class UIAutomationUtil
    {
        private static readonly NLog.Logger Logger                   = NLog.LogManager.GetCurrentClassLogger();
        public static           string      DIALOG_WINDOW_CLASS_NAME = "#32770";
        public static int STRING_BUFFER_SIZE = 8 * 1024 + 1;
        

        public static List<WindowInfo> GetEnabledDialogsInfo(int processId)
        {
            return (from hwnd in Win32Api.GetTopLevelWindows(DIALOG_WINDOW_CLASS_NAME, null, processId)
                where Win32Api.IsWindowEnabled(hwnd) select new WindowInfo(hwnd)).ToList();
        }

        public static string TextWithoutAmpersands(string text)
        {
            return text.Replace("&", string.Empty).Replace("&", string.Empty);
        }

        public static string GetButtonText(IntPtr buttonInfo)
        {
            Win32Api.GetDlgItem(buttonInfo, int.MinValue);
            var buttonText = buttonInfo.ToString();
            return TextWithoutAmpersands(buttonText);
        }

        public static string GetWindowText(IntPtr hwnd)
        {
            StringBuilder s = new();
            s.EnsureCapacity(STRING_BUFFER_SIZE);
            var numberOfChars = Win32Api.GetWindowText(hwnd, s, STRING_BUFFER_SIZE);
            return s.ToString();
        }
        //controls may be WindowInfo here?
        public static List<IntPtr> FilterControlsByText(List<IntPtr> controls, string controlText)
        {
            List<IntPtr> targetControls = (from control in controls
                where string.Equals(TextWithoutAmpersands(GetButtonText(control)).Trim(), controlText.Trim(), StringComparison.CurrentCultureIgnoreCase)
                select control).ToList();
            return targetControls;
        }

        public static string GetWindowClassName(IntPtr hwnd)
        {
            StringBuilder s = new();
            s.EnsureCapacity(STRING_BUFFER_SIZE);
            var numberOfChars = Win32Api.GetClassName(hwnd, s, STRING_BUFFER_SIZE);
            return s.ToString();
        }
    }

    public class WindowInfo
    {
        public IntPtr Hwnd            { get; set; }
        public bool   IsWindowEnabled { get; set; }
        public IntPtr OwnerWindow     { get; set; }
        public IntPtr ParentWindow    { get; set; }
        public IntPtr DialogControlId { get; set; }
        public string WindowClassName { get; set; }
        public string WindowText      { get; set; }

        public WindowInfo(IntPtr hwnd)
        {
            Hwnd            = hwnd;
            IsWindowEnabled = Win32Api.IsWindowEnabled(hwnd);
            OwnerWindow     = Win32Api.GetOwnerWindow(hwnd);
            ParentWindow    = Win32Api.GetParent(hwnd);
            DialogControlId = Win32Api.GetDialogControlId(hwnd);
            WindowClassName = UIAutomationUtil.GetWindowClassName(hwnd);
            WindowText      = UIAutomationUtil.GetWindowText(hwnd);
        }
    }
}
