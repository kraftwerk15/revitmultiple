﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace RevitFileCore
{
    public class MonitorProcess
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static bool IsProcessResponding(Process process)
        {
            bool isResponding = false;
            try
            {
                isResponding = process.Responding;
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return isResponding;
        }

        private MonitorProcess(Process process, Action monitoringAction, double monitorIntervalInSeconds,
            // ReSharper disable once IdentifierTypo
            int unresponsiveThreshholdInSeconds)
        {
            bool isResponding = true;
            DateTime unresponsiveStartTime = DateTime.MinValue;
            bool haveNotifiedBeginUnresponsive = false;
            process.Refresh();
            while (!process.HasExited)
            {
                bool wasResponding = isResponding;
                isResponding = IsProcessResponding(process);
                if (wasResponding && !isResponding)
                {
                    // responsive -> unresponsive
                    unresponsiveStartTime = TimeUtil.GetDateTimeNow();
                    haveNotifiedBeginUnresponsive = false;
                }
                else if (isResponding && !wasResponding)
                {
                    // unresponsive -> responsive
                    if (haveNotifiedBeginUnresponsive)
                    {
                        // notify end of unresponsiveness
                        OnEndUnresponsive(TimeUtil.GetSecondsElapsedSince(unresponsiveStartTime));
                        haveNotifiedBeginUnresponsive = false;
                    }
                }
                else if (!isResponding && !wasResponding)
                {
                    // continuing unresponsiveness
                    if (!haveNotifiedBeginUnresponsive)
                    {
                        // notify unresponsiveness beyond threshold
                        if (TimeUtil.GetSecondsElapsedSince(unresponsiveStartTime) >= unresponsiveThreshholdInSeconds)
                        {
                            OnBeginUnresponsive();
                            haveNotifiedBeginUnresponsive = true;
                        }
                    }
                }
                ThreadUtil.SleepForSeconds(Convert.ToInt32(monitorIntervalInSeconds));
                Application.DoEvents();
                monitoringAction();
                process.Refresh();
            }
            // TODO check this
            // Was notified of beginning of unresponsiveness, therefore need to notify end of unresponsiveness.
            if (!haveNotifiedBeginUnresponsive) return;
            OnEndUnresponsive(TimeUtil.GetSecondsElapsedSince(unresponsiveStartTime));
            // TODO also check this
            haveNotifiedBeginUnresponsive = false;
        }

        private const double MonitorIntervalInSeconds = 0.25;

        // ReSharper disable once IdentifierTypo
        private const int UnresponsiveThreshholdInSeconds = 10;

        private const string RevitBusyHandlerPrefix = "[ REVIT BUSY MONITOR ]";

        private static void OnBeginUnresponsive()
        {
            Logger.Info("Revit process appears to be busy or unresponsive...");
        }

        private static void OnEndUnresponsive(int unresponsiveTimeInSeconds)
        {
            Logger.Info("Revit process appeared busy or unresponsive for about " + unresponsiveTimeInSeconds + " seconds.");
        }

        public static void MonitorHostRevitProcess(Process hostRevitProcess, Action monitoringAction)
        {
            Logger.Info("Monitoring host Revit process (PID: " + hostRevitProcess.Id + ")");
            string busyOutput = GlobalTestMode.PrefixedOutputForGlobalTestMode(RevitBusyHandlerPrefix);
            _ = new MonitorProcess(hostRevitProcess, monitoringAction, MonitorIntervalInSeconds, UnresponsiveThreshholdInSeconds);

            Logger.Info("Revit process (PID: " + hostRevitProcess.Id + ") has exited!");
            // TODO: do something with last pending read line task if it exists?
            //return;
        }
    }
}
