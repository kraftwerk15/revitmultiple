﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace RevitFileCore
{
    public class RevitTaskDialog
    {
        internal static RevitTaskDialogId TryParse(string something, bool somethingelse)
        {




            return RevitTaskDialogId.Unknown;
        }

    }



    public enum RevitTaskDialogId
    {
        [Description("This DialogID is unknown.")]
        Unknown = -1,
        DialogLoadCanceled = 4,
        TaskDialogFileNameInUse = 7,
        LocationPositionChanged = 89,
        InvalidPrinterSettings = 19,
        NoValidElementsInModelSpace = 20
    }


}
