﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.UI.Events;

namespace RevitFileCore
{
    public class RevitDialogDetection
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private const string RevitDialogMessageHandlerPrefix = "[ REVIT DIALOG BOX HANDLER ]";
        private const string ModelUpgradeWindowTitle = "Model Upgrade";
        private const string LoadLinkWindowTitle = "Load Link";
        private const string ChangesNotSavedTitle = "Changes Not Saved";
        private const string CloseProjectWithoutSavingTitle = "Close Project Without Saving";
        private const string SaveFileWindowTitle = "Save File";
        private const string EditableElementsTitle = "Editable Elements";
        private const string AutodeskCustomerInvolvementProgramTitle = "Autodesk Customer Involvement Program";
        private static readonly List<string> OpeningWorksetsTitles = new() {
            "Worksets",
            "Opening Worksets"
        };

        // ReSharper disable once IdentifierTypo
        private const string DirectuiClassName = "DirectUIHWND";
        // ReSharper disable once IdentifierTypo
        private const string CtrlnotifysinkClassName = "CtrlNotifySink";
        private const string ButtonClassName = "Button";
        private const string StaticControlClassName = "Static";
        private const string CloseButtonText = "Close";
        private const string OkButtonText = "OK";
        private const string NoButtonText = "No";
        private const string YesButtonText = "Yes";
        private const string AlwaysLoadButtonText = "Always Load";
        private const string CancelLinkButtonText = "Cancel Link";
        private const string DoNotSaveTheProjectText = "Do not save the project";
        private const string RelinquishAllElementsAndWorksetsText = "Relinquish all elements and worksets";
        private const string RelinquishElementsAndWorksetsText = "Relinquish elements and worksets";
        private static readonly List<bool> HaveReportedBatchRvtErrorWindowDetection = new() {
            false
        };

        public class RevitDialogInfo
        {
            internal WindowInfo       Window { get; set; }
            internal  List<WindowInfo> Win32Buttons;
            internal  List<WindowInfo> Buttons { get; }
            public RevitDialogInfo(IntPtr dialogHwnd)
            {
                WindowInfo buttonInfo;
                Window = new WindowInfo(dialogHwnd);
                Win32Buttons = new List<WindowInfo>();
                Buttons = new List<WindowInfo>();
                //Win32Api.EnumChildWindows(dialogHwnd);

                foreach (IntPtr win32Button in Win32Api.GetAllChildrenWindowHandles(dialogHwnd, ButtonClassName))
                {
                    buttonInfo = new WindowInfo(win32Button);
                    Win32Buttons.Add(buttonInfo);
                }
                foreach (IntPtr directUI in Win32Api.GetAllChildrenWindowHandles(dialogHwnd, DirectuiClassName))
                {
                    foreach (IntPtr ctrlNotifySink in Win32Api.GetAllChildrenWindowHandles(directUI, CtrlnotifysinkClassName))
                    {
                        foreach (IntPtr button in Win32Api.GetAllChildrenWindowHandles(ctrlNotifySink, ButtonClassName))
                        {
                            buttonInfo = new WindowInfo(button);
                            Buttons.Add(buttonInfo);
                        }
                    }
                }
                return;
            }
        }

        // ReSharper disable once MemberCanBeMadeStatic.Global
        // ReSharper disable once CA1822
        public void ControlDialogBox(DialogBoxShowingEventArgs e)
        {
            Logger.Info("Revit is displaying a Dialog Box with an id : "+ e.DialogId + ". Attempting to dismiss.");
            Logger.Debug("Preparing to cast to TaskDialogShowingEventArgs");
            TaskDialogShowingEventArgs e2       = e as TaskDialogShowingEventArgs;
            RevitTaskDialogId          dialogId = RevitTaskDialogId.Unknown;
            string                     s        = string.Empty;

            //This Dialog is a TaskDialog
            if (null != e2)
            {
                s = $", dialog id {e2.DialogId}, message '{e2.Message}'";

                bool isConfirm = e2.DialogId.Equals("TaskDialog_Save_File");

                if (isConfirm)
                {
                    //e2.OverrideResult((int)WinForms.DialogResult.Yes);

                    s += ", auto-confirmed.";
                }
            }

            //Debug.Print("DialogBoxShowing: help id {0}, cancellable {1}{2}", e.HelpId, e.Cancellable ? "Yes" : "No", s);

            if (e2 != null)
            {
                if (RevitTaskDialog.TryParse(e.DialogId, false) != RevitTaskDialogId.Unknown)
                { // log any unhandled dialogs in DEBUG } // These are dialog boxes we always want to handle if (tde.DialogId == string.Empty) {
                    if (e2.Message.EndsWith("print settings will be used.")) { _=new RevitTaskDialogHandler(e2, RevitTaskDialogId.InvalidPrinterSettings); return; }
                    if (e2.Message.StartsWith("Import detected no valid elements in the file's Model space.")) { _=new RevitTaskDialogHandler(e2, RevitTaskDialogId.NoValidElementsInModelSpace); return; }
                    if (e2.Message.ToUpper().Equals("LOAD CANCELLED")) { _=new RevitTaskDialogHandler(e2, RevitTaskDialogId.DialogLoadCanceled); return; }
                }
                //if (e.DialogId.Equals(Enum<RevitTaskDialogId>.GetName((int)RevitTaskDialogId.TaskDialogFileNameInUse))) { return; }
                //if (e.DialogId.Equals(Enum<RevitTaskDialogId>.GetName((int)RevitTaskDialogId.LocationPositionChanged)))
                //{
                //    //e.OverrideResult(new LocationPositionChanged(LocationPositionChanged.DoNotSave).OverrideResult); return;
                //}
                _=new RevitTaskDialogHandler(e, dialogId);
            }
            else
            {
                if (e is not MessageBoxShowingEventArgs mbe) return; // nothing here right now } else {
                string taskDialogIdAsString = Enum.GetName(typeof(RevitTaskDialogId), e.DialogId); if (string.IsNullOrEmpty(taskDialogIdAsString)) { return; }
                if (RevitTaskDialog.TryParse(taskDialogIdAsString, false) != RevitTaskDialogId.Unknown) { return; }
                _=new RevitTaskDialogHandler(e, dialogId);
            }
        }

        // ReSharper disable once ParameterTypeCanBeEnumerable.Global
        private static void SendButtonClick(List<WindowInfo> buttons)
        {
            List<IntPtr> wrap = buttons.Select(k => k.Hwnd).ToList();
            SendButtonClick(wrap);
        }
        public static void SendButtonClick(List<IntPtr> buttons)
        {
            List<IntPtr> okButtons         = UIAutomationUtil.FilterControlsByText(buttons, OkButtonText);
            List<IntPtr>    closeButtons      = UIAutomationUtil.FilterControlsByText(buttons, CloseButtonText);
            List<IntPtr>    noButtons         = UIAutomationUtil.FilterControlsByText(buttons, NoButtonText);
            List<IntPtr> alwaysLoadButtons = UIAutomationUtil.FilterControlsByText(buttons, AlwaysLoadButtonText);
            IntPtr    targetButton      = IntPtr.Zero;
            if (okButtons.Count == 1)
            {
                targetButton = okButtons[0];
            }
            else if (closeButtons.Count == 1)
            {
                targetButton = closeButtons[0];
            }
            else if (noButtons.Count == 1)
            {
                targetButton = noButtons[0];
            }
            else if (alwaysLoadButtons.Count == 1)
            {
                targetButton = alwaysLoadButtons[0];
            }
            else
            {
                Logger.Warn("WARNING: Could not find suitable button to click.");
            }

            if (targetButton == IntPtr.Zero) return;
            string targetButtonText = UIAutomationUtil.GetButtonText(targetButton);
                
            Logger.Info("Sending button click to '" + targetButtonText + "' button...");
            Win32Api.ClickControl(targetButton, targetButtonText);
                
            Logger.Info("...sent.");
        }

        // ReSharper disable once ParameterTypeCanBeEnumerable.Global
        // ReSharper disable once ParameterTypeCanBeEnumerable.Local
        private static void DismissRevitDialogBox(string title, List<WindowInfo> buttons,
            string                                      targetButtonText)
        {
            List<IntPtr> targetButtons = buttons.Select(k => k.Hwnd).ToList();
            DismissRevitDialogBox(title, targetButtons, targetButtonText);
        }

        private static void DismissRevitDialogBox(string title, List<IntPtr> buttons, string targetButtonText)
        {
            IntPtr       targetButton;
            List<IntPtr> targetButtons = UIAutomationUtil.FilterControlsByText(buttons, targetButtonText);
            if (targetButtons.Count == 1)
            {
                targetButton = targetButtons[0];
            }
            else
            {
                Logger.Warn("WARNING: Could not find suitable button to click for '" + title + "' dialog box!");
                targetButton = IntPtr.Zero;
                foreach (string buttonText in buttons.Select(UIAutomationUtil.GetButtonText))
                {
                    Logger.Info("Button: '" + buttonText + "'");
                }
            }

            if (targetButton == IntPtr.Zero) return;
            targetButtonText = UIAutomationUtil.GetButtonText(targetButton);
                
            Logger.Info("Sending button click to '" + targetButtonText + "' button...");
            Win32Api.ClickControl(targetButton, targetButtonText);
            Logger.Info("...sent.");
        }

        public static void DismissCheekyRevitDialogBoxes(int revitProcessId)
        {
            string    output         = GlobalTestMode.PrefixedOutputForGlobalTestMode(RevitDialogMessageHandlerPrefix);
            List<WindowInfo> enabledDialogs = UIAutomationUtil.GetEnabledDialogsInfo(revitProcessId);
            if (enabledDialogs.Count <= 0) return;
            foreach (WindowInfo enabledDialog in enabledDialogs)
            {
                RevitDialogInfo                   revitDialog  = new(enabledDialog.Hwnd);
                List<WindowInfo> buttons      = revitDialog.Buttons;
                List<WindowInfo> win32Buttons = revitDialog.Win32Buttons;
                if (enabledDialog.WindowText == ModelUpgradeWindowTitle && buttons.Count == 0)
                {
                }
                else if (enabledDialog.WindowText == LoadLinkWindowTitle && win32Buttons.Count == 1 
                                                                         && UIAutomationUtil.GetButtonText(win32Buttons[0].Hwnd) == CancelLinkButtonText)
                {
                }
                else
                {
                    string staticControlText;
                    List<WindowInfo> staticControls;
                    if (enabledDialog.WindowText == Error.BatchRvtErrorWindowTitle)
                    {
                        // Report dialog detection but do nothing for BatchRvt error message windows.
                        if (HaveReportedBatchRvtErrorWindowDetection[0]) continue;
                        Logger.Warn("WARNING: Revit Batch Processor error window detected! Processing has halted!");
                        HaveReportedBatchRvtErrorWindowDetection[0] = true;
                        staticControls = (from hwnd in Win32Api.GetAllChildrenWindowHandles(enabledDialog.Hwnd, StaticControlClassName)
                            select new WindowInfo(hwnd)).ToList();
                        if (staticControls.Count <= 0) continue;
                        Logger.Info("Dialog has the following static control text:");
                        foreach (WindowInfo staticControl in staticControls)
                        {
                            staticControlText = staticControl.WindowText;
                            if (!string.IsNullOrWhiteSpace(staticControlText))
                            {
                                Logger.Info(staticControlText);
                            }
                        }
                    }
                    else if (enabledDialog.WindowText == ChangesNotSavedTitle && buttons.Count == 4)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");
                        DismissRevitDialogBox(enabledDialog.WindowText, buttons, DoNotSaveTheProjectText);
                    }
                    else if (enabledDialog.WindowText == CloseProjectWithoutSavingTitle && buttons.Count == 3)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");
                        DismissRevitDialogBox(enabledDialog.WindowText, buttons, RelinquishAllElementsAndWorksetsText);
                    }
                    else if (enabledDialog.WindowText == SaveFileWindowTitle && buttons.Count == 3)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");
                        DismissRevitDialogBox(enabledDialog.WindowText, buttons, NoButtonText);
                    }
                    else if (enabledDialog.WindowText == EditableElementsTitle && buttons.Count == 3)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");
                        DismissRevitDialogBox(enabledDialog.WindowText, buttons, RelinquishElementsAndWorksetsText);
                    }
                    else if (new List<object> {
                        "Revit",
                        string.Empty
                    }.Contains(enabledDialog.WindowText) && buttons.Count == 0 && win32Buttons.Count > 0)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");

                        // ReSharper disable once IdentifierTypo
                        staticControls = (from hwnd in Win32Api.GetAllChildrenWindowHandles(enabledDialog.Hwnd, StaticControlClassName)
                            select new WindowInfo(hwnd)).ToList();
                        if (staticControls.Count > 0)
                        {
                            Logger.Info("Dialog has the following static control text:");
                            foreach (WindowInfo staticControl in staticControls)
                            {
                                staticControlText = staticControl.WindowText;
                                if (!string.IsNullOrWhiteSpace(staticControlText))
                                {
                                    Logger.Info(staticControlText);
                                }
                            }
                        }
                        SendButtonClick(win32Buttons);
                    }
                    else if (enabledDialog.WindowText == AutodeskCustomerInvolvementProgramTitle && buttons.Count == 0 && win32Buttons.Count > 0)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");
                        Logger.Info("Sending close message...");
                        Win32Api.SendCloseMessage(enabledDialog.Hwnd);
                        Logger.Info("...sent.");
                    }
                    else if (OpeningWorksetsTitles.Contains(enabledDialog.WindowText) && buttons.Count == 0 && win32Buttons.Count > 0)
                    {
                        Logger.Info("'" + enabledDialog.WindowText + "' dialog box detected.");
                        SendButtonClick(win32Buttons);
                    }
                    else
                    {
                        Logger.Info("Revit dialog box detected!");
                        Logger.Info("Dialog box title: '" + enabledDialog.WindowText + "'");
                        buttons = buttons.Count > 0 ? buttons : win32Buttons;
                        foreach (WindowInfo button in buttons)
                        {
                            string buttonText = UIAutomationUtil.GetButtonText(button.Hwnd);
                            Logger.Info("Button: '" + buttonText + "'");
                        }
                        SendButtonClick(buttons);
                    }
                }
            }
        }
    }
}
