﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace RevitFileCore
{
    public class TestModeUtil
    {
        private static readonly NLog.Logger Logger                      = NLog.LogManager.GetCurrentClassLogger();
        public static           string      TestModeDataSessionId = "sessionId";

        public static string TestModeDataRevitProcessIds = "revitProcessIds";

        public class TestMode
        {
            private string TestModeFolderPath { get; set; }
            public TestMode(string testModeFolderPath)
            {
                TestModeFolderPath = testModeFolderPath;
            }

            public void CreateTestModeFolder()
            {
                if (!string.IsNullOrWhiteSpace(TestModeFolderPath))
                {
                    Directory.CreateDirectory(TestModeFolderPath);
                }
                if (!Directory.Exists(TestModeFolderPath))
                {
                    throw new Exception("ERROR: failed to create the test mode folder!");
                }
            }

            public string GetTestModeDataFilePath()
            {
                return Path.Combine(TestModeFolderPath, "test_mode_data.json");
            }

            public virtual JObject GetTestModeData()
            {
                JObject testModeData = null;
                string testModeDataFilePath = GetTestModeDataFilePath();
                if (File.Exists(testModeDataFilePath))
                {
                    testModeData = JsonSerialization.DeserializeFromJson(File.ReadAllText(testModeDataFilePath));
                }
                else
                {
                    testModeData = new JObject
                    {
                        [TestModeDataSessionId] = null, [TestModeDataRevitProcessIds] = new JArray()
                    };
                }
                return testModeData;
            }

            public void SaveTestModeData(JObject testModeData)
            {
                string testModeDataString = JsonSerialization.SerializeToJson(testModeData, prettyPrint: true);
                string testModeDataFilePath = GetTestModeDataFilePath();
                File.WriteAllText(testModeDataFilePath, testModeDataString);
            }

            public virtual void WithTestModeData(Action<JObject> testModeDataAction)
            {
                var testModeData = GetTestModeData();
                testModeDataAction(testModeData);
                SaveTestModeData(testModeData);
            }

            public virtual void ExportRevitProcessId(int revitProcessId)
            {
                void Action(JObject testModeData)
                {
                    JToken       revitProcessIds = testModeData[TestModeDataRevitProcessIds];
                    List<string> k               = revitProcessIds.ToObject<List<string>>();
                    k?.Add(revitProcessId.ToString());
                }

                WithTestModeData(Action);
            }

            public virtual void ExportSessionId(string sessionId)
            {
                void Action(JObject testModeData)
                {
                    testModeData[TestModeDataSessionId] = sessionId;
                }

                WithTestModeData(Action);
            }
        }

        public static string GetSessionId(JObject testModeData)
        {
            JToken rJObject = testModeData.GetValue("TestModeDataSessionId");
            return rJObject?.ToString();
        }

        public static List<string> GetRevitProcessIds(JObject testModeData)
        {
            List<string> processIds = new();
            JToken       jToken     = testModeData.GetValue("TestModeDataRevitProcessIds");
            foreach (var k in jToken)
            {
                processIds.Add(k.ToString());
            }

            return processIds;
            //return (from revitProcessId in testModeData[TestModeDataRevitProcessIds]
            //        select JsonSerialization.GetValueFromJValue(testModeData, revitProcessId)).ToList();
        }
    }
}
