﻿using System;
using Newtonsoft.Json;

namespace RevitFileCore
{
    public class SnapshotData
    {
        
        public bool IsCloudModel { get; set; }
        public string CloudProjectId { get; set; }
        public string CloudModelId { get; set; }
        public string ProjectFolderName { get; set; }
        public string ModelFolder { get; set; }
        public string ModelName { get; set; }
        public DateTime FileLastModified { get; set; }
        public long ModelFileSize { get; set; }
        public string ModelRevitVersion { get; set; }
        public string ModelRevitVersionDetails { get; set; }
        public DateTime SnapshotStartTime { get; set; }
        public DateTime SnapshotEndTime { get; set; }
        public string SessionId { get; set; }
        public string SnapshotFolder { get; set; }
        public object SnapshotError { get; set; }
        public readonly string UserName = Environment.UserName;
        public readonly string MachineName = Environment.MachineName;
        public readonly object GatewayAddresses = NetworkUtil.GetGatewayAddresses();
        public readonly object IpAddresses = NetworkUtil.GetIPAddresses();
        public string RevitJournalFile { get; set; }
    }
}