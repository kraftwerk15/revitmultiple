﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.Linq;

namespace RevitFileCore
{
    public static class CommandLineUtil
    {
        private static readonly NLog.Logger Logger             = NLog.LogManager.GetCurrentClassLogger();
        public const            string      OptionSwitchPrefix = "--";

        private static int FindArgOptionSwitch(string[] args, string optionSwitch)
        {
            return Array.FindIndex(args, 1, arg => arg.ToLower() == (OptionSwitchPrefix + optionSwitch.ToLower()));
        }

        private static string GetArgOptionValue(string[] args, string optionSwitch)
        {
            string optionValue = null;

            int optionSwitchIndex = FindArgOptionSwitch(args, optionSwitch);

            // ReSharper disable once ExpressionIsAlwaysNull
            if (optionSwitchIndex == -1) return optionValue;
            // ReSharper disable once ExpressionIsAlwaysNull
            if ((optionSwitchIndex + 1) >= args.Length) return optionValue;
            optionValue = args[optionSwitchIndex + 1];

            if (optionValue.StartsWith(OptionSwitchPrefix))
            {
                optionValue = null;
            }

            return optionValue;
        }

        public static bool CommandLineHasArguments()
        {
            string[] args = Environment.GetCommandLineArgs();

            return (args.Length > 1);
        }

        public static string GetCommandLineOption(string optionSwitch, bool expectOptionValue = true)
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length <= 1) return null;
            int optionSwitchIndex = FindArgOptionSwitch(args, optionSwitch);

            if (optionSwitchIndex == -1) return null;
            string optionValue = expectOptionValue ? GetArgOptionValue(args, optionSwitch) : string.Empty;

            return optionValue;
        }

        public static IEnumerable<string> GetAllCommandLineOptionSwitches()
        {
            IEnumerable<string> allOptionSwitches = Enumerable.Empty<string>();

            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1)
            {
                allOptionSwitches = args.Where(arg => arg.StartsWith(OptionSwitchPrefix));
            }

            return allOptionSwitches.Select(optionSwitch => optionSwitch.Substring(OptionSwitchPrefix.Length).ToLower()).ToList();
        }

        public static bool HasCommandLineOption(string optionSwitch, bool expectOptionValue = true)
        {
            return GetCommandLineOption(optionSwitch, expectOptionValue) != null;
        }

        public static void WaitForSpaceBarKeyPress()
        {
            while (true)
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.Spacebar)
                {
                    break;
                }
            }
        }

        public static object IsInputRedirected()
        {
            return Console.IsInputRedirected;
        }

        public static string? ReadLine()
        {
            return Console.ReadLine();
        }

        public static List<string> ReadLines()
        {
            List<string> lines = new();
            string?      line  = Console.ReadLine();
            while (line != null)
            {
                lines.Append(line);
                line = Console.ReadLine();
            }
            return lines;
        }
    }
}
