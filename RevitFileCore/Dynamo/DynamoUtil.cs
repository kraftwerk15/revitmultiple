﻿#region Using
using System;
using XmlDocument = System.Xml.XmlDocument;
using XmlException = System.Xml.XmlException;
using File = System.IO.File;
using Path = System.IO.Path;
using IOException = System.IO.IOException;
using DynamoRevit = Dynamo.Applications.DynamoRevit;
using DynamoRevitCommandData = Dynamo.Applications.DynamoRevitCommandData;
using JournalKeys = Dynamo.Applications.JournalKeys;
using System.Collections.Generic;
using System.Xml;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Newtonsoft.Json.Linq;
#endregion

namespace RevitFileCore.Dynamo
{
    public class DynamoUtil
    {
        private const string DynamoRuntypeAutomatic = "Automatic";
        private const string DynamoRuntypeManual = "Manual";
        private const string DynamoWorkspaceXmlNode = "Workspace";
        private const string DynamoJObjectView = "View";
        private const string DynamoJObjectDynamo = "Dynamo";
        private const string DynamoRuntypeAttribute = "RunType";
        private const string DynamoHasRunWithoutCrashAttribute = "HasRunWithoutCrash";
        private const string DynamoAttributeXmlValueTrue = "True";
        private const string DynamoAttributeXmlValueFalse = "False";

        /// <summary>
        /// Provides the foundation for operations on an XML file.
        /// </summary>
        /// <param name="xmlDocumentFilePath">A string to the file path of the XML file.</param>
        /// <param name="action">The Action to complete on the file.</param>
        /// <returns>The result of the action, otherwise null for exceptions.</returns>
        private static object WithLoadedXmlDocument(string xmlDocumentFilePath, Func<XmlDocument, object> action)
        {
            object result;
            XmlDocument doc = new();
            try
            {
                doc.Load(xmlDocumentFilePath);
                result = action(doc);
            }
            catch (XmlException)
            {
                result = null;
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }

        private static object WithTextFileJsonObject(string textFilePath, Func<JObject, object> action)
        {
            object result = null;
            try
            {
                var textFileContents = TextFileUtil.ReadFromTextFile(textFilePath);
                try
                {
                    JObject jobject = JsonSerialization.DeserializeFromJson(textFileContents.ToString());
                    result = action(jobject);
                }
                catch
                {
                    result = null;
                }
            }
            catch (IOException)
            {
                result = null;
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }

        public static string WithDynamoWorkspaceJsonObject(string dynamoScriptFilePath, Func<JObject, JToken, object> action, bool writeBackToFile = false)
        {
            // ReSharper disable once ConvertToLocalFunction
            Func<JObject, string> jObjectAction = jObject =>
            {
                //Gets the "View" object
                JToken viewJObject = jObject[DynamoJObjectView];
                //Gets the "Dynamo" object
                JToken dynamoJObject = viewJObject[DynamoJObjectDynamo];
                //Inside the "Dynamo" object, run some type of action.
                object result = action(jObject, dynamoJObject);
                if (!writeBackToFile) return result.ToString();
                string serializedJObjectText = JsonSerialization.SerializeToJson(jObject, true);
                TextFileUtil.WriteToTextFile(dynamoScriptFilePath, serializedJObjectText);
                return result.ToString();
            };
            var result = WithTextFileJsonObject(dynamoScriptFilePath, jObjectAction);
            return result.ToString();
        }

        /// <summary>
        /// Returns if the Dynamo Graph File Path is JSON
        /// </summary>
        /// <param name="dynamoScriptFilePath">A string to the File Path.</param>
        /// <returns>A bool if the File Path is JSON</returns>
        private static bool IsDynamoWorkspaceJsonFile(string dynamoScriptFilePath)
        {
            static object Action(JObject jobject, JToken dynamoJObject) => dynamoJObject != null;
            object response = WithDynamoWorkspaceJsonObject(dynamoScriptFilePath, Action);
            return response.Equals(DynamoAttributeXmlValueTrue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dynamoScriptFilePath"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private static object WithDynamoWorkspaceXmlNode(string dynamoScriptFilePath, Func<XmlDocument, XmlNode, object> action)
        {
            object DocAction(XmlDocument doc)
            {
                XmlNode dynamoWorkspaceXmlNode = doc[DynamoWorkspaceXmlNode];
                object result = action(doc, dynamoWorkspaceXmlNode);
                return result;
            }
            object response = WithLoadedXmlDocument(dynamoScriptFilePath, DocAction);
            return response;
        }

        /// <summary>
        /// Returns if the Dynamo Graph File Path is XML
        /// </summary>
        /// <param name="dynamoScriptFilePath">A string to the File Path.</param>
        /// <returns>A bool if the File Path is XML</returns>
        private static bool IsDynamoWorkspaceXmlFile(string dynamoScriptFilePath)
        {
            static object Action(XmlDocument doc, XmlNode dynamoWorkspaceXmlNode)
            {
                return dynamoWorkspaceXmlNode != null;
            }
            object response = WithDynamoWorkspaceXmlNode(dynamoScriptFilePath, Action);
            return response.Equals(DynamoAttributeXmlValueTrue);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dynamoScriptFilePath">A string to the File Path.</param>
        /// <param name="runType">A string value of the Run Type.</param>
        /// <returns>A string value of the modified Run Type.</returns>
        private static object SetDynamoScriptRunType(string dynamoScriptFilePath, string runType)
        {
            string prevRunType = null;
            object ActionXml(XmlDocument doc, XmlNode dynamoWorkspaceXmlNode)
            {
                XmlAttribute? dynamoRunTypeAttribute = dynamoWorkspaceXmlNode.Attributes?[DynamoRuntypeAttribute];
                if (dynamoRunTypeAttribute == null) return null;
                string prevRunTypeValue = dynamoRunTypeAttribute.Value;
                dynamoRunTypeAttribute.Value = runType;
                XmlAttribute? dynamoHasRunWithoutCrashAttribute = dynamoWorkspaceXmlNode.Attributes[DynamoHasRunWithoutCrashAttribute];
                if (dynamoHasRunWithoutCrashAttribute != null)
                {
                    dynamoHasRunWithoutCrashAttribute.Value = DynamoAttributeXmlValueTrue;
                }
                doc.Save(dynamoScriptFilePath);
                return prevRunTypeValue;
            }

            object Action(object jobject, JToken dynamoJObject)
            {
                string prevRunTypeJson = (string) dynamoJObject[DynamoRuntypeAttribute];
                if (string.IsNullOrEmpty(prevRunTypeJson)) return null;
                dynamoJObject[DynamoRuntypeAttribute] = runType;
                JToken? dynamoHasRunWithoutCrashJValue = dynamoJObject[DynamoHasRunWithoutCrashAttribute];
                if (dynamoHasRunWithoutCrashJValue != null)
                {
                    dynamoJObject[DynamoHasRunWithoutCrashAttribute] = DynamoAttributeXmlValueTrue;
                }

                return prevRunType;
            }

            object temp = null;
            
            if (IsDynamoWorkspaceXmlFile(dynamoScriptFilePath))
            {
                temp = WithDynamoWorkspaceXmlNode(dynamoScriptFilePath, ActionXml);
            }
            else if (IsDynamoWorkspaceJsonFile(dynamoScriptFilePath))
            {
                temp = WithDynamoWorkspaceJsonObject(dynamoScriptFilePath, (Func<object, JToken, object>) Action, true);
            }
            switch (temp)
            {
                case null:
                    return null;
                case DynamoRuntypeAutomatic:
                case DynamoRuntypeManual:
                    prevRunType = temp.ToString();
                    break;
            }

            return prevRunType;
        }

        public static string GetDynamoScriptRunType(string dynamoScriptFilePath)
        {
            object temp = null;
            string runType = null;
            if (IsDynamoWorkspaceXmlFile(dynamoScriptFilePath))
            {
                Func<XmlDocument, XmlNode, string> action = (doc, dynamoWorkspaceXmlNode) =>
                {
                    string runType = dynamoWorkspaceXmlNode.Attributes[DynamoRuntypeAttribute].Value;
                    return runType;
                };
                temp = WithDynamoWorkspaceXmlNode(dynamoScriptFilePath, action);
            }
            else if (IsDynamoWorkspaceJsonFile(dynamoScriptFilePath))
            {
                Func<JObject, JToken, string> action = (jobject, dynamoJObject) =>
                {
                    JToken runTypeJson = (string) dynamoJObject[DynamoRuntypeAttribute];
                    return runTypeJson.ToString();
                };
                temp = WithDynamoWorkspaceJsonObject(dynamoScriptFilePath, action);
            }
            switch (temp)
            {
                case null:
                    return null;
                case DynamoRuntypeAutomatic:
                case DynamoRuntypeManual:
                    runType = temp.ToString();
                    break;
            }
            return runType;
        }

        private static string GetDynamoScriptHasRunWithoutCrash(string dynamoScriptFilePath)
        {
            static string ActionXml(XmlDocument doc, XmlNode dynamoWorkspaceXmlNode)
            {
                if (dynamoWorkspaceXmlNode.Attributes == null) return string.Empty;
                string hasRunWithoutCrashXml = dynamoWorkspaceXmlNode.Attributes[DynamoHasRunWithoutCrashAttribute]?.Value;
                return hasRunWithoutCrashXml;
            }

            static string ActionJson(JObject jobject, JToken dynamoJObject)
            {
                string hasRunWithoutCrashJson = (string) dynamoJObject[DynamoHasRunWithoutCrashAttribute];
                return hasRunWithoutCrashJson;
            }

            string hasRunWithoutCrash = string.Empty;
            if (IsDynamoWorkspaceXmlFile(dynamoScriptFilePath))
            {
                hasRunWithoutCrash = WithDynamoWorkspaceXmlNode(dynamoScriptFilePath, (Func<XmlDocument, XmlNode, string>) ActionXml).ToString();
            }
            else if (IsDynamoWorkspaceJsonFile(dynamoScriptFilePath))
            {
                hasRunWithoutCrash = WithDynamoWorkspaceJsonObject(dynamoScriptFilePath, (Func<JObject, JToken, string>) ActionJson);
            }
            return hasRunWithoutCrash;
        }

        // NOTE: Dynamo requires an active UIDocument! The document must be active before executing this function.
        //       The Dynamo script must have been saved with the 'Automatic' run mode!
        private static Result ExecuteDynamoScriptInternal(UIApplication uiApp, string dynamoScriptFilePath, bool showUi = false)
        {
            string @JOURNAL_KEY__DYN_PATH;
            string @JOURNAL_KEY__SHOW_UI;
            string @JOURNAL_KEY__AUTOMATION_MODE;
            string dynamoScriptRunType = GetDynamoScriptRunType(dynamoScriptFilePath);
            if (dynamoScriptRunType == null)
            {
                throw new Exception("Could not determine the Run mode of this Dynamo script!");
            }
            if (dynamoScriptRunType != DynamoRuntypeAutomatic)
            {
                throw new Exception("The Dynamo script has Run mode set to '" + dynamoScriptRunType + "'. " +
                                    "It must be set to '" + DynamoRuntypeAutomatic +
                                    "' in order for Dynamo script automation to work.");
            }

            var hasRunWithoutCrash = GetDynamoScriptHasRunWithoutCrash(dynamoScriptFilePath);
            if (hasRunWithoutCrash != null)
            {
                if (hasRunWithoutCrash != DynamoAttributeXmlValueTrue && hasRunWithoutCrash.Equals("true"))
                {
                    throw new Exception("The Dynamo script has attribute '" + DynamoHasRunWithoutCrashAttribute +
                                        "' set to '" + hasRunWithoutCrash.ToString() + "'. " + "It must be set to '" +
                                        DynamoAttributeXmlValueTrue + "' in order for Dynamo script automation to work.");
                }
            }

            string revitVersionNumber = uiApp.Application.VersionNumber;
            switch (revitVersionNumber)
            {
                case "2015":
                    throw new Exception("Automation of Dynamo scripts is not supported in Revit 2015!");
                case "2016":
                    @JOURNAL_KEY__AUTOMATION_MODE = "dynAutomation";
                    @JOURNAL_KEY__SHOW_UI = "dynShowUI";
                    @JOURNAL_KEY__DYN_PATH = "dynPath";
                    break;
                default:
                    @JOURNAL_KEY__AUTOMATION_MODE = JournalKeys.AutomationModeKey;
                    @JOURNAL_KEY__SHOW_UI = JournalKeys.ShowUiKey;
                    @JOURNAL_KEY__DYN_PATH = JournalKeys.DynPathKey;
                    break;
            }

            DynamoRevitCommandData dynamoRevitCommandData = new()
            {
                Application = uiApp,
                JournalData = new Dictionary<string, string>
                {
                    {@JOURNAL_KEY__AUTOMATION_MODE, true.ToString()},
                    {@JOURNAL_KEY__SHOW_UI, showUi.ToString()},
                    {@JOURNAL_KEY__DYN_PATH, dynamoScriptFilePath}
                }
            };
            DynamoRevit dynamoRevit = new();
            Result externalCommandResult = dynamoRevit.ExecuteCommand(dynamoRevitCommandData);
            return externalCommandResult;
        }

        public static object ExecuteDynamoScript(UIApplication uiApp, string dynamoScriptFilePath, bool showUi = false)
        {
            object externalCommandResult = null;
            // NOTE: The temporary copy of the Dynamo script file is created in same folder as the original so
            // that any relative paths in the script won't break.
            string tempDynamoScriptFilePath =
                Path.Combine(Path.GetDirectoryName(dynamoScriptFilePath) ?? throw new InvalidOperationException(),
                    Path.GetRandomFileName());
            File.Copy(dynamoScriptFilePath, tempDynamoScriptFilePath);
            try
            {
                SetDynamoScriptRunType(tempDynamoScriptFilePath, DynamoRuntypeAutomatic);
                externalCommandResult = ExecuteDynamoScriptInternal(uiApp, tempDynamoScriptFilePath, showUi);
            }
            finally
            {
                File.Delete(tempDynamoScriptFilePath);
            }

            return externalCommandResult;
        }
    }
}