﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitFileCore
{
    public class ScriptEnvironment
    {
        public static string @BATCHRVT_SCRIPTS_FOLDER_PATH__ENVIRONMENT_VARIABLE_NAME = "BATCHRVT__SCRIPTS_FOLDER_PATH";

        public static string @SCRIPT_FILE_PATH__ENVIRONMENT_VARIABLE_NAME = @"BATCHRVT__SCRIPT_FILE_PATH";

        public static string @SCRIPT_DATA_FILE_PATH__ENVIRONMENT_VARIABLE_NAME = @"BATCHRVT__SCRIPT_DATA_FILE_PATH";

        public static string @PROGRESS_NUMBER__ENVIRONMENT_VARIABLE_NAME = @"BATCHRVT__PROGRESS_NUMBER";

        public static string @SCRIPT_OUTPUT_PIPE_HANDLE_STRING__ENVIRONMENT_VARIABLE_NAME = @"BATCHRVT__SCRIPT_OUTPUT_PIPE_HANDLE_STRING";

        public static string @BATCHRVT_PROCESS_UNIQUE_ID__ENVIRONMENT_VARIABLE_NAME = @"BATCHRVT__PROCESS_UNIQUE_ID";

        public static string @BATCHRVT_TEST_MODE_FOLDER_PATH__ENVIRONMENT_VARIABLE_NAME = @"BATCHRVT__TEST_MODE_FOLDER_PATH";

        public static string GetEnvironmentVariable(StringDictionary environmentVariables, string variableName)
        {
            return environmentVariables[variableName];
        }

        public static void SetEnvironmentVariable(StringDictionary environmentVariables, string variableName, string value)
        {
            environmentVariables[variableName] = value;
        }

        public static void SetBatchRvtScriptsFolderPath(StringDictionary environmentVariables, string batchRvtScriptsFolderPath)
        {
            SetEnvironmentVariable(environmentVariables, @BATCHRVT_SCRIPTS_FOLDER_PATH__ENVIRONMENT_VARIABLE_NAME, batchRvtScriptsFolderPath);
        }

        public static void SetScriptFilePath(StringDictionary environmentVariables, string scriptFilePath)
        {
            SetEnvironmentVariable(environmentVariables, @SCRIPT_FILE_PATH__ENVIRONMENT_VARIABLE_NAME, scriptFilePath);
        }

        public static void SetScriptDataFilePath(StringDictionary environmentVariables, string scriptDataFilePath)
        {
            SetEnvironmentVariable(environmentVariables, @SCRIPT_DATA_FILE_PATH__ENVIRONMENT_VARIABLE_NAME, scriptDataFilePath);
        }

        public static void SetProgressNumber(StringDictionary environmentVariables, int progressNumber)
        {
            SetEnvironmentVariable(environmentVariables, @PROGRESS_NUMBER__ENVIRONMENT_VARIABLE_NAME, progressNumber.ToString());
        }

        public static void SetScriptOutputPipeHandleString(StringDictionary environmentVariables, string scriptOutputPipeHandleString)
        {
            SetEnvironmentVariable(environmentVariables, @SCRIPT_OUTPUT_PIPE_HANDLE_STRING__ENVIRONMENT_VARIABLE_NAME, scriptOutputPipeHandleString);
        }

        public static void SetBatchRvtProcessUniqueId(StringDictionary environmentVariables, string batchRvtProcessUniqueId)
        {
            SetEnvironmentVariable(environmentVariables, @BATCHRVT_PROCESS_UNIQUE_ID__ENVIRONMENT_VARIABLE_NAME, batchRvtProcessUniqueId);
        }

        public static void SetTestModeFolderPath(StringDictionary environmentVariables, string testModeFolderPath)
        {
            SetEnvironmentVariable(environmentVariables, @BATCHRVT_TEST_MODE_FOLDER_PATH__ENVIRONMENT_VARIABLE_NAME, testModeFolderPath);
        }

        public static string GetBatchRvtScriptsFolderPath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, @BATCHRVT_SCRIPTS_FOLDER_PATH__ENVIRONMENT_VARIABLE_NAME);
        }

        public static string GetScriptFilePath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, @SCRIPT_FILE_PATH__ENVIRONMENT_VARIABLE_NAME);
        }

        public static string GetScriptDataFilePath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, @SCRIPT_DATA_FILE_PATH__ENVIRONMENT_VARIABLE_NAME);
        }

        public static int GetProgressNumber(StringDictionary environmentVariables)
        {
            string progressNumber = GetEnvironmentVariable(environmentVariables, @PROGRESS_NUMBER__ENVIRONMENT_VARIABLE_NAME);
            return Convert.ToInt32(progressNumber);
        }

        public static string GetScriptOutputPipeHandleString(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, @SCRIPT_OUTPUT_PIPE_HANDLE_STRING__ENVIRONMENT_VARIABLE_NAME);
        }

        public static string GetBatchRvtProcessUniqueId(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, @BATCHRVT_PROCESS_UNIQUE_ID__ENVIRONMENT_VARIABLE_NAME);
        }

        public static string GetTestModeFolderPath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, @BATCHRVT_TEST_MODE_FOLDER_PATH__ENVIRONMENT_VARIABLE_NAME);
        }

        public static StringDictionary InitEnvironmentVariables(StringDictionary environmentVariables, string batchRvtScriptsFolderPath, string scriptFilePath,
            string scriptDataFilePath, int progressNumber, string scriptOutputPipeHandleString, string batchRvtProcessUniqueId, string testModeFolderPath)
        {
            SetBatchRvtScriptsFolderPath(environmentVariables, batchRvtScriptsFolderPath);
            SetScriptFilePath(environmentVariables, scriptFilePath);
            SetScriptDataFilePath(environmentVariables, scriptDataFilePath);
            SetProgressNumber(environmentVariables, progressNumber);
            SetScriptOutputPipeHandleString(environmentVariables, scriptOutputPipeHandleString);
            SetBatchRvtProcessUniqueId(environmentVariables, batchRvtProcessUniqueId);
            SetTestModeFolderPath(environmentVariables, testModeFolderPath);
            return environmentVariables;
        }
        
        /// <summary>
        /// Gets Environment Variables previously set before the application started.
        /// </summary>
        /// <returns>A string dictionary of Environment Variables</returns>
        public static StringDictionary GetEnvironmentVariables()
        {
            StringDictionary environmentVariables = null;
            // NOTE: Have encountered (at least once) a NullReferenceException upon accessing the EnvironmentVariables property!
            try
            {
                environmentVariables = Process.GetCurrentProcess().StartInfo.EnvironmentVariables;
            }
            catch (NullReferenceException)
            {
                environmentVariables = null;
            }
            return environmentVariables;
        }
    }
}
