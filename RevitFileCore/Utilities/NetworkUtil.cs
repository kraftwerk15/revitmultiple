﻿using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace RevitFileCore
{
    public class NetworkUtil
    {
        public static bool IsSpecialAddress(IPAddress address) {
            return address.Equals(IPAddress.Any) || address.Equals(IPAddress.Broadcast) || address.Equals(IPAddress.IPv6Any) 
                   || address.Equals(IPAddress.IPv6Loopback) || address.Equals(IPAddress.IPv6None) || address.Equals(IPAddress.Loopback);
        }
        
        public static object GetGatewayAddresses() {
            return (from address in Network.GetGatewayAddresses().Where(a => !IsSpecialAddress(a)).Select(a => a.ToString()).Distinct().OrderBy(a => a)
                select address).ToList();
        }
        
        public static object GetIPAddresses() {
            return (from address in Network.GetIPAddresses().Where(a => !IsSpecialAddress(a)).Select(a => a.ToString()).Distinct().OrderBy(a => a)
                select address).ToList();
        }
    }

    public class Network
    {
        private static IEnumerable<IPAddress> GetGatewayAddresses(
            NetworkInterface networkInterface
        )
        {
            return networkInterface
                .GetIPProperties()
                .GatewayAddresses
                .Select(ga => ga.Address);
        }

        public static IEnumerable<IPAddress> GetGatewayAddresses()
        {
            var gatewayAddresses = Enumerable.Empty<IPAddress>();

            try
            {
                var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces()
                    .Where(ni => ni.NetworkInterfaceType != NetworkInterfaceType.Loopback);

                gatewayAddresses = networkInterfaces
                    .SelectMany(GetGatewayAddresses)
                    .ToList();
            }
            catch (Exception)
            {
                gatewayAddresses = Enumerable.Empty<IPAddress>();
            }

            return gatewayAddresses;
        }

        public static IEnumerable<IPAddress> GetIPAddresses(
            NetworkInterface networkInterface
        )
        {
            return networkInterface
                .GetIPProperties()
                .UnicastAddresses
                .Select(ua => ua.Address);
        }

        public static IEnumerable<IPAddress> GetIPAddresses()
        {
            var ipAddresses = Enumerable.Empty<IPAddress>();

            try
            {
                var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces()
                    .Where(ni => ni.NetworkInterfaceType != NetworkInterfaceType.Loopback);

                ipAddresses = networkInterfaces
                    .SelectMany(GetIPAddresses)
                    .ToList();
            }
            catch (Exception)
            {
                ipAddresses = Enumerable.Empty<IPAddress>();
            }

            return ipAddresses;
        }
    }
    
}