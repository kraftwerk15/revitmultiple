﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitFileCore
{
    public class ThreadUtil
    {
        public static object GetManagedThreadId()
        {
            int managedThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            return managedThreadId;
        }

        public static bool SleepForMilliseconds(int milliseconds)
        {
            return System.Threading.Thread.CurrentThread.Join(milliseconds);
        }

        public static bool SleepForSeconds(int seconds)
        {
            return SleepForMilliseconds(seconds * 1000);
        }
    }
}
