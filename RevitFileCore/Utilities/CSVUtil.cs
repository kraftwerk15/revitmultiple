﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace RevitFileCore
{
    class CSVUtil
    {
        private static readonly NLog.Logger Logger             = NLog.LogManager.GetCurrentClassLogger();
        public static           string      CSV_FILE_EXTENSION = ".csv";

        public static string[] ReadAllLines(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);
            if (lines.Length <= 0) return lines;
            // Workaround for a potential lack of detection of Unicode txt files.
            if (lines[0].Contains("\x00"))
            {
                lines = File.ReadAllLines(filePath, Encoding.Unicode);
            }
            return lines;
        }

        public static List<string[]> GetRowsFromLines(string[] lines)
        {
            List<string[]> list = lines.Select(line => line.Split(",")).ToList();

            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="csvFilePath"></param>
        /// <param name="delimiter"></param>
        /// <param name="encoding"></param>
        /// <returns>A boolean indicating if the write action was successful. True indicates success.</returns>
        public static bool WriteToCSVFile(List<string> rows, string csvFilePath, string delimiter, Encoding encoding)
        {
            List<string> q = new();
            foreach (string row in rows)
            {
                //q.Add(row.Join(delimiter, row));
                q.Add(row);
            }
            try
            {
                File.WriteAllLines(csvFilePath, q, encoding);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }
        }

        public static bool WriteToTabDelimitedTxtFile(List<string> rows, string txtFilePath, Encoding encoding = null)
        {
            return WriteToCSVFile(rows, txtFilePath, "\t", encoding);
        }

        public static bool HasCSVFileExtension(string filePath)
        {
            return PathUtil.HasFileExtension(filePath, CSV_FILE_EXTENSION);
        }

        // ReSharper disable once InconsistentNaming
        public static List<string[]> GetRowsFromCSVFile(string csvFilePath)
        {
            string[] lines = ReadAllLines(csvFilePath);
            return GetRowsFromLines(lines);
        }
    }
}
