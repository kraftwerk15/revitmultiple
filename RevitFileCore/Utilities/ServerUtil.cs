﻿#region Using
using HandleInheritability = System.IO.HandleInheritability;
using Pipes = System.IO.Pipes;
using Principal = System.Security.Principal;
using AccessControl = System.Security.AccessControl;
#endregion


namespace RevitFileCore
{
    public class ServerUtil
    {
        public static int MAX_NUMBER_OF_INSTANCES = 16;

        public static int PIPE_IO_BUFFER_SIZE = 8192;

        public static Pipes.PipeDirection IN = Pipes.PipeDirection.In;

        public static Pipes.PipeDirection OUT = Pipes.PipeDirection.Out;

        public static Pipes.PipeOptions Options = Pipes.PipeOptions.Asynchronous;

        public static Pipes.NamedPipeServerStream CreateNamedPipeServer(string pipeName)
        {
            Principal.SecurityIdentifier worldsid = new(Principal.WellKnownSidType.WorldSid, null);
            Principal.SecurityIdentifier localsid = new(Principal.WellKnownSidType.LocalSid, null);
            Pipes.PipeAccessRule         worldpr  = new(worldsid, Pipes.PipeAccessRights.ReadWrite,   AccessControl.AccessControlType.Allow);
            Pipes.PipeAccessRule         localpr  = new(localsid, Pipes.PipeAccessRights.FullControl, AccessControl.AccessControlType.Allow);
            Pipes.PipeSecurity                          ps       = new Pipes.PipeSecurity();
            ps.AddAccessRule(worldpr);
            ps.AddAccessRule(localpr);
            return new Pipes.NamedPipeServerStream(pipeName, Pipes.PipeDirection.InOut, MAX_NUMBER_OF_INSTANCES, 
                Pipes.PipeTransmissionMode.Byte, Pipes.PipeOptions.Asynchronous, PIPE_IO_BUFFER_SIZE, PIPE_IO_BUFFER_SIZE);
        }

        public static Pipes.AnonymousPipeServerStream CreateAnonymousPipeServer(Pipes.PipeDirection pipeDirection, HandleInheritability handleInheritability)
        {
            return new (pipeDirection, handleInheritability, PIPE_IO_BUFFER_SIZE);
        }

        public static Pipes.NamedPipeClientStream CreateNamedPipeClient(string node, string pipeName)
        {
            return new(node, pipeName, IN, Options);
        }

        public static Pipes.AnonymousPipeClientStream CreateAnonymousPipeClient(Pipes.PipeDirection pipeDirection,
            string pipeHandleString)
        {
            return new(pipeDirection, pipeHandleString);
        }
    }
}
