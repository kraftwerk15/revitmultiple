﻿////
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RevitFileCore
{
    // ReSharper disable once InconsistentNaming
    public static class StreamIOUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static Tuple<List<string>, Task<string>> ReadAvailableLines(StreamReader streamReader,
            Task<string>                                                                pendingReadLineTask = null)
        {
            Task<string> nextPendingReadLineTask = null;
            List<string> lines                   = new();

            Task<string> readLineTask = null;

            readLineTask = pendingReadLineTask ?? streamReader.ReadLineAsync();

            bool reachedEndOfStream = false;

            while ((readLineTask.Status == TaskStatus.RanToCompletion) && !reachedEndOfStream)
            {
                string line = readLineTask.Result;
                if (line != null)
                {
                    lines.Add(line);
                    readLineTask = streamReader.ReadLineAsync();
                }
                else
                {
                    reachedEndOfStream = true;
                }
            }

            if (reachedEndOfStream)
            {
                nextPendingReadLineTask = null;
            }
            else
                switch (readLineTask.Status)
                {
                    case TaskStatus.Faulted:
                    case TaskStatus.Canceled:
                        nextPendingReadLineTask = null;
                        break;
                    case TaskStatus.Created:
                        break;
                    case TaskStatus.WaitingForActivation:
                        break;
                    case TaskStatus.WaitingToRun:
                        break;
                    case TaskStatus.Running:
                        break;
                    case TaskStatus.WaitingForChildrenToComplete:
                        break;
                    case TaskStatus.RanToCompletion:
                        break;
                    default:
                        nextPendingReadLineTask = readLineTask;
                        break;
                }

            return Tuple.Create(lines, nextPendingReadLineTask);
        }

        private static object WithIgnoredIoException(Func<object> action)
        {
            object result = null;
            try
            {
                result = action();
            }
            catch (IOException)
            {
            }
        
            return result;
        }

        public static object GetSafeWriteLine(StreamWriter streamWriter)
        {
            Func<string, object> safeWriteLine = msg =>
            {
                //WithIgnoredIOException(() => streamWriter.WriteLine(msg));
                return string.Empty;
            };
            return safeWriteLine;
        }
        //Not Used
        public static Tuple<List<string>, Task> ReadAvailableLines(StreamReader streamReader, Task pendingReadLineTask = null)
        {
            Task<string>         readLineTask;
            Task         nextPendingReadLineTask = null;
            List<string> lines                   = new();
            if (pendingReadLineTask != null)
            {
                //readLineTask = pendingReadLineTask;
                //Below is temporary?
                readLineTask = new Task<string>(() => string.Empty);
            }
            else
            {
                readLineTask = streamReader.ReadLineAsync();
            }

            var reachedEndOfStream = false;
            while (readLineTask.Status == TaskStatus.RanToCompletion && !reachedEndOfStream)
            {
                string line = readLineTask.Result;
                if (line != null)
                {
                    //lines.append(line);
                    readLineTask = streamReader.ReadLineAsync();
                }
                else
                {
                    reachedEndOfStream = true;
                }
            }

            if (reachedEndOfStream)
            {
                nextPendingReadLineTask = null;
            }
            else if (readLineTask.Status == TaskStatus.Faulted)
            {
                nextPendingReadLineTask = null;
            }
            else if (readLineTask.Status == TaskStatus.Canceled)
            {
                nextPendingReadLineTask = null;
            }
            else
            {
                nextPendingReadLineTask = readLineTask;
            }

            return Tuple.Create(lines, nextPendingReadLineTask);
        }

        public static StreamReader GetStreamReader(Stream stream)
        {
            StreamReader reader = new(stream);
            return reader;
        }

        public static StreamWriter GetStreamWriter(Stream stream, bool autoFlush = true)
        {
            StreamWriter writer = new(stream) {AutoFlush = autoFlush};
            return writer;
        }

        public static object UsingStreamWriter(TextWriter streamWriter, Func<object> action)
        {
            Func<object> safeCloseAndDispose = () =>
            {
                streamWriter.Close();
                streamWriter.Dispose();
                return null;
            };
            return ExecuteStream(safeCloseAndDispose, action);
        }

        public static object UsingStreamReader(TextReader streamWriter, Func<object> action)
        {
            Func<object> safeCloseAndDispose = () =>
            {
                streamWriter.Close();
                streamWriter.Dispose();
                return null;
            };
            return ExecuteStream(safeCloseAndDispose, action);
        }

        public static object UsingStream(Stream stream, Func<object> action)
        {
            Func<object> safeCloseAndDispose = () =>
            {
                stream.Close();
                stream.Dispose();
                return null;
            };
            return ExecuteStream(safeCloseAndDispose, action);
        }

        private static object ExecuteStream(Func<object> closeDispose, Func<object> action)
        {
            object result = null;
            try
            {
                result = action();
            }
            finally
            {
                // NOTE: the reason for using WithIgnoredIOException() here is that Close() can throw an IOException (Pipe is broken).
                WithIgnoredIoException(closeDispose);
                //safeCloseAndDispose();
            }

            return result;
        }
        public static FileStream CreateFile(string filePath, bool overwrite = false)
        {
            FileMode fileMode   = overwrite ? FileMode.Create : FileMode.CreateNew;
            FileStream fileStream = new(filePath, fileMode, FileAccess.Write, FileShare.ReadWrite);
            return fileStream;
        }

        public static FileStream OpenFile(string filePath, bool isReadonly = true) 
        {
            FileAccess fileAccess =  isReadonly ? FileAccess.Read : FileAccess.ReadWrite;
            FileStream fileStream = new(filePath, FileMode.Open, fileAccess, FileShare.ReadWrite);
            return fileStream;
        }
    }
}