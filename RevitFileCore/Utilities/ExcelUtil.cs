﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using NLog;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace RevitFileCore
{
    public static class ExcelUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static bool IsExcelInstalled()
        {
            return Type.GetTypeFromProgID("Excel.Application") != null;
        }

        public static bool HasExcelExtension(string filePath)
        {
            var extension = Path.GetExtension(filePath).ToLower();

            return new[] { ".xls", ".xlsx" }.Any(excelExtension => extension == excelExtension.ToLower());
        }

        public static object MissingValue = System.Reflection.Missing.Value;

        public static object xlUnicodeText = 42;

        public static object xlNoChange = 1;

        public static Range FindFirstValue(Range range, XlSearchOrder searchOrder, XlSearchDirection searchDirection)
        {
            dynamic afterCell = range.Cells[searchDirection == XlSearchDirection.xlNext ? range.Rows.Count : 1, 
                searchDirection == XlSearchDirection.xlNext ? range.Columns.Count : 1];
            return range.Find("*", afterCell, XlFindLookIn.xlValues, MissingValue, searchOrder, 
                searchDirection, MissingValue, MissingValue, MissingValue);
        }

        // Returns the row number of the first non-blank row in the range.
        // If all rows are blank, returns 0.
        public static int GetFirstUsedRowNumber(Range range)
        {
            Range firstUsedCell = FindFirstValue(range, XlSearchOrder.xlByRows, XlSearchDirection.xlNext);
            return firstUsedCell != null ? firstUsedCell.Row : 0;
        }

        // Returns the column number of the first non-blank column in the range.
        // If all columns are blank, returns 0.
        public static int GetFirstUsedColumnNumber(Range range)
        {
            Range firstUsedCell = FindFirstValue(range, XlSearchOrder.xlByColumns, XlSearchDirection.xlNext);
            return firstUsedCell != null ? firstUsedCell.Column : 0;
        }

        // Returns the row number of the last non-blank row in the range.
        // If all rows are blank, returns 0.
        public static int GetLastUsedRowNumber(Range range)
        {
            Range lastUsedCell = FindFirstValue(range, XlSearchOrder.xlByRows, XlSearchDirection.xlPrevious);
            return lastUsedCell?.Row ?? 0;
        }

        // Returns the column number of the last non-blank column in the range.
        // If all columns are blank, returns 0.
        public static int GetLastUsedColumnNumber(Range range)
        {
            Range lastUsedCell = FindFirstValue(range, XlSearchOrder.xlByColumns, XlSearchDirection.xlPrevious);
            return lastUsedCell?.Column ?? 0;
        }

        public static void WriteRowsToWorksheet(Worksheet worksheet, Range rows)
        {
            for (int i = 0; i < rows.Count; i++)
            {
                int   rowIndex  = i;
                Range row       = rows[i];
                Range   excelRows = worksheet.Rows;
                dynamic   excelRow  = excelRows[rowIndex + 1];
                excelRow.NumberFormat = "@";
                for (int j = 0; j < row.Cells.Count; j++)
                {
                    int cellIndex = j;
                    dynamic cellValue = row.Cells[j];
                    dynamic cells     = excelRow.Cells;
                    dynamic cell      = cells[cellIndex + 1];
                    cell.Value2 = cellValue;
                    Marshal.FinalReleaseComObject(cell);
                    Marshal.FinalReleaseComObject(cells);
                }
                Marshal.FinalReleaseComObject(excelRow);
                Marshal.FinalReleaseComObject(excelRows);
            }
        }

        public static List<dynamic> ReadRowsFromWorksheet(Worksheet worksheet)
        {
            Range        usedRange = worksheet.UsedRange;
            List<dynamic> rows      = new();
            foreach (Range excelRow in usedRange.Rows)
            {
                List<dynamic> row = new ();
                foreach (dynamic cellValue in excelRow.Value2)
                {
                    row.Add(cellValue);
                }
                rows.Add(row);
            }
            return rows;
        }

        public static object GetNumberOfRowsAndColumns(Worksheet worksheet)
        {
            Range usedRange = worksheet.UsedRange;
            return (usedRange.Row + usedRange.Rows.Count - 1, usedRange.Column + usedRange.Columns.Count - 1);
        }

        public static Range GetWorksheetRange(Worksheet worksheet, int firstRowNumber, int firstColumnNumber, 
            int lastRowNumber, int lastColumnNumber)
        {
            return worksheet.Range[worksheet.Cells[firstRowNumber, firstColumnNumber], worksheet.Cells[lastRowNumber, lastColumnNumber]];
        }

        public static List<string[]> ReadRowsText(Range range)
        {
            List<string[]> rows = new();
            foreach (Range excelRow in range.Rows)
            {
                List<string> row = new();
                foreach (Range excelCell in excelRow.Cells)
                {
                    row.Add(excelCell.Text);
                }

                string[] rowArray = row.ToArray();
                rows.Add(rowArray);
            }
            return rows;
        }

        public static List<string[]> ReadRowsTextFromWorksheet(Worksheet worksheet)
        {
            List<string[]> rows                 = new();
            Range  usedRange            = worksheet.UsedRange;
            int    lastUsedRowNumber    = GetLastUsedRowNumber(usedRange);
            int    lastUsedColumnNumber = GetLastUsedColumnNumber(usedRange);
            if (lastUsedRowNumber != 0 && lastUsedColumnNumber != 0)
            {
                rows = ReadRowsText(GetWorksheetRange(worksheet, 1, 1, lastUsedRowNumber, lastUsedColumnNumber));
            }
            return rows;
        }

        public static Application OpenExcelApp()
        {
            
            Application app = new()
            {
                Visible = false, DisplayAlerts = false, ScreenUpdating = false, AskToUpdateLinks = false
            };
            Logger.Info("Excel Application Opened");
            return app;
        }

        public static bool CloseExcelApp(Application app)
        {
            try
            {
                app.Quit();
                Marshal.FinalReleaseComObject(app);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }

            return true;
        }

        public static Workbook GetExcelWorkbook(string excelFilePath)
        {
            Workbook result = null;
            Application app = OpenExcelApp();

            try
            {
                Workbooks workbooks = app.Workbooks;
                Workbook  workbook  = workbooks.Open(excelFilePath);
                result    = workbook;
                Logger.Info("Excel Workbook Successfully Opened.");
            }
            catch(Exception e)
            {
                Logger.Error(e);
            }
            return result;
        }
        public static bool CloseExcelWorkbook(Workbook workbook, bool saveChanges = false)
        {
            try
            {
                workbook.Close(saveChanges);
                Marshal.FinalReleaseComObject(workbook);
                Logger.Info("Excel Workbook Successfully Closed.");
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }
        }

        public static bool WithNewExcelWorkbook(string excelFilePath, bool saveChanges = false)
        {
            Application app      = OpenExcelApp();
            Workbook    workbook = GetExcelWorkbook(excelFilePath);
            Workbooks workbooks = app.Workbooks;
            workbook  = workbooks.Add();

            //result    = workbookAction(workbook);
            if(workbook != null)
            {
                if (saveChanges)
                {
                    workbook.SaveAs(excelFilePath);
                }
                workbook.Close(saveChanges);
                Marshal.FinalReleaseComObject(workbook);
            }
            if (workbooks != null)
            {
                Marshal.FinalReleaseComObject(workbooks);
            }

            CloseExcelWorkbook(workbook, true);
            CloseExcelApp(app);
            
            return CloseExcelApp(app); ;
        }

        public static List<string[]> ReadRowsTextFromWorkbook(string excelFilePath, string worksheetName = null)
        {
            Application        app           = OpenExcelApp();
            Workbook           workbook      = GetExcelWorkbook(excelFilePath);
            Sheets             worksheets    = workbook.Worksheets;
            dynamic            worksheet     = worksheetName != null ? worksheets[worksheetName] : worksheets[1];
            List<string[]> fromWorksheet = ReadRowsTextFromWorksheet(worksheet);
            Marshal.FinalReleaseComObject(worksheet);
            Marshal.FinalReleaseComObject(worksheets);

            CloseExcelWorkbook(workbook, true);
            CloseExcelApp(app);
            return fromWorksheet;
        }

        public static bool WriteRowsTextToWorkbook(string excelFilePath, Range rows, object worksheetName = null)
        {
            Application app = OpenExcelApp();
            Workbook workbook =  GetExcelWorkbook(excelFilePath);
            
            Sheets  worksheets = workbook.Worksheets;
            dynamic worksheet  = worksheetName != null ? worksheets[worksheetName] : worksheets[1];
            WriteRowsToWorksheet(worksheet, rows);
            Marshal.FinalReleaseComObject(worksheet);
            Marshal.FinalReleaseComObject(worksheets);

            CloseExcelWorkbook(workbook, true);
            return CloseExcelApp(app);
        }

        public static object WriteRowsTextToNewWorkbook(string excelFilePath, Range rows, string worksheetName = null)
        {
            Application app       = OpenExcelApp();
            Workbook    workbook  = GetExcelWorkbook(excelFilePath);
            Workbooks   workbooks = app.Workbooks;
            workbook = workbooks.Add();

            //result    = workbookAction(workbook);
            if (workbook != null)
            {
                string newWorksheetName = worksheetName ?? "Sheet1";
                Sheets worksheets       = workbook.Worksheets;
                worksheets.Add();
                Worksheet worksheet = worksheets[1];
                worksheet.Name = newWorksheetName;
                WriteRowsToWorksheet(worksheet, rows);
                Marshal.FinalReleaseComObject(worksheet);
                Marshal.FinalReleaseComObject(worksheets);
                
                workbook.SaveAs(excelFilePath);
                workbook.Close(true);
                Marshal.FinalReleaseComObject(workbook);
            }
            if (workbooks != null)
            {
                Marshal.FinalReleaseComObject(workbooks);
            }

            CloseExcelWorkbook(workbook, true);
            CloseExcelApp(app);

            return CloseExcelApp(app);
        }
    }
}
