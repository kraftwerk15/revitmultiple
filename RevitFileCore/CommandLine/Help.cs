﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitFileCore
{
    public static class Help
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static void ShowCommandLineHelp()
        {
            Logger.Info("Help:");
            Console.WriteLine("Help:");
            Logger.Info("Usage (using a settings file):");
            //Original
            //Logger.Info("RevitFileUpdater.exe --settings_file <SETTINGS FILE PATH> [--log_folder <LOG FOLDER PATH>]");
            Logger.Info("RevitFileUpdater.exe --settings_file <SETTINGS FILE PATH>");
            Logger.Info("Example:");
            //Original
            //Logger.Info("RevitFileUpdater.exe --settings_file BatchRvt.Settings.json --log_folder .");
            Logger.Info("RevitFileUpdater.exe --settings_file BatchRvt.Settings.json --log_folder .");
            Logger.Info("Usage (without a settings file):");
            Logger.Info("RevitFileUpdater.exe --file_list <REVIT FILE LIST PATH>");
            Logger.Info("(NOTE: this mode operates in batch mode only; by default operates in detach mode for central files.)");
            Logger.Info("Additional command-line options:");
            
            Logger.Info("--library");
            Logger.Info("--revit_version <REVIT VERSION>");
            //Logger.Info("--log_folder <LOG FOLDER PATH>");
            Logger.Info("--detach | --create_new_local");
            Logger.Info("--worksets <open_all | close_all>");
            Logger.Info("--audit");
            Logger.Info("--per_file_timeout <PER-FILE PROCESSING TIMEOUT IN MINUTES>");
            Logger.Info("--help");

            Logger.Info("Examples:");
            Logger.Info("RevitFileUpdater.exe --file_list RevitFileList.xlsx");
            Logger.Info("RevitFileUpdater.exe --file_list RevitFileList.xlsx --detach --audit");
            Logger.Info("RevitFileUpdater.exe --file_list RevitFileList.txt --create_new_local --worksets open_all");
            Logger.Info("RevitFileUpdater.exe --file_list RevitFileList.xlsx --revit_version 2019 --detach --worksets close_all");
            Logger.Info("RevitFileUpdater.exe --file_list RevitFileList.txt --per_file_timeout 15");
        }
    }
}
