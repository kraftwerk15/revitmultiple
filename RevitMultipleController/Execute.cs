﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using Autodesk.Revit.UI;
using RevitMultipleProcessCore.RevitScript;

namespace RevitMultipleController
{
    public class Execute
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        //TODO fill this in
        private const string BatchScriptHostFilename = "";
        // ReSharper disable once InconsistentNaming
        private const string BatchRVTScriptsFolderPathEnvironmentVariableName = "BATCHRVT__SCRIPTS_FOLDER_PATH";
        //This is being called from inside of the Revit Addin
        //Requires Environment Variables
        public Execute(string plugin, UIApplication uiApplication)
        {
            StringDictionary environmentVariables = GetEnvironmentVariables();

            if (environmentVariables == null)
            {
                return;
            }

            string batchRvtScriptsFolderPath = GetBatchRvtScriptsFolderPath(environmentVariables);

            if (batchRvtScriptsFolderPath == null)
            {
                return;
            }

            string pluginFullFolderPath = Path.GetFullPath(plugin);
            string scriptHostFilePath = Path.Combine(batchRvtScriptsFolderPath, BatchScriptHostFilename);
            string batchRvtFolderPath = GetBatchRvtFolderPath(environmentVariables);

            //RevitMultipleCore.BatchRvt.StartBatchRvt(settingsFilePath);
            RevitScriptHost.Main(uiApplication);
        }

        private static string GetParentFolder(string folderPath)
        {
            return Directory.GetParent(folderPath)?.FullName;
        }

        private static string RemoveTrailingDirectorySeparators(string folderPath)
        {
            return folderPath.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }

        public static string GetBatchRvtFolderPath(StringDictionary environmentVariables)
        {
            string batchRvtScriptsFolderPath = GetBatchRvtScriptsFolderPath(environmentVariables);

            return (batchRvtScriptsFolderPath != null) ?
                GetParentFolder(RemoveTrailingDirectorySeparators(batchRvtScriptsFolderPath)) :
                null;
        }

        public static string GetBatchRvtScriptsFolderPath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, BatchRVTScriptsFolderPathEnvironmentVariableName);
        }

        private static string GetEnvironmentVariable(StringDictionary environmentVariables, string variableName)
        {
            return environmentVariables[variableName];
        }

        private static StringDictionary GetEnvironmentVariables()
        {
            StringDictionary environmentVariables = null;

            // NOTE: Have encountered (at least once) a NullReferenceException upon accessing the EnvironmentVariables property!
            try
            {
                environmentVariables = Process.GetCurrentProcess().StartInfo.EnvironmentVariables;
            }
            catch (NullReferenceException)
            {
                environmentVariables = null;
            }

            return environmentVariables;
        }
    }
}
