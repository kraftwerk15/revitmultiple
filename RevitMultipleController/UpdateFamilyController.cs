using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RevitMultipleCore;
using Autodesk.Revit.DB;

namespace RevitMultipleController
{
    public class UpdateFamilyController
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Provides an option to update Revit families that were tracked from a SQL database.
        /// </summary>
        /// <param name="filepath">The File Path navigating to the year version of the library to which we are going to work.</param>
        public void UpdateFamilyFromDatabase(string filepath, Guid instance)
        {
            Logger.Info("Starting UpdateFamilyFromDatabase Method");
            Dictionary<int, Exception> exceptions = new();
            Dictionary<int, string> completedFamilies = new();
            try
            {
                List<Dictionary<object, object>> rawList = RevitMultipleDatabase.Sql.ExecuteRead_RevitFiles();
                List<RevitMultipleCore.Family> dict = new();
                RevitMultipleCore.FamilyConverter r = new();
                foreach (Dictionary<object, object> a in rawList)
                {
                    dict.Add(RevitMultipleCore.FamilyConverter.DecodeFamily(a));
                }
                
                //System.Diagnostics.Debug.WriteLine("Write Revit API Save As Options");
                SaveAsOptions saveAs = new() { Compact = true, OverwriteExistingFile = true };

                if (dict.Count != 0)
                {
                    //Sort the list to include True Nested Families at the top.
                    // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                    dict.OrderBy(q => q.IsNested ? 0 : 1);

                    foreach (RevitMultipleCore.Family v in dict)
                    {
                        //if the family path coming from the database is empty or does not match the incoming library file path, ignore it
                        if (string.IsNullOrEmpty(v.FamilyPath) ||
                            !v.FamilyPath.ToLower().StartsWith(filepath.ToLower())) dict.Remove(v);
                        //confirm the year version is below the version of the application we are working in
                        //if (Convert.ToInt32(v.RevitVersion) >= Convert.ToInt32(app.VersionNumber)) dict.Remove(v);
                    }

                    foreach (RevitMultipleCore.Family v in dict)
                    {
                        //if (Convert.ToInt32(v.RevitVersion) >= Convert.ToInt32(app.VersionNumber)) continue;
                        //Write to a JSON file to have the application pick it up?

                        //Start the Application?

                        //Let the application run an addin to run the upgrade?
                        
                        
                        
                        //Open the document
                        //Document doc = OpenDocument(v);
                        //Once the document is open, we can safely change the file path to save
                        //string newPath = v.FamilyPath.Replace(v.RevitVersion, app.VersionNumber);
                        //Helper(Convert.ToInt32(app.VersionNumber), newPath);
                        //Confirm our new file path exists
                        //CheckFileLocation(newPath);
                        //Save and Close the file
                        //DocumentSaveAs(newPath, doc, saveAs);
                        //System.Diagnostics.Debug.WriteLine("Family Successfully Closed");
                        //DocumentClose(doc);
                        completedFamilies.Add(v.Key, v.FamilyPath);
                        Dictionary<object, object> net = RevitMultipleCore.FamilyConverter.EncodeFamily(v);
                        RevitMultipleDatabase.Sql.ExecuteWrite_FamilyUser_Update(net);
                    }
                }
                else
                {
                    //TODO send an email message here that there were no families to update so the process escaped.
                }
            }
            catch (Exception ex)
            {
                //Logging.WriteLogError(GetType(), ex.Message);
            }
            System.Diagnostics.Debug.WriteLine("Beginning to Send Email");
            //kraftwerk.Core.Code.Notification.SendEmail k = new kraftwerk.Core.Code.Notification.SendEmail();
           // k.CreateEmail("Update Families Report", exceptions, completedFamilies);
            System.Diagnostics.Debug.WriteLine("Email Successfully Sent");
        }

        public void UpdateFamiliesInLibrary(string libraryPath, Guid instance)
        {
            
            string path = libraryPath;

        }

        public void UpdateFamiliesInFolder(string folderPath, Guid instance)
        {

        }

        public void UpdateFamily(string filePath, Guid instance)
        {
            //bool isSaved = SaveSettings();

            // TODO: show error message if save failed!!

            
            //UpdateAdvancedSettings();

            //readBatchRvtOutput_Timer.Start();
        }
    }
}
