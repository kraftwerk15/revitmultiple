﻿using System;

namespace RevitMultipleCore
{
    public class License
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static void ShowCommandLineLicense()
        {
            //TODO call this from the command line
            Logger.Info("Revit Batch Processor");
            Logger.Info("Copyright (c) 2021  Micah Gray");
            Logger.Info("This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.");
            Logger.Info("This is free software, and you are welcome to redistribute it");
            Logger.Info("under certain conditions; type `show c' for details.");
        }
    }
}