﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using PInvoke;

namespace RevitMultipleCore
{
    
    class UIAutomationUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private const string DialogWindowClassName = "#32770";
        private const int StringBufferSize = 8 * 1024 + 1;


        public static List<WindowInfo> GetEnabledDialogsInfo(int processId)
        {
            List<WindowInfo> windowInfos = new();
            foreach(IntPtr hwnd in Win32Api.GetTopLevelWindows(DialogWindowClassName, null, processId))
            {
                if (Win32Api.Win32_IsWindowEnabled(hwnd))
                    windowInfos.Add(new (hwnd));
            }
            return windowInfos;
        }

        public static string TextWithoutAmpersands(string text)
        {
            return text.Replace("&", string.Empty).Replace("&", string.Empty);
        }

        public static string GetButtonText(IntPtr buttonInfo)
        {
            Win32Api.GetDialogItem(buttonInfo, int.MinValue);
            var buttonText = buttonInfo.ToString();
            return TextWithoutAmpersands(buttonText);
        }

        public static string GetWindowText(IntPtr hwnd)
        {
            StringBuilder s = new();
            s.EnsureCapacity(StringBufferSize);
            var numberOfChars = Win32Api.GetWindowText(hwnd);
            return s.ToString();
        }
        //controls may be WindowInfo here?
        public static List<IntPtr> FilterControlsByText(List<IntPtr> controls, string controlText)
        {
            List<IntPtr> targetControls = (from control in controls
                where string.Equals(TextWithoutAmpersands(GetButtonText(control)).Trim(), controlText.Trim(), StringComparison.CurrentCultureIgnoreCase)
                select control).ToList();
            return targetControls;
        }

       //public static string GetWindowClassName(IntPtr hwnd)
       //{
       //    StringBuilder s = new();
       //    s.EnsureCapacity(StringBufferSize);
       //    var numberOfChars = User32.GetClassName(hwnd, s, StringBufferSize);
       //    return s.ToString();
       //}
    }

    public class WindowInfo
    {
        public IntPtr Hwnd            { get; set; }
        public bool   IsWindowEnabled { get; set; }
        public IntPtr OwnerWindow     { get; set; }
        public IntPtr ParentWindow    { get; set; }
        public int DialogControlId { get; set; }
        public string WindowClassName { get; set; }
        public string WindowText      { get; set; }

        public WindowInfo(IntPtr hwnd)
        {
            Hwnd            = hwnd;
            IsWindowEnabled = Win32Api.Win32_IsWindowEnabled(hwnd);
            OwnerWindow     = Win32Api.GetOwnerWindow(hwnd);
            ParentWindow    = Win32Api.GetParentSafe(hwnd);
            DialogControlId = Win32Api.GetDialogControlId(hwnd);
            WindowClassName = Win32Api.GetWindowClassName(hwnd);
            WindowText      = UIAutomationUtil.GetWindowText(hwnd);
        }
    }
}
