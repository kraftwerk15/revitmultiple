﻿#region Using
using System;
using Autodesk.Revit.UI;
using EventHandler = System.EventHandler;
using StringBuilder = System.Text.StringBuilder;
using DialogBoxShowingEventArgs = Autodesk.Revit.UI.Events.DialogBoxShowingEventArgs;
using TaskDialogShowingEventArgs = Autodesk.Revit.UI.Events.TaskDialogShowingEventArgs;
using MessageBoxShowingEventArgs = Autodesk.Revit.UI.Events.MessageBoxShowingEventArgs;
#endregion

namespace RevitMultipleCore
{
    public class RevitDialogUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        
        public static int IDOK = 1;
        
        public static int IDCANCEL = 2;
        
        public static int IDYES = 6;
        
        public static int IDNO = 7;
        
        public static int IDCLOSE = 8;
        
        public static object Try(Func<object> action) {
            object result = null;
            try {
                result = action();
            } catch {
            }
            return result;
        }
        
        public static void DialogShowingEventHandler(object sender, DialogBoxShowingEventArgs eventArgs) {
            try {
                var dialogResult = IDOK;
                Logger.Info("Dialog box shown:");
                
                switch (eventArgs)
                {
                    case TaskDialogShowingEventArgs taskDialogShowingEventArgs:
                        Logger.Info("Message: " + taskDialogShowingEventArgs.Message);
                        dialogResult = taskDialogShowingEventArgs.DialogId switch
                        {
                            "TaskDialog_Missing_Third_Party_Updater" => 1001,
                            "TaskDialog_Location_Position_Changed" => 1002,
                            _ => dialogResult
                        };
                        break;
                    case MessageBoxShowingEventArgs messageBoxShowingEventArgs:
                        Logger.Info("Message: " + messageBoxShowingEventArgs.Message);
                        Logger.Info("DialogType: " + messageBoxShowingEventArgs.DialogType);
                        break;
                }
                object dialogId = Try(() => eventArgs.DialogId);
                if (dialogId != null) {
                    Logger.Info("DialogId: " + dialogId);
                }
                //object helpId = Try(() => eventArgs.HelpId);
                //if (helpId != null) {
                //    Logger.Info("HelpId: " + helpId.ToString());
                //}
                eventArgs.OverrideResult(dialogResult);
            } catch (Exception e) {
                Logger.Error("Caught exception in dialog event handler! Exception message: {0}", e.Message);
                ExceptionUtil.LogOutputErrorDetails(e);
            }
        }
        
        public static object WithDialogBoxShowingHandler(UIApplication uiApp, Func<object> action) {
            object result = null;
            var dialogShowingEventHandler = new EventHandler<DialogBoxShowingEventArgs>((sender,eventArgs) => DialogShowingEventHandler(sender, eventArgs));
            uiApp.DialogBoxShowing += dialogShowingEventHandler;
            try {
                result = action();
            } finally {
                uiApp.DialogBoxShowing -= dialogShowingEventHandler;
            }
            return result;
        }
    }
}