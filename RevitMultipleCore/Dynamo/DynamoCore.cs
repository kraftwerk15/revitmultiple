﻿#region License
// Revit Batch Processor
//
// Copyright (c) 2020  Dan Rumery, BVN
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#endregion
#region Using
using System;
using Autodesk.Revit.UI;
using IOException = System.IO.IOException;


#endregion

namespace RevitMultipleCore.Dynamo
{
    public class DynamoCore
    {
        public const string DynamoRevitModuleNotFoundErrorMessage = "Could not load the Dynamo module! There must be EXACTLY ONE VERSION of Dynamo installed!";

        public static bool IsDynamoRevitModuleLoaded()
        {
            bool isLoaded = false;
            try
            {
                isLoaded = true;
            }
            catch (IOException)
            {
                isLoaded = false;
            }

            return isLoaded;
        }

        public static object ExecuteDynamoScript(UIApplication uiApp, string dynamoScriptFilePath, bool showUi = false)
        {
            var result = DynamoUtil.ExecuteDynamoScript(uiApp, dynamoScriptFilePath, showUi);
            return result;
        }
        public static bool IsDynamoNotFoundException(Exception exception) {
            return exception.Message == DynamoRevitModuleNotFoundErrorMessage;
        }
        
    }
}