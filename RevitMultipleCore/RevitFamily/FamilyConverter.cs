﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitMultipleCore
{
    public class FamilyConverter
    {
        /// <summary>
        /// Coverts a Family Schema to a Dictionary to write to a database.
        /// </summary>
        /// <param name="family">The Family Schema.</param>
        public static Dictionary<object, object> EncodeFamily(Family family)
        {
            return new()
            {
                //{0,key1},
                {1, family.UserNameId},
                {2, family.FamilyName},
                {3, family.FamilyPath},
                {4, family.Category},
                {5, family.DateModified},
                {6, family.SharedId},
                {7, family.RevitVersion},
                {8, family.SemanticVersion},
                {9, family.Updated},
                {10, family.IsNested}
            };
        }

        public static Family DecodeFamily(Dictionary<object, object> rawFamily)
        {
            //Convert Key
            int key = -1;
            if (rawFamily.ContainsKey(0))
            {
                rawFamily.TryGetValue(0, out object temp);
                if (temp != null)
                {
                    key = Convert.ToInt32(temp);
                }
            }
            //Convert User
            int user = -1;
            if (rawFamily.ContainsKey(1))
            {
                rawFamily.TryGetValue(1, out object temp);
                if (temp != null)
                {
                    user = Convert.ToInt32(temp);
                }
            }
            //Convert FamilyName
            string familyName = string.Empty;
            if (rawFamily.ContainsKey(2))
            {
                rawFamily.TryGetValue(2, out object temp);
                if (temp != null)
                {
                    familyName = Convert.ToString(temp);
                }
            }
            //Convert FamilyName
            string familyPath = string.Empty;
            if (rawFamily.ContainsKey(3))
            {
                rawFamily.TryGetValue(3, out object temp);
                if (temp != null)
                {
                    familyPath = Convert.ToString(temp);
                }
            }
            //Convert Category
            string category = string.Empty;
            if (rawFamily.ContainsKey(4))
            {
                rawFamily.TryGetValue(4, out object temp);
                if (temp != null)
                {
                    category = Convert.ToString(temp);
                }
            }
            //Convert DateTime
            DateTime dateModified = DateTime.MinValue;
            if (rawFamily.ContainsKey(5))
            {
                rawFamily.TryGetValue(5, out object temp);
                if (temp != null)
                {
                    dateModified = Convert.ToDateTime(temp);
                }
            }
            //Convert SharedId
            string sharedId = string.Empty;
            if (rawFamily.ContainsKey(6))
            {
                rawFamily.TryGetValue(6, out object temp);
                if (temp != null)
                {
                    sharedId = Convert.ToString(temp);
                }
            }
            //Convert RevitVersion
            string revitVersion = string.Empty;
            if (rawFamily.ContainsKey(7))
            {
                rawFamily.TryGetValue(7, out object temp);
                if (temp != null)
                {
                    revitVersion = Convert.ToString(temp);
                }
            }
            //Convert SemanticVersion
            string semanticVersion = string.Empty;
            if (rawFamily.ContainsKey(8))
            {
                rawFamily.TryGetValue(8, out object temp);
                if (temp != null)
                {
                    semanticVersion = Convert.ToString(temp);
                }
            }
            //Convert IsNested
            bool isNested = false;
            if (rawFamily.ContainsKey(9))
            {
                rawFamily.TryGetValue(9, out object temp);
                if (temp != null)
                {
                    isNested = Convert.ToBoolean(temp);
                }
            }
            //Convert Updated
            bool isUpdated = false;
            if (rawFamily.ContainsKey(10))
            {
                rawFamily.TryGetValue(10, out object temp);
                if (temp != null)
                {
                    isUpdated = Convert.ToBoolean(temp);
                }
            }

            Family family = new()
            {
                Key = key,
                UserNameId = user,
                FamilyName = familyName,
                FamilyPath = familyPath,
                Category = category,
                DateModified = dateModified,
                SharedId = sharedId,
                RevitVersion = revitVersion,
                SemanticVersion = semanticVersion,
                IsNested = isNested,
                Updated = isUpdated
            };
            return family;
        }
    }
}
