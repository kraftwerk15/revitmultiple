﻿using System;

namespace RevitMultipleCore
{

    /// <summary>
    /// A Family Object for this addin.
    /// </summary>
    public class Family
    {
        public int Key { get; set; }
        /// <summary>
        /// The integer corresponding to the UserName in the database.
        /// </summary>
        public int UserNameId { get; set; }
        /// <summary>
        /// A human-readable name for this family.
        /// </summary>
        public string FamilyName { get; set; }
        /// <summary>
        /// The file path this family to which this family was saved.
        /// </summary>
        public string FamilyPath { get; set; }
        /// <summary>
        /// The Revit category for this family.
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// The DateModified for this family.
        /// </summary>
        public DateTime DateModified { get; set; }
        /// <summary>
        /// When the family is saved, it is given a SharedId to make it readable across the ecosystem.
        /// </summary>
        public string SharedId { get; set; }
        /// <summary>
        /// This is an indication of the Revit version this family was saved.
        /// </summary>
        public string RevitVersion { get; set; }
        /// <summary>
        /// The Semantic Version for this family.
        /// </summary>
        public string SemanticVersion { get; set; }
        /// <summary>
        /// Tracking if this family has been updated in the database.
        /// </summary>
        public bool Updated { get; set; }
        /// <summary>
        /// Tracking if this family is supposed to be a nested family.
        /// </summary>
        public bool IsNested { get; set; }
    }
}
