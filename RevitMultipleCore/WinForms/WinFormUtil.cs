﻿#region License
//
// Revit Batch Processor
//
// Copyright (c) 2020  Dan Rumery, BVN
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion

#region Using
using System;
using Process = System.Diagnostics.Process;
using MessageBox = System.Windows.Forms.MessageBox;
using IWin32Window = System.Windows.Forms.IWin32Window;
using Cursor = System.Windows.Forms.Cursor;
using Point = System.Drawing.Point;
#endregion


namespace RevitMultipleCore.WinForms
{
    public class WinFormUtil
    {
        public class WindowHandleWrapper : IWin32Window
        {
            private object handle {
                get;
            }
            public WindowHandleWrapper(IntPtr hwnd) {
                handle = hwnd;
            }
            
            //public object get_Handle() {
            //    return this.hwnd;
            //}
            
            public static IWin32Window GetMainWindowHandle() {
                return new WindowHandleWrapper(Process.GetCurrentProcess().MainWindowHandle);
            }

            public IntPtr Handle { get; }
        }
        
        
        public static Point SetMousePosition(int x, int y) {
            Cursor.Position = new Point(x, y);
            return Cursor.Position;
        }
    }
}