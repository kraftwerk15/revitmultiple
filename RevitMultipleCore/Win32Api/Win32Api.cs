﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StringBuilder = System.Text.StringBuilder;
using PInvoke;
using Interop = System.Runtime.InteropServices;
using System.Linq;
using System.Runtime.InteropServices;

namespace RevitMultipleCore
{
    public static class Win32Api
    {
        public static object USER32_MODULE_NAME = "user32.dll";

        public static object GW_OWNER = 4;

        public static object GA_PARENT = 1;

        public static object BOOL_TRUE = 1;

        public static object BOOL_FALSE = 0;
        [DllImport("User32.dll")]
        private static extern IntPtr FindWindowExA(IntPtr hWndParent, IntPtr hWndChildAfter, string lpszClass, string lpszWindow);

        //public static object Win32_GetWindowText = User32.GetWinApiFunctionAnsi("GetWindowText", USER32_MODULE_NAME, System.Int32, System.IntPtr, StringBuilder, System.Int32);
        [DllImport("User32.dll")]
        private static extern object GetClassName(IntPtr intPtr, int type);
        [DllImport("User32.dll")]
        private static extern int GetDlgCtrlID(IntPtr intPtr);
        [DllImport("User32.dll")]
        private static extern bool IsWindowEnabled(IntPtr intPtr);

        //public static object Win32_GetWindow = win32_pinvoke.GetWinApiFunction("GetWindow", USER32_MODULE_NAME, System.IntPtr, System.IntPtr, System.Int32);

        //public static object Win32_GetAncestor = win32_pinvoke.GetWinApiFunction("GetAncestor", USER32_MODULE_NAME, System.IntPtr, System.IntPtr, System.Int32);

        //public static object Win32_GetWindowThreadProcessId = win32_pinvoke.GetWinApiFunction("GetWindowThreadProcessId", USER32_MODULE_NAME, System.Int32, System.IntPtr, System.IntPtr);

        //public static object Win32_SendMessage = win32_pinvoke.GetWinApiFunction("SendMessage", USER32_MODULE_NAME, System.IntPtr, System.IntPtr, System.Int32, System.IntPtr, System.IntPtr);
        [DllImport("User32.dll")]
        private static extern IntPtr GetDlgItem(IntPtr hwnd, int nIDDlgItem);

        //public static object Win32_SetFocus = win32_pinvoke.GetWinApiFunction("SetFocus", USER32_MODULE_NAME, System.IntPtr, System.IntPtr);

        //public static object Win32_PostMessage = win32_pinvoke.GetWinApiFunction("PostMessage", USER32_MODULE_NAME, System.Int32, System.IntPtr, System.Int32, System.IntPtr, System.IntPtr);

        //public static object Win32_IsWindowVisible = win32_pinvoke.GetWinApiFunction("IsWindowVisible", USER32_MODULE_NAME, System.Int32, System.IntPtr);

        // public static object Win32_GetDesktopWindow = win32_pinvoke.GetWinApiFunction("GetDesktopWindow", USER32_MODULE_NAME, System.IntPtr);
        /// <summary>
        /// Delegate for the EnumChildWindows method
        /// </summary>
        /// <param name="hWnd">Window handle</param>
        /// <param name="parameter">Caller-defined variable; we use it for a pointer to our list</param>
        /// <returns>True to continue enumerating, false to bail.</returns>
        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool EnumChildWindows(IntPtr hwnd, EnumWindowProc func, IntPtr lParam);
        [DllImport("User32.dll")]
        private static extern bool EnableWindow(IntPtr intPtr, bool boolean);
        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        private static extern IntPtr GetParent(IntPtr hWnd);

        public static List<IntPtr> FindWindows(IntPtr parentHwnd, string className, string windowTitle)
        {
            List<IntPtr> handles = new();
            IntPtr hwnd = System.IntPtr.Zero;
            if(parentHwnd == IntPtr.Zero)
            {

            }
            else 
            { 
                
                hwnd = User32.FindWindow(className, windowTitle);
                if (hwnd == System.IntPtr.Zero)
                {
                    return null;
                }
                else
                {
                    handles.Add(hwnd);
                }
            }
            return handles;
        }

        public static int STRING_BUFFER_SIZE = 8 * 1024 + 1;

        public static string GetWindowText(IntPtr hwnd)
        {
            //StringBuilder s = new();
            //s.EnsureCapacity(STRING_BUFFER_SIZE);
            char[] c = new char[STRING_BUFFER_SIZE];
            User32.GetWindowText(hwnd, c, STRING_BUFFER_SIZE);
            return new string(c);
        }

        public static string GetWindowClassName(IntPtr hwnd)
        {
            //StringBuilder s = new();
            //s.EnsureCapacity(STRING_BUFFER_SIZE);
            char[] c = new char[STRING_BUFFER_SIZE];
            User32.GetClassName(hwnd, c, STRING_BUFFER_SIZE);
            return new string(c);
        }

        public static int GetDialogControlId(IntPtr hwnd)
        {
            return GetDlgCtrlID(hwnd);
        }

        public static IntPtr GetOwnerWindow(IntPtr hwnd)
        {
            return User32.GetWindow(hwnd, User32.GetWindowCommands.GW_OWNER);
        }

        public static IntPtr GetParentWindow(IntPtr hwnd)
        {
            return User32.GetAncestor(hwnd, User32.GetAncestorFlags.GA_PARENT);
        }

        public static bool Win32_IsWindowEnabled(IntPtr hwnd)
        {
            return IsWindowEnabled(hwnd);
        }

        public static bool EnableWin32Window(IntPtr hwnd, bool enable)
        {
            return EnableWindow(hwnd, enable);
            // NOTE on return value:
            //   If the window was previously disabled, the return value is non-zero.
            //   If the window was not previously disabled, the return value is zero.
            //return result != BOOL_FALSE;
        }

        public static IntPtr SendCloseMessage(IntPtr hwnd)
        {
            IntPtr result = User32.SendMessage(hwnd, User32.WindowMessage.WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            return result;
        }

        public static object SendButtonClickMessage(IntPtr hwnd)
        {
            return User32.SendMessage(hwnd, User32.WindowMessage.WM_BM_CLICK, IntPtr.Zero, IntPtr.Zero);
        }

        public static Tuple<int, int> GetWindowThreadProcessId(IntPtr hwnd)
        {
            var processId = 0;
            IntPtr pProcessId = Marshal.AllocHGlobal(Marshal.SizeOf(processId));
            Marshal.StructureToPtr(processId, pProcessId, false);
            int threadId = User32.GetWindowThreadProcessId(hwnd, out processId);
            if (threadId != 0)
            {
                processId = Marshal.PtrToStructure<int>(pProcessId);
            }
            Marshal.FreeHGlobal(pProcessId);
            return Tuple.Create(threadId, processId);
        }

        public static int GetWindowProcessId(IntPtr hwnd)
        {
            Tuple<int, int> _tup_1 = GetWindowThreadProcessId(hwnd);
            return _tup_1.Item2;
        }

        public static int GetWindowThreadId(IntPtr hwnd)
        {
            Tuple<int, int> _tup_1 = GetWindowThreadProcessId(hwnd);
            return _tup_1.Item1;
        }

        public static IntPtr GetDialogItem(IntPtr hwnd, int value)
        {
            return GetDlgItem(hwnd, value);
        }
           
        public static List<IntPtr> GetAllChildrenWindowHandles(IntPtr hwnd, string className)
        {
            return null;
        }

        public static IntPtr GetParentSafe(IntPtr handle)
        {
            IntPtr result = GetParent(handle);
            if (result == IntPtr.Zero)
            {
                // An error occured
                throw new Win32Exception(Marshal.GetLastWin32Error());
            }
            return result;
        }

        /// <summary>
        /// Returns a list of child windows
        /// </summary>
        /// <param name="parent">Parent of the windows to return</param>
        /// <returns>List of child windows</returns>
        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        /// <summary>
        /// Callback method to be used when enumerating windows.
        /// </summary>
        /// <param name="handle">Handle of the next window</param>
        /// <param name="pointer">Pointer to a GCHandle that holds a reference to the list to fill</param>
        /// <returns>True to continue the enumeration, false to bail</returns>
        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            if (gch.Target is not List<IntPtr> list)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            list.Add(handle);
            //  You can modify this to check to see if you want to cancel the operation, then return a null here
            return true;
        }



        public static List<IntPtr> GetTopLevelWindows(string className, string windowTitle, int processId = -1)
        {
            List<IntPtr> intPtrList = new();
            foreach (IntPtr hwnd in FindWindows(IntPtr.Zero, className, windowTitle))
            {
                if (processId != -1 || GetWindowProcessId(hwnd) == processId)
                    intPtrList.Add(hwnd);
            }
            return intPtrList;
        }
    }
}
