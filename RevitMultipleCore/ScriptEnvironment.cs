﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitMultipleCore
{
    public class ScriptEnvironment
    {
        private const string RevitMultipleScriptsFolderPathEnvironmentVariableName = "REVITMULTIPLE__SCRIPTS_FOLDER_PATH";

        private const string ScriptFilePathEnvironmentVariableName = @"REVITMULTIPLE__SCRIPT_FILE_PATH";

        private const string ScriptDataFilePathEnvironmentVariableName = @"REVITMULTIPLE__SCRIPT_DATA_FILE_PATH";

        private const string ProgressNumberEnvironmentVariableName = @"REVITMULTIPLE__PROGRESS_NUMBER";

        private const string ScriptOutputPipeHandleStringEnvironmentVariableName = @"REVITMULTIPLE__SCRIPT_OUTPUT_PIPE_HANDLE_STRING";

        private const string RevitMultipleProcessUniqueIdEnvironmentVariableName = @"REVITMULTIPLE__PROCESS_UNIQUE_ID";

        private const string RevitMultipleTestModeFolderPathEnvironmentVariableName = @"REVITMULTIPLE__TEST_MODE_FOLDER_PATH";

        public static string GetEnvironmentVariable(StringDictionary environmentVariables, string variableName)
        {
            return environmentVariables[variableName];
        }

        public static void SetEnvironmentVariable(StringDictionary environmentVariables, string variableName, string value)
        {
            environmentVariables[variableName] = value;
        }

        public static void SetBatchRvtScriptsFolderPath(StringDictionary environmentVariables, string batchRvtScriptsFolderPath)
        {
            SetEnvironmentVariable(environmentVariables, RevitMultipleScriptsFolderPathEnvironmentVariableName, batchRvtScriptsFolderPath);
        }

        public static void SetScriptFilePath(StringDictionary environmentVariables, string scriptFilePath)
        {
            SetEnvironmentVariable(environmentVariables, ScriptFilePathEnvironmentVariableName, scriptFilePath);
        }

        public static void SetScriptDataFilePath(StringDictionary environmentVariables, string scriptDataFilePath)
        {
            SetEnvironmentVariable(environmentVariables, ScriptDataFilePathEnvironmentVariableName, scriptDataFilePath);
        }

        public static void SetProgressNumber(StringDictionary environmentVariables, int progressNumber)
        {
            SetEnvironmentVariable(environmentVariables, ProgressNumberEnvironmentVariableName, progressNumber.ToString());
        }

        public static void SetScriptOutputPipeHandleString(StringDictionary environmentVariables, string scriptOutputPipeHandleString)
        {
            SetEnvironmentVariable(environmentVariables, ScriptOutputPipeHandleStringEnvironmentVariableName, scriptOutputPipeHandleString);
        }

        public static void SetBatchRvtProcessUniqueId(StringDictionary environmentVariables, string batchRvtProcessUniqueId)
        {
            SetEnvironmentVariable(environmentVariables, RevitMultipleProcessUniqueIdEnvironmentVariableName, batchRvtProcessUniqueId);
        }

        public static void SetTestModeFolderPath(StringDictionary environmentVariables, string testModeFolderPath)
        {
            SetEnvironmentVariable(environmentVariables, RevitMultipleTestModeFolderPathEnvironmentVariableName, testModeFolderPath);
        }

        public static string GetBatchRvtScriptsFolderPath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, RevitMultipleScriptsFolderPathEnvironmentVariableName);
        }

        public static string GetScriptFilePath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, ScriptFilePathEnvironmentVariableName);
        }

        public static string GetScriptDataFilePath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, ScriptDataFilePathEnvironmentVariableName);
        }

        public static int GetProgressNumber(StringDictionary environmentVariables)
        {
            string progressNumber = GetEnvironmentVariable(environmentVariables, ProgressNumberEnvironmentVariableName);
            return Convert.ToInt32(progressNumber);
        }

        public static string GetScriptOutputPipeHandleString(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, ScriptOutputPipeHandleStringEnvironmentVariableName);
        }

        public static string GetBatchRvtProcessUniqueId(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, RevitMultipleProcessUniqueIdEnvironmentVariableName);
        }

        public static string GetTestModeFolderPath(StringDictionary environmentVariables)
        {
            return GetEnvironmentVariable(environmentVariables, RevitMultipleTestModeFolderPathEnvironmentVariableName);
        }

        public static StringDictionary InitEnvironmentVariables(StringDictionary environmentVariables, string batchRvtScriptsFolderPath, string scriptFilePath,
            string scriptDataFilePath, int progressNumber, string scriptOutputPipeHandleString, string batchRvtProcessUniqueId, string testModeFolderPath)
        {
            SetBatchRvtScriptsFolderPath(environmentVariables, batchRvtScriptsFolderPath);
            SetScriptFilePath(environmentVariables, scriptFilePath);
            SetScriptDataFilePath(environmentVariables, scriptDataFilePath);
            SetProgressNumber(environmentVariables, progressNumber);
            SetScriptOutputPipeHandleString(environmentVariables, scriptOutputPipeHandleString);
            SetBatchRvtProcessUniqueId(environmentVariables, batchRvtProcessUniqueId);
            SetTestModeFolderPath(environmentVariables, testModeFolderPath);
            return environmentVariables;
        }
        
        /// <summary>
        /// Gets Environment Variables previously set before the application started.
        /// </summary>
        /// <returns>A string dictionary of Environment Variables</returns>
        public static StringDictionary GetEnvironmentVariables()
        {
            StringDictionary environmentVariables = null;
            // NOTE: Have encountered (at least once) a NullReferenceException upon accessing the EnvironmentVariables property!
            try
            {
                environmentVariables = Process.GetCurrentProcess().StartInfo.EnvironmentVariables;
            }
            catch (NullReferenceException)
            {
                environmentVariables = null;
            }
            return environmentVariables;
        }
    }
}
