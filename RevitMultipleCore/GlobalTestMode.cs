﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitMultipleCore
{
    public class GlobalTestMode
    {
        private static readonly NLog.Logger                 Logger           = NLog.LogManager.GetCurrentClassLogger();
        public static           List<TestModeUtil.TestMode> GLOBAL_TEST_MODE = new( );

        private static TestModeUtil.TestMode GetGlobalTestMode()
        {
            return GLOBAL_TEST_MODE.Count > 0 ? GLOBAL_TEST_MODE[0] : null;
        }

        private static bool IsGlobalTestMode()
        {
            return GetGlobalTestMode() != null;
        }

        public static void InitializeGlobalTestMode(string testModeFolderPath)
        {
            if (IsGlobalTestMode())
            {
                throw new Exception("ERROR: Global test mode is already initialized!");
            }
            Logger.Debug("TestModeFolderPath is empty : {0}", string.IsNullOrWhiteSpace(testModeFolderPath));
            if (string.IsNullOrWhiteSpace(testModeFolderPath)) return;
            TestModeUtil.TestMode globalTestMode = new TestModeUtil.TestMode(testModeFolderPath);
            globalTestMode.CreateTestModeFolder();
            GLOBAL_TEST_MODE[0] = globalTestMode;
        }

        public static void ExportSessionId(string sessionId)
        {
            if (IsGlobalTestMode())
            {
                TestModeUtil.TestMode globalTestMode = GetGlobalTestMode();
                globalTestMode.ExportSessionId(sessionId);
            }
        }

        public static void ExportRevitProcessId(int revitProcessId)
        {
            if (IsGlobalTestMode())
            {
                TestModeUtil.TestMode globalTestMode = GetGlobalTestMode();
                globalTestMode.ExportRevitProcessId(revitProcessId);
            }
        }

        public static string PrefixedOutputForGlobalTestMode(string prefixForTestMode)
        {
            if (IsGlobalTestMode())
            {
            }
            else
            {
                //var output = output_;
            }
            //Func<object, object> output = m => {
            //    output_(prefixForTestMode + " " + m);
            //    return;
            //};
            //return output;
            return null;
        }
    }
}
