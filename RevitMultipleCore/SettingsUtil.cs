﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace RevitMultipleCore
{
    public interface IPersistent
    {
        void Load(JObject jObject);
        void Store(JObject jObject);
    }

    public interface ISetting<T> : IPersistent
    {
        string GetName();
        T GetValue();
        void SetValue(T value);
    }

    public class Setting<T> : ISetting<T>
    {
        private readonly string name;
        private readonly Action<JObject, string, T> serialize;
        private readonly Func<JObject, string, T> deserialize;
        private readonly T defaultValue;
        private T value;

        public Setting(string name, Action<JObject, string, T> serialize, Func<JObject, string, T> deserialize,
            T defaultValue)
        {
            this.name = name;
            this.serialize = serialize;
            this.deserialize = deserialize;
            this.defaultValue = defaultValue;
            this.value = defaultValue;
        }

        public string GetName()
        {
            return name;
        }

        public void Load(JObject jObject)
        {
            value = deserialize(jObject, this.name);
        }

        public void Store(JObject jObject)
        {
            serialize(jObject, name, value);
        }

        public virtual T GetValue()
        {
            return value;
        }

        public virtual void SetValue(T value)
        {
            this.value = value;
        }
    }

    public class OptionalSetting<T> : Setting<T>
    {
        public OptionalSetting(string name, Action<JObject, string, T> serialize, Func<JObject, string, T> deserialize,
            T defaultValue)
            : base(
                name,
                serialize,
                (jObject, propertyName) => TryDeserialize(deserialize, jObject, propertyName, defaultValue),
                defaultValue
            )
        {
        }

        private static T TryDeserialize(Func<JObject, string, T> deserialize, JObject jObject, string propertyName,
            T defaultValue)
        {
            T value = defaultValue;

            try
            {
                value = deserialize(jObject, propertyName);
            }
            catch
            {
                // ignored
            }

            return value;
        }
    }

    public class BooleanSetting : OptionalSetting<bool>
    {
        public BooleanSetting(string name)
            : base(name, SetBooleanPropertyValue, GetBooleanPropertyValue, false)
        {
        }

        private static bool GetBooleanPropertyValue(JObject jObject, string propertyName)
        {
            return (jObject[propertyName] as JValue).ToObject<bool>();
        }

        private static void SetBooleanPropertyValue(JObject jObject, string propertyName, bool value)
        {
            jObject[propertyName] = value;
        }
    }

    public class IntegerSetting : OptionalSetting<int>
    {
        public IntegerSetting(string name)
            : base(name, SetIntegerPropertyValue, GetIntegerPropertyValue, 0)
        {
        }

        private static int GetIntegerPropertyValue(JObject jObject, string propertyName)
        {
            return (jObject[propertyName] as JValue).ToObject<int>();
        }

        private static void SetIntegerPropertyValue(JObject jObject, string propertyName, int value)
        {
            jObject[propertyName] = value;
        }
    }

    public class StringSetting : OptionalSetting<string>
    {
        public StringSetting(string name)
            : base(name, SetStringPropertyValue, GetStringPropertyValue, string.Empty)
        {
        }

        public override void SetValue(string value)
        {
            base.SetValue(InitializeFromString((value)));
        }

        public override string GetValue()
        {
            return InitializeFromString(base.GetValue());
        }

        private static string GetStringPropertyValue(JObject jObject, string propertyName)
        {
            return InitializeFromString((jObject[propertyName] as JValue).Value as string);
        }

        private static void SetStringPropertyValue(JObject jObject, string propertyName, string value)
        {
            jObject[propertyName] = InitializeFromString(value);
        }

        private static string InitializeFromString(string value)
        {
            return !string.IsNullOrWhiteSpace(value) ? value : string.Empty;
        }
    }

    public class EnumSetting<T> : OptionalSetting<T>
        where T : struct, IConvertible
    {
        public EnumSetting(string name)
            : base(name, SetEnumPropertyValue, GetEnumPropertyValue, default(T))
        {
        }

        private static T GetEnumPropertyValue(JObject jObject, string propertyName)
        {
            return StringToEnum((jObject[propertyName] as JValue).Value as string);
        }

        private static void SetEnumPropertyValue(JObject jObject, string propertyName, T value)
        {
            jObject[propertyName] = EnumToString(value);
        }

        private static T StringToEnum(string value)
        {
            var enumValue = default(T);

            if (!string.IsNullOrWhiteSpace(value))
            {
                bool isParsed = Enum.TryParse<T>(value, true, out enumValue);
            }

            return enumValue;
        }

        private static string EnumToString(T value)
        {
            return Enum.GetName(typeof(T), value);
        }
    }

    public class ListSetting<T> : OptionalSetting<List<T>>
    {
        public ListSetting(string name)
            : base(name, SetListPropertyValue, GetListPropertyValue, Enumerable.Empty<T>().ToList())
        {
        }

        public override void SetValue(List<T> value)
        {
            base.SetValue(InitializeFromList((value)));
        }

        public override List<T> GetValue()
        {
            return InitializeFromList(base.GetValue());
        }

        private static List<T> GetListPropertyValue(JObject jObject, string propertyName)
        {
            var jarray = (jObject[propertyName] as JArray);

            return InitializeFromList(jarray.Select(jvalue => jvalue.ToObject<T>()).ToList());
        }

        private static void SetListPropertyValue(JObject jObject, string propertyName, List<T> value)
        {
            jObject[propertyName] = new JArray(InitializeFromList(value));
        }

        private static List<T> InitializeFromList(IEnumerable<T> value)
        {
            return (value ?? Enumerable.Empty<T>()).ToList();
        }
    }

    public class PersistentSettings : IPersistent
    {
        private readonly IEnumerable<IPersistent> _persistentSettings;

        public PersistentSettings(IEnumerable<IPersistent> persistentSettings)
        {
            this._persistentSettings = persistentSettings.ToList();
        }

        public void Load(JObject jObject)
        {
            foreach (IPersistent persistent in _persistentSettings)
            {
                persistent.Load(jObject);
            }
        }

        public void Store(JObject jObject)
        {
            foreach (IPersistent persistent in _persistentSettings)
            {
                persistent.Store(jObject);
            }
        }
    }

    // ReSharper disable once InconsistentNaming
    public interface IUIConfigItem
    {
        void UpdateUI();
        void UpdateConfig();
    }

    // ReSharper disable once InconsistentNaming
    public class UIConfigItem : IUIConfigItem
    {
        // ReSharper disable once InconsistentNaming
        public UIConfigItem(Action updateUI, Action updateConfig)
        {
            _updateUi = updateUI;
            _updateConfig = updateConfig;
        }

        public void UpdateUI()
        {
            _updateUi();
        }

        public void UpdateConfig()
        {
            _updateConfig();
        }

        private readonly Action _updateUi;
        private readonly Action _updateConfig;
    }

    // ReSharper disable once InconsistentNaming
    public class UIConfig : IUIConfigItem
    {
        private readonly IEnumerable<IUIConfigItem> _uiConfigItems;

        public UIConfig(IEnumerable<IUIConfigItem> uiConfigItems)
        {
            this._uiConfigItems = uiConfigItems.ToList();
        }

        public void UpdateUI()
        {
            foreach (IUIConfigItem uiConfigItem in _uiConfigItems)
            {
                uiConfigItem.UpdateUI();
            }
        }

        public void UpdateConfig()
        {
            foreach (IUIConfigItem uiConfigItem in _uiConfigItems)
            {
                uiConfigItem.UpdateConfig();
            }
        }
    }
}
