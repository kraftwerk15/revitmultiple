﻿#region License
//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
// ReSharper disable StringLiteralTypo

namespace RevitMultipleCore
{
    public static class RevitVersion
    {
        private const string RevitExecutableFileName = "Revit.exe";
        public record RevitVersionRecord
        {
            public int YearVersion { get; init; }
            public string RelativeAddinPath { get; init; }
            public string RevitMultipleAddinName { get; init; }
            public string RevitMultipleBaseName { get; init; }
            public string[] RevitExecutablePaths { get; init; }

            public string LocalFolderPath { get; init; }
        }

        public static List<RevitVersionRecord> SupportedRevitVersions = new()
        {
            new RevitVersionRecord
            {
                YearVersion = 2018, RelativeAddinPath = @".\Autodesk\Revit\Addins\2018",
                RevitMultipleAddinName = "RevitMultiple2018.addin",
                RevitMultipleBaseName = "RevitMultiple",
                RevitExecutablePaths = new[]
                    { @"C:\Program Files\Autodesk\Revit 2018", @"D:\Program Files\Autodesk\Revit 2018" },
                LocalFolderPath = @"C:\Users\" + Environment.UserName + @"\Documents\BIMLOCAL"
            },

            new RevitVersionRecord
            {
                YearVersion = 2019, RelativeAddinPath = @".\Autodesk\Revit\Addins\2019",
                RevitMultipleAddinName = "RevitMultiple2019.addin",
                RevitMultipleBaseName = "RevitMultiple",
                RevitExecutablePaths = new[]
                    { @"C:\Program Files\Autodesk\Revit 2019", @"D:\Program Files\Autodesk\Revit 2019" },
                LocalFolderPath = @"C:\Users\" + Environment.UserName + @"\Documents\BIMLOCAL"
            },

            new RevitVersionRecord
            {
                YearVersion = 2020, RelativeAddinPath = @".\Autodesk\Revit\Addins\2020",
                RevitMultipleAddinName = "RevitMultiple2020.addin",
                RevitMultipleBaseName = "RevitMultiple",
                RevitExecutablePaths = new[]
                    { @"C:\Program Files\Autodesk\Revit 2020", @"D:\Program Files\Autodesk\Revit 2020" },
                LocalFolderPath = @"C:\Users\" + Environment.UserName + @"\Documents\BIMLOCAL"
            },

            new RevitVersionRecord
            {
                YearVersion = 2021, RelativeAddinPath = @".\Autodesk\Revit\Addins\2021",
                RevitMultipleAddinName = "RevitMultiple2021.addin",
                RevitMultipleBaseName = "RevitMultiple",
                RevitExecutablePaths = new[]
                    { @"C:\Program Files\Autodesk\Revit 2021", @"D:\Program Files\Autodesk\Revit 2021" },
                LocalFolderPath = @"C:\Users\" + Environment.UserName + @"\Documents\BIMLOCAL"
            },

            new RevitVersionRecord
            {
                YearVersion = 2022, RelativeAddinPath = @".\Autodesk\Revit\Addins\2022",
                RevitMultipleAddinName = "RevitMultiple2022.addin",
                RevitMultipleBaseName = "RevitMultiple",
                RevitExecutablePaths = new[]
                    { @"C:\Program Files\Autodesk\Revit 2022", @"D:\Program Files\Autodesk\Revit 2022" },
                LocalFolderPath = @"C:\Users\" + Environment.UserName + @"\Documents\BIMLOCAL"
            }

        };

        public static string GetRevitExecutableFolderPath(int revitVersion)
        {
            if (!SupportedRevitVersions.Exists(v => v.YearVersion == revitVersion)) return null;
            string[] folderPaths = SupportedRevitVersions.Single(r => r.YearVersion.Equals(revitVersion))
                .RevitExecutablePaths;
            return folderPaths.FirstOrDefault(folderPath =>
                File.Exists(Path.Combine(folderPath, RevitExecutableFileName)));
        }

        public static string GetRevitExecutableFilePath(int revitVersion)
        {
            string folderPath = GetRevitExecutableFolderPath(revitVersion);

            return (folderPath != null) ? Path.Combine(folderPath, RevitExecutableFileName) : null;
        }

        public static string GetRevitLocalFolderPath(int revitVersion)
        {
            foreach (RevitVersionRecord v in SupportedRevitVersions.Where(v => v.YearVersion == revitVersion))
            {
                return v.LocalFolderPath;
            }
            return "UNSUPPORTED";
        }

        public static string GetRevitLocalFilePath(int revitVersion, string centralFilePath)
        {
            string localFilePath = null;

            string localFolderPath = GetRevitLocalFolderPath(revitVersion);

            if (localFolderPath == null) return localFilePath;
            string localFileName = Path.GetFileNameWithoutExtension(centralFilePath) + "_" + Environment.UserName + Path.GetExtension(centralFilePath);

            localFilePath = Path.Combine(localFolderPath, localFileName);

            return localFilePath;
        }

        public static bool IsRevitVersionInstalled(int revitVersion)
        {
            return File.Exists(GetRevitExecutableFilePath(revitVersion));
        }

        public static int GetMinimumInstalledRevitVersion()
        {
            return GetInstalledRevitVersions().OrderBy(supportedRevitVersion => supportedRevitVersion).FirstOrDefault();
        }

        public static IEnumerable<int> GetInstalledRevitVersions()
        {
            return SupportedRevitVersions.Select(q => q.YearVersion).Where(IsRevitVersionInstalled).ToList();
        }
        
        /// <summary>
        /// Returns Revit Installed Versions Greater than the given Revit Version
        /// </summary>
        /// <param name="revitVersion">The Revit Version Number</param>
        /// <returns>A List of Integers matching the Revit Versions Available.</returns>
        public static IEnumerable<int> GetInstalledRevitVersionsAboveVersion(int revitVersion)
        {
            return SupportedRevitVersions.Where(x => x.YearVersion > revitVersion).Select(q => q.YearVersion)
                .OrderByDescending(x => x);
        }
        // ReSharper disable once IdentifierTypo
        public static IEnumerable<int> GetInstalledRevitAddinVersions()
        {
            return SupportedRevitVersions.Select(q => q.YearVersion).Where(IsRevitVersionInstalled)
                .Where(IsRevitMultipleAddinInstalled).ToList();
        }

        public static string GetRevitVersionText(int revitVersion)
        {
            foreach (RevitVersionRecord v in SupportedRevitVersions.Where(v => v.YearVersion == revitVersion))
            {
                return v.YearVersion.ToString();
            }
            return "UNSUPPORTED";
        }

        public static bool IsSupportedRevitVersionNumber(string revitVersionNumber)
        {
            return SupportedRevitVersions.Exists(r => r.YearVersion.ToString().Equals(revitVersionNumber));
        }

        public static int GetSupportedRevitVersion(string revitVersionNumber)
        {
            return SupportedRevitVersions.Single(keyValue => keyValue.YearVersion.ToString() == revitVersionNumber)
                .YearVersion;
        }

        public static string GetRevitAddinsFolderPath(int revitVersion, Environment.SpecialFolder specialFolder)
        {
            return Path.Combine(Environment.GetFolderPath(specialFolder), 
                SupportedRevitVersions.Single(z => z.YearVersion.Equals(revitVersion)).RelativeAddinPath);
        }

        public static void InstallAddins()
        {
            //GetExecutableFolderPath();

            //File.Copy();
        }
        
        // ReSharper disable once IdentifierTypo
        /// <summary>
        /// Confirms whether the BatchRevitFileAddin Manifest is installed.
        /// </summary>
        /// <param name="revitVersion">The Revit Version to check.</param>
        /// <returns>A boolean of whether the addin is installed.</returns>
        internal static bool IsRevitMultipleAddinInstalled(int revitVersion)
        {
            // ReSharper disable once IdentifierTypo
            bool isAddinInstalled = false;

            Environment.SpecialFolder[] revitAddinsBaseFolders = { Environment.SpecialFolder.CommonApplicationData, Environment.SpecialFolder.ApplicationData };

            List<string> revitAddinsFolderPaths = revitAddinsBaseFolders.Select(f => GetRevitAddinsFolderPath(revitVersion, f)).ToList();
            
            // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
            foreach (string revitAddinsFolderPath in revitAddinsFolderPaths)
            {
                string batchRvtAddinFilePath = Path.Combine(revitAddinsFolderPath, 
                    SupportedRevitVersions.Single(r => r.YearVersion == revitVersion).RevitMultipleAddinName);

                if (File.Exists(batchRvtAddinFilePath))
                {
                    isAddinInstalled = true;
                }
            }

            return isAddinInstalled;
        }
    }
}
