﻿using System;
using System.IO;
using System.IO.Packaging;
using System.Text;

namespace RevitMultipleCore
{
    public class RevitInfoFromBasic
    {
        private const string StreamName = "BasicFileInfo";
        public int RevitVersion { get; set; }
        private string RevitFilePath { get; set;}
        public RevitInfoFromBasic(string filePath)
        {
            RevitFilePath = filePath;
            var rawData = GetRawBasicFileInfo(filePath);
            BasicFileInfo basicFileInfo = new BasicFileInfo();
            using (var ms = new MemoryStream(rawData))
            {
                using (var br = new BinaryReader(ms, System.Text.Encoding.Unicode))
                {
                    basicFileInfo.A = br.ReadInt32();
                    basicFileInfo.B = br.ReadInt32();
                    basicFileInfo.C = br.ReadInt16();
 
                    var sLen = br.ReadInt32();
 
                    basicFileInfo.Path1 
                        = System.Text.Encoding.Unicode.GetString(br.ReadBytes(sLen * 2));
 
                    sLen = br.ReadInt32();
                    basicFileInfo.Version = System.Text.Encoding.Unicode.GetString(br.ReadBytes(sLen * 2));
 
                    sLen = br.ReadInt32();
                    basicFileInfo.Path2 = System.Text.Encoding.Unicode.GetString(br.ReadBytes(sLen * 2));
                    
                    
                    basicFileInfo.Unknown = br.ReadBytes(5);
 
                    //sLen = br.ReadInt32();
                    //basicFileInfo.UID = System.Text.Encoding.Unicode.GetString(br.ReadBytes(sLen * 2));
                    //sLen = br.ReadInt32();
                    //basicFileInfo.Localization = System.Text.Encoding.Unicode.GetString(br.ReadBytes(sLen * 2));
 
                    
 
                    //read to end
 
                    //br.ReadBytes(2); // \r \n
                    //sLen = (int) (br.BaseStream.Length - br.BaseStream.Position) - 2;

                    //var buffer =
                    //    br.ReadBytes(sLen);

                    //basicFileInfo.Data = Encoding.Unicode.GetString(buffer);

                    //br.ReadBytes(2); // \r \n
                }
            }
 
            var rawString = System.Text.Encoding.Unicode.GetString(rawData);
 
            var fileInfoData = rawString.Split(new string[] { "\0", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (string.IsNullOrEmpty(basicFileInfo.Version)) return;
            if (basicFileInfo.Version.Length != 4)
            {
                string temp = basicFileInfo.Version.Remove(0, 15);
                string temp2 = temp[..4];
                RevitVersion = Convert.ToInt32(temp2);
            }
            else
            {
                RevitVersion = Convert.ToInt32(basicFileInfo.Version);   
            }
        }

        private byte[] GetRawBasicFileInfo(string revitFileName)
        {
            if (!StructuredStorageUtils.IsFileStucturedStorage(revitFileName))
                throw new NotSupportedException("File is not a structured storage file");


            using (StructuredStorageRoot ssRoot =
                new StructuredStorageRoot(RevitFilePath))
            {
                if (!ssRoot.BaseRoot.StreamExists(StreamName))
                    throw new NotSupportedException(string.Format("File doesn't contain {0} stream", StreamName));

                StreamInfo imageStreamInfo =
                    ssRoot.BaseRoot.GetStreamInfo(StreamName);
                using (Stream stream = imageStreamInfo.GetStream(FileMode.Open, FileAccess.Read))
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    return buffer;
                }
            }
        }
    }
}