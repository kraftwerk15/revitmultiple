﻿#region License
// Revit Batch Processor
//
// Copyright (c) 2020  Dan Rumery, BVN
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Guid = System.Guid;

using PathTooLongException = System.IO.PathTooLongException;
using NLog;

namespace RevitMultipleCore
{
    public class RevitFileList
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public class RevitFilePathData
        {
            public string RevitFilePath  { get; set; }
            public List<string> AssociatedData { get; set; }
            public RevitFilePathData(string revitFilePath, string[] associatedData = null)
            {
                RevitFilePath = revitFilePath.Trim();
                foreach (string k in associatedData)
                {
                    AssociatedData.Add( k.Trim());
                }
            }
        }

           
        public static string FirstOrDefault(string[] items)
        {
            foreach (string item in items)
            {
                return item;
            }

            return string.Empty;
        }

        public static List<RevitFilePathData> GetRevitFileListData(List<string[]> rows)
        {
            List<RevitFilePathData> r = new();
            foreach (string[] k in rows)
            {
                if (!string.IsNullOrWhiteSpace(FirstOrDefault(k)))
                {
                    string[] temp = Array.Empty<string>();
                    if (k.Length != 1) temp[0] = k[1]; 
                    r.Add(new RevitFilePathData(k[0], temp));
                }
            }
            return r;
        }

        public static List<RevitFilePathData> FromTextFile(string textFilePath)
        {
            List<string[]> rows = TextFileUtil.GetRowsFromTextFile(textFilePath);
            return GetRevitFileListData(rows);
        }

        public static List<RevitFilePathData> FromText(string text)
        {
            List<string[]> rows = TextFileUtil.GetRowsFromText(text);
            return GetRevitFileListData(rows);
        }

        public static List<RevitFilePathData> FromLines(List<string> lines)
        {
            List<string[]> rows = TextFileUtil.GetRowsFromLines(lines);
            return GetRevitFileListData(rows);
        }

        public static List<RevitFilePathData> FromCSVFile(string csvFilePath)
        {
            List<string[]> rows = CSVUtil.GetRowsFromCSVFile(csvFilePath);
            return GetRevitFileListData(rows);
        }

        public static bool IsExcelInstalled()
        {
            return System.Type.GetTypeFromProgID("Excel.Application") != null;
        }

        public static bool HasExcelFileExtension(string filePath)
        {
            return PathUtil.HasFileExtension(filePath, ".xlsx") || PathUtil.HasFileExtension(filePath, ".xls");
        }

        public static List<RevitFilePathData> FromExcelFile(string excelFilePath)
        {
            return GetRevitFileListData(ExcelUtil.ReadRowsTextFromWorkbook(excelFilePath));
        }

        public static object FromConsole()
        {
            return FromLines(CommandLineUtil.ReadLines());
        }

        public class RevitCloudModelInfo
        {
            private static readonly NLog.Logger Logger  = NLog.LogManager.GetCurrentClassLogger();
            public                  bool        IsValid = false;
            public                  Guid        ProjectGuid;
            public                  Guid        ModelGuid;
            private                 string      _cloudModelDescriptor;
            private                 string      revitVersionText;
            public RevitCloudModelInfo(string cloudModelDescriptor)
            {
                _cloudModelDescriptor = cloudModelDescriptor;
                ProjectGuid           = Guid.Empty;
                ModelGuid             = Guid.Empty;
                revitVersionText = null;
               
                string[] parts = GetCloudModelDescriptorParts(cloudModelDescriptor);
                int numberOfParts = parts.Length;
                if (numberOfParts is not (2 or 3)) return;
                string   revitVersionPart = string.Empty;
                if (numberOfParts == 3)
                {
                    revitVersionPart = parts[0];
                    parts[0]    = parts[1];
                    parts[1]    = parts[2];
                }
                ProjectGuid = SafeParseGuidText(parts[0]);
                ModelGuid   = SafeParseGuidText(parts[1]);
                if (RevitVersion.IsSupportedRevitVersionNumber(revitVersionPart))
                {
                    revitVersionText = revitVersionPart;
                }
                IsValid = ProjectGuid != Guid.Empty && ModelGuid != Guid.Empty;
            }

            public string GetRevitVersionText()
            {
                return revitVersionText;
            }

            public string[] GetCloudModelDescriptorParts(string cloudModelDescriptor)
            {
                return cloudModelDescriptor.Split(new List<string> {" "}.ToArray(), StringSplitOptions.RemoveEmptyEntries);
            }

            public Guid SafeParseGuidText(string guidText)
            {
                bool response = Guid.TryParse(guidText, out Guid guid2);
                return response ? guid2 : Guid.Empty;
            }

            public virtual string GetCloudModelDescriptor()
            {
                return _cloudModelDescriptor;
            }
        }

        public class RevitFileInfo
        {
            private static readonly NLog.Logger         Logger = NLog.LogManager.GetCurrentClassLogger();
            private                 Guid                ProjectGuid    { get; set; }
            public                  RevitCloudModelInfo CloudModelInfo { get; set; }
            public                  string              RevitFilePath  { get; set; }
            public                  long                RevitFileSize  { get; set; }
            
            public RevitFileInfo(string revitFilePath)
            {
                CloudModelInfo = new RevitCloudModelInfo(revitFilePath);
                try
                {
                    RevitFilePath = PathUtil.GetFullPath(revitFilePath);
                    RevitFileSize = PathUtil.GetFileSize(RevitFilePath);
                }
                catch (ArgumentException e)
                {
                    // Catch exceptions such as 'Illegal characters in path.'
                    Logger.Error(e);
                }
                catch (NotSupportedException e)
                {
                    // Catch exceptions such as 'The given path's format is not supported.'
                    Logger.Error(e);
                }
                catch (PathTooLongException e)
                {
                    // Catch exceptions such as 'The specified path, file name, or both are too long.'
                    Logger.Error(e);
                }
                RevitFilePath = revitFilePath;
            }

            //public virtual object IsCloudModel()
            //{
            //    return this.GetRevitCloudModelInfo().IsValid();
            //}
            //
            //public virtual object IsValidFilePath()
            //{
            //    return this.pathException == null;
            //}
            //
            //public virtual object IsFilePathTooLong()
            //{
            //    return this.pathException is PathTooLongException;
            //}

            public virtual string GetFullPath()
            {
                return !CloudModelInfo.IsValid ? RevitFilePath : CloudModelInfo.GetCloudModelDescriptor();
            }

            public virtual object GetFileSize()
            {
                return PathUtil.GetFileSize(RevitFilePath);
            }

            public string TryGetRevitVersionText()
            {
                string revitVersionText = null;
                try
                {
                    revitVersionText = CloudModelInfo.GetRevitVersionText();
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }

                try
                {
                    if (string.IsNullOrEmpty(revitVersionText))
                    {
                        RevitInfoFromBasic basic = new(RevitFilePath);
                        revitVersionText = basic.RevitVersion.ToString();
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }
                
                
                return revitVersionText;
            }

            public virtual bool Exists()
            {
                return PathUtil.FileExists(RevitFilePath);
            }
        }

        public static List<RevitFilePathData> FromFile(string settingsFilePath)
        {
            List<RevitFilePathData> revitFileListData = null;
            if (TextFileUtil.HasTextFileExtension(settingsFilePath))
            {
                revitFileListData = FromTextFile(settingsFilePath);
            }
            else if (CSVUtil.HasCSVFileExtension(settingsFilePath))
            {
                revitFileListData = FromCSVFile(settingsFilePath);
            }
            else if (HasExcelFileExtension(settingsFilePath))
            {
                revitFileListData = FromExcelFile(settingsFilePath);
            }
            return revitFileListData;
        }

        public class SupportedRevitFileInfo
        {
            private static readonly NLog.Logger       Logger = NLog.LogManager.GetCurrentClassLogger();
            public                  RevitFilePathData RevitFilePathData  { get; set; }
            public                  string            RevitVersionText   { get; set; }
            public                  int               RevitVersionNumber { get; set; }
            public                 RevitFileInfo     RevitFileInfo      { get; set; }
            public SupportedRevitFileInfo(RevitFilePathData revitFilePathData)
            {
                RevitFileInfo revitFileInfo = new(revitFilePathData.RevitFilePath);
                RevitFileInfo     = revitFileInfo;
                RevitFilePathData = revitFilePathData;
                string revitVersionText = string.Empty;
                int revitVersionNumber = -1;
                if (revitFileInfo.CloudModelInfo.IsValid)
                {
                    revitVersionText = revitFileInfo.CloudModelInfo.GetRevitVersionText();
                    if (!string.IsNullOrWhiteSpace(revitVersionText))
                    {
                        if (RevitVersion.IsSupportedRevitVersionNumber(revitVersionText))
                        {
                            revitVersionNumber = RevitVersion.GetSupportedRevitVersion(revitVersionText);
                        }
                    }
                }
                else
                {
                    revitVersionText = RevitFileInfo.TryGetRevitVersionText();
                    if (!string.IsNullOrWhiteSpace(revitVersionText))
                    {
                        if(revitVersionText.StartsWith(RevitVersion.SupportedRevitVersions[0].YearVersion.ToString()))
                        {
                            revitVersionNumber = RevitVersion.SupportedRevitVersions[0].YearVersion;
                        }
                        if (revitVersionText.StartsWith(RevitVersion.SupportedRevitVersions[1].ToString()))
                        {
                            revitVersionNumber = RevitVersion.SupportedRevitVersions[1].YearVersion;
                        }
                        if (revitVersionText.StartsWith(RevitVersion.SupportedRevitVersions[2].ToString()))
                        {
                            revitVersionNumber = RevitVersion.SupportedRevitVersions[2].YearVersion;
                        }
                        if (revitVersionText.StartsWith(RevitVersion.SupportedRevitVersions[3].ToString()))
                        {
                            revitVersionNumber = RevitVersion.SupportedRevitVersions[3].YearVersion;
                        }
                        if (revitVersionText.StartsWith(RevitVersion.SupportedRevitVersions[4].ToString()))
                        {
                            revitVersionNumber = RevitVersion.SupportedRevitVersions[4].YearVersion;
                        }
                    }
                }
                RevitVersionText = revitVersionText;
                RevitVersionNumber = revitVersionNumber;
            }

            public SupportedRevitFileInfo Clone()
            {
                return new (RevitFilePathData);
            }
        }
    }
}