﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NLog.LayoutRenderers;

namespace RevitMultipleCore
{
    public static class TextFileUtil
    {
        private static readonly NLog.Logger Logger            = NLog.LogManager.GetCurrentClassLogger();
        public const            string      TextFileExtension = ".txt";
        public const            string      TextFileFilter    = "Text files (*.txt)|*.txt";

        // ReSharper disable once InconsistentNaming
        public static void WriteToCSVFile(
            IEnumerable<IEnumerable<object>> rows,
            string csvFilePath,
            string delimiter,
            Encoding encoding
        )
        {
            List<string> lines = (
                rows.Select(row => string.Join(delimiter, row.Select(value => value.ToString())))
                    .ToList()
            );

            File.WriteAllLines(csvFilePath, lines, encoding);
        }

        public static void WriteToTabDelimitedTxtFile(IEnumerable<IEnumerable<object>> rows, string txtFilePath, Encoding encoding = null)
        {
            WriteToCSVFile(rows, txtFilePath, "\t", encoding ?? Encoding.UTF8);
        }

        public static string[] ReadAllLines(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath);
            if (lines.Length <= 0) return lines;
            // Workaround for a potential lack of detection of Unicode txt files.
            if (lines[0].Contains("\x00"))
            {
                lines = File.ReadAllLines(filePath, Encoding.Unicode);
            }
            return lines;
        }

        public static bool HasTextFileExtension(string filePath)
        {
            return PathUtil.HasFileExtension(filePath, TextFileExtension);
        }

        public static List<string> GetLinesFromText(string text)
        {
            StringReader reader = new(text);
            List<string> lines  = new();
            try
            {
                string? line = reader.ReadLine();
                while (line != null)
                {
                    lines.Append(line);
                    line = reader.ReadLine();
                }
            }
            finally
            {
                reader.Dispose();
            }
            return lines;
        }

        public static List<string[]> GetRowsFromLines(List<string> lines)
        {
            return (from line in lines select line.Split("\t")).ToList();
        }

        public static List<string[]> GetRowsFromText(string text)
        {
            List<string> lines = GetLinesFromText(text);
            return GetRowsFromLines(lines);
        }

        public static List<string[]> GetRowsFromTextFile(string filePath)
        {
            List<string> lines = ReadAllLines(filePath).ToList();
            return GetRowsFromLines(lines);
        }

        public static void WriteToTextFile(string textFilePath, string text)
        {
            Logger.Debug("Preparing Write to Text File");
            PathUtil.CreateDirectoryForFilePath(textFilePath);
            Logger.Debug("Opening File Stream");
            FileStream fileStream = StreamIOUtil.CreateFile(textFilePath, true);
            Func<object> fileStreamAction = () => {
                StreamWriter textWriter = StreamIOUtil.GetStreamWriter(fileStream);
                Func<object> textWriterAction = () =>
                {
                    Logger.Debug("Writing message now");
                    textWriter.Write(text);
                    return null;
                };
                Logger.Debug("Using Inner Stream");
                //Potentially should fail at this point
                StreamIOUtil.UsingStream(fileStream, textWriterAction);
                return null;
            };
            Logger.Debug("Using Outer Stream.");
            StreamIOUtil.UsingStream(fileStream, fileStreamAction);
            Logger.Debug("Stream completed.");
        }

        public static object WriteLinesToTextFile(string textFilePath, string[] lines)
        {
            PathUtil.CreateDirectoryForFilePath(textFilePath);
            FileStream fileStream = StreamIOUtil.CreateFile(textFilePath, true);

            object FileStreamAction()
            {
                StreamWriter textWriter = StreamIOUtil.GetStreamWriter(fileStream);

                object TextWriterAction()
                {
                    foreach (string line in lines)
                    {
                        textWriter.WriteLine(line);
                    }

                    return null;
                }

                //Potentially should fail at this point
                StreamIOUtil.UsingStreamWriter(textWriter, TextWriterAction);
                return null;
            }

            StreamIOUtil.UsingStream(fileStream, FileStreamAction);
            return null;
        }

        public static object ReadFromTextFile(string textFilePath)
        {
            FileStream fileStream = StreamIOUtil.OpenFile(textFilePath, true);
            Func<object> fileStreamAction = () => {
                StreamReader textReader = StreamIOUtil.GetStreamReader(fileStream);
                Func<object> textReaderAction = () => {
                    string text = textReader.ReadToEnd();
                    return text;
                };
                var text = StreamIOUtil.UsingStreamReader(textReader, textReaderAction);
                return text;
            };
            var text = StreamIOUtil.UsingStream(fileStream, fileStreamAction);
            return text;
        }
    }
}
