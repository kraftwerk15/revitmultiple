﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevitMultipleCore
{
    public class TimeUtil
    {
        // ReSharper disable once InconsistentNaming
        public static string ISO8601_FORMAT_LOCAL = "yyyy-MM-ddTHH:mm:ss.fff";

        // ReSharper disable once InconsistentNaming
        public static string ISO8601_FORMAT_UTC = "yyyy-MM-ddTHH:mm:ss.fffZ";

        public static DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }

        public static DateTime GetDateTimeUtcNow()
        {
            return DateTime.UtcNow;
        }

        // ReSharper disable once InconsistentNaming
        public static string GetISO8601FormattedUtcDate(DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString(ISO8601_FORMAT_UTC);
        }

        // ReSharper disable once InconsistentNaming
        public static string GetISO8601FormattedLocalDate(DateTime dateTime)
        {
            return dateTime.ToLocalTime().ToString(ISO8601_FORMAT_LOCAL);
        }

        public static Dictionary<string, string> GetTimestampObject(DateTime dateTime)
        {
            return new () {
                { "local", GetISO8601FormattedLocalDate(dateTime) },
                { "utc", GetISO8601FormattedUtcDate(dateTime) }
            };
        }

        private static int GetDateTimeDifferenceInSeconds(DateTime startDateTime, DateTime endDateTime)
        {
            return Convert.ToInt32((endDateTime - startDateTime).TotalSeconds);
        }

        public static int GetSecondsElapsedSince(DateTime dateTime)
        {
            return GetDateTimeDifferenceInSeconds(dateTime, GetDateTimeNow());
        }

        public static int GetSecondsElapsedSinceUtc(DateTime utcDateTime)
        {
            return GetDateTimeDifferenceInSeconds(utcDateTime, GetDateTimeUtcNow());
        }

        public static DateTime GetDateTimeFromISO8601FormattedDate(ReadOnlySpan<char> isoFormattedDate)
        {
            return DateTime.ParseExact(isoFormattedDate, ISO8601_FORMAT_LOCAL, CultureInfo.InvariantCulture);
        }

        public static DateTime GetDateTimeUtcFromISO8601FormattedDate(ReadOnlySpan<char> isoFormattedDate)
        {
            return DateTime.ParseExact(isoFormattedDate, ISO8601_FORMAT_UTC, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
        }

        //public static object WithMeasuredTimeElapsed(object action)
        //{
        //    var start = DateTime.Now;
        //    var result = action();
        //    var end = DateTime.Now;
        //    return Tuple.Create(result, end - start);
        //}
    }
}
