﻿using System.Text.RegularExpressions;

namespace RevitMultipleCore
{
    public class Regex
    {
        private const string Pattern = @"(\\\d{4}\\)";

        public static string Match(string input)
        {
            foreach (Match m in System.Text.RegularExpressions.Regex.Matches(input, Pattern))
            {
                string temp = m.Value;
                if (m.Value.StartsWith(@"\"))
                    temp = temp.Substring(1, temp.Length-1);
                if (m.Value.EndsWith(@"\"))
                    temp = temp.Substring(0, temp.Length-1);
                return temp;
            }

            return string.Empty;
        }
    }
}
