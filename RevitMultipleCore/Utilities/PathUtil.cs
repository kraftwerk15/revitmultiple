﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using NLog;

namespace RevitMultipleCore
{
    public static class PathUtil
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static bool FileExists(string filePath)
        {
            return File.Exists(filePath);
        }

        public static bool DirectoryExists(string folderPath)
        {
            return Directory.Exists(folderPath);
        }

        public static string GetFullPath(string path)
        {
            return Path.GetFullPath(path);
        }

        public static string GetLocalAppDataFolderPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        }

        public static string GetExistingFileDirectoryPath(string existingFilePath)
        {
            string initialDirectory = null;

            if (File.Exists(existingFilePath))
            {
                initialDirectory = Path.GetDirectoryName(existingFilePath);
            }

            return initialDirectory;
        }

        public static string GetFileDirectoryName(string filePath)
        {
            return new FileInfo(filePath).Directory?.Name;
        }

        public static bool HasExtension(string filePath, string extension)
        {
            return string.Equals(Path.GetExtension(filePath), extension, StringComparison.CurrentCultureIgnoreCase);
        }

        private static void IgnoringPathExceptions(Action action)
        {
            IgnoringPathExceptions(
                    () => {
                        action();
                        return true; // dummy return type/value.
                    }
                );

            return;
        }

        private static T IgnoringPathExceptions<T>(Func<T> func)
        {
            T result = default;

            try
            {
                result = func();
            }
            catch (UnauthorizedAccessException)
            {
                // Do nothing.
            }
            catch (PathTooLongException)
            {
                // Do nothing.
            }
            catch (IOException)
            {
                // Do nothing.
            }

            return result;
        }

        public static IEnumerable<string> SafeEnumerateFiles(string root, string pattern, SearchOption searchOption)
        {
            // NOTE: DirectoryInfo can throw path-related exceptions, hence the use of IgnoringPathExceptions here.
            return IgnoringPathExceptions(
                    () => SafeEnumerateFiles(new DirectoryInfo(root), pattern, searchOption)) ?? Enumerable.Empty<string>();
        }

        // See https://stackoverflow.com/questions/13130052/directoryinfo-enumeratefiles-causes-unauthorizedaccessexception-and-other
        public static IEnumerable<string> SafeEnumerateFiles(DirectoryInfo root, string pattern, SearchOption searchOption)
        {
            if (root == null || !root.Exists) yield break;
            List<string> topLevelFilePaths = IgnoringPathExceptions(
                () => {
                    return root
                        .EnumerateFiles(pattern, SearchOption.TopDirectoryOnly)
                        .Select(fileInfo => fileInfo.FullName)
                        .ToList(); // Ensures these are fully enumerate here so any exceptions are caught.
                }
            );

            foreach (string filePath in (topLevelFilePaths ?? Enumerable.Empty<string>()))
            {
                yield return filePath;
            }

            if (searchOption != SearchOption.AllDirectories) yield break;
            {
                IEnumerable<string> subFolderFilePaths = IgnoringPathExceptions(
                    () => {
                        List<DirectoryInfo> topLevelFolderPaths = root
                            .EnumerateDirectories("*", SearchOption.TopDirectoryOnly)
                            .ToList(); // Ensures these are fully enumerate here so any exceptions are caught.

                        return topLevelFolderPaths.SelectMany(dir => SafeEnumerateFiles(dir, pattern, searchOption));
                    }
                );

                foreach (string filePath in (subFolderFilePaths ?? Enumerable.Empty<string>()))
                {
                    yield return filePath;
                }
            }
        }

        public static IEnumerable<string> SafeEnumerateFolders(string root, string pattern, SearchOption searchOption)
        {
            // NOTE: DirectoryInfo can throw path-related exceptions, hence the use of IgnoringPathExceptions here.
            return IgnoringPathExceptions(
                    () => SafeEnumerateFolders(new DirectoryInfo(root), pattern, searchOption)) ?? Enumerable.Empty<string>();
        }

        // See https://stackoverflow.com/questions/13130052/directoryinfo-enumeratefiles-causes-unauthorizedaccessexception-and-other
        public static IEnumerable<string> SafeEnumerateFolders(DirectoryInfo root, string pattern, SearchOption searchOption)
        {
            if (root is not {Exists: true}) yield break;
            List<string> topLevelFolderPaths = IgnoringPathExceptions(
                () => {
                    return root
                        .EnumerateDirectories(pattern, SearchOption.TopDirectoryOnly)
                        .Select(folderInfo => folderInfo.FullName)
                        .ToList(); // Ensures these are fully enumerate here so any exceptions are caught.
                }
            );

            foreach (string folderPath in (topLevelFolderPaths ?? Enumerable.Empty<string>()))
            {
                yield return folderPath;
            }

            if (searchOption != SearchOption.AllDirectories) yield break;
            {
                IEnumerable<string> subFolderFolderPaths = IgnoringPathExceptions(
                    () => {
                        List<DirectoryInfo> subFolderPaths = root
                            .EnumerateDirectories("*", SearchOption.TopDirectoryOnly)
                            .ToList(); // Ensures these are fully enumerate here so any exceptions are caught.

                        return subFolderPaths.SelectMany(dir => SafeEnumerateFolders(dir, pattern, searchOption));
                    }
                );

                foreach (string folderPath in (subFolderFolderPaths ?? Enumerable.Empty<string>()))
                {
                    yield return folderPath;
                }
            }
        }
        
        public static IEnumerable<string> EnumerateExpandedFullNetworkPaths(IEnumerable<string> fullPaths)
        {
           // InitPythonFunctions();
        
            return fullPaths.Select(ExpandedFullNetworkPath);
        }
        
        private static string[] GetRevitVersionTexts(string fullPath)
        {
            //var revitVersionText = (PYTHON_FUNCTION_RevitFileInfo(fullPath).TryGetRevitVersionText() as string) ?? string.Empty;
            //var revitVersionNumberText = (PYTHON_FUNCTION_GetRevitVersionNumberTextFromRevitVersionText(revitVersionText) as string) ?? string.Empty;

            //return new[] { revitVersionNumberText, revitVersionText };
            return new []{string.Empty};
        }
        
        public static IEnumerable<string[]> EnumerateRevitVersionTexts(IEnumerable<string> fullPaths)
        {
            //InitPythonFunctions();
        
            return fullPaths.Select(GetRevitVersionTexts);
        }

        public static void AddSearchPath(string searchPath)
        {
            // TODO: only add the path if it's not already added.
            //sys.path.append(searchPath);
        }

        public static object GetUserDesktopFolderPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }

        public static string GetFileExtension(string filePath)
        {
            return Path.GetExtension(filePath);
        }

        public static bool HasFileExtension(string filePath, string extension)
        {
            return string.Equals(GetFileExtension(filePath), extension, StringComparison.CurrentCultureIgnoreCase);
        }

        public static DirectoryInfo CreateDirectory(string folderPath)
        {
            DirectoryInfo directoryInfo = Directory.CreateDirectory(folderPath);
            return directoryInfo;
        }

        public static object CreateDirectoryForFilePath(string filePath)
        {
            DirectoryInfo directoryInfo = CreateDirectory(Path.GetDirectoryName(filePath));
            return directoryInfo;
        }

        public static long GetFileSize(string filePath)
        {
            long fileSize = -1;
            try
            {
                fileSize = new FileInfo(filePath).Length;
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return fileSize;
        }

        public static DateTime GetLastWriteTimeUtc(string filePath)
        {
            DateTime lastWriteTime = DateTime.MinValue;
            try
            {
                lastWriteTime = File.GetLastWriteTimeUtc(filePath);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return lastWriteTime;
        }

        public static string GetDriveLetter(string path)
        {
            string driveLetter = Path.GetPathRoot(path)?.Split(":")[0];
            return driveLetter?.Length == 1 ? driveLetter.ToUpper() : null;
        }
        
        public static string GetDriveRemoteName(string path)
        {
            string driveRemoteName = null;
            string driveLetter = GetDriveLetter(path);
            if (driveLetter != null)
            {
                driveRemoteName = GetUNCPath(driveLetter + ":");
            }
            return driveRemoteName;
        }

        [DllImport("mpr.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int WNetGetConnection(
            [MarshalAs(UnmanagedType.LPTStr)] string localName,
            [MarshalAs(UnmanagedType.LPTStr)] StringBuilder remoteName,
            ref int length);
        /// <summary>
        /// Given a path, returns the UNC path or the original. (No exceptions
        /// are raised by this function directly). For example, "P:\2008-02-29"
        /// might return: "\\networkserver\Shares\Photos\2008-02-09"
        /// </summary>
        /// <param name="originalPath">The path to convert to a UNC Path</param>
        /// <returns>A UNC path. If a network drive letter is specified, the
        /// drive letter is converted to a UNC or network path. If the 
        /// originalPath cannot be converted, it is returned unchanged.</returns>
        public static string GetUNCPath(string originalPath)
        {
            StringBuilder sb   = new(512);
            int           size = sb.Capacity;

            // look for the {LETTER}: combination ...
            if (originalPath.Length <= 2 || originalPath[1] != ':') return originalPath;
            // don't use char.IsLetter here - as that can be misleading
            // the only valid drive letters are a-z && A-Z.
            char c = originalPath[0];
            if (c is (< 'a' or > 'z') and (< 'A' or > 'Z')) return originalPath;
            int error = WNetGetConnection(originalPath[..2],
                sb, ref size);
            if (error != 0) return originalPath;
            DirectoryInfo dir = new(originalPath);

            string path = Path.GetFullPath(originalPath)[Path.GetPathRoot(originalPath).Length..];
            return Path.Combine(sb.ToString().TrimEnd(), path);

        }

        public static string GetFullNetworkPath(string path)
        {
            string fullNetworkPath = string.Empty;
            try
            {
                if (!Path.IsPathRooted(path))
                {
                    throw new ArgumentException("A full file path must be specified.", path);
                }
                string pathRoot = Path.GetPathRoot(path);
                string driveRemoteName = GetDriveRemoteName(pathRoot);
                if (driveRemoteName != null)
                {
                    if (pathRoot != null)
                    {
                        string pathWithoutRoot = path[pathRoot.Length..];
                        fullNetworkPath = Path.Combine(driveRemoteName, pathWithoutRoot);
                    }
                    //Path Root is null. report error?
                }
            }
            catch (PathTooLongException)
            {
                fullNetworkPath = null;
            }
            return fullNetworkPath;
        }

        public static string ExpandedFullNetworkPath(string path)
        {
            string expandedPath = GetFullNetworkPath(path);
            if (expandedPath != null) return expandedPath;
            expandedPath = path;
            return expandedPath;
        }

        public static bool DirectoryHasName(DirectoryInfo directoryInfo, string name)
        {
            return string.Equals(directoryInfo.Name, name, StringComparison.OrdinalIgnoreCase);
        }

        public static List<string> GetDirectoryParts(string folderPath)
        {
            List<string>  parts   = new();
            DirectoryInfo current = new(folderPath);
            while (current != null)
            {
                parts.Insert(0, current.Name);
                current = current.Parent;
            }
            return parts;
        }

        public static bool IsProjectYearFolderName(string folderName)
        {
            return folderName.Length == 2 && char.IsDigit(folderName[0]) && char.IsDigit(folderName[1]);
        }

        public static string GetProjectFolderNameFromRevitProjectFilePath(string revitProjectFilePath)
        {
            string projectFolderName = string.Empty;
            // NOTE: called Path.GetDirectoryName() before expanding the path (rather than after) in
            // order to reduce the risk of hitting .NET path length limit (260 characters?)
            string       folderPath    = ExpandedFullNetworkPath(Path.GetDirectoryName(revitProjectFilePath));
            List<string> parts         = GetDirectoryParts(folderPath);
            int          numberOfParts = parts.Count;
            if (numberOfParts <= 2) return null;
            if (IsProjectYearFolderName(parts[1]))
            {
                projectFolderName = parts[2];
            }
            return projectFolderName;
        }
    }
}
