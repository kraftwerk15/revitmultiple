﻿#nullable enable
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;

namespace RevitMultipleCore
{
    public class SnapshotDataUtil
    {
        private static readonly NLog.Logger Logger                      = NLog.LogManager.GetCurrentClassLogger();
        public const string SnapshotFolderNameFormat = "yyyyMMdd_HHmmss_fff";

        public static string PROJECT_BIM_FOLDER_NAME = "BIM";

        public static string PROJECT_FILES_FOLDER_NAME = "1.0 Project Files";

        public const string SnapshotDataFilename = "snapshot.json";

        public const string TempSnapshotDataFilename = "temp_snapshot.json";

        public const string SnapshotDataRevitJournalFile = "revitJournalFile";

        private static string GetUnknownProjectUniqueFolderName()
        {
            return Path.GetRandomFileName().Replace(".", string.Empty).ToUpper();
        }
        
        /// <summary>
        /// Transforms a DateTime stamp in to the FolderName for the Snapshot
        /// </summary>
        /// <param name="timestamp">The DateTime Stamp</param>
        /// <returns>The folder name.</returns>
        public static string GetSnapshotFolderName(DateTime timestamp)
        {
            string folderName = timestamp.ToString(SnapshotFolderNameFormat);
            return folderName;
        }

        internal static string GetRevitModelName(string revitFilePath)
        {
            return Path.GetFileNameWithoutExtension(revitFilePath);
        }

        public static string GetRevitFileVersionDetails(string revitFilePath)
        {
            RevitFileList.RevitFileInfo revitFileInfo = new(revitFilePath);
            string revitFileVersionDetails = revitFileInfo.TryGetRevitVersionText();
            return revitFileVersionDetails;
        }
        
        /// <summary>
        /// Gets the Snapshot Folder Path
        /// </summary>
        /// <param name="dataExportFolderPath">The Data Export Folder Path</param>
        /// <param name="revitFilePath">A string to the Revit File Path</param>
        /// <param name="isCloudModel">If this file is a Cloud Model</param>
        /// <param name="cloudProjectId">The Project ID for the Cloud Model</param>
        /// <param name="cloudModelId">The Model ID for the Cloud Model</param>
        /// <param name="timestamp">The Snapshots Particular Time Stamp</param>
        /// <returns>A string of the Folder Path to the Snapshot folder.</returns>
        public static string GetSnapshotFolderPath(string dataExportFolderPath, string revitFilePath, bool isCloudModel,
            string cloudProjectId, string cloudModelId, DateTime timestamp)
        {
            string snapshotFolderPath;
            string snapshotFolderName = GetSnapshotFolderName(timestamp.ToLocalTime());
            if (isCloudModel)
            {
                snapshotFolderPath = Path.Combine(dataExportFolderPath, cloudProjectId, cloudModelId, snapshotFolderName);
            }
            else
            {
                string? projectFolderName = PathUtil.GetProjectFolderNameFromRevitProjectFilePath(revitFilePath);
                // ReSharper disable once ConvertIfStatementToNullCoalescingExpression
                if (projectFolderName == null)
                {
                    projectFolderName = GetUnknownProjectUniqueFolderName();
                }
                string? modelName = GetRevitModelName(revitFilePath);
                snapshotFolderPath = Path.Combine(dataExportFolderPath, projectFolderName, modelName, snapshotFolderName);
            }
            return snapshotFolderPath;
        }

        public static string GetSnapshotDataFilePath(string snapshotDataFolderPath)
        {
            return Path.Combine(snapshotDataFolderPath, SnapshotDataFilename);
        }

        public static string GetTemporarySnapshotDataFilePath(string snapshotDataFolderPath)
        {
            return Path.Combine(snapshotDataFolderPath, TempSnapshotDataFilename);
        }

        public static string ReadSnapshotDataRevitJournalFilePath(string snapshotDataFilePath)
        {
            string text = TextFileUtil.ReadFromTextFile(snapshotDataFilePath).ToString();
            JObject jobjectSnapshotData = JsonSerialization.DeserializeFromJson(text);
            
            return jobjectSnapshotData[SnapshotDataRevitJournalFile].ToString();
        }

        public static void CopySnapshotRevitJournalFile(string snapshotDataFolderPath)
        {
            string snapshotDataFilePath;
            string revitJournalFilePath = string.Empty;
            try
            {
                snapshotDataFilePath = GetSnapshotDataFilePath(snapshotDataFolderPath);
                revitJournalFilePath = ReadSnapshotDataRevitJournalFilePath(snapshotDataFilePath);
            }
            catch (Exception)
            {
                Logger.Warn("WARNING: failed to read journal file path from snapshot data file.");
                //exception_util.LogOutputErrorDetails(e, output);
                Logger.Warn("Attempting to read journal file path from temporary snapshot data file instead.");
                snapshotDataFilePath = GetTemporarySnapshotDataFilePath(snapshotDataFolderPath);
                revitJournalFilePath = ReadSnapshotDataRevitJournalFilePath(snapshotDataFilePath);
            }
            string? revitJournalFileName = Path.GetFileName(revitJournalFilePath);
            string snapshotRevitJournalFilePath = Path.Combine(snapshotDataFolderPath, revitJournalFileName);
            File.Copy(revitJournalFilePath, snapshotRevitJournalFilePath);
        }

        public static void ConsolidateSnapshotData(string dataExportFolderPath)
        {
            try
            {
                string? snapshotDataFilePath = GetSnapshotDataFilePath(dataExportFolderPath);
                string? temporarySnapshotDataFilePath = GetTemporarySnapshotDataFilePath(dataExportFolderPath);
                if (File.Exists(snapshotDataFilePath))
                {
                    if (File.Exists(temporarySnapshotDataFilePath))
                    {
                        File.Delete(temporarySnapshotDataFilePath);
                    }
                }
                else if (File.Exists(temporarySnapshotDataFilePath))
                {
                    File.Move(temporarySnapshotDataFilePath, snapshotDataFilePath);
                }
                else
                {
                    Logger.Warn("WARNING: could not find snapshot data file in snapshot data folder : {0}", dataExportFolderPath);
                }
            }
            catch (Exception)
            {
                Logger.Warn("WARNING: failed to properly consolidate the snapshot data in folder : {0}", dataExportFolderPath);
                //exception_util.LogOutputErrorDetails(e, output);
            }
        }
    }
}
