﻿#region License
//
// Revit Batch Processor
//
// Copyright (c) 2020  Dan Rumery, BVN
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
#region Using
using System;
using Path = System.IO.Path;
using File = System.IO.File;
using System.Collections.Generic;
#endregion
namespace RevitMultipleCore
{
    public class SnapshotDataExporter
    {
        public static SnapshotData GetSnapshotData(string sessionId, string revitFilePath, bool isCloudModel, string cloudProjectId, string cloudModelId,
            DateTime snapshotStartTime, DateTime snapshotEndTime, string snapshotFolderPath, string revitJournalFilePath, object snapshotError) 
        {
            string modelRevitVersionDetails;
            string modelRevitVersion;
            long modelFileSize;
            DateTime modelFileLastModified;
            string modelName;
            string modelFolder;
            string projectModelFolderPath;
            string projectFolderName;
            if (isCloudModel) {
                projectFolderName = null;
                projectModelFolderPath = null;
                modelFolder = null;
                modelName = null;
                modelFileLastModified = DateTime.MinValue;
                modelFileSize = 0;
                modelRevitVersion = null;
                modelRevitVersionDetails = null;
            } else {
                projectFolderName = PathUtil.GetProjectFolderNameFromRevitProjectFilePath(revitFilePath);
                projectModelFolderPath = Path.GetDirectoryName(revitFilePath);
                modelFolder = PathUtil.ExpandedFullNetworkPath(projectModelFolderPath);
                modelName = SnapshotDataUtil.GetRevitModelName(revitFilePath);
                modelFileLastModified = PathUtil.GetLastWriteTimeUtc(revitFilePath);
                modelFileSize = PathUtil.GetFileSize(revitFilePath);
                modelRevitVersion = RevitFileUtil.GetRevitFileVersion(revitFilePath);
                modelRevitVersionDetails = SnapshotDataUtil.GetRevitFileVersionDetails(revitFilePath);
            }

            return new SnapshotData
            {
                IsCloudModel = isCloudModel,
                CloudModelId = cloudModelId,
                ProjectFolderName = projectFolderName,
                CloudProjectId = cloudModelId,
                FileLastModified = modelFileLastModified,
                ModelFileSize = modelFileSize,
                ModelRevitVersion = modelRevitVersion,
                ModelRevitVersionDetails = modelRevitVersionDetails,
                SnapshotStartTime = snapshotStartTime,
                SnapshotEndTime = snapshotEndTime,
                SessionId = sessionId,
                SnapshotFolder = snapshotFolderPath,
                SnapshotError = snapshotError,
                RevitJournalFile = revitJournalFilePath,
                ModelFolder = modelFolder,
                ModelName = modelName,
            };
        }
        
        public static SnapshotData ExportSnapshotDataInternal(string snapshotDataFilePath, string sessionId, string revitProjectFilePath, bool isCloudModel,
             string cloudProjectId, string cloudModelId, DateTime snapshotStartTime, DateTime snapshotEndTime, string dataExportFolderPath, string revitJournalFilePath,
             object snapshotError) 
        {
            SnapshotData snapshotData = GetSnapshotData(sessionId, revitProjectFilePath, isCloudModel, cloudProjectId, 
                cloudModelId, snapshotStartTime, snapshotEndTime, dataExportFolderPath, revitJournalFilePath, snapshotError);
            //var serializedSnapshotData =
            JsonSerialization.WriteToJsonFile(snapshotDataFilePath, snapshotData, true, true);
            //TextFileUtil.WriteToTextFile(snapshotDataFilePath, serializedSnapshotData);
            return snapshotData;
        }
        
        public static SnapshotData ExportSnapshotData(string sessionId, string revitProjectFilePath, bool isCloudModel, string cloudProjectId, string cloudModelId,
            DateTime snapshotStartTime, DateTime snapshotEndTime, string dataExportFolderPath, string revitJournalFilePath, object snapshotError) 
        {
            SnapshotData snapshotData = ExportSnapshotDataInternal(SnapshotDataUtil.GetSnapshotDataFilePath(dataExportFolderPath), sessionId, 
                revitProjectFilePath, isCloudModel, cloudProjectId, cloudModelId, snapshotStartTime, snapshotEndTime, dataExportFolderPath, 
                revitJournalFilePath, snapshotError);
            return snapshotData;
        }
        
        public static SnapshotData ExportTemporarySnapshotData(string sessionId, string revitProjectFilePath, bool isCloudModel, string cloudProjectId, string cloudModelId,
            DateTime snapshotStartTime, DateTime snapshotEndTime, string dataExportFolderPath, string revitJournalFilePath, object snapshotError) {
            SnapshotData snapshotData = ExportSnapshotDataInternal(SnapshotDataUtil.GetTemporarySnapshotDataFilePath(dataExportFolderPath), 
                sessionId, revitProjectFilePath, isCloudModel, cloudProjectId, cloudModelId, snapshotStartTime, snapshotEndTime, dataExportFolderPath, 
                revitJournalFilePath, snapshotError);
            return snapshotData;
        }
    }
}