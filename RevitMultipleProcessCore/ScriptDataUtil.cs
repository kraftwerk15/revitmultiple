﻿//
//Code in this file is heavily modified from the Repository below. The license from the original repository is included in this file.
//

#region license
// Revit Batch Processor
//
// Copyright (c) 2020  Daniel Rumery, BVN
//
//https://github.com/bvn-architecture/RevitBatchProcessor
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using NLog;
using RevitMultipleCore;

namespace RevitMultipleProcessCore
{
    public static class ScriptDataUtil
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        private const string ScriptDataFilenamePrefix = "Session.ScriptData.";
        private const string SessionProgressRecordPrefix = "Session.ProgressRecord.";
        private const string JsonFileExtension = ".json";

        public class ScriptData : IPersistent
        {
            private readonly PersistentSettings _persistentSettings;

            public readonly StringSetting SessionId = new("sessionId");
            public readonly StringSetting RevitFilePath = new("revitFilePath");
            public readonly BooleanSetting IsCloudModel = new("isCloudModel");
            public readonly StringSetting CloudRegion = new("cloudRegion");
            public readonly StringSetting CloudProjectId = new("cloudProjectId");
            public readonly StringSetting CloudModelId = new("cloudModelId");
            public readonly BooleanSetting EnableDataExport = new("enableDataExport");
            public readonly StringSetting TaskScriptFilePath = new("taskScriptFilePath");
            public readonly StringSetting TaskData = new("taskData");
            public readonly StringSetting SessionDataFolderPath = new("sessionDataFolderPath");
            public readonly StringSetting DataExportFolderPath = new("dataExportFolderPath");
            public readonly BooleanSetting ShowMessageBoxOnTaskScriptError = new("showMessageBoxOnTaskError");
            public readonly EnumSetting<BatchRvt.RevitProcessingOption> RevitProcessingOption = new("revitProcessingOption");
            public readonly EnumSetting<BatchRvt.CentralFileOpenOption> CentralFileOpenOption = new("centralFileOpenOption");
            public readonly BooleanSetting DeleteLocalAfter = new("deleteLocalAfter");
            public readonly BooleanSetting DiscardWorksetsOnDetach = new("discardWorksetsOnDetach");
            public readonly EnumSetting<BatchRvt.WorksetConfigurationOption> WorksetConfigurationOption = new("worksetConfigurationOption");
            // ReSharper disable once InconsistentNaming
            public readonly BooleanSetting OpenInUI = new("openInUI");
            public readonly BooleanSetting AuditOnOpening = new("auditOnOpening");
            public readonly IntegerSetting ProgressNumber = new("progressNumber");
            public readonly IntegerSetting ProgressMax = new("progressMax");
            public readonly ListSetting<string> AssociatedData = new("associatedData");

            public BooleanSetting LibraryRequested = new("libraryRequested");
            public BooleanSetting DatabaseRequested = new("databaseRequested");

            public ScriptData()
            {
                _persistentSettings = new PersistentSettings(
                    new IPersistent[]
                    {
                        SessionId,
                        RevitFilePath,
                        IsCloudModel,
                        CloudProjectId,
                        CloudModelId,
                        EnableDataExport,
                        TaskScriptFilePath,
                        TaskData,
                        SessionDataFolderPath,
                        DataExportFolderPath,
                        ShowMessageBoxOnTaskScriptError,
                        RevitProcessingOption,
                        CentralFileOpenOption,
                        DeleteLocalAfter,
                        DiscardWorksetsOnDetach,
                        WorksetConfigurationOption,
                        OpenInUI,
                        AuditOnOpening,
                        ProgressNumber,
                        ProgressMax,
                        AssociatedData,
                        LibraryRequested,
                        DatabaseRequested
                    }
                );
            }

            public void Load(JObject jObject)
            {
                _persistentSettings.Load(jObject);
            }

            public void Store(JObject jObject)
            {
                _persistentSettings.Store(jObject);
            }

            public bool LoadFromFile(string filePath)
            {
                bool success;

                if (!File.Exists(filePath)) return false;
                try
                {
                    string text = File.ReadAllText(filePath);
                    JObject jObject = JsonSerialization.DeserializeFromJson(text);
                    _persistentSettings.Load(jObject);
                    success = true;
                }
                catch (Exception)
                {
                    success = false;
                }

                return success;
            }

            public bool SaveToFile(string filePath)
            {
                bool success;

                JObject jObject = new();

                try
                {
                    _persistentSettings.Store(jObject);
                    string settingsText = JsonSerialization.SerializeToJson(jObject, true);
                    FileInfo fileInfo = new(filePath);
                    fileInfo.Directory?.Create();
                    File.WriteAllText(fileInfo.FullName, settingsText);

                    success = true;
                }
                catch (Exception)
                {
                    success = false;
                }

                return success;
            }

            public string ToJsonString()
            {
                JObject jObject = new();
                Store(jObject);
                return jObject.ToString();
            }

            public static ScriptData FromJsonString(string scriptDataJson)
            {
                ScriptData scriptData;

                try
                {
                    JObject jObject = JsonSerialization.DeserializeFromJson(scriptDataJson);
                    scriptData = new ScriptData();
                    scriptData.Load(jObject);
                }
                catch (Exception)
                {
                    scriptData = null;
                }

                return scriptData;
            }
        }

        public static IEnumerable<ScriptData> LoadManyFromFile(string filePath)
        {
            // ReSharper disable once IdentifierTypo
            List<ScriptData> scriptDatas;

            if (!File.Exists(filePath)) return null;
            try
            {
                string text = File.ReadAllText(filePath);

                JArray jArray = JsonSerialization.DeserializeArrayFromJson(text);

                scriptDatas = new List<ScriptData>();

                foreach (JToken jToken in jArray)
                {
                    if (jToken is not JObject jObject) continue;
                    ScriptData scriptData = new();

                    scriptData.Load(jObject);

                    scriptDatas.Add(scriptData);
                }
            }
            catch (Exception)
            {
                scriptDatas = null; // null on failure.
            }

            return scriptDatas;
        }

        // ReSharper disable once IdentifierTypo
        public static bool SaveManyToFile(string filePath, IEnumerable<ScriptData> scriptDatas)
        {
            bool success;

            try
            {
                JArray jArray = new();

                foreach (ScriptData scriptData in scriptDatas)
                {
                    JObject jObject = new();

                    scriptData.Store(jObject);

                    jArray.Add(jObject);
                }

                string settingsText = JsonSerialization.SerializeToJson(jArray, true);

                FileInfo fileInfo = new(filePath);

                fileInfo.Directory?.Create();

                File.WriteAllText(fileInfo.FullName, settingsText);

                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public static string GetUniqueScriptDataFilePath()
        {
            string uniqueId = Guid.NewGuid().ToString();

            return Path.Combine(
                    BatchRvt.GetDataFolderPath(),
                    ScriptDataFilenamePrefix + uniqueId + JsonFileExtension
                );
        }

        public static string GetProgressRecordFilePath(string scriptDataFilePath)
        {
            string uniqueId = (
                    Path.GetFileNameWithoutExtension(scriptDataFilePath)[ScriptDataFilenamePrefix.Length..]
                );

            return Path.Combine(
                    Path.GetDirectoryName(scriptDataFilePath) ?? string.Empty,
                    SessionProgressRecordPrefix + uniqueId + JsonFileExtension
                );
        }

        public static bool SetProgressNumber(string progressRecordFilePath, int progressNumber)
        {
            bool success;

            try
            {
                Logger.Info("Preparing to write Progress Number to Json File");
                FileInfo fileInfo = new(progressRecordFilePath);
                if (fileInfo.Directory is null)
                {
                    Logger.Info("Directory does not exist for the Progress Record Path. Creating it now.");
                }
                fileInfo.Directory?.Create();
                
                File.WriteAllText(fileInfo.FullName, progressNumber.ToString());
                Logger.Info("Progress Number successfully created. " + progressNumber);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }

            return success;
        }

        public static int? GetProgressNumber(string progressRecordFilePath)
        {
            int? progressNumber;

            try
            {
                Logger.Debug("Get FileInfo object from progressRecordFilePath at {0}", progressRecordFilePath);
                FileInfo fileInfo = new(progressRecordFilePath);
                progressNumber = int.Parse(File.ReadAllText(fileInfo.FullName).Trim());
                Logger.Info("Progress Number : {0}", progressNumber);
            }
            catch (Exception x)
            {
                Logger.Error(x);
                progressNumber = null;
            }
            return progressNumber;
        }
    }
}
