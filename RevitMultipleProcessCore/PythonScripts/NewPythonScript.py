'''Output "Hello Revit world!" to the log.'''

# This section is common to all of these scripts. 
import clr
import System

clr.AddReference("RevitAPI")
clr.AddReference("RevitAPIUI")
from Autodesk.Revit.DB import *

import RevitScriptUtil
from RevitScriptUtil import Output

sessionId = RevitScriptUtil.GetSessionId()
uiapp = RevitScriptUtil.GetUIApplication()

# NOTE: these only make sense for batch Revit file processing mode.
doc = RevitScriptUtil.GetScriptDocument()
revitFilePath = RevitScriptUtil.GetRevitFilePath()

# The code above is boilerplate, everything below is yours!

Output()
Output("Hello Revit world!")