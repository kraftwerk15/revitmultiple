﻿using System.Collections.Generic;
using System.Diagnostics;
using RevitMultipleCore;

namespace RevitMultipleProcessCore.CommandLine
{
    public static class Settings
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        public static bool Read()
        {
            Dictionary<string, object>  commandLineOptions = CommandSettings.GetCommandLineOptions();
            if (commandLineOptions == null) return false;
            commandLineOptions.TryGetValue(CommandSettings.HelpOption, out object value);
            if (value != null)
            {
                string helpOptionValue = value.ToString();
                Debug.Assert(helpOptionValue != null, nameof(helpOptionValue) + " != null");
                bool requestedHelpOption = helpOptionValue.Equals("true");
                if (!CommandLineUtil.CommandLineHasArguments() || requestedHelpOption)
                {
                    Help.ShowCommandLineHelp();
                    return true;
                }
            }
            commandLineOptions.TryGetValue(CommandSettings.LicenseOption, out object license);
            // ReSharper disable once InvertIf
            if (license != null)
            {
                string licenseOptionValue = license.ToString();
                Debug.Assert(licenseOptionValue != null, nameof(licenseOptionValue) + " != null");
                bool requestedLicenseOption = licenseOptionValue.Equals("true");
                if (CommandLineUtil.CommandLineHasArguments() && !requestedLicenseOption) return false;
                License.ShowCommandLineLicense();
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Displays to the UI that some Invalid Options were provided
        /// </summary>
        /// <param name="invalidOptions">A list of invalid options</param>
        public static void ShowInvalidOptionsError(IEnumerable<string> invalidOptions)
        {
            Logger.Error("ERROR: unknown command-line option(s):");
            foreach (string invalidOption in invalidOptions)
            {
                Logger.Error(CommandLineUtil.OptionSwitchPrefix + invalidOption);
            }
            
            Logger.Warn("See --help option for command-line usage.");
        }
    }
}