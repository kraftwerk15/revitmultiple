﻿using System;
using Process = System.Diagnostics.Process;
using Application = System.Windows.Forms.Application;
using File = System.IO.File;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using RevitMultipleCore;
using RevitMultipleCore.Dynamo;
using RevitMultipleProcessCore.ErrorHandling;
using RevitMultipleProcessCore.RevitScript;


namespace RevitMultipleProcessCore.RevitScript
{
    public class RevitScriptHost
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        
        public static UIApplication UiApplication { get; set; }
        private const int EndSessionDelayInSeconds = 5;
        private const int CloseMainWindowAttempts = 10;

        private static Process GetCurrentProcess() {
            return Process.GetCurrentProcess();
        }
        
        /// <summary>
        /// Runs a Revit Task in Single context given a RevitMultiple Script Path
        /// </summary>
        /// <param name="scriptFilePath">A RevitMultiple Script Path</param>
        /// <returns>A bool of true if the process failed.</returns>
        private static bool RunSingleTaskScript(string scriptFilePath) {
            bool aborted = false;
            bool showMessageBoxOnTaskError = RevitScriptUtil.GetShowMessageBoxOnTaskError();

            object ExecuteTaskScript()
            {
                Logger.Info("Task script operation started.");
                ScriptUtil.ExecuteScript(scriptFilePath);
                Logger.Info("Task script operation completed.");
                return true;
            }

            try {
                object result = ScriptHostError.WithErrorHandling(ExecuteTaskScript, 
                    "ERROR: An error occurred while executing the task script! Operation aborted.", showMessageBoxOnTaskError);
            } catch (Exception) {
                aborted = true;
                throw;
            } finally {
                // Ensure aborted message is shown in the event of an exception.
                if (aborted) {
                    Logger.Error("Operation aborted.");
                }
            }
            
            Logger.Info("Operation completed.");
            
            return false;
        }
        
        /// <summary>
        /// Runs a Revit Task in Batch context given a RevitMultiple Script Path
        /// </summary>
        /// <param name="scriptFilePath">A RevitMultiple Script Path</param>
        /// <returns>A bool of true if the process failed.</returns>
        public static object RunBatchTaskScript(string scriptFilePath) 
        {
            object snapshotData;
            DateTime snapshotStartTime = TimeUtil.GetDateTimeNow();
            DateTime snapshotEndTime;
            object snapshotError = null;
            bool aborted = false;
            UIApplication uiApp = UiApplication;
            string revitJournalFilePath = uiApp.Application.RecordingJournalFilename;
            string sessionId = RevitScriptUtil.GetSessionId();
            string revitFilePath = string.Empty;
            string cloudProjectId = string.Empty;
            string cloudModelId = string.Empty;
            string cloudRegion = string.Empty;
            bool isCloudModel = RevitScriptUtil.IsCloudModel();
            if (isCloudModel) 
            {
                cloudProjectId = RevitScriptUtil.GetCloudProjectId();
                cloudModelId = RevitScriptUtil.GetCloudModelId();
                cloudRegion = RevitScriptUtil.GetCloudRegion();
            } 
            else 
            {
                revitFilePath = RevitScriptUtil.GetRevitFilePath();
            }
            bool openInUi = RevitScriptUtil.GetOpenInUi();
            bool enableDataExport = RevitScriptUtil.GetEnableDataExport();
            string dataExportFolderPath = RevitScriptUtil.GetDataExportFolderPath();
            bool showMessageBoxOnTaskError = RevitScriptUtil.GetShowMessageBoxOnTaskError();
            BatchRvt.CentralFileOpenOption centralFileOpenOption = RevitScriptUtil.GetCentralFileOpenOption();
            bool deleteLocalAfter = RevitScriptUtil.GetDeleteLocalAfter();
            bool discardWorksetsOnDetach = RevitScriptUtil.GetDiscardWorksetsOnDetach();
            BatchRvt.WorksetConfigurationOption worksetConfigurationOption = RevitScriptUtil.GetWorksetConfigurationOption();
            bool auditOnOpening = RevitScriptUtil.GetAuditOnOpening();
            int progressNumber = RevitScriptUtil.GetProgressNumber();
            int progressMax = RevitScriptUtil.GetProgressMax();
            if (enableDataExport && !PathUtil.DirectoryExists(dataExportFolderPath)) 
            {
                Logger.Error("ERROR: data export folder does not exist!");
                if (!string.IsNullOrWhiteSpace(dataExportFolderPath)) {
                    Logger.Info(dataExportFolderPath);
                }
                aborted = true;
            } else if (!isCloudModel && !PathUtil.FileExists(revitFilePath)) {
                Logger.Error("ERROR: Revit file does not exist!");
                if (!string.IsNullOrWhiteSpace(revitFilePath)) {
                    Logger.Info(revitFilePath);
                }
                aborted = true;
            } 
            else 
            {
                if (enableDataExport) {
                    snapshotEndTime = DateTime.MinValue;
                    snapshotData = SnapshotDataExporter.ExportTemporarySnapshotData(sessionId, revitFilePath, isCloudModel, cloudProjectId, 
                        cloudModelId, snapshotStartTime, snapshotEndTime, dataExportFolderPath, revitJournalFilePath, snapshotError);
                }
                string localFilePath = string.Empty;
                bool openCreateNewLocal = false;
                bool isCentralModel = false;
                bool isLocalModel = false;
                try {
                    if (isCloudModel) 
                    {
                        Logger.Info("Processing file (" + progressNumber + " of " + progressMax + ") : " + "CLOUD MODEL");
                        Logger.Info("Project ID: " + cloudProjectId);
                        Logger.Info("Model ID: " + cloudModelId);
                    } 
                    else 
                    {
                        Logger.Info("Processing file (" + progressNumber + " of " + progressMax + ") : " + revitFilePath);
                    }
                    if (isCloudModel) 
                    {
                        Logger.Info("The file is a Cloud Model.");
                    } 
                    else if (RevitFileUtil.IsWorkshared(revitFilePath)) 
                    {
                        if (RevitFileUtil.IsLocalModel(revitFilePath)) 
                        {
                            Logger.Warn("WARNING: the file being processed appears to be a Workshared Local file!");
                            isLocalModel = true;
                        }
                        if (RevitFileUtil.IsCentralModel(revitFilePath)) 
                        {
                            Logger.Info("The file is a Central Model file.");
                            isCentralModel = true;
                        }
                        if (centralFileOpenOption == BatchRvt.CentralFileOpenOption.CreateNewLocal) 
                        {
                            openCreateNewLocal = true;
                        }
                    } 
                    else if (PathUtil.HasFileExtension(revitFilePath, ".rfa")) 
                    {
                        Logger.Info("The file is a Family file.");
                    } 
                    else 
                    {
                        Logger.Info("The file is a Non-workshared file.");
                    }
                    if (enableDataExport) 
                    {
                        Logger.Info("Export folder is: " + dataExportFolderPath);
                    }
                    object result = null;
                    //BUG don't know if the Document is open at this moment in time
                    Document activeDoc = uiApp.ActiveUIDocument.Document;
                    if (activeDoc != null) {
                        result = ProcessDocument(activeDoc);
                    } 
                    else if (isCloudModel) 
                    {
                        result = RevitScriptUtil.RunCloudDocumentAction(uiApp, openInUi, cloudRegion, cloudProjectId, cloudModelId,
                            worksetConfigurationOption, auditOnOpening, ProcessDocument);
                    } 
                    else if (openCreateNewLocal) 
                    {
                        int revitVersion = RevitVersion.GetSupportedRevitVersion(uiApp.Application.VersionNumber);
                        localFilePath = RevitVersion.GetRevitLocalFilePath(revitVersion, revitFilePath);
                        try {
                            if (File.Exists(localFilePath)) {
                                Logger.Info("Deleting existing local file...");
                                File.Delete(localFilePath);
                                Logger.Info("Local file deleted.");
                            }
                        } catch (Exception) {
                            Logger.Warn("WARNING: failed to delete the local file!");
                        }
                        PathUtil.CreateDirectoryForFilePath(localFilePath);
                        result = RevitScriptUtil.RunNewLocalDocumentAction(uiApp, openInUi, revitFilePath, localFilePath, worksetConfigurationOption, 
                            auditOnOpening, ProcessDocument);
                    } 
                    else if (isCentralModel || isLocalModel) 
                    {
                        result = RevitScriptUtil.RunDetachedDocumentAction(uiApp, openInUi, revitFilePath, discardWorksetsOnDetach, worksetConfigurationOption, 
                            auditOnOpening, ProcessDocument);
                    } 
                    else 
                    {
                        result = RevitScriptUtil.RunDocumentAction(uiApp, openInUi, revitFilePath, auditOnOpening, ProcessDocument);
                    }
                } 
                catch (Exception e) 
                {
                    aborted = true;
                    snapshotError = ExceptionUtil.GetExceptionDetails(e);
                    throw;
                } 
                finally 
                {
                    if (isCloudModel) 
                    {
                    } 
                    else if (openCreateNewLocal && deleteLocalAfter) 
                    {
                        try {
                            if (File.Exists(localFilePath)) {
                                Logger.Info("Deleting local file...");
                                File.Delete(localFilePath);
                                Logger.Info("Local file deleted.");
                            }
                        } catch (Exception) {
                            Logger.Warn("WARNING: failed to delete the local file!");
                        }
                    }
                    if (enableDataExport) 
                    {
                        snapshotEndTime = TimeUtil.GetDateTimeNow();
                        snapshotData = SnapshotDataExporter.ExportSnapshotData(sessionId, revitFilePath, isCloudModel, cloudProjectId, cloudModelId, 
                            snapshotStartTime, snapshotEndTime, dataExportFolderPath, revitJournalFilePath, snapshotError);
                        SnapshotDataUtil.ConsolidateSnapshotData(dataExportFolderPath);
                    }
                    // Ensure aborted message is shown in the event of an exception.
                    if (aborted) 
                    {
                        Logger.Warn("Operation aborted.");
                    }
                }
            }

            object ProcessDocument(Document doc)
            {
                RevitScriptUtil.ScriptDocument = doc;
                //RevitScriptUtil.SetScriptDocument(doc);

                object ExecuteTaskScript()
                {
                    bool success = false;
                    Logger.Info("Task script operation started.");
                    if (PathUtil.HasFileExtension(scriptFilePath, ".dyn"))
                    {
                        if (DynamoCore.IsDynamoRevitModuleLoaded())
                        {
                            DynamoCore.ExecuteDynamoScript(uiApp, scriptFilePath, false);
                            success = true;
                        }
                        else
                        {
                            success = false;
                            Logger.Error(DynamoCore.DynamoRevitModuleNotFoundErrorMessage);
                        }
                    }
                    else if (PathUtil.HasFileExtension(scriptFilePath, ".py"))
                    {
                        //TODO python script here!!!!
                        ScriptUtil.ExecuteScript(scriptFilePath);
                        success = true;
                    }
                    else if(string.IsNullOrEmpty(scriptFilePath))
                    {
                        Logger.Info("Task Script was empty, assuming to excecute a Save action.");
                        doc.Save();
                    }
                    else
                    {
                        Logger.Error("Task Script was not empty (necessary for saving only), but also was not a .dyn or .py file. " +
                                     "Only Dynamo or Python files can be used.");
                    }

                    if (success)
                    {
                        Logger.Info("Task script operation completed.");
                    }
                    else
                    {
                        Logger.Error("ERROR: An error occurred while executing the task script! Operation aborted.");
                    }

                    return null;
                }

                object result = ScriptHostError.WithErrorHandling(ExecuteTaskScript, 
                    "ERROR: An error occurred while executing the task script! Operation aborted.", showMessageBoxOnTaskError);
                return result;
            }

            if (aborted) {
                Logger.Info("Operation aborted.");
            } else {
                Logger.Error("Operation completed.");
            }
            return aborted;
        }

        private static void ShutdownSession(Process process) {
            for (int i = 0; i < CloseMainWindowAttempts; i++)
            {
                process.Refresh();
                process.CloseMainWindow();
                Application.DoEvents();
            }
        }

        private static void DoEvents(int seconds) {
            for (int i = 0; i < seconds; i++)
            {
                Application.DoEvents();
                ThreadUtil.SleepForSeconds(1);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptDatas">A List of ScriptDataUtil.ScriptData</param>
        /// <returns>BatchRvt.RevitProcessingOption</returns>
        private static BatchRvt.RevitProcessingOption GetRevitProcessingOptionForSession(List<ScriptDataUtil.ScriptData> scriptDatas) {
            ScriptDataUtil.ScriptData oldScriptData = RevitScriptUtil.GetCurrentScriptData();
            RevitScriptUtil.SetCurrentScriptData(scriptDatas[0]);
            BatchRvt.RevitProcessingOption revitProcessingOption = RevitScriptUtil.GetRevitProcessingOption();
            RevitScriptUtil.SetCurrentScriptData(oldScriptData);
            return revitProcessingOption;
        }
        
        /// <summary>
        /// Executes Revit Session
        /// </summary>
        /// <param name="scriptFilePath"></param>
        /// <param name="scriptDataFilePath"></param>
        /// <param name="progressNumber"></param>
        /// <param name="batchRvtProcessUniqueId"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static object DoRevitSessionProcessing(string scriptFilePath, string scriptDataFilePath, int progressNumber, string batchRvtProcessUniqueId) {
            object result;
            List<object> results = new();
            //RevitScriptUtil.SetUIApplication(RevitSession.GetSessionUIApplication());
            RevitScriptUtil.ScriptDataFilePath = scriptDataFilePath;
            //RevitScriptUtil.SetScriptDataFilePath(scriptDataFilePath);
            List<ScriptDataUtil.ScriptData> scriptDatas = RevitScriptUtil.LoadScriptDatas()
                .Where(scriptData => scriptData.ProgressNumber.GetValue() >= progressNumber).OrderBy(scriptData => scriptData.ProgressNumber.GetValue()).ToList();
            if (scriptDatas.Count > 0) {
                BatchRvt.RevitProcessingOption revitProcessingOption = GetRevitProcessingOptionForSession(scriptDatas);
                if (revitProcessingOption == BatchRvt.RevitProcessingOption.BatchRevitFileProcessing) {
                    foreach (ScriptDataUtil.ScriptData scriptData in scriptDatas) {
                        RevitScriptUtil.SetCurrentScriptData(scriptData);
                        if (!RevitProcessHost.IsBatchRvtProcessRunning(batchRvtProcessUniqueId)) {
                            ScriptHostError.ShowScriptErrorMessageBox("ERROR: The BatchRvt process appears to have terminated! Operation aborted.");
                            break;
                        }
                        string progressRecordFilePath = ScriptDataUtil.GetProgressRecordFilePath(scriptDataFilePath);
                        bool progressRecorded = ScriptDataUtil.SetProgressNumber(progressRecordFilePath, scriptData.ProgressNumber.GetValue());
                        if (!progressRecorded) {
                            Logger.Warn("WARNING: Failed to update the session progress record file!");
                        }
                        result = ScriptHostError.WithErrorHandling(() => RunBatchTaskScript(scriptFilePath), 
                            "ERROR: An error occurred while processing the file!", false);
                        results.Add(result);
                    }
                } else {
                    ScriptDataUtil.ScriptData scriptData = scriptDatas[0];
                    RevitScriptUtil.SetCurrentScriptData(scriptData);
                    result = RunSingleTaskScript(scriptFilePath);
                    results.Add(result);
                }
            } else {
                throw new Exception("ERROR: received no script data!");
            }
            return results;
        }
        
        /// <summary>
        /// Main Entry Point to execute the RevitMultiple Revit context
        /// </summary>
        /// <param name="uiApp">Revit's UIApplication</param>
        public static void Main(UIApplication uiApp) {
            StringDictionary environmentVariables = ScriptEnvironment.GetEnvironmentVariables();
            string outputPipeHandleString = ScriptEnvironment.GetScriptOutputPipeHandleString(environmentVariables);
            string scriptFilePath = ScriptEnvironment.GetScriptFilePath(environmentVariables);
            string scriptDataFilePath = ScriptEnvironment.GetScriptDataFilePath(environmentVariables);
            int progressNumber = ScriptEnvironment.GetProgressNumber(environmentVariables);
            string batchRvtProcessUniqueId = ScriptEnvironment.GetBatchRvtProcessUniqueId(environmentVariables);
            string testModeFolderPath = ScriptEnvironment.GetTestModeFolderPath(environmentVariables);
            GlobalTestMode.InitializeGlobalTestMode(testModeFolderPath);
            UiApplication = uiApp;
            if (outputPipeHandleString == null || scriptFilePath == null) return;
            AnonymousPipeClientStream outputStream = ServerUtil.CreateAnonymousPipeClient(ServerUtil.OUT, outputPipeHandleString);
                
            object OutputStreamAction()
            {
                StreamWriter outputStreamWriter = StreamIOUtil.GetStreamWriter(outputStream);

                object OutputStreamWriterAction()
                {
                    RevitScriptUtil.SetOutputFunction(StreamIOUtil.GetSafeWriteLine(outputStreamWriter));
                    object result = ScriptHostError.WithErrorHandling(() => DoRevitSessionProcessing(scriptFilePath, scriptDataFilePath, 
                            progressNumber, batchRvtProcessUniqueId), "ERROR: An error occurred while executing the script host! Operation aborted.", 
                        showErrorMessageBox: false);
                    return result;
                }
                StreamIOUtil.UsingStream(outputStreamWriter.BaseStream, OutputStreamWriterAction);
                return null;
            }
            StreamIOUtil.UsingStream(outputStream, OutputStreamAction);
        }

        private static void TerminateSession() {
            Process currentProcess = GetCurrentProcess();
            DoEvents(EndSessionDelayInSeconds);
            ShutdownSession(currentProcess);
            DoEvents(EndSessionDelayInSeconds);
            currentProcess.Kill();
        }
    }
}