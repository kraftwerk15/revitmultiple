﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using ControlzEx.Theming;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using RevitMultipleCore;
using RevitMultipleProcessCore;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly IDialogCoordinator _dialogCoordinator;
        //private const string _windowTitle = "Revit Multiple";

        public MainWindowViewModel(IDialogCoordinator dialogCoordinator)
        {
            Logger.Info("Starting MainWindowViewModel");
            _dialogCoordinator = dialogCoordinator;
            AccentColors = ThemeManager.Current.Themes.Where(x => x.BaseColorScheme == "Light")
                .Select(a => new AccentColorMenuData { Name = a.ColorScheme, ColorBrush = a.ShowcaseBrush }).Distinct()
                .ToList();

            // create metro theme color menu items for the demo
            AppThemes = ThemeManager.Current.Themes
                .GroupBy(x => x.BaseColorScheme)
                .Select(x => x.First())
                .Select(a => new AppThemeMenuData() { Name = a.BaseColorScheme, BorderColorBrush = a.Resources["MahApps.Brushes.ThemeForeground"] as Brush, ColorBrush = a.Resources["MahApps.Brushes.ThemeBackground"] as Brush })
                .ToList();

            CanStart = true;
            CanCancel = false;
            WindowTitle = _windowTitle;
            //confirm that Revit is installed else display an Error Model Dialog box.
            if (RevitVersion.GetInstalledRevitVersions().Any())
            {
                RevitAddinsInstalled = true;
                Logger.Info("Revit Versions Installed");
            }
            else
            {
                RevitAddinsInstalled = false;
                Logger.Warn("Revit Versions Not Installed");
            }

            // ReSharper disable once IdentifierTypo
            List<int> smallInts = RevitVersion.SupportedRevitVersions.Select(r => r.YearVersion).ToList();
            smallInts.RemoveAt(RevitVersion.SupportedRevitVersions.Count - 1);
            RevitFromYears = smallInts;

            _settings = new RevitMultipleUiSettings();
            TaskScriptMainEnabled = false;
            TaskScriptSingleEnabled = false;
            TaskScriptPreEnabled = false;
            TaskScriptPostEnabled = false;
            Logger.Info("MainWindowViewModel Finished");
        }

        public List<AccentColorMenuData> AccentColors { get; set; }

        public List<AppThemeMenuData> AppThemes { get; set; }

        #region Commands
        
        private ICommand _displayFlyoutModalCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand FlyoutModalCommand => _displayFlyoutModalCommand ??= new SimpleCommand(_ => CanStart, 
            _ => IsScriptingHelpFlyoutOpen = !IsScriptingHelpFlyoutOpen);
        
        private ICommand _displayRevitOptionsFlyoutModalCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand FlyoutRevitOptionsModalCommand => _displayRevitOptionsFlyoutModalCommand ??= new SimpleCommand(_ => CanStart, 
            _ => IsRevitFlyoutOpen = !IsRevitFlyoutOpen);
        
        private ICommand _processingTimeEnabledCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand ProcessingTimeEnabledCommand => _processingTimeEnabledCommand ??= new SimpleCommand(_ => CanStart, 
            _ => ProcessingTimeEnabled = !ProcessingTimeEnabled);

        private ICommand _dataExportEnabledCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand DataExportEnabledCommand => _dataExportEnabledCommand ??= new SimpleCommand(_ => CanStart, 
            _ => DataExportEnabled = !DataExportEnabled);
        
        private ICommand _dataExportBrowseCommand;

        public ICommand DataExportDatabaseCredentialsCommand =>
            _dataExportBrowseCommand ??= new SimpleCommand(_ => CanStart, DataExportBrowse);

        private ICommand _cancelDatabaseCredentialsCommand;

        
        #endregion
        
        #region Menu File Commands
        private ICommand _clearSettingsCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand ClearSettingsCommand => _clearSettingsCommand ??= new SimpleCommand(_ => CanStart, ClearSettings);

        private ICommand _loadSettingsCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand LoadSettingsCommand => _loadSettingsCommand ??= new SimpleCommand(_ => CanStart, LoadSettings);

        private ICommand _saveSettingsCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand SaveSettingsCommand => _saveSettingsCommand ??= new SimpleCommand(_ => CanStart, SaveSettings);
        
        private ICommand _logFileCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand LogFileCommand => _logFileCommand ??= new SimpleCommand(_ => CanStart, SpecifyLogFile);
        
        private ICommand _revitInstallationVerificationCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand RevitInstallationVerificationCommand => _revitInstallationVerificationCommand ??= 
            new SimpleCommand(_ => CanStart, VerifyRevitInstallation);
        
        private ICommand _excelVerificationCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand ExcelVerificationCommand => _excelVerificationCommand ??= 
            new SimpleCommand(_ => CanStart, VerifyExcelInstallation);
        
        private ICommand _setLibraryCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand SetLibraryCommand => _setLibraryCommand ??= 
            new SimpleCommand(_ => CanStart, SetLibraryLocation);
        
        private ICommand _setDatabaseCredentialsCommand;
        public ICommand SetDatabaseCredentialsCommand => _setDatabaseCredentialsCommand ??=
            new SimpleCommand(_ => CanStart, SetDatabaseCredentials);
        
        private ICommand _closeCommand;
        public ICommand CloseCommand => _closeCommand ??= new SimpleCommand(_ => CanStart, CloseApplication);
        #endregion

        #region Menu Run Commands
        private ICommand _startWorkCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand StartWorkCommand => _startWorkCommand ??= new SimpleCommand(_ => CanStart, Start);

        private ICommand _cancelWorkCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand CancelWorkCommand => _cancelWorkCommand ??= new SimpleCommand(_ => CanCancel, Cancel);
        #endregion

        #region Menu Help Commands
        private ICommand _aboutCommand;
        public ICommand AboutCommand => _aboutCommand ??= new SimpleCommand(_ => CanStart, License);
        #endregion
        
        #region FileSearch
        private ICommand _selectFileCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand SelectFileCommand => _selectFileCommand ??= new SimpleCommand(_ => CanStart, SelectFile);
        
        private ICommand _selectFilesCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand SelectFilesCommand => _selectFilesCommand ??= new SimpleCommand(_ => CanStart, SelectFiles);
        private ICommand _selectFolderCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand SelectFolderCommand => _selectFolderCommand ??= new SimpleCommand(_ => CanStart, SelectFolder);
        private ICommand _selectLibraryCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand SelectLibraryCommand => _selectLibraryCommand ??= new SimpleCommand(_ => CanStart, StartLibrary);
        private ICommand _selectDatabaseCommand;
        public ICommand SelectDatabaseCommand => _selectDatabaseCommand ??= new SimpleCommand(_ => CanStart, SelectDatabase);

        private ICommand _startFileSearchCommand;
        // ReSharper disable once UnusedMember.Global
        public ICommand StartFileSearchCommand => _startFileSearchCommand ??= new SimpleCommand(o => CanStart, StartFileSearch);
        #endregion

        #region Scripting Commands

        private ICommand _taskScriptMainToggledCommand;
        public ICommand TaskScriptMainToggledCommand => _taskScriptMainToggledCommand ??=
            new SimpleCommand(_ => CanStart, TaskScriptMainToggled);
        
        private ICommand _taskScriptSingleToggledCommand;
        public ICommand TaskScriptSingleToggledCommand => _taskScriptSingleToggledCommand ??=
            new SimpleCommand(_ => CanStart, TaskScriptSingleToggled);
        
        private ICommand _taskScriptPreToggledCommand;
        public ICommand TaskScriptPreToggledCommand => _taskScriptPreToggledCommand ??=
            new SimpleCommand(_ => CanStart, TaskScriptPreToggled);
        
        private ICommand _taskScriptPostToggledCommand;
        public ICommand TaskScriptPostToggledCommand => _taskScriptPostToggledCommand ??=
            new SimpleCommand(_ => CanStart, TaskScriptPostToggled);

        #endregion
        
        #region NavigationCommands
        private ICommand _tabIncrementCommand;
        public ICommand TabIncrementCommand => _tabIncrementCommand ??= new SimpleCommand(_ => CanStart, TabIncrement);
        
        private ICommand _tabDecrementCommand;
        public ICommand TabDecrementCommand => _tabDecrementCommand ??= new SimpleCommand(_ => CanStart, TabDecrement);
        #endregion
        
    }

    public class AccentColorMenuData
    {
        public string Name { get; set; }

        public Brush BorderColorBrush { get; set; }

        public Brush ColorBrush { get; set; }

        public AccentColorMenuData()
        {
            ChangeAccentCommand = new SimpleCommand(_ => true, DoChangeTheme);
        }

        public ICommand ChangeAccentCommand { get; }

        protected virtual void DoChangeTheme(object sender)
        {
            ThemeManager.Current.ChangeThemeColorScheme(Application.Current, Name);
        }
    }

    public class AppThemeMenuData : AccentColorMenuData
    {
        protected override void DoChangeTheme(object sender)
        {
            ThemeManager.Current.ChangeThemeBaseColor(Application.Current, Name);
        }
    }
}
