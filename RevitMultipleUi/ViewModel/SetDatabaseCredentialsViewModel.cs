﻿using System;
using System.Security;
using System.Windows.Input;

namespace RevitFileUpdaterUI
{
    public class SetDatabaseCredentialsViewModel : ViewModelBase
    {
        public SetDatabaseCredentialsViewModel(Action<SetDatabaseCredentialsViewModel> closeHandler)
        {
            this.CloseCommand = new SimpleCommand(o => true, o => closeHandler(this));
        }

        private string _userName;

        public string UserName {
            get => _userName;
            set { _userName = value; OnPropertyChanged(); }
        }

        private SecureString _password;
    
        public SecureString Password
        {
            get => _password;
            set { _password = value; OnPropertyChanged(); }
        }

        private string _connectionString;
    
        public string ConnectionString
        {
            get => _connectionString;
            set { _connectionString = value; OnPropertyChanged(); }
        }
        public ICommand CloseCommand { get; }
        
       
    }
}