﻿using System;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel
    {
        private TimeSpan _averageTime;
        public TimeSpan AverageTime
        {
            get => _averageTime;
            set { _averageTime = value; OnPropertyChanged(); }
        }

        private TimeSpan _longTime;
        public TimeSpan LongTime
        {
            get => _longTime;
            set { _longTime = value; OnPropertyChanged(); }
        }
        private int _currentProgress;
        public int CurrentProgress
        {
            get => _currentProgress;
            set { _currentProgress = value; OnPropertyChanged(); }
        }
        private int _familiesCompleted;
        public int FamiliesCompleted
        {
            get => _familiesCompleted;
            set{ _familiesCompleted = value; OnPropertyChanged(); }
        }
        private int _totalFamilies;
        public int TotalFamilies
        {
            get => _totalFamilies;
            set { _totalFamilies = value; OnPropertyChanged(); }
        }
        private int _familiesRemaining;
        public int FamiliesRemaining
        {
            get => _familiesRemaining;
            set { _familiesRemaining = value; OnPropertyChanged(); }
        }
        private string _startTime;
        public string StartTime
        {
            get => _startTime;
            set { _startTime = value; OnPropertyChanged(); }
        }

        private string _completedTime;
        public string CompletedTime
        {
            get => _completedTime;
            set { _completedTime = value; OnPropertyChanged(); }
        }

        private string _elapsedTime;
        public string ElapsedTime
        {
            get => _elapsedTime;
            set { _elapsedTime = value; OnPropertyChanged(); }
        }

        private string _element;
        public string Element
        {
            get => _element;
            set { _element = value; OnPropertyChanged(); }
        }
        private string _status;
        public string Status
        {
            get => _status;
            set { _status = value; OnPropertyChanged(); }
        }

        private string _completionLabel;
        public string CompletionLabel
        {
            get => _completionLabel;
            set { _completionLabel = value; OnPropertyChanged(); }
        }

        private int _folderLevel;
        public int FolderLevel
        {
            get => _folderLevel;
            set { _folderLevel = value; OnPropertyChanged(); }
        }
    }
}