﻿namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        
        //Content here appears on the Options Page of the UI
        private bool _createNewLocal;
        public bool CreateNewLocal    { get => _createNewLocal;
            set { _createNewLocal = value;
                DiscardWorksets = false; OnPropertyChanged(); }
        }

        private bool _deleteLocalAfter;
        public bool DeleteLocalAfter  { get => _deleteLocalAfter;
            set { _deleteLocalAfter = value; OnPropertyChanged(); } }

        private bool _detachFromCentral;
        public bool DetachFromCentral { get => _detachFromCentral;
            set { _detachFromCentral = value;
                DeleteLocalAfter = false; 
                OnPropertyChanged(); } }

        private bool _discardWorksets;
        public bool DiscardWorksets  { get => _discardWorksets;
            set { _discardWorksets = value; OnPropertyChanged(); } }

        private bool _closeAllWorksets;
        public bool CloseAllWorksets  { get => _closeAllWorksets;
            set { _closeAllWorksets = value; OnPropertyChanged(); } }

        private bool _openAllWorksets;
        public bool OpenAllWorksets   { get => _openAllWorksets;
            set { _openAllWorksets = value; OnPropertyChanged(); } }

        private bool _openLastViewed;
        public bool OpenLastViewed    { get => _openLastViewed;
            set { _openLastViewed = value; OnPropertyChanged(); } }

        private bool _useRevitVersion;
        public bool UseRevitVersion { get => _useRevitVersion;
            set { _useRevitVersion = value; OnPropertyChanged(); } }

        private bool _useMinimumVersion;
        public bool UseMinimumVersion { get => _useMinimumVersion;
            set { _useMinimumVersion = value; OnPropertyChanged(); } }

        private bool _useSpecificRevitVersion;
        public bool UseSpecificRevitVersion { get => _useSpecificRevitVersion;
            set { _useSpecificRevitVersion = value; OnPropertyChanged(); } }

        private bool _auditOnOpening;
        public bool AuditOnOpening { get => _auditOnOpening;
            set { _auditOnOpening = value; OnPropertyChanged(); } }

        private bool _bim360US;
        public bool BIM360US { get => _bim360US;
            set { _bim360US = value; OnPropertyChanged(); } }
        
        private bool _bim360EU;
        public bool BIM360EU { get => _bim360EU;
            set { _bim360EU = value; OnPropertyChanged(); } }

    }
}