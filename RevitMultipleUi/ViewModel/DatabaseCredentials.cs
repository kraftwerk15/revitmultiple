﻿using System.Security;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel
    {
        private string _userName;

        public string UserName {
            get => _userName;
            set { _userName = value; OnPropertyChanged(); }
        }

        private SecureString _password;
        
        public SecureString Password
        {
            get => _password;
            set { _password = value; OnPropertyChanged(); }
        }

        private string _connectionString;
        
        public string ConnectionString
        {
            get => _connectionString;
            set { _connectionString = value; OnPropertyChanged(); }
        }
    }
}