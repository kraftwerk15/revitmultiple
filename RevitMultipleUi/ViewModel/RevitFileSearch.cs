﻿using System.Collections.Generic;
using RevitMultipleCore;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel
    {
        
        public RevitFileScanning.RevitFileType SelectedRevitFileType    { get; set; }
        public bool                            ProjectSelected          { get; set; }
        public bool                            FamilySelected           { get; set; }
        public bool                            ProjectAndFamilySelected { get; set; }
        public bool                            FamilyTemplates          { get; set; }
        

        private bool _searchEnabled;
        public bool SearchEnabled
        {
            get => _searchEnabled;
            set
            { _searchEnabled = value;
                SearchFamiliesEnabled = IsFamily;
                SearchProjectsEnabled = IsProject;
                OnPropertyChanged(); } 
        }

        public bool SearchProjectsEnabled
        {
            get => _searchProjectsEnabled;
            set
            {
                if (value == _searchProjectsEnabled) return;
                _searchProjectsEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool SearchFamiliesEnabled
        {
            get => _searchFamiliesEnabled;
            set
            {
                if (value == _searchFamiliesEnabled) return;
                _searchFamiliesEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool _includeSubfolders;
        public bool IncludeSubfolders {
            get => _includeSubfolders;
            set
            { _includeSubfolders = value; OnPropertyChanged(); } 
        }

        private bool _expandNetworkPaths;
        public bool ExpandNetworkPaths 
        {
            get => _expandNetworkPaths;
            set
            { _expandNetworkPaths = value; OnPropertyChanged(); } 
        }
        
        private bool _extractRevitVersionInfo;
        private bool _searchFamiliesEnabled;
        private bool _searchProjectsEnabled;

        public bool ExtractRevitVersionInfo 
        {
            get => _extractRevitVersionInfo;
            set
            { _extractRevitVersionInfo = value; OnPropertyChanged(); } 
        }

        public List<string[]> FilePaths { get; set; }
    }
}