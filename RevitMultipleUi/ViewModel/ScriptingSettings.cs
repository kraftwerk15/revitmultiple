﻿using System;
using System.IO;
using System.Windows.Input;
using RevitMultipleCore;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel 
    {
        private bool _taskScriptMainEnabled;
        public bool TaskScriptMainEnabled { get => _taskScriptMainEnabled; set { _taskScriptMainEnabled = value;OnPropertyChanged() ; } }
        private string _taskScriptMainFileName;
        public string TaskScriptMainFileName {
            get => _taskScriptMainFileName;
            set { _taskScriptMainFileName = value; OnPropertyChanged(); } 
        }

        private bool _taskScriptSingleEnabled;
        public bool TaskScriptSingleEnabled { get => _taskScriptSingleEnabled; set { _taskScriptSingleEnabled = value; OnPropertyChanged(); } }
        private string _taskScriptSingleFileName;
        public string TaskScriptSingleFileName {
            get => _taskScriptSingleFileName;
            set { _taskScriptSingleFileName = value; OnPropertyChanged(); } 
        }

        private bool _taskScriptPreEnabled;
        public bool TaskScriptPreEnabled { get => _taskScriptPreEnabled; set { _taskScriptPreEnabled = value; OnPropertyChanged(); } }
        
        private string _taskScriptPreFileName;
        public string TaskScriptPreFileName
        {
            get => _taskScriptPreFileName;
            set { _taskScriptPreFileName = value; OnPropertyChanged(); } 
        }

        private bool _taskScriptPostEnabled;
        public bool TaskScriptPostEnabled { get => _taskScriptPostEnabled; set { _taskScriptPostEnabled = value; OnPropertyChanged(); } }
        
        private string _taskScriptPostFileName;
        public string TaskScriptPostFileName {             get => _taskScriptPostFileName;
            set { _taskScriptPostFileName = value; OnPropertyChanged(); } 
        }
        
        public void TaskScriptMainToggled(object sender)
        {
            TaskScriptMainEnabled = !TaskScriptMainEnabled;
        }

        public void TaskScriptSingleToggled(object sender)
        {
            TaskScriptSingleEnabled = !TaskScriptSingleEnabled;
        }
        
        public void TaskScriptPreToggled(object sender)
        {
            TaskScriptPreEnabled = !TaskScriptPreEnabled;
        }
        
        public void TaskScriptPostToggled(object sender)
        {
            TaskScriptPostEnabled = !TaskScriptPostEnabled;
        }

        private ICommand _browseScript;
        public ICommand BrowseScript => _browseScript ??= new SimpleCommand(_ => CanStart, BrowseTaskScript);

        private ICommand _newScript;
        public ICommand NewScript => _newScript ??= new SimpleCommand(_ => CanStart, NewTaskScript);
        private void BrowseTaskScript(object sender)
        {
            if (sender is not null)
            {
                if (!sender.ToString().Equals(string.Empty))
                {
                    string file= UI_IO_Options.BrowseForFile("Dynamo Graph or Python File", ".dyn", ".dyn|.py", true);
                    if (file.Equals(string.Empty))
                        return;

                    SetTaskScript(sender.ToString(), file);
                }
            }
            
        }

        private void NewTaskScript(object sender)
        {
            if (sender is not null)
            {
                // ReSharper disable once PossibleNullReferenceException
                if (!sender.ToString().Equals(string.Empty))
                {
                    string file= UI_IO_Options.BrowseForSave("Dynamo Graph or Python File", SaveTaskAction, ".py", ".dyn|.py");
                    if (file.Equals(string.Empty))
                        return;

                    SetTaskScript(sender.ToString(), file);
                }

                _dialogCoordinator.ShowModalMessageExternal(this, "New Python Script Created",
                    "A new python script was created at the location you specified. You should open it up and complete your logic before continuing.");
            }
        }
        
        private async void SaveTaskAction(string filePath)
        {
            try
            {
                //TODO copy the .py base file here to the requested file path
                //TODO reflection/ get the script and copy
                //File.Copy(ldkfj, filePath);
                
                //string settingsText = JsonSerialization.SerializeToJson(jobject, true);
                //FileInfo fileInfo = new(filePath);
                //fileInfo.Directory?.Create();
                //await File.WriteAllTextAsync(fileInfo.FullName, settingsText);
                Logger.Info("Saved new Python Script successfully.");
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        private void SetTaskScript(string sender, string name)
        {
            switch (sender)
            {
                case "taskScript":
                    TaskScriptMainFileName = name;
                    break;
                case "taskSingle":
                    TaskScriptSingleFileName = name;
                    break;
                case "preProcessing":
                    TaskScriptPreFileName = name;
                    break;
                case "postProcessing" :
                    TaskScriptPostFileName = name;
                    break;
                default:
                    break;
            }
        }
    }
}