﻿using System;
using MahApps.Metro.Controls.Dialogs;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel
    {
        private string _libraryLocation;
        public string LibraryLocation
        {
            get => _libraryLocation;
            set { _libraryLocation = value; OnPropertyChanged(); }
        }

        private void SetLibraryLocation(object sender)
        {
            MetroDialogSettings metroDialogSettings = new()
            {
                AffirmativeButtonText = "Set",
                NegativeButtonText = "Cancel"
            };
            //do something because the year could not be found.
            string dialogResult = _dialogCoordinator.ShowModalInputExternal(this, "Set Library Location",
                "Define the Root Level for you Library below without the year version. " +
                "This assumes your library is under one folder with a folder for each year version beneath this folder. " +
                "Such as, 2020, 2021, 2022, etc." + Environment.NewLine + Environment.NewLine + "For more information, see the Help in the About Section.",
                metroDialogSettings);
            if (!string.IsNullOrEmpty(dialogResult))
            {
                LibraryLocation = dialogResult;
            }
        }
    }
}