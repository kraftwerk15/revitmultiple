﻿using System;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using NLog;

namespace RevitFileUpdaterUI
{
    // ReSharper disable once InconsistentNaming
    internal class UI_IO_Options
    {
        /// <summary>
        /// Nlog Logger object.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        internal static string BrowseForSave(string dialogTitle, Action<string> fileAction, string defaultExt, string filter, 
            string initialDirectory = null, string initialFileName = null)
        {
            SaveFileDialog saveFileDialog = new() {DefaultExt = defaultExt, Filter = filter, Title = dialogTitle};

            if (!string.IsNullOrWhiteSpace(initialDirectory))
            {
                saveFileDialog.InitialDirectory = initialDirectory;
            }

            if (!string.IsNullOrWhiteSpace(initialFileName))
            {
                saveFileDialog.FileName = initialFileName;
            }

            DialogResult dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult != DialogResult.OK) return null;
            string selectedFilePath = saveFileDialog.FileName;

            if (!string.IsNullOrWhiteSpace(selectedFilePath))
            {
                fileAction(selectedFilePath);
            }

            return selectedFilePath;
        }

        internal static string BrowseForFile(string dialogTitle, string defaultExt, string filter, bool checkFileExists, string initialDirectory = null)
        {
            OpenFileDialog dialog = OpenFileDialog(dialogTitle, defaultExt, filter, checkFileExists, false, initialDirectory);
            if (dialog?.FileName is null) return string.Empty;
            string selectedFilePath = dialog.FileName;

            return !string.IsNullOrWhiteSpace(selectedFilePath) ? selectedFilePath : string.Empty;
        }
        
        internal static string[] BrowseForFiles(string dialogTitle, string defaultExt, string filter, bool checkFileExists, string initialDirectory = null)
        {
            OpenFileDialog dialog = OpenFileDialog(dialogTitle, defaultExt, filter, checkFileExists, true, initialDirectory);
            
            string[] selectedFilePaths = dialog.FileNames;

            return selectedFilePaths.Length > 0 ? selectedFilePaths : null;
        }

        private static OpenFileDialog OpenFileDialog(string dialogTitle, string defaultExt, string filter, bool checkFileExists,
            bool selectMultiple, string initialDirectory = null)
        {
            OpenFileDialog openFileDialog = new()
            {
                DefaultExt = defaultExt,
                Filter = filter,
                CheckFileExists = checkFileExists,
                ReadOnlyChecked = true,
                Multiselect = selectMultiple,
                Title = dialogTitle
            };


            if (!string.IsNullOrWhiteSpace(initialDirectory))
            {
                openFileDialog.InitialDirectory = initialDirectory;
            }

            DialogResult dialogResult = openFileDialog.ShowDialog();

            return dialogResult != DialogResult.OK ? null : openFileDialog;
        }
        

        public static string BrowseForFolder()
        {
            try
            {
                CommonOpenFileDialog dialog = new()
                {
                    InitialDirectory = "C:\\Users", IsFolderPicker = true
                };
                return dialog.ShowDialog() != CommonFileDialogResult.Ok ? string.Empty : dialog.FileName;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return string.Empty;
        }
    }
}
