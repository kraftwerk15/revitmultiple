﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RevitMultipleCore;
using RevitMultipleProcessCore;

namespace RevitFileUpdaterUI
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        #region Properties

        private RevitMultipleUiSettings _settings;
        public bool RevitAddinsInstalled = true;

        public Process RevitFileUpdaterProcess { get; set; }
        
        private string _windowTitle = "Revit Multiple";
        public string WindowTitle { get => _windowTitle;
            private set { _windowTitle = value; OnPropertyChanged(); }
        }
        public string LogFolder { get; set; }
        public DateTime StartDateTime;
        public DateTime CompletedDateTime;
        public Stopwatch ElapsedStopwatch = new();
        private int _selectedTab;
        public int SelectedTab
        {
            get => _selectedTab;
            set { _selectedTab = value; OnPropertyChanged(); }
        }
        
        public string Title { get; set; }
        
        public bool CancelRequested { get; set; }
        private bool _canCancel;
        public bool CanCancel
        {
            get => _canCancel;
            set { _canCancel = value; OnPropertyChanged(); }
        }
        private bool _canStart;
        public bool CanStart
        {
            get => _canStart;
            set { _canStart = value; OnPropertyChanged(); }
        }
        private bool _canFinish;
        public bool CanFinish
        {
            get => _canFinish;
            set { _canFinish = value; OnPropertyChanged(); }
        }

        private bool _isFamily;
        public bool IsFamily 
        { 
            get => _isFamily;
            set { _isFamily = value; OnPropertyChanged(); }
        }
        
        private bool _isProject;
        public bool IsProject 
        { 
            get => _isProject;
            set { _isProject = value; OnPropertyChanged(); }
        }
        
        private string _upgradePath;
        public string UpgradePath
        {
            get => _upgradePath;
            set { _upgradePath = value; OnPropertyChanged(); }
        }

        private bool _processingOption; 
        public bool ProcessingMultipleOption {
            get => _processingOption;
            set { _processingOption = value;
                ProcessingSingleOption = !_processingOption; OnPropertyChanged(); }
        }
        
        public bool ProcessingSingleOption { get; set; }

        public List<int> RevitFromYears { get; set; }
        private int _revitFromYear;
        /// <summary>
        /// The Year of the Application
        /// </summary>
        public int RevitFromYear
        {
            get => _revitFromYear;
            set
            {
                _revitFromYear = value;
                if (RevitFromYear < RevitFromYears[0])
                {
                    Is2018Available = false;
                    Is2018Selected = false;
                    Is2019Available = false;
                    Is2019Selected = false;
                    Is2020Available = false;
                    Is2020Selected = false;
                    Is2021Available = false;
                    Is2021Selected = false;
                    Is2022Available = false;
                    Is2022Selected = false;
                }
                else if (RevitFromYear < RevitFromYears[1])
                {
                    Is2018Available = false;
                    Is2019Available = true;
                    Is2020Available = true;
                    Is2021Available = true;
                    Is2022Available = true;
                    Is2018Selected = false;
                    Is2019Selected = true;
                    Is2020Selected = true;
                    Is2021Selected = true;
                    Is2022Selected = true;
                }
                else if (RevitFromYear < RevitFromYears[2])
                {
                    Is2018Available = false;
                    Is2019Available = false;
                    Is2020Available = true;
                    Is2021Available = true;
                    Is2022Available = true;
                    Is2018Selected = false;
                    Is2019Selected = false;
                    Is2020Selected = true;
                    Is2021Selected = true;
                    Is2022Selected = true;
                }
                else if (RevitFromYear < RevitFromYears[3])
                {
                    Is2018Available = false;
                    Is2019Available = false;
                    Is2020Available = false;
                    Is2021Available = true;
                    Is2022Available = true;
                    Is2018Selected = false;
                    Is2019Selected = false;
                    Is2020Selected = false;
                    Is2021Selected = true;
                    Is2022Selected = true;
                }
                else
                {
                    Is2018Available = false;
                    Is2019Available = false;
                    Is2020Available = false;
                    Is2021Available = false;
                    Is2022Available = true;
                    Is2018Selected = false;
                    Is2019Selected = false;
                    Is2020Selected = false;
                    Is2021Selected = false;
                    Is2022Selected = true;
                }
                OnPropertyChanged(nameof(Is2018Available));
                
                OnPropertyChanged(nameof(Is2019Available));
                OnPropertyChanged(nameof(Is2020Available));
                OnPropertyChanged(nameof(Is2021Available));
                OnPropertyChanged(nameof(Is2022Available));
                OnPropertyChanged(nameof(Is2018Selected));
                OnPropertyChanged(nameof(Is2019Selected));
                OnPropertyChanged(nameof(Is2020Selected));
                OnPropertyChanged(nameof(Is2021Selected));
                OnPropertyChanged(nameof(Is2022Selected));
            } 
        }
        
        
        private string _validationText;
        public string ValidationText
        {
            get => _validationText;
            set { _validationText = value; OnPropertyChanged(); }
        }

        public string FolderLocation { get; set; }

        private bool _is2018;
        public bool Is2018Available { get => _is2018; set { _is2018 = value; OnPropertyChanged(); } }
        private bool _is2019;
        public bool Is2019Available { get => _is2019; set { _is2019 = value; OnPropertyChanged(); } }
        private bool _is2020;
        public bool Is2020Available { get => _is2020; set { _is2020 = value; OnPropertyChanged(); } }
        private bool _is2021;
        public bool Is2021Available { get => _is2021; set { _is2021 = value; OnPropertyChanged(); } }
        private bool _is2022;
        public bool Is2022Available { get => _is2022; set { _is2022 = value; OnPropertyChanged(); } }

        private bool _is2018Selected;
        public bool Is2018Selected { get => _is2018Selected; set { _is2018Selected = value; OnPropertyChanged(); } }
        private bool _is2019Selected;
        public bool Is2019Selected { get => _is2019Selected; set { _is2019Selected = value; OnPropertyChanged(); } }
        private bool _is2020Selected;
        public bool Is2020Selected { get => _is2020Selected; set { _is2020Selected = value; OnPropertyChanged(); } }
        private bool _is2021Selected;
        public bool Is2021Selected { get => _is2021Selected; set { _is2021Selected = value; OnPropertyChanged(); } }
        private bool _is2022Selected;
        public bool Is2022Selected { get => _is2022Selected; set { _is2022Selected = value; OnPropertyChanged(); } }

        #endregion

        #region Flyouts

        private bool _isFolderFlyoutOpen;
        public bool IsFolderFlyoutOpen
        {
            get => _isFolderFlyoutOpen;
            set { _isFolderFlyoutOpen = value; OnPropertyChanged(); }
        }

        private bool _isScriptingHelpFlyoutOpen;
        /// <summary>
        /// Controls the visibility of the Scripting Help Flyout on the MainWindow
        /// </summary>
        public bool IsScriptingHelpFlyoutOpen
        {
            get => _isScriptingHelpFlyoutOpen;
            set { _isScriptingHelpFlyoutOpen = value; OnPropertyChanged(); }
        }
        /// <summary>
        /// Controls the visibility of the Revit Options Flyout on the MainWindow
        /// </summary>
        public bool IsRevitFlyoutOpen
        {
            get => _isRevitFlyoutOpen;
            set
            {
                _isRevitFlyoutOpen = value;
                OnPropertyChanged();
            } 
        }

        private string _filePath;
        /// <summary>
        /// Deprecated
        /// </summary>
        public string FilePath
        {
            get => _filePath;
            set { _filePath = value; OnPropertyChanged();}
        }

        #endregion
        
        #region Session Option Properties

        //Content here appears on the Session Page of the UI
        /// <summary>
        /// Controls whether the users wants to see the files open in the UI
        /// this also determines Revit API call methods
        /// </summary>
        public bool OpenInUi
        {
            get => _openInUi;
            set { _openInUi = value; OnPropertyChanged(); }
        }
        /// <summary>
        /// Determines whether this window is set to Topmost
        /// </summary>
        public bool WindowAlwaysOnTop
        {
            get => _windowAlwaysOnTop;
            set { _windowAlwaysOnTop = value; OnPropertyChanged();}
        }
        /// <summary>
        /// Determines if a separate Revit session should be launched per file
        /// </summary>
        public bool RevitSeparateSession
        {
            get => _revitSeparateSession;
            set { _revitSeparateSession = value; OnPropertyChanged(); }
        }
        /// <summary>
        /// Determines if the same Revit session can be used for multiple files
        /// </summary>
        public bool RevitSameSession
        {
            get => _revitSameSession;
            set { _revitSameSession = value; OnPropertyChanged(); }
        }

        public bool ProcessingTimeEnabled
        {
            get => _processingTimeEnabled;
            set
            {
                if (value == _processingTimeEnabled) return;
                _processingTimeEnabled = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ProcessingTimeEnabledCommand));
            }
        }

        public int ProcessingTime { get; set; }
       

        private bool _isFrom;
        public bool IsFromAvailable { get => _isFrom; set { _isFrom = value; OnPropertyChanged(); } }

        private bool _dataExport;
        public bool DataExportEnabled { get =>_dataExport;
            set
            {
                _dataExport = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(DataExportEnabledCommand));
            }
        }

        private string _dataExportPath;
        private bool _isRevitFlyoutOpen;
        private bool _processingTimeEnabled;
        private bool _openInUi;
        private bool _windowAlwaysOnTop;
        private bool _revitSeparateSession;
        private bool _revitSameSession;

        public string DataExportPath
        {
            get => _dataExportPath;
            set
            {
                _dataExportPath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The Year the User Selected for the Revit Library
        /// </summary>
        public int RevitToYear { get; set; }
        #endregion

    }
}
