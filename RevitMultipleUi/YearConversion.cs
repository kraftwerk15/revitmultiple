﻿namespace RevitFileUpdaterUI
{
    public class YearConversion
    {
        public YearConversion(MainWindowViewModel viewModel, int year)
        {
            switch (year)
            {
                case 2018:
                    viewModel.Is2018Available = false;
                    viewModel.Is2019Available = true;
                    viewModel.Is2020Available = true;
                    viewModel.Is2021Available = true;
                    viewModel.Is2022Available = true;
                    break;
                case 2019:
                    viewModel.Is2018Available = false;
                    viewModel.Is2019Available = false;
                    viewModel.Is2020Available = true;
                    viewModel.Is2021Available = true;
                    viewModel.Is2022Available = true;
                    break;
                case 2020:
                    viewModel.Is2018Available = false;
                    viewModel.Is2019Available = false;
                    viewModel.Is2020Available = false;
                    viewModel.Is2021Available = true;
                    viewModel.Is2022Available = true;
                    break;
                case 2021:
                    viewModel.Is2018Available = false;
                    viewModel.Is2019Available = false;
                    viewModel.Is2020Available = false;
                    viewModel.Is2021Available = false;
                    viewModel.Is2022Available = true;
                    break;
                case 2022:
                    viewModel.Is2018Available = false;
                    viewModel.Is2019Available = false;
                    viewModel.Is2020Available = false;
                    viewModel.Is2021Available = false;
                    viewModel.Is2022Available = false;
                    break;
            }
        }
    }
}
