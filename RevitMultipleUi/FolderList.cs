﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MahApps.Metro.Controls.Dialogs;
using RevitMultipleCore;

namespace RevitFileUpdaterUI
{
    /// <summary>
    /// A class devoted to working with Windows Folders.
    /// </summary>
    internal class FolderList
    {
        /// <summary>
        /// Saves a Tab-Delimited .txt file of Revit File Paths to the "initialDirectory".
        /// </summary>
        /// <param name="initialDirectory">The folder location to save the file.</param>
        /// <param name="filePaths">The Revit File Paths to be saved.</param>
        /// <returns>The file path if successful. string.Empty if unsuccessful.</returns>
        internal string Execute(string initialDirectory, IEnumerable<string[]> filePaths)
        {
            IEnumerable<IEnumerable<string>> rows = filePaths;
            string returnPath = string.Empty;
            UI_IO_Options.BrowseForSave(
                "Save New Revit file list",
                revitFileListPath => {

                    bool isSaved = false;

                    try
                    {
                        TextFileUtil.WriteToTabDelimitedTxtFile(rows, revitFileListPath);
                        isSaved = true;
                    }
                    catch (Exception)
                    {
                        isSaved = false;
                    }

                    if (isSaved)
                    {
                        returnPath = revitFileListPath;
                    }
                },
                TextFileUtil.TextFileExtension,
                TextFileUtil.TextFileFilter,
                initialDirectory,
                "revit_file_list.txt"
            );

            return returnPath;
        }
        /// <summary>
        /// Displays a FolderBrowserDialog.
        /// </summary>
        /// <returns>A string to the folder path.</returns>
        public static string SearchLocation()
        {
            FolderBrowserDialog folderBrowserDialog = new();
            folderBrowserDialog.Description = "Select a folder containing Revit files...";

            DialogResult dialogResult = folderBrowserDialog.ShowDialog();
            return dialogResult != DialogResult.OK ? string.Empty : folderBrowserDialog.SelectedPath;
        }
        /// <summary>
        /// Shows a Modal Processing Dialog when trying to find information about Revit Files.
        /// </summary>
        /// <param name="window">The Window.</param>
        /// <param name="vm">The View Model.</param>
        /// <returns>Task of bool</returns>
        internal static async Task<bool> ShowProcessingDialog(IDialogCoordinator window, MainWindowViewModel vm)
        {
            MetroDialogSettings mySettings = new()
            {
                NegativeButtonText = "Cancel",
                AnimateShow = false,
                AnimateHide = false
            };

            var controller = await window.ShowProgressAsync(vm, "Starting Search...", "Finding Revit Files in User Selected Location.", true, mySettings);
            controller.SetIndeterminate();
            
            RevitFileScanning.RevitFileType selectedRevitFileType = vm.SelectedRevitFileType;

            bool includeSubfolders = vm.IncludeSubfolders;

            SearchOption selectedSearchOption = includeSubfolders ?
                SearchOption.AllDirectories :
                SearchOption.TopDirectoryOnly;
            
            bool ignoreRevitBackupFiles = vm.IgnoreRevitBackupFiles;
            //has a for loop ... needs expanding
            List<string> listRevit = RevitFileScanning.FindRevitFiles(vm.UpgradePath, selectedSearchOption,
                selectedRevitFileType, ignoreRevitBackupFiles).ToList();

            int listRevitCount = listRevit.Count;

            controller.SetCancelable(true);

            var infoRows = new List<string[]>();

            if (vm.ExpandNetworkPaths)
            {
                controller.SetMessage("Stage 1" + Environment.NewLine + "Expanding network paths");

                List<string> expandedRevitFilePaths = new();
                IEnumerable< string> networkPaths = RevitFileScanning.ExpandNetworkPaths(listRevit);
                List< string> lists = networkPaths.ToList();
                for (int j = 0; j < listRevitCount; j++)
                {
                    double val = j / listRevitCount; 
                       // (j / 100.0) * listRevitCount;
                    controller.SetProgress(val);
                    expandedRevitFilePaths.Add(lists[j]);
                    if (controller.IsCanceled)
                        break;
                    //j += 1;
                }
                //watch this
                listRevit = expandedRevitFilePaths;
            }
            infoRows = listRevit.Select(revitFilePath => new[] { revitFilePath }).ToList();
            if (vm.ExtractRevitVersionInfo)
            {
                string message;
                if (vm.ExpandNetworkPaths)
                {
                    message = "Stage 2" + Environment.NewLine + "Extracting Revit files version information";
                }
                else
                {
                    message = "Stage 1" + Environment.NewLine + "Extracting Revit files version information";
                }
               
                controller.SetMessage(message);
                List<string[]> allRevitVersionTexts = new();
                List<string[]> lists = RevitFileScanning.ExtractVersionInfo(listRevit).ToList();
                for (int j = 0; j < listRevitCount; j++)
                {
                    double val = (j / 100.0) * listRevitCount;
                    controller.SetProgress(val);
                    allRevitVersionTexts.Add(lists[j]);
                    if (controller.IsCanceled)
                        break;
                    j += 1;
                }

                infoRows = (
                    listRevit.Zip(
                            allRevitVersionTexts,
                            (revitFilePath, revitVersionTexts) => new[] { revitFilePath, revitVersionTexts[0], revitVersionTexts[1] }
                        )
                        .ToList()
                );
            }

            vm.FilePaths = infoRows;
            
            await controller.CloseAsync();

            return true;
        }
    }
}
