﻿using Autodesk.Revit.UI;
using System;
using System.ComponentModel;
using System.IO;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.UI.Events;

namespace RevitMultipleAddin
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    [DisplayName("RevitMultipleAddin")]
    [Description("RevitMultipleAddin")]
    public class RevitMultipleAddinApplication : IExternalApplication
    {
        private static void SetupBatchScriptHost(ControlledApplication controlledApplication)
        {
            string location = typeof(RevitMultipleAddinApplication).Assembly.Location;
            string pluginFolderPath = Path.GetDirectoryName(location);

            RevitMultipleExternalEventHandler revitMultipleExternalEventHandler = new(pluginFolderPath);

            revitMultipleExternalEventHandler.Raise();
        }

        public Result OnStartup(UIControlledApplication uiApplication)
        {
            uiApplication.DialogBoxShowing += DialogBoxShowingEvents;
            SetupBatchScriptHost(uiApplication.ControlledApplication);

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication uiApplication)
        {
            uiApplication.DialogBoxShowing -= DialogBoxShowingEvents;
            return Result.Succeeded;
        }

        private static void DialogBoxShowingEvents(object sender, DialogBoxShowingEventArgs e)
        {
            Application app = sender as Application;
            new RevitMultipleCore.RevitDialogDetection().ControlDialogBox(e);
        }
    }

    public class RevitMultipleExternalEventHandler : IExternalEventHandler
    {
        private readonly ExternalEvent externalEvent_;
        private readonly string pluginFolderPath_;

        public RevitMultipleExternalEventHandler(string pluginFolderPath)
        {
            externalEvent_ = ExternalEvent.Create(this);
            pluginFolderPath_ = pluginFolderPath;
        }

        public void Execute(UIApplication uiApp)
        {
            try
            {
                //original
                //ScriptHostUtil.ExecuteBatchScriptHost(this.pluginFolderPath_, uiApp);
                _ = new RevitMultipleController.Execute(pluginFolderPath_, uiApp);

                
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString(), "REVIT_MULTIPLE_ERROR_WINDOW_TITLE");
            }
        }

        public string GetName()
        {
            return "RevitMultiple_ExternalEventHandler";
        }

        public ExternalEventRequest Raise()
        {
            return externalEvent_.Raise();
        }
    }
}
