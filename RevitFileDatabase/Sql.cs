using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace RevitFileDatabase
{
    public static class Sql
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static string Connection()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Connect to SQL Server.");

                // Build connection string
                Logger.Info("Connecting to SQL Server ... ");
                IConfigurationRoot config = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json").Build();
                IConfigurationSection connString = config.GetSection("ConnectionStrings");
                return  connString.Value;
            }
            catch (SqlException ex)
            {
                System.Diagnostics.Debug.WriteLine("Error: " + ex);
                return null;
            }
        }

        private static SqlParameter AddWithNullableValue(this SqlParameterCollection collection, string parameterName, object value)
        {
            return collection.AddWithValue(parameterName, value ?? DBNull.Value);
        }

        public static List<Dictionary<object, object>> ExecuteRead_RevitFiles(string message = null)
        {
            // Connect to SQL
            using SqlConnection connector = new(Connection());
            connector.Open();
            Logger.Info("Connection Opened.");

            // Create a Dictionary Object
            List<Dictionary<object, object>> families = new();
            Logger.Info("Database operation: {0}", message);
            // ReSharper disable twice StringLiteralTypo
            const string sql = "SELECT * FROM dbo.RevitFamily WHERE DateModified >= DATEADD(day, -21, GETDATE())";

            using (SqlCommand command = new(sql, connector))
            {
                using SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int key1 = -1;
                    int us = -1;
                    string famName = string.Empty;
                    string famPath = string.Empty;
                    string cat = string.Empty;
                    DateTime dt = DateTime.MinValue;
                    string shared = string.Empty;
                    string revitVersion = string.Empty;
                    string semanticVersion = string.Empty;
                    bool none = false;
                    bool nested = false;
                    if (!reader.IsDBNull(0)) key1 = reader.GetInt32(0);
                    if (!reader.IsDBNull(1)) us = reader.GetInt32(1);
                    if (!reader.IsDBNull(2)) famName = reader.GetString(2);
                    if (!reader.IsDBNull(3)) famPath = reader.GetString(3);
                    if (!reader.IsDBNull(4)) cat = reader.GetString(4);
                    if (!reader.IsDBNull(5)) dt = reader.GetDateTime(5);
                    if (!reader.IsDBNull(6)) shared = reader.GetString(6);
                    if (!reader.IsDBNull(7)) revitVersion = reader.GetString(7);
                    if (!reader.IsDBNull(8)) semanticVersion = reader.GetString(8);
                    if (!reader.IsDBNull(9)) none = reader.GetBoolean(9);
                    if (!reader.IsDBNull(10)) nested = reader.GetBoolean(10);

                    families.Add(new Dictionary<object, object>()
                    {
                        {0, key1},
                        {1, us},
                        {2, famName},
                        {3, famPath},
                        {4, cat},
                        {5, dt},
                        {6, shared},
                        {7, revitVersion},
                        {8, semanticVersion},
                        {9, none},
                        {10, nested}
                    });
                }
            }
            Logger.Info("Database Operation Completed.");
            Logger.Info("Number of Families scheduled to be processed : ", families.Count);
            return families;
        }

        public static void ExecuteWrite_FamilyUser_Update(Dictionary<object, object> family)
        {
            // Connect to SQL
            Logger.Info("Connecting to SQL Server ...");

            using SqlConnection connector = new(Connection());
            connector.Open();
            Logger.Info("Connection Opened.");
            object key = null;
            object familyName = null;
            object familyPath = null;
            object category = null;
            object dateModified = null;
            object sharedId = null;
            object revitVersion = null;
            object semanticVersion = null;
            object userNameId = null;
            object updated = null;
            object isNested = null;
            // Create a sample database
            try
            {
                key = family[0];
                familyName = family[1];
                familyPath = family[2];
                category = family[3];
                dateModified = family[4];
                sharedId = family[5];
                revitVersion = family[6];
                semanticVersion = family[7];
                userNameId = family[8];
                updated = family[9];
                isNested = family[10];
            }
            catch (ArgumentNullException ex)
            {
                //new Exception.ExceptionHandle(ex);
                Logger.Error(ex);
            }
            Logger.Info("Database operation: ");
            const string sql = "UPDATE dbo.RevitFamily SET UserNameId = @UserNameId, FamilyName = @FamilyName, " +
                               "FamilyPath = @FamilyPath, Category = @Category, DateModified = @DateModified, SharedId = @SharedId, " +
                               "RevitVersion = @RevitVersion, SemanticVersion = @SemanticVersion, UserNameId = @UserNameId, " +
                               "Updated = @Updated, IsNested = @IsNested";
            using SqlCommand command = new(sql, connector);
            _ = AddWithNullableValue(command.Parameters, "@UserNameId", userNameId);
            _ = AddWithNullableValue(command.Parameters, "@FamilyName", familyName);
            _ = AddWithNullableValue(command.Parameters, "@FamilyPath", familyPath);
            _ = AddWithNullableValue(command.Parameters, "@Category", category);
            _ = AddWithNullableValue(command.Parameters, "@DateModified", dateModified);
            _ = AddWithNullableValue(command.Parameters, "@SharedId", sharedId);
            _ = AddWithNullableValue(command.Parameters, "@RevitVersion", revitVersion);
            _ = AddWithNullableValue(command.Parameters, "@SemanticVersion", semanticVersion);
            _ = AddWithNullableValue(command.Parameters, "@Updated", updated);
            _ = AddWithNullableValue(command.Parameters, "@IsNested", isNested);
            _ = command.ExecuteNonQuery();
            Logger.Info("Database Operation Completed.");
        }
    }
}
