﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace RevitFileDatabase
{
    public class SqlRevit
    {
        private static readonly NLog.Logger Logger   = NLog.LogManager.GetCurrentClassLogger();
        public List<RevitFileCore.Family> Get()
        {
            Logger.Info("Starting UpdateFamilyFromDatabase Method");
            Dictionary<int, Exception> exceptions = new();
            Dictionary<int, string> completedFamilies = new();
            try
            {
                List<Dictionary<object, object>> rawList = Sql.ExecuteRead_RevitFiles();
                List<RevitFileCore.Family> dict = new();
                RevitFileCore.FamilyConverter r = new();
                foreach (Dictionary<object, object> a in rawList)
                {
                    dict.Add(RevitFileCore.FamilyConverter.DecodeFamily(a));
                }

                if (dict.Count != 0)
                {
                    //Sort the list to include True Nested Families at the top.
                    // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                    dict = dict.OrderBy(q => q.IsNested ? 0 : 1).ToList();

                    return dict;
                }
            }
            catch (Exception ex)
            {
                exceptions.Add(0, ex);
            }

            return null;
        }
    }
}