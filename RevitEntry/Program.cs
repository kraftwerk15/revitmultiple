﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using RevitFileCore;
using RevitFileUpdaterCore.CommandLine;
using RevitFileUpdater;
using RevitFileUpdaterCore;

namespace RevitFileUpdater
{
    public static class Program
    {
        private const string BatchRvtConsoleTitle = "Batch Revit File Processor";
        private static readonly NLog.Logger                 Logger           = NLog.LogManager.GetCurrentClassLogger();
        private const int WindowWidth = 160;
        private const int WindowHeight = 60;
        private const int BufferWidth = 320;
        private const int BufferHeight = WindowHeight * 50;
        
        internal static string GUID = String.Empty;
        
        public static string[] arguments;
        [STAThread]
        // ReSharper disable once UnusedMember.Local
        private static void Main(string[] args)
        {
            Guid instance = Guid.NewGuid();
            Console.Title = BatchRvtConsoleTitle + " : Session " + instance;
            GUID = instance.ToString();
            
            GlobalDiagnosticsContext.Set("instanceGUID", GUID);
            //var config = new ConfigurationBuilder()
            //    .SetBasePath(System.IO.Directory.GetCurrentDirectory()) //From NuGet Package Microsoft.Extensions.Configuration.Json
            //    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            //    .Build();
//
            //var servicesProvider = BuildDi(config);
            //using (servicesProvider as IDisposable)
            //{
            //    var runner = servicesProvider.GetRequiredService<Runner>();
            //    runner.DoAction("Action1");
//
            //    Console.WriteLine("Press ANY key to exit");
            //    Console.ReadKey();
            //}
            Logger.Debug("Application Started");
            Application.EnableVisualStyles();
            arguments = args;
            if (HasConsole())
            {
                InitConsole();
            }

            try
            {
                //Check if Revit is Installed
                if (!RevitVersion.GetInstalledRevitVersions().Any())
                {
                    Logger.Error(
                        "ERROR: Could not detect a valid Revit application for any year version installed on this machine!" +
                        "You must first install the Revit File Updater addin for at least one version of Revit.");
                    Logger.Error("INFO: Revit File Updater will close. Press any key to continue.");
                    Console.ReadLine();
                }
                //Check if Revit Addin is Installed
                //TODO turn this back on when this is functional
                //if (!RevitVersion.GetInstalledRevitAddinVersions().Any())
                //{
                //    Logger.Error("ERROR: Could not detect the Revit File Updater addin for any version of Revit installed on this machine!" +
                //                 "You must first install the Revit File Updater addin for at least one version of Revit." +
                //                 "Would you like to install that now?");
                //    string value = Console.ReadLine();
                //    // ReSharper disable once MergeIntoLogicalPattern //This did not work
                //    if (value != "y" || value != "Y") return;
                //    //Install Addins here
                //    RevitVersion.InstallAddins();
                //}

                
                //CommandSettings.Data cmdSettings = new();
                //cmdSettings.SessionId = instance.ToString();


                //Checks if the User Requested to see the help or the license
                bool check = Settings.Read();
                if (check) return;

                //BatchRevitMonitor.Main should be the entry point
                BatchRevitMonitor.Main(instance);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        private static void InitConsole()
        {
            Console.Title = BatchRvtConsoleTitle;

            try
            {
                Console.SetWindowSize(
                    Math.Min(WindowWidth, Console.LargestWindowWidth),
                    Math.Min(WindowHeight, Console.LargestWindowHeight)
                );

                Console.SetBufferSize(BufferWidth, BufferHeight);
            }
            catch (ArgumentOutOfRangeException)
            {
                // Can occur when output has been redirected via the command-line.
            }
            catch (IOException)
            {
                // Can occur when output has been redirected via the command-line.
            }

            return;
        }

        internal static string GetExecutableFolderPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        private static bool HasConsole()
        {
            return (GetConsoleWindow() != IntPtr.Zero);
        }
        
        private static IServiceProvider BuildDi(IConfiguration config)
        {
            return new ServiceCollection()
                .AddTransient<Runner>() // Runner is the custom class
                .AddLogging(loggingBuilder =>
                {
                    // configure Logging with NLog
                    loggingBuilder.ClearProviders();
                    loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                    loggingBuilder.AddNLog(config);
                })
                .BuildServiceProvider();
        }
    }
}
